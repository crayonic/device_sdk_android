Crayonic Device SDK - Android
========

Provides functional interface for Crayonic Devices, such as L1, Pen or others.
  
Features of SDK
--------
- List surrounding devices based on specified filter
- Connect and disconnect specific device
- Communicate with specific device upon connection is established

* This document links to other documentations.

Documentation
--------
- [SDK Implementation](./docs/sdk/README_initial.md)
- [Crayonic Docs](docs/legacy/README.md)
- [APDU Commands Table](docs/legacy/apdu_commands.md)
- Each of project modules contains separate "docs" folder with specific details on the module

Usage
------------

### Gradle

Add dependency to module level build.gradle:

```groovy
dependencies {
    implementation 'com.crayonic:device-sdk:X.X.X'
}
```

Make sure that you have our repository at project level in build.gradle:

```groovy
allprojects {
    repository {
        maven{ url "https://gitlab.com/crayonic/maven/raw/master/android/stable/" }
    }
}
```

Sample
----------
See the sample module for more information or checkout the [SDK Implementation](./docs/sdk/README_initial.md) documentation. 

Document Version
-------
Work in progress version. Content of the SDK may vary until release. Using Semantic Versioning 2.0.0.

License
--------
```
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

