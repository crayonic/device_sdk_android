# Project Title

Device SDK Android - Samples 

Platform dependent part of communication control for Crayonic hardware devices

## Structure

* There are 3 main use cases covered.
  
1. Shipper - Kotlin/Java - Run to provision *Unprovisioned* device
1. Handler - Kotlin/Java - Reads events from *Provisioned* device
1. End of Journey - Kotlin/Java - Puts the device to deep sleep and resets its state back to *Unprovisioned* 

* Other samples are used for Unit testing the device 

* Main activity is just listing other sample activities 

### Usage

There is a sample module that simulates usage of this library.

## Running the tests

No tests included in this pre-release version. 

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 
