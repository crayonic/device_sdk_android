package com.crayonic.device_sdk_android.sample

import android.content.Context
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions

interface QuickPermissionJavaUtilInterface{
    fun onPermissionCallback();
}

fun runWithPermissions(context: Context, listener:QuickPermissionJavaUtilInterface, vararg permissions: String){
    context.runWithPermissions(permissions = *permissions, callback = { listener.onPermissionCallback() })
}