package com.crayonic.device_sdk_android.sample


import android.Manifest
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.crayonic.device_sdk.device.DeviceFilter
import com.crayonic.device_sdk.device.errors.CrayonicDeviceError
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.device.CrayonicDevice
import com.crayonic.device_sdk.device.CrayonicL1
import com.crayonic.device_sdk.device.CrayonicL1.L1DeviceVerboseHolder
import com.crayonic.device_sdk.util.*
import com.crayonic.device_sdk_android.core.CrayonicDevicesService
import com.google.android.material.snackbar.Snackbar
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_sample.*
import kotlinx.android.synthetic.main.content_sample.*
import kotlinx.coroutines.*
import kotlin.coroutines.resume

/**
 * Demonstration of Crayonic Device SDK Usage
 *
 * Starts service onCreate or on fab button
 * Service is stopped when pressed again and running
 *
 *
 *
 */
class SampleGatewayActivity : AppCompatActivity() {

    enum class GwMode {
        multiple_listings_multiple_connects,
        multiple_listings_single_connect,
        single_listing_multiple_connect,
        single_listing_single_connect
    }

    val TAG = "SampleHandlerActivity"

    var service: CrayonicDevicesService? = null
    var listed_count = 0
    var connected_count = 0

    var selectedGwMode = GwMode.multiple_listings_multiple_connects

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)
        val toolbar = toolbar
        setSupportActionBar(toolbar as Toolbar?)

        initCrayonicDeviceService()
    }


    private fun endCrayonicDeviceService() {
        Log.e("Sample", "service endCrayonicDeviceService : $service")
        service?.let {
            it.unbind()
            service = null
        }
    }

    private fun initCrayonicDeviceService() {
        Log.e("Sample", "service initCrayonicDeviceService : $service")
        runWithPermissions(
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) {
            Toast.makeText(this, "Bluetooth permissions granted", Toast.LENGTH_SHORT).show()

            val activity = this
            CoroutineScope(Dispatchers.Default).launch {
                CrayonicDevicesService.getInstance(
                    bindToContext = activity,
                    onServiceError = {
                        CoroutineScope(Dispatchers.Main).launch {
                            Toast.makeText(applicationContext, "Service Error", Toast.LENGTH_LONG)
                                .show()
                            Log.e(TAG, "Service Error: ", it)
                        }
                    },
                    onServiceAttached = { service: CrayonicDevicesService ->
                        CoroutineScope(Dispatchers.Main).launch {
                            //                            Toast.makeText(activity, "Service Bound", Toast.LENGTH_LONG).show()
                            activity.service = service
                            Log.e("Sample", "Started service : $service")
                            // execute the listing of devices
                            startGateway()
                        }
                    }
                )
            }

        }
    }

    private fun startGateway() {
        listDevices()
        if(isMultipleListingMode()){
            CoroutineScope(Dispatchers.IO + Job()).launch {
                delay(seconds_20)
                CoroutineScope(Dispatchers.Main).launch {
                    listDevices()
                }
                delay(seconds_20)
                CoroutineScope(Dispatchers.Main).launch {
                    listDevices()
                }
                delay(seconds_20)
                CoroutineScope(Dispatchers.Main).launch {
                    listDevices()
                    service?.unbind()
                }
            }
        }else{
            service?.unbind()
        }
    }


    private fun isSingleListingMode():Boolean = when(selectedGwMode){
            GwMode.single_listing_single_connect, GwMode.single_listing_multiple_connect -> true
            else -> false
        }

    private fun isMultipleListingMode():Boolean = when(selectedGwMode){
            GwMode.multiple_listings_single_connect, GwMode.multiple_listings_multiple_connects -> true
            else -> false
        }

    private fun isSingleConnectMode():Boolean = when(selectedGwMode){
            GwMode.single_listing_single_connect, GwMode.multiple_listings_single_connect -> true
            else -> false
        }

    private fun isMultipleConnectsMode():Boolean = when(selectedGwMode){
            GwMode.multiple_listings_multiple_connects, GwMode.single_listing_multiple_connect -> true
            else -> false
        }


    val postponedDevices: JailTimeArrayList<CrayonicDevice> = JailTimeArrayList()

    val seenDevices = hashSetOf<CrayonicDevice>()
    val syncedDevices = hashMapOf<CrayonicDevice, Long>()
    val gatheredEvents = hashMapOf<String, List<String>>()
    val usedFilter = DeviceFilter()
    val deviceHolders = hashMapOf<String, L1DeviceVerboseHolder>()

    //  device filter for specific tag:
    //  it can be anything device contains in toString() return value
    //    e.g. adddress
    //    val usedFilter = DeviceFilter("DF:B6:7B:80:F0:8B")

    // device filter matching all crayonic devices
    private fun listDevices() {
        if (listed_count == 0 || isMultipleListingMode()){
            listed_count += 1
            Log.d(TAG, "listDevices[$listed_count] started")
            seenDevices.clear()
            service?.listDevices(usedFilter) { originFilter: DeviceFilter, list: List<CrayonicDevice> ->
                Log.d(TAG, "listDevices[$listed_count] found: ${list.joinToString(separator = "\n")}")
                seenDevices.addAll(list)
                updateTextDevicesList()
                if(isMultipleConnectsMode()){
                    list.forEach { device: CrayonicDevice ->
                        if ( device is CrayonicL1 ) {
                            if(!postponedDevices.contains(device)){
                                connectDevice(device)
                                postponedDevices.addWithTimeout(device, seconds_10)
                                connected_count += 1
                            }
                        }
                    }
                    service?.stopAllDeviceListings()
                }else if (connected_count == 0){
                    if(list.isNotEmpty()){
                        val device = list.get(0)
                        if ( device is CrayonicL1 ) {
                            // connect device
                            connectDevice(device)
                            postponedDevices.addWithTimeout(device, seconds_30)

                            // increase count
                            connected_count += 1
                        }
                        service?.stopAllDeviceListings()
                    }
                }
            }
        }
    }

    private fun updateTextDevicesList() {
        updateText(
            "$usedFilter\n\n"
                    + "Listings : $listed_count\n"
                    + "Connects : $connected_count\n"
                    + "Seen Devices : \n"
                    + seenDevices.joinToString(separator = "\n") { dev -> "${dev.name}   ${dev.address}" } + "\n\n"
                    + "Synced Devices : \n"
                    + deviceHolders.values.toList().joinToString("\n\n")+ "\n\n"
                    + "Gathered Events : \n"
                    + gatheredEvents.entries.joinToString(separator = "\n") { entry -> "${entry.key}:\n ${entry.value.take(100).joinToString(separator = "\n") }" }
        )
    }

    private fun connectDevice(device :CrayonicL1){
        // do the connect
        device.connect(
            onConnected = { connectedDevice: CrayonicDevice ->
                Log.i(TAG, "device : ${device.address} ${device.name}: CONNECTED")
                CoroutineScope(Dispatchers.IO + Job()).launch {
                    Log.e(TAG, "reading device values : ")
                    async {
                        if (connectedDevice is CrayonicL1) {
                            syncDevice(connectedDevice, device)
                            syncedDevices.put(connectedDevice, currentUnixTime() + 15)
                        }
                        updateTextDevicesList()
                    }.await()

                }
            },
            onDisconnected = { connectedDevice ->
                Log.i(
                    TAG,
                    "device : ${connectedDevice.address} ${connectedDevice.name}: DISCONNECTED "
                )
            },
            onError = { connectedDevice: CrayonicDevice, error: CrayonicDeviceError ->
                Log.e(
                    TAG,
                    "device : ${connectedDevice.address} ${connectedDevice.name}: ERROR : $error"
                )
            },
            onProgress = { connectedDevice: CrayonicDevice, requestKey: CrayonicRequestCode, progress: Progress ->
                Log.i(TAG, "Progress update : $requestKey - ${progress.perc()}")
                updateProgress(progress)
            }
        )
    }

    private suspend fun syncDevice(
        connectedDevice: CrayonicL1,
        device: CrayonicDevice
    ) {

        val key = device.toString()
        var holder = deviceHolders[key] ?: L1DeviceVerboseHolder(key)

        deviceHolders.put(key, connectedDevice.toL1DeviceVerboseHolder())
        updateTextDevicesList()


        // device info sync
        val asterix = updateDeviceState(device)
        holder = connectedDevice.toL1DeviceVerboseHolder()
        deviceHolders.put(key, holder)
        updateTextDevicesList()


        // device time sync
//        holder.unixTime = readClock(connectedDevice).toString()
//        deviceHolders.put(key, holder)
//        updateTextDevicesList()


//        readEvents(connectedDevice)
//        updateTextDevicesList()

        Log.e(TAG, "disconnecting from device : $connectedDevice")
        connectedDevice.disconnect()
    }


    private suspend fun updateDeviceState(device: CrayonicDevice) {
        suspendCancellableCoroutine<Int> { cont ->
            device.updateAllMemberValues(onDone = {
                cont.resume(it)
            })
        }
    }

    suspend fun readClock(connectedDevice: CrayonicDevice): Long {
        return suspendCancellableCoroutine<Long> { cont ->
            Log.e(TAG, "Read clock ($connectedDevice)")
            connectedDevice.runCommandReadClock { device, unixTime ->
                Log.e(TAG, "Time obtained from device ($connectedDevice): $unixTime")
                cont.resume(unixTime)
            }
        }
    }

    suspend fun readEvents(connectedL1: CrayonicL1) {
        withTimeout(seconds_120) {
            suspendCancellableCoroutine<Any> { cont ->
                Log.e(TAG, "Read event ($connectedL1)")
                connectedL1.runTaskReadEventsOffline { device, deviceEvents ->
                    Log.e(TAG, "Events read from device ($connectedL1) : \n" +
                            deviceEvents.joinToString(separator = "\n")
                    )
                    gatheredEvents.put(connectedL1.address, deviceEvents)
                    cont.resume(Any())
                }
            }
        }
    }

    private fun updateText(text: String) {
        CoroutineScope(Dispatchers.Main).launch {
            helloTextView.text = text
        }
    }

    private fun updateProgress(progressUpdate: Progress) {
        CoroutineScope(Dispatchers.Main).launch {
            progress.progress = progressUpdate.perc()
            progress.show()
        }
    }

    private fun logMsg(message: String) {
        Log.d(TAG, message)
    }
}
