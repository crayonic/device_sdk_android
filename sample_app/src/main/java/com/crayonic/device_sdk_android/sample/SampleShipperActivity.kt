package com.crayonic.device_sdk_android.sample


import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.crayonic.device_sdk.device.DeviceFilter
import com.crayonic.device_sdk.device.errors.CrayonicDeviceError
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.device.l1.models.AlarmZonesCollection
import com.crayonic.device_sdk.device.l1.models.L1InitOfflineValues
import com.crayonic.device_sdk.device.l1.models.L1ProvisionedDeviceData
import com.crayonic.device_sdk.device.CrayonicDevice
import com.crayonic.device_sdk.device.CrayonicL1
import com.crayonic.device_sdk.device.CrayonicL1.L1DeviceVerboseHolder
import com.crayonic.device_sdk.util.Progress
import com.crayonic.device_sdk.util.isValidBleAddress
import com.crayonic.device_sdk.util.seconds_120
import com.crayonic.device_sdk_android.core.CrayonicDevicesService
import com.crayonic.device_sdk_android.utils.nfc.NFCConsumingActivity
import com.crayonic.device_sdk_android.utils.nfc.NFCUtil.deviceAddressFromIntent
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_sample.*
import kotlinx.android.synthetic.main.content_sample.*
import kotlinx.coroutines.*
import kotlin.coroutines.resume

/**
 * Demonstration of Crayonic Device SDK Usage
 *
 * Starts service onCreate or on fab button
 * Service is stopped when pressed again and running
 *
 *
 *
 */
class SampleShipperActivity : NFCConsumingActivity() {

    val TAG = "SampleActivity"
    var service: CrayonicDevicesService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)
        val toolbar = toolbar
        setSupportActionBar(toolbar as Toolbar?)
        fab.visibility = View.GONE

        updateText("Tap device with NFC enabled phone")
    }

    private fun initCrayonicDeviceService() {
        Log.e("Sample", "service initCrayonicDeviceService : $service")
        runWithPermissions(
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) {
            Toast.makeText(this, "Bluetooth permissions granted", Toast.LENGTH_SHORT).show()

            val activity = this
            CoroutineScope(Dispatchers.Default).launch {
                CrayonicDevicesService.getInstance(
                    bindToContext = activity,
                    onServiceError = {
                        CoroutineScope(Dispatchers.Main).launch {
                            Toast.makeText(applicationContext, "Service Error", Toast.LENGTH_LONG)
                                .show()
                            Log.e(TAG, "Service Error: ", it)
                        }
                    },
                    onServiceAttached = { service: CrayonicDevicesService ->
                        CoroutineScope(Dispatchers.Main).launch {
                            //                            Toast.makeText(activity, "Service Bound", Toast.LENGTH_LONG).show()
                            this@SampleShipperActivity.service = service
                            Log.e("Sample", "Started service : $service")
                            // execute the listing of devices
                            listDevices()
                        }
                    }
                )
            }

        }
    }

    private fun endCrayonicDeviceService() {
        Log.e("Sample", "service endCrayonicDeviceService : $service")
        service?.let {
            it.unbind()
            service = null
        }
        fab.visibility = View.VISIBLE
        fab.setOnClickListener {
            initCrayonicDeviceService()
            fab.visibility = View.GONE
        }
    }


    val seenDevices = hashSetOf<CrayonicDevice>()
    var gatheredData = L1ProvisionedDeviceData("","","")
    //  device filter for specific tag:
    //  it can be anything device contains in toString() return value
    //    e.g. adddress
//    val usedFilter = DeviceFilter("DF:B6:7B:80:F0:8B")
    var deviceMacAddress = ""
    var deviceFilter = DeviceFilter(deviceMacAddress)
    var deviceHolder = L1DeviceVerboseHolder()
    var isDeviceProvisioned = false

    override fun onNfcIntentFound(intent: Intent) {
        val nfcCapturedAddress = deviceAddressFromIntent(intent)
        if (nfcCapturedAddress != null) {
            if(nfcCapturedAddress.isValidBleAddress()){
                if(isDeviceProvisioned){
                    Log.e(TAG, "Device is already provisioned, we should turn it off now. Ignoring NFC ADDRESS until then.")
                }else{
                    deviceMacAddress = nfcCapturedAddress
                    deviceFilter = DeviceFilter(nfcCapturedAddress)
                    updateTextDevicesList()
                    initCrayonicDeviceService()
                }
            }else{
                Log.e(TAG, "NFC Address is not valid: $nfcCapturedAddress")
            }
        }else{
            Log.e(TAG, "Captured NFC intent is not valid: $intent")
        }
    }


    // device filter matching all crayonic devices
    private fun listDevices() {
        seenDevices.clear()
        Log.e(TAG, "listDevices: $service")

        service?.listDevices(deviceFilter) { originFilter: DeviceFilter, list: List<CrayonicDevice> ->
            Log.d(TAG, "list found : ${list.joinToString(separator = "\n")}")
            seenDevices.addAll(list)
            updateTextDevicesList()
            if(list.isNotEmpty()){
                val crayonicDevice = list[0]
                connectDevice(crayonicDevice)
            }
        }
    }


    val initialDeviceData = L1InitOfflineValues(
        "1234567890",
        "0x9b2aee3bf2e65f480976d6006ea9c545834557ac9a3dfdd99deaffaee958e9bd",
        AlarmZonesCollection.defaultAlarmZones().toList()
    )

    private fun updateTextDevicesList() {
        updateText(
            "$deviceFilter\n\n"
                    + "Found BLE Devices : \n"
                    + seenDevices.joinToString(separator = "\n") { dev -> "${dev.name}   ${dev.address}" } + "\n\n"
                    + "Connected device : \n"
                    + deviceHolder + "\n\n"
                    + "Sent Data : \n"
                    + initialDeviceData +"\n\n"
                    + "Device Provisioning Data : \n"
                    + gatheredData
        )
    }


    private fun connectDevice(device: CrayonicDevice) {
        // stop the scanner
        service?.stopAllDeviceListings()
        device.connect(
            onConnected = { connectedDevice: CrayonicDevice ->
                Log.i(TAG, "device : ${device.address} ${device.name}: CONNECTED")
                CoroutineScope(Dispatchers.IO + Job()).launch {
                    async {
                        if (connectedDevice is CrayonicL1) {
                            if(isDeviceProvisioned){
                                resetDevice(connectedDevice)
                            }else{

                                syncDevice(connectedDevice)
                            }
                        }
                        updateTextDevicesList()
                    }.await()

                }
            },
            onDisconnected = { connectedDevice ->
                Log.i(
                    TAG,
                    "device : ${connectedDevice.address} ${connectedDevice.name}: DISCONNECTED "
                )
                endCrayonicDeviceService()
            },
            onError = { connectedDevice: CrayonicDevice, error: CrayonicDeviceError ->
                Log.e(
                    TAG,
                    "device : ${connectedDevice.address} ${connectedDevice.name}: ERROR : $error"
                )
            },
            onProgress = { connectedDevice: CrayonicDevice, requestKey: CrayonicRequestCode, progress: Progress ->
                Log.i(TAG, "Progress update : $requestKey - ${progress.perc()}")
                updateProgress(progress)
            }
        )
    }

    private suspend fun syncDevice(
        connectedDevice: CrayonicL1
    ) {
        deviceHolder = connectedDevice.toL1DeviceVerboseHolder()

        initializeDevice(connectedDevice)
        updateTextDevicesList()

        Log.e(TAG, "disconnecting from device : $connectedDevice")
        connectedDevice.disconnect()
    }


    private suspend fun resetDevice(
        connectedDevice: CrayonicL1
    ): Any {
        return suspendCancellableCoroutine<Any> { cont ->
            connectedDevice.runCommandResetToPowerOff("", onResult = {
                Log.e(TAG, "Device ${connectedDevice.address} was turned off - end of shipmentt")
                isDeviceProvisioned = false
                cont.resume(Any())
            })
        }
    }
    suspend fun initializeDevice(connectedL1: CrayonicL1) {
        withTimeout(seconds_120) {
            suspendCancellableCoroutine<Any> { cont ->
                Log.e(TAG, "Initialize ($connectedL1)")
                connectedL1.runTaskInitializationOffline(initialDeviceData) { device, deviceProvisioningData ->
                    Log.e(TAG, "Data obtained from provisioned device ($connectedL1) : $deviceProvisioningData\n" )
                    gatheredData = deviceProvisioningData
                    isDeviceProvisioned = true
                    cont.resume(Any())
                }
            }
        }
    }


    private fun updateText(text: String) {
        CoroutineScope(Dispatchers.Main).launch {
            helloTextView.text = text
        }
    }

    private fun updateProgress(progressUpdate: Progress) {
        CoroutineScope(Dispatchers.Main).launch {
            progress.progress = progressUpdate.perc()
            progress.show()
        }
    }


}
