package com.crayonic.device_sdk_android.sample


import android.Manifest
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.crayonic.device_sdk.device.DeviceFilter
import com.crayonic.device_sdk.device.errors.CrayonicDeviceError
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.http.l1_json_api.L1ApiJson_L1Event
import com.crayonic.device_sdk.device.CrayonicDevice
import com.crayonic.device_sdk.device.CrayonicL1
import com.crayonic.device_sdk.device.CrayonicL1.L1DeviceVerboseHolder
import com.crayonic.device_sdk.util.Progress
import com.crayonic.device_sdk.util.currentUnixTime
import com.crayonic.device_sdk.util.seconds_120
import com.crayonic.device_sdk_android.core.CrayonicDevicesService
import com.google.android.material.snackbar.Snackbar
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_sample.*
import kotlinx.android.synthetic.main.content_sample.*
import kotlinx.coroutines.*
import kotlin.coroutines.resume

/**
 * Demonstration of Crayonic Device SDK Usage
 *
 * Starts service onCreate or on fab button
 * Service is stopped when pressed again and running
 *
 *
 *
 */
class SampleActivity : AppCompatActivity() {

    val TAG = "SampleActivity"
    var service: CrayonicDevicesService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)
        val toolbar = toolbar
        setSupportActionBar(toolbar as Toolbar?)

        fab.setOnClickListener { view ->
            serviceInteraction(view)
        }

        initCrayonicDeviceService()
    }

    private fun serviceInteraction(view: View) {
        val localInstance = service
        if (localInstance == null) {
            initCrayonicDeviceService()
        } else {
            endCrayonicDeviceService()
        }
        Snackbar.make(
            view,
            if (localInstance == null) "Started service" else "Service is running, will stop automatically",
            Snackbar.LENGTH_LONG
        ).apply {
            if (localInstance == null) {
                setAction("Kill it") { service?.killSwitch() }
            }
        }.show()
    }

    private fun endCrayonicDeviceService() {
        Log.e("Sample", "service endCrayonicDeviceService : $service")
        service?.let {
            it.unbind()
            service = null
        }
    }

    private fun initCrayonicDeviceService() {
        Log.e("Sample", "service initCrayonicDeviceService : $service")
        runWithPermissions(
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) {
            Toast.makeText(this, "Bluetooth permissions granted", Toast.LENGTH_SHORT).show()

            val activity = this
            CoroutineScope(Dispatchers.Default).launch {
                CrayonicDevicesService.getInstance(
                    bindToContext = activity,
                    onServiceError = {
                        CoroutineScope(Dispatchers.Main).launch {
                            Toast.makeText(applicationContext, "Service Error", Toast.LENGTH_LONG)
                                .show()
                            Log.e(TAG, "Service Error: ", it)
                        }
                    },
                    onServiceAttached = { service: CrayonicDevicesService ->
                        CoroutineScope(Dispatchers.Main).launch {
                            //                            Toast.makeText(activity, "Service Bound", Toast.LENGTH_LONG).show()
                            activity.service = service
                            Log.e("Sample", "Started service : $service")
                            // execute the listing of devices
                            listDevices()
                        }
                    }
                )
            }

        }
    }

    val seenDevices = hashSetOf<CrayonicDevice>()
    val syncedDevices = hashMapOf<CrayonicDevice, Long>()
    val gatheredEvents = hashMapOf<String, List<L1ApiJson_L1Event>>()
    val usedFilter = DeviceFilter()
    val deviceHolders = hashMapOf<String, L1DeviceVerboseHolder>()

    private fun getValidDeviceForConnect(): CrayonicDevice? {
        val time = currentUnixTime()
        val blockedDevices = syncedDevices.filter { entry -> entry.value > time }.keys
        val availableDevices = seenDevices.filter { device -> !blockedDevices.contains(device) }
        val takenDevice = availableDevices.getOrNull(0)
        Log.e(TAG, "Taken device: $takenDevice")
        return takenDevice
    }


    //  device filter for specific tag:
    //  it can be anything device contains in toString() return value
    //    e.g. adddress
    //    val usedFilter = DeviceFilter("DF:B6:7B:80:F0:8B")

    // device filter matching all crayonic devices
    private fun listDevices() {
        seenDevices.clear()
        Log.e(TAG, "listDevices: $service")

        service?.listDevices(usedFilter) { originFilter: DeviceFilter, list: List<CrayonicDevice> ->
            Log.d(TAG, "list found : ${list.joinToString(separator = "\n")}")
            updateTextDevicesList()
            seenDevices.addAll(list)
            connectFirst()
        }
        //service?.unbind()
    }

    private fun updateTextDevicesList() {
        updateText(
            "$usedFilter\n\n"
                    + "Seen Devices : \n"
                    + seenDevices.joinToString(separator = "\n") { dev -> "${dev.name}   ${dev.address}" } + "\n\n"
                    + "Synced Devices : \n"
                    + deviceHolders.values.toList().joinToString("\n\n")+ "\n\n"
                    + "Gathered Events : \n"
                    + gatheredEvents.entries.joinToString(separator = "\n") { entry -> "${entry.key}:\n ${entry.value.joinToString(separator = "\n") { event: L1ApiJson_L1Event -> event.eventId } }" }
        )
    }

    var syncLock = false
        set(value) {
            when (value) {
                true -> Log.d(TAG, "LOCKED")
                false -> Log.d(TAG, "UNLOCKED")
            }
            field = value
        }

    private fun connectFirst() {

        if (syncLock) return
        syncLock = true

        // no devices to connect to
        if (seenDevices.isEmpty()) {
            syncLock = false
            return
        }

        // get first valid device
        val device = getValidDeviceForConnect()
        if (device == null) {
            syncLock = false
            return
        }
        // stop the scanner
        service?.stopAllDeviceListings()
        device.connect(
            onConnected = { connectedDevice: CrayonicDevice ->
                Log.i(TAG, "device : ${device.address} ${device.name}: CONNECTED")
                CoroutineScope(Dispatchers.IO + Job()).launch {
                    Log.e(TAG, "reading device values : ")
                    async {
                        if (connectedDevice is CrayonicL1) {
                            syncDevice(connectedDevice, device)
                            syncedDevices.put(connectedDevice, currentUnixTime() + 15)
                        }
                        updateTextDevicesList()
                    }.await()

                }
            },
            onDisconnected = { connectedDevice ->
                Log.i(
                    TAG,
                    "device : ${connectedDevice.address} ${connectedDevice.name}: DISCONNECTED "
                )
                syncLock = false
            },
            onError = { connectedDevice: CrayonicDevice, error: CrayonicDeviceError ->
                Log.e(
                    TAG,
                    "device : ${connectedDevice.address} ${connectedDevice.name}: ERROR : $error"
                )
            },
            onProgress = { connectedDevice: CrayonicDevice, requestKey: CrayonicRequestCode, progress: Progress ->
                Log.i(TAG, "Progress update : $requestKey - ${progress.perc()}")
                updateProgress(progress)
            }
        )
    }

    private suspend fun syncDevice(
        connectedDevice: CrayonicL1,
        device: CrayonicDevice
    ) {

        val key = device.toString()
        var holder = deviceHolders[key] ?: L1DeviceVerboseHolder(key)

        deviceHolders.put(key, connectedDevice.toL1DeviceVerboseHolder())
        updateTextDevicesList()

        val asterix = updateDeviceState(device)
        holder = connectedDevice.toL1DeviceVerboseHolder()
        deviceHolders.put(key, holder)
        updateTextDevicesList()

        holder.unixTime = readClock(connectedDevice).toString()
        deviceHolders.put(key, holder)
        updateTextDevicesList()

        readEvents(connectedDevice)
        updateTextDevicesList()

        Log.e(TAG, "disconnecting from device : $connectedDevice")
        connectedDevice.disconnect()
    }


    private suspend fun updateDeviceState(device: CrayonicDevice) {
        suspendCancellableCoroutine<Int> { cont ->
            device.updateAllMemberValues(onDone = {
                cont.resume(it)
            })
        }
    }

    suspend fun readClock(connectedDevice: CrayonicDevice): Long {
        return suspendCancellableCoroutine<Long> { cont ->
            Log.e(TAG, "Read clock ($connectedDevice)")
            connectedDevice.runCommandReadClock { device, unixTime ->
                Log.e(TAG, "Time obtained from device ($connectedDevice): $unixTime")
                cont.resume(unixTime)
            }
        }
    }

    suspend fun readEvents(connectedL1: CrayonicL1) {
        withTimeout(seconds_120) {
            suspendCancellableCoroutine<Any> { cont ->
                Log.e(TAG, "Read event ($connectedL1)")
                connectedL1.runTaskReadEvents(uploadEventsOnSuccess = true) { device, deviceEvents ->
                    Log.e(TAG, "Events read from device ($connectedL1) : \n" +
                            deviceEvents.joinToString(separator = "\n") { event: L1ApiJson_L1Event ->
                                event.toJsonString()
                            })
                    gatheredEvents.put(connectedL1.address, deviceEvents)
                    cont.resume(Any())
                }
            }
        }
    }


    private fun updateText(text: String) {
        CoroutineScope(Dispatchers.Main).launch {
            helloTextView.text = text
        }
    }

    private fun updateProgress(progressUpdate: Progress) {
        CoroutineScope(Dispatchers.Main).launch {
            progress.progress = progressUpdate.perc()
            progress.show()
        }
    }

    private fun logMsg(message: String) {
        Log.d(TAG, message)
    }
}
