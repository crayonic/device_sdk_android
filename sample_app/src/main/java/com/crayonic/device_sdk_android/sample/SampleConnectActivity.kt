package com.crayonic.device_sdk_android.sample


import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.crayonic.device_sdk.device.DeviceFilter
import com.crayonic.device_sdk.device.errors.CrayonicDeviceError
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.device.l1.models.AlarmZonesCollection
import com.crayonic.device_sdk.device.l1.models.L1InitOfflineValues
import com.crayonic.device_sdk.device.l1.models.L1ProvisionedDeviceData
import com.crayonic.device_sdk.device.CrayonicDevice
import com.crayonic.device_sdk.device.CrayonicL1
import com.crayonic.device_sdk.device.CrayonicL1.L1DeviceVerboseHolder
import com.crayonic.device_sdk.util.Progress
import com.crayonic.device_sdk.util.isValidBleAddress
import com.crayonic.device_sdk.util.seconds_120
import com.crayonic.device_sdk_android.core.CrayonicDevicesService
import com.crayonic.device_sdk_android.utils.nfc.NFCConsumingActivity
import com.crayonic.device_sdk_android.utils.nfc.NFCUtil.deviceAddressFromIntent
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_sample.*
import kotlinx.android.synthetic.main.content_sample.*
import kotlinx.coroutines.*
import kotlin.coroutines.resume

/**
 * Demonstration of Crayonic Device SDK Usage
 *
 * Starts service onCreate or on fab button
 * Service is stopped when pressed again and running
 *
 *
 *
 */
class SampleConnectActivity : NFCConsumingActivity() {

    val TAG = "SampleActivity"
    var service: CrayonicDevicesService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)
        val toolbar = toolbar
        setSupportActionBar(toolbar as Toolbar?)
        fab.visibility = View.GONE

        updateText("Tap device with NFC enabled phone")
    }

    private fun initCrayonicDeviceService() {
        Log.e("Sample", "service initCrayonicDeviceService : $service")
        runWithPermissions(
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) {
            Toast.makeText(this, "Bluetooth permissions granted", Toast.LENGTH_SHORT).show()

            val activity = this
            CoroutineScope(Dispatchers.Default).launch {
                CrayonicDevicesService.getInstance(
                    bindToContext = activity,
                    onServiceError = {
                        CoroutineScope(Dispatchers.Main).launch {
                            Toast.makeText(applicationContext, "Service Error", Toast.LENGTH_LONG)
                                .show()
                            Log.e(TAG, "Service Error: ", it)
                        }
                    },
                    onServiceAttached = { service: CrayonicDevicesService ->
                        sampleState = SampleState.serviceAttached
                        updateTextDevicesList()
                        CoroutineScope(Dispatchers.Main).launch {
                            //                            Toast.makeText(activity, "Service Bound", Toast.LENGTH_LONG).show()
                            this@SampleConnectActivity.service = service
                            Log.e("Sample", "Started service : $service")
                            // execute the listing of devices
                            listDevices()
                        }
                    }
                )
            }

        }
    }

    private fun endCrayonicDeviceService() {
        Log.e("Sample", "service endCrayonicDeviceService : $service")
        service?.let {
            it.unbind()
            service = null
        }
        fab.visibility = View.VISIBLE
        fab.setOnClickListener {
            initCrayonicDeviceService()
            fab.visibility = View.GONE
        }
    }


    val seenDevices = hashSetOf<CrayonicDevice>()
    var deviceMacAddress = ""
    var deviceFilter = DeviceFilter(deviceMacAddress)
    var deviceHolder = L1DeviceVerboseHolder()
    var isDeviceProvisioned = false

    override fun onNfcIntentFound(intent: Intent) {
        val nfcCapturedAddress = deviceAddressFromIntent(intent)
        if (nfcCapturedAddress != null) {
            if(nfcCapturedAddress.isValidBleAddress()){
                if(isDeviceProvisioned){
                    Log.e(TAG, "Device is already provisioned, we should turn it off now. Ignoring NFC ADDRESS until then.")
                }else{
                    deviceMacAddress = nfcCapturedAddress
                    deviceFilter = DeviceFilter(nfcCapturedAddress)
                    updateTextDevicesList()
                    initCrayonicDeviceService()
                }
            }else{
                Log.e(TAG, "NFC Address is not valid: $nfcCapturedAddress")
            }
        }else{
            Log.e(TAG, "Captured NFC intent is not valid: $intent")
        }
    }


    // device filter matching all crayonic devices
    private fun listDevices() {
        sampleState = SampleState.searchingForDevice
        seenDevices.clear()
        Log.e(TAG, "listDevices: $service")

        service?.listDevices(deviceFilter) { originFilter: DeviceFilter, list: List<CrayonicDevice> ->
            Log.d(TAG, "list found : ${list.joinToString(separator = "\n")}")
            seenDevices.addAll(list)
            updateTextDevicesList()
            if(list.isNotEmpty()){
                sampleState = SampleState.foundDevice
                updateTextDevicesList()
                val crayonicDevice = list[0]
                connectDevice(crayonicDevice)
            }
        }
    }

    private fun updateTextDevicesList() {
        updateText(
            "$deviceFilter\n\n"
                    + "Found BLE Devices : \n"
                    + seenDevices.joinToString(separator = "\n") { dev -> "${dev.name}   ${dev.address}" } + "\n\n"
                    + "Connected device : \n"
                    + deviceHolder + "\n\n"
                    + "Connect times : \n"
                    + connectCount +"\n\n"
                    + "Disconnect times: \n"
                    + disconnectCount +"\n\n"
                    + "Sample State : " + sampleState.toString()
        )
    }

    enum class SampleState{
        started, serviceAttached, searchingForDevice, foundDevice, connecting, disconnecting, ended
    }

    var sampleState = SampleState.started
    var connectCountMax = 5
    var connectCount = 0
    var disconnectCount = 0

    private fun connectDevice(device: CrayonicDevice) {
        // stop the scanner
        service?.stopAllDeviceListings()
        if(connectCount < connectCountMax) {
            sampleState = SampleState.connecting
            updateTextDevicesList()
            device.connect(
                onConnected = { connectedDevice: CrayonicDevice ->
                    Log.i(TAG, "device : ${device.address} ${device.name}: CONNECTED")
                    connectCount += 1
                    sampleState = SampleState.disconnecting
                    updateTextDevicesList()
                    connectedDevice.disconnect()
                },
                onDisconnected = { connectedDevice ->
                    Log.i(
                        TAG,
                        "device : ${connectedDevice.address} ${connectedDevice.name}: DISCONNECTED "
                    )
                    disconnectCount += 1
                    updateTextDevicesList()
                    if (connectCount>0){
                        connectDevice(connectedDevice)
                    }else{
                        endOfConnections()
                    }
                },
                onError = { connectedDevice: CrayonicDevice, error: CrayonicDeviceError ->
                    Log.e(
                        TAG,
                        "device : ${connectedDevice.address} ${connectedDevice.name}: ERROR : $error"
                    )
                },
                onProgress = { connectedDevice: CrayonicDevice, requestKey: CrayonicRequestCode, progress: Progress ->
                    Log.i(TAG, "Progress update : $requestKey - ${progress.perc()}")
                    updateProgress(progress)
                }
            )
        }else{
            endOfConnections()
        }
    }

    private fun endOfConnections() {
        sampleState = SampleState.ended
        service?.unbind()
    }

    private fun updateText(text: String) {
        CoroutineScope(Dispatchers.Main).launch {
            helloTextView.text = text
        }
    }

    private fun updateProgress(progressUpdate: Progress) {
        CoroutineScope(Dispatchers.Main).launch {
            progress.progress = progressUpdate.perc()
            progress.show()
        }
    }

    override fun onDestroy() {
        service?.unbind()
        super.onDestroy()
    }
}
