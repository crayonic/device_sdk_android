package com.crayonic.device_sdk_android.sample;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.ContentLoadingProgressBar;

import com.crayonic.device_sdk.device.DeviceFilter;
import com.crayonic.device_sdk.device.errors.CrayonicDeviceError;
import com.crayonic.device_sdk.device.kotojava.CrayonicDeviceListener;
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode;
import com.crayonic.device_sdk.device.CrayonicDevice;
import com.crayonic.device_sdk.device.CrayonicL1;
import com.crayonic.device_sdk.device.CrayonicL1.L1DeviceVerboseHolder;
import com.crayonic.device_sdk.util.MatchingExtKt;
import com.crayonic.device_sdk.util.Progress;
import com.crayonic.device_sdk_android.core.CrayonicDevicesService;
import com.crayonic.device_sdk_android.kotojava.CrayonicServiceListener;
import com.crayonic.device_sdk_android.utils.nfc.NFCConsumingActivity;
import com.crayonic.device_sdk_android.utils.nfc.NFCUtil;
import com.livinglifetechway.quickpermissions_kotlin.PermissionsManagerKt;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import space.lupu.kapdu.transmission.TransmissionProgress;

import static com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode.connectDevice;

public class SampleEndOfShipmentJava extends NFCConsumingActivity implements CrayonicDeviceListener, CrayonicServiceListener {

    public static final String TAG = "SampleEndOfShipmentJava";
    private ContentLoadingProgressBar progressBar = null;
    private TextView textView = null;

    private CrayonicDevicesService service = null;
    private String deviceMacAddress = "";
    private DeviceFilter deviceFilter = new DeviceFilter(deviceMacAddress);
    private L1DeviceVerboseHolder deviceHolder = new L1DeviceVerboseHolder();

    private HashSet<CrayonicDevice> seenDevices = new HashSet<CrayonicDevice>();
    private Boolean isDeviceOff = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);
        setSupportActionBar(findViewById(R.id.toolbar));
        findViewById(R.id.fab).setVisibility(View.GONE);
        progressBar = findViewById(R.id.progress);
        textView = findViewById(R.id.helloTextView);
        updateText("Tap device with NFC enabled phone to end its shipment");
    }

    @Override
    public void onNfcIntentFound(@NotNull Intent intent) {
        String nfcCapturedAddress = NFCUtil.INSTANCE.deviceAddressFromIntent(intent);
        if (nfcCapturedAddress != null) {
            if(MatchingExtKt.isValidBleAddress(nfcCapturedAddress)){
                deviceMacAddress = nfcCapturedAddress;
                deviceFilter = new DeviceFilter(nfcCapturedAddress);
                updateTextDevicesList();
                initCrayonicDeviceService();
            }else{
                Log.e(TAG, "NFC Address is not valid: "+nfcCapturedAddress.toString());
            }
        }else{
            Log.e(TAG, "Captured NFC intent is not valid: "+intent.toString());
        }
    }

    private void initCrayonicDeviceService() {
        SampleEndOfShipmentJava inst = this;
        QuickPermissionJavaUtilKt.runWithPermissions(this, new QuickPermissionJavaUtilInterface() {
            @Override
            public void onPermissionCallback() {
                CrayonicDevicesService.Companion.getInstance(inst, inst);
            }
        }, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    @Override
    public void onServiceAttached(CrayonicDevicesService crayonicService) {
        service = crayonicService;
        listDevices();
    }

    @Override
    public void onServiceError(Exception exception) {
        Log.e(TAG,"Servcie ERROR : ", exception);
    }

    private void listDevices(){
        seenDevices.clear();
        if (service != null) {
            service.listDevices(deviceFilter, this);
        }
    }

    @Override
    public void onDevicesFound(DeviceFilter originFilter, List<CrayonicDevice> devices) {
        Log.d(TAG, "list found : "+devices.size()+" devices");
        seenDevices.addAll(devices);
        updateTextDevicesList();
        if(devices.size() > 0){
            CrayonicDevice crayonicDevice = devices.get(0);
            isDeviceOff = false;
            connectDevice(crayonicDevice);
        }
    }

    private void connectDevice(CrayonicDevice crayonicDevice){
        if (service != null) {
            service.stopAllDeviceListings();
        }
        crayonicDevice.connect(this);
    }

    @Override
    public void onConnect(CrayonicDevice device) {
        if(device instanceof CrayonicL1) {
            deviceHolder = ((CrayonicL1) device).toL1DeviceVerboseHolder();
            updateTextDevicesList();
            resetDevice((CrayonicL1) device);
        }
    }

    private void resetDevice(CrayonicL1 connectedDevice){
        connectedDevice.runCommandResetToPowerOff("", this);
    }

    @Override
    public void onResult(CrayonicDevice device, CrayonicRequestCode requestKey, Map<String, Object> values) {
        if(requestKey == CrayonicRequestCode.runCommandResetToPowerOff){
            isDeviceOff = true;
            updateTextDevicesList();
        }
        device.disconnect();
    }

    @Override
    public void onError(CrayonicDevice device, CrayonicRequestCode requestKey, CrayonicDeviceError error) {
        Log.e(TAG,"device : "+ device.getAddress()+" "+ device.getName() + " ERROR : "+error.toString());
    }

    @Override
    public void onProgress(CrayonicDevice device, CrayonicRequestCode requestKey, Progress progress) {
        Log.i(TAG, "Progress update : "+requestKey.name()+" - "+progress.perc());
        updateProgress(progress);
    }

    @Override
    public void onDisconnect(CrayonicDevice device) {
        endCrayonicDeviceService();
    }

    private void endCrayonicDeviceService() {
        Log.e("Sample", "service endCrayonicDeviceService : "+service);
        try {
            service.unbind();
            service = null;
        } catch (Exception e) {
        }

    }

    private void updateTextDevicesList() {
        StringBuilder sb = new StringBuilder();
        sb.append(deviceFilter);
        sb.append("\n\n");
        sb.append("Found BLE Devices : \n");
        for (CrayonicDevice dev : seenDevices) {
            sb.append(dev.getAddress()).append("\n");
        }

        sb.append("\n\n");
        sb.append("Selected device : \n").append(deviceHolder.toString());
        sb.append("\n\n");
        if(isDeviceOff) sb.append("Device was turned OFF" );
        else sb.append("Device shutting down");
        updateText(sb.toString());
    }

    private void updateText(String text) {
        textView.setText(text);
    }

    private void updateProgress(Progress progressUpdate) {
        progressBar.setProgress(progressUpdate.perc());
        progressBar.show();
    }

}
