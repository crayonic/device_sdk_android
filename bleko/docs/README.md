# Project Title

BLE KOtlin library for Android - BLEKO 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Add dependency to your module build gradle. 

### Usage

There is a sample module that simulates usage of this library. 

This exact library is used internally as a dependency of another project library.  

## Running the tests

No tests included in this pre-release version. 


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 
