package space.lupu.bleko.scanner

import kotlinx.coroutines.Job
import no.nordicsemi.android.support.v18.scanner.ScanCallback
import no.nordicsemi.android.support.v18.scanner.ScanResult
import space.lupu.bleko.utils.isValidBleAddress
import space.lupu.bleko.utils.scanForBleDevicesWithTimeout
import space.lupu.bleko.utils.stopScanning
import space.lupu.bleko.utils.toParcelUUIDorNull
import space.lupu.koex.lambda.emptyLambda1inUout
import space.lupu.koex.lambda.emptyLambda2inUout


class BleKoScanner {
    val allScanCallbacksSeen = mutableSetOf<ScanCallback>()

    val parentJob = Job()

    fun startScan(
        stringServiceCharacteristic: String = "",
        macAddress: String = "",
        bleDeviceName: String = "",
        timeoutMs: Long = -1L,
        onScanResult:(callbackType:Int, result:ScanResult) -> Unit = emptyLambda2inUout(),
        onBatchScanResults: (results: List<ScanResult>)-> Unit = emptyLambda1inUout(),
        onScanFailed: (errorCode: Int) -> Unit = emptyLambda1inUout(),
        parentJob: Job = Job(this.parentJob)
    ): ScanCallback {
        val isValidScanRequest = bleDeviceName.isNotBlank()
                || stringServiceCharacteristic.toParcelUUIDorNull() != null
                || macAddress.isValidBleAddress()

        val newScanCalback = BleKoScanCallback(
            onScanResult, onBatchScanResults, onScanFailed,
            isValidScanRequest
        )
        if (isValidScanRequest) {
            scanForBleDevicesWithTimeout(
                newScanCalback,
                timeoutMs,
                parentJob,
                stringServiceCharacteristic,
                macAddress,
                bleDeviceName
            )
            allScanCallbacksSeen.add(newScanCalback)
        }else{
            parentJob.cancel()
        }
        return newScanCalback
    }

    fun stopScan(callback: ScanCallback) {
        stopScanning(callback)
        allScanCallbacksSeen.remove(callback)
    }


    fun stopAllScans() {
        allScanCallbacksSeen.forEach {
            stopScanning(it)
        }
        allScanCallbacksSeen.clear()
    }
}

