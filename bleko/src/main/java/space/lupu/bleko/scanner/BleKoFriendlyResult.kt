package space.lupu.bleko.scanner

import android.bluetooth.BluetoothDevice
import android.util.TimeUtils
import no.nordicsemi.android.support.v18.scanner.ScanResult

data class BleKoFriendlyResult(
    var bleDevice: BluetoothDevice?,
    var macAddress: String,
    var bleName: String,
    var timestampMs: Long,
    var timestampNanos: Long,
    var rssi: Int,
    var isConnectable: Boolean,
    var detailedScanResult: ScanResult
) {

    companion object {
        fun fromScanResult(scanResult: ScanResult): BleKoFriendlyResult {
            val device = scanResult.device
            val devMacAddress = device.address?:""
            val devName = device.name?:""
            val timestampMs = System.currentTimeMillis()
            return BleKoFriendlyResult(
                device,
                devMacAddress,
                devName,
                timestampMs,
                scanResult.timestampNanos,
                scanResult.rssi,
                scanResult.isConnectable,
                scanResult
                )
        }
    }
}