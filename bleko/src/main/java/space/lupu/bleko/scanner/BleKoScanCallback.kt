package space.lupu.bleko.scanner

import no.nordicsemi.android.support.v18.scanner.ScanCallback
import no.nordicsemi.android.support.v18.scanner.ScanResult

class BleKoScanCallback(
    var onScanResult: (callbackType: Int, result: ScanResult) -> Unit,
    var onBatchScanResults: (results: List<ScanResult>) -> Unit,
    var onScanFailed: (errorCode: Int) -> Unit,
    var scanStarted:Boolean = false
) : ScanCallback() {

    /**
     * Callback when a BLE advertisement has been found.
     *
     * @param callbackType Determines how this callback was triggered. Could be one of
     *            {@link ScanSettings#CALLBACK_TYPE_ALL_MATCHES},
     *            {@link ScanSettings#CALLBACK_TYPE_FIRST_MATCH} or
     *            {@link ScanSettings#CALLBACK_TYPE_MATCH_LOST}
     * @param result A Bluetooth LE scan result.
     */
    override fun onScanResult(callbackType: Int, result: ScanResult) {
        onScanResult.invoke(callbackType, result)
    }

    /**
     * Callback when batch results are delivered.
     *
     * @param results List of scan results that are previously scanned.
     */
    override fun onBatchScanResults(results: List<ScanResult?>) {
        onBatchScanResults.invoke(results.filterNotNull())
    }

    /**
     * Callback when scan could not be started.
     *
     * @param errorCode Error code (one of SCAN_FAILED_*) for scan failure.
     */
    override fun onScanFailed(errorCode: Int) {
        onScanFailed(errorCode)
    }
}