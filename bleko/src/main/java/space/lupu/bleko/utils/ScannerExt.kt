package space.lupu.bleko.utils

import kotlinx.coroutines.*
import no.nordicsemi.android.support.v18.scanner.*


fun scanForBleDevicesWithTimeout(
    scanCallback: ScanCallback,
    timeoutMs: Long = -1L,
    parentJob:Job = Job(),
    stringServiceCharacteristic: String = "",
    macAddress: String = "",
    bleDeviceName: String = ""
): BluetoothLeScannerCompat {
    if (timeoutMs > 0){
        val scanJob = Job(parentJob)
        CoroutineScope(Dispatchers.IO + scanJob).launch{
            delay(timeoutMs)
            stopScanning(scanCallback)
            parentJob.cancel()
        }
    }else{
        parentJob.cancel()
    }
    val scanner = scanForBleDevices(
        scanCallback,
        stringServiceCharacteristic,
        macAddress,
        bleDeviceName
    )
    return scanner
}

fun scanForBleDevices(
    scanCallback: ScanCallback,
    stringServiceCharacteristic: String = "",
    macAddress: String = "",
    bleDeviceName: String = ""
): BluetoothLeScannerCompat {

    val scanner: BluetoothLeScannerCompat =
        BluetoothLeScannerCompat.getScanner()

    val settings: ScanSettings = ScanSettings.Builder()
        .setLegacy(false)
        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
        .setReportDelay(1000)
        .setUseHardwareBatchingIfSupported(true)
        .build()

    val parcelChar = stringServiceCharacteristic.toParcelUUIDorNull()

    scanner.startScan(
        listOf<ScanFilter>(
            ScanFilter.Builder().apply {
                if (bleDeviceName.isNotBlank()) setDeviceName(bleDeviceName)
                if (macAddress.isValidBleAddress()) setDeviceAddress(macAddress)
                if (parcelChar != null) setServiceUuid(parcelChar)
            }.build()
        ),
        settings,
        scanCallback
    )

    return scanner
}

fun stopScanning(scanCallback: ScanCallback) {
    val scanner =
        BluetoothLeScannerCompat.getScanner()
    scanner.stopScan(scanCallback)
}

fun String.toScanFilter_fromServiceCharacteristic() =
    this.let { string ->
        val parcelUuid = string.toParcelUUIDorNull()
        if (parcelUuid != null)
            ScanFilter.Builder().apply {
                setServiceUuid(parcelUuid)
            }.build()
    }

fun String.toScanFilter_fromDeviceName(): ScanFilter?  =
    this.let { string ->
        ScanFilter.Builder().apply {
            setDeviceName(string)
        }.build()
    }

fun String.toScanFilter_fromDeviceAddress(): ScanFilter? =
    this.let { string ->
        ScanFilter.Builder().apply {
            setDeviceName(string)
        }.build()
    }
