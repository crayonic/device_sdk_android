package space.lupu.bleko.utils

import android.os.ParcelUuid
import space.lupu.koex.hex.HEX_CHARS_S
import java.lang.Exception
import java.util.*

fun String.toParcelUUIDorNull():ParcelUuid? = toUUIDorNull()?.let {ParcelUuid(it)}
fun String.toUUIDorNull():UUID? {
    try {
        return UUID.fromString(this)
    }catch (e:Exception){
    }
    return null
}

fun UUID.toParcelUUID():ParcelUuid = ParcelUuid(this)

/*
 * Throwing conversions
 */
fun String.toParcelUUID():ParcelUuid = ParcelUuid(toUUID())

fun String.toUUID():UUID = UUID.fromString(this)



fun String?.isValidBleAddress(): Boolean {
    if (this == null) return false

    // something like : D2:BB:3F:51:D2:42
    val split = this.split(":")
    if (split.size == 6) {
        var correctCount = 0
        split.forEach { str ->
            str.forEach { char ->
                if (HEX_CHARS_S.contains(char.toLowerCase())) {
                    correctCount++
                }
            }
        }
        if (correctCount == 12) {
            // IFF 6 HEX encoded Bytes separated by ':'
            return true
        }
    }
    return false
}
