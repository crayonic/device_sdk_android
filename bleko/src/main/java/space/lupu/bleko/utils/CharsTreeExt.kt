package space.lupu.bleko.utils

import space.lupu.bleko.connection.SubCharChannelType
import space.lupu.bleko.connection.SubCharHolder
import java.util.*

fun formServiceCharacteristicMap(
    serviceCharacteristic: String,
    subServiceReadCharacteristics: List<String>,
    subServiceWriteCharacteristics: List<String>,
    subServiceNotifyCharacteristics: List<String>
): Map<UUID, List<SubCharHolder>> {
    val entry =formServiceCharacteristicMapEntry(
        serviceCharacteristic,
        subServiceReadCharacteristics,
        subServiceWriteCharacteristics,
        subServiceNotifyCharacteristics
    ) ?: return mapOf()
    return mapOf(entry)
}

fun formServiceCharacteristicMapEntry(
    serviceCharacteristic: String,
    subServiceReadCharacteristics: List<String>,
    subServiceWriteCharacteristics: List<String>,
    subServiceNotifyCharacteristics: List<String>
): Pair<UUID, List<SubCharHolder>>? {
    val service = serviceCharacteristic.toUUIDorNull()
    if (service == null) return null

    val subServicesRead =
        try {
            subServiceReadCharacteristics.map { str ->
                SubCharHolder(str, str.toUUID(), SubCharChannelType.read )
            }
        } catch (e: Exception) {
            return null
        }
    val subServicesWrite =
        try {
            subServiceWriteCharacteristics.map { str ->
                SubCharHolder(str, str.toUUID(), SubCharChannelType.write )
            }
        } catch (e: Exception) {
            return null
        }
    val subServicesWriteNotif =
        try {
            subServiceNotifyCharacteristics.map { str ->
                SubCharHolder(str, str.toUUID(), SubCharChannelType.notify )
            }
        } catch (e: Exception) {
            return null
        }

    val resList = arrayListOf<SubCharHolder>()
    resList.addAll(subServicesRead)
    resList.addAll(subServicesWrite)
    resList.addAll(subServicesWriteNotif)

    return service to resList
}
