package space.lupu.bleko.connection

import android.bluetooth.BluetoothDevice
import space.lupu.koex.lambda.emptyLambda1inUout

data class ConnectCallbackHolder(
    val bleDevice: BluetoothDevice?,
    val address:String = "",
    val onConnect:()->Unit = {},
    val onDisconnect:()->Unit = {},
    val onError:(errorReason:String)->Unit = emptyLambda1inUout()
){
    constructor():this(null,"", {}, {}, {})

    fun changeOnlyDisconnect(onDisconnect: () -> Unit): ConnectCallbackHolder {
        return ConnectCallbackHolder(
            bleDevice, address, onConnect, onDisconnect, onError
        )
    }
    fun changeOnlyError(onError: (errString:String) -> Unit): ConnectCallbackHolder {
        return ConnectCallbackHolder(
            bleDevice, address, onConnect, onDisconnect, onError
        )
    }

}