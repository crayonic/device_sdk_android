package space.lupu.bleko.connection

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.util.Log
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import space.lupu.bleko.utils.isValidBleAddress
import space.lupu.koex.hex.toHexSeparated
import java.util.*
import kotlin.coroutines.resume

/**
 * Single connect multiple devices
 */
@Suppress("unused")
class BleKoConnector(
    context: Context,
    val parentJob: Job = Job()
) {
    val TAG = "BleKoSimpleOneDev"

    var connectedDeviceCallbacks: BleKoPerDeviceCallbacks? = null

    val devicesMap = hashMapOf<String, BluetoothDevice>()
    val devicesCallbacksMap = hashMapOf<String, BleKoPerDeviceCallbacks>()

    val bleKoManager = BleKoBleBleManager(context, Job(parentJob))

    val singleConnectMutex = Mutex()
    var singleConnectJob : Job = Job(parentJob)

    fun killSingleConnection() = singleConnectJob.cancel()
    fun killBleManagerOperation() = bleKoManager.parentJob.cancel()
    fun killAll() = parentJob.cancel()

    var conectionNumber = 0

    fun initialize(){
        bleKoManager.initialize()
    }

    fun read(
        address: String,
        characteristicReadStr: String,
        onError: (errorReason: String) -> Unit,
        onResult: (ByteArray) -> Unit
    ) {
        val operation = "Read"

        if (isConnected(address, onError, operation)
            && hasCharacteristic(characteristicReadStr, onError, operation)
        ) {
            val readChar = connectedDeviceCallbacks!!.characteristicsMap[characteristicReadStr]!!

            // no need for routing map here, we will use SimpleDataReceivedCallback directly from readOnConnectedDevice

            bleKoManager.runOnBleManager(connectedDeviceCallbacks!!) { bleKoBleBleManager: BleKoBleBleManager, onFinished: (result: String) -> Unit ->
                bleKoBleBleManager.readOnConnectedDevice(
                    readChar,
                    onError
                ) { receivedBytes ->
//                    Log.e(TAG, "received bytes : ${receivedBytes.toHexSeparated()}")
                    CoroutineScope(Dispatchers.Main).launch {
                        onResult(receivedBytes)
                    }
                }
                onFinished("Enqueued $operation")
            }
        }
    }

    fun writeWithNotify(
        address: String,
        characteristicWriteStr: String,
        characteristicNotifyStr: String,
        onError: (errorReason: String) -> Unit,
        sendBytes: ByteArray,
        onResult: (ByteArray) -> Unit
    ) {
        val operation = "Write"

        if (isConnected(address, onError, operation)
            && hasCharacteristic(characteristicWriteStr, onError, operation)
            && hasCharacteristic(characteristicNotifyStr, onError, operation)
        ) {
            val writeChar = connectedDeviceCallbacks!!.characteristicsMap[characteristicWriteStr]!!
            val notifyChar = connectedDeviceCallbacks!!.characteristicsMap[characteristicWriteStr]!!

            // need to use the routing map, set current callback to the routing map
            connectedDeviceCallbacks!!.charsRouteCallbackMap.put(characteristicNotifyStr, onResult)

            bleKoManager.runOnBleManager(connectedDeviceCallbacks!!) { bleKoBleBleManager: BleKoBleBleManager, onFinished: (result: String) -> Unit ->
                Log.e(TAG, "writeWithNotify")

                bleKoBleBleManager.writeNotifyOnConnectedDevice(
                    writeChar,
                    notifyChar,
                    sendBytes,
                    onError
                ) { receivedBytes ->
                    CoroutineScope(Dispatchers.Main).launch {
                        onResult(receivedBytes)
                    }
                }
                onFinished("Enqueued $operation")
            }
        }
    }

    fun isConnected(
        address: String,
        onError: (errorReason: String) -> Unit,
        operation: String
    ): Boolean {
        // check if connected
        val connectedDevice = connectedDeviceCallbacks?.device
        if (connectedDevice == null || connectedDeviceCallbacks == null) {
            val msg = "Failed BLE $operation : Device is disconnected"
            Log.e(TAG, "Operation failed : $msg", NullPointerException(msg))
            onError(msg)
            return false
        }
        // check if address match
        val connectedAddress = connectedDevice.address
        if (!address.equals(connectedAddress, true)) {
            val msg =
                "Failed BLE $operation : Requested device address $address is not currently connected. Disconnect $connectedAddress first."
            Log.e(TAG, "Operation failed : $msg", NullPointerException(msg))
            onError(msg)
            return false
        }
        return true
    }

    fun hasCharacteristic(
        characteristicStr: String,
        onError: (errorReason: String) -> Unit,
        operation: String
    ): Boolean {
        // check if connected
        val connectedDevice = connectedDeviceCallbacks?.device
        if (connectedDevice == null || connectedDeviceCallbacks == null) {
            val msg = "Failed BLE $operation : Device is disconnected"
            Log.e(TAG, "Operation failed : $msg", NullPointerException(msg))
            onError(msg)
            return false
        }
        // check if we have the characteristic initialized from BleManager / BleManagerGattCallback / isRequiredServiceSupported
        val characteristic = connectedDeviceCallbacks!!.characteristicsMap[characteristicStr]
        if (characteristic == null) {
            val msg =
                "Failed BLE $operation : Requested characteristic $characteristicStr was not initialized in BleManagerGattCallback"
            Log.e(TAG, "Operation failed : $msg", NullPointerException(msg))
            onError(msg)
            return false
        }

        return true
    }

    fun isValidBleAddress(
        address: String,
        onError: (errorReason: String) -> Unit,
        operation: String
    ): Boolean {
        // check if connected
        if (!address.isValidBleAddress()) {
            val msg = "Failed BLE $operation : '$address' is not a valid Bluetooth address"
            Log.e(TAG, "Operation failed : $msg", NullPointerException(msg))
            onError(msg)
            return false
        }
        return true
    }

    fun connectDevice(
        address: String,
        requiredServices: Map<UUID, List<SubCharHolder>>,
        onConnect: () -> Unit,
        onDisconnect: () -> Unit,
        onError: (errorReason: String) -> Unit
    ) {
        val operation = "Connect Address"
        if (isValidBleAddress(address, { errorReason ->
                onError(errorReason)
            }, operation)) {
            // alles ok lets connect
            val device: BluetoothDevice =
                BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address)
            connectDevice(device, requiredServices, onConnect, onDisconnect, onError)
        }


    }

    fun connectDevice(
        bleDevice: BluetoothDevice,
        requiredServices: Map<UUID, List<SubCharHolder>>,
        onConnect: () -> Unit,
        onDisconnect: () -> Unit,
        onError: (errorReason: String) -> Unit
    ) {
        val operation = "Connect Device"
        val address = bleDevice.address ?: ""
        // invalid address check
        if (!isValidBleAddress(address, onError, operation)) {
            return
        }
        // check if connected
        val connectedDevice = connectedDeviceCallbacks?.device
        if (connectedDevice == null || connectedDeviceCallbacks == null) {
            val msg = "BLE $operation : $address will be connected"
            Log.i(TAG, "Info: $msg")
            // shouldConnect = true
        } else {
            // check if address match
            val connectedAddress = connectedDevice.address
            if (address.equals(connectedAddress, true)) {
                // already connected
                CoroutineScope(Dispatchers.Main).launch {
                    onConnect.invoke()
                }
                return
            } else {
                val msg =
                    "BLE $operation : Cannot connect to $address. Already have one device connected $connectedAddress. Disconnect $connectedAddress first."
                Log.e(TAG, "Operation failed : $msg", NullPointerException(msg))
                onError(msg)
                return
            }
        }
        // need to connect
        // lock as single connect


        CoroutineScope(Dispatchers.IO + Job()).launch {
            val upper = this.coroutineContext
//            Log.e(TAG, "Tries to connect : @$singleConnectJob #$upper")
            singleConnectMutex.withLock {
                conectionNumber += 1
                singleConnectJob = CoroutineScope(Dispatchers.IO + Job(parentJob)).launch {
                    Log.e(TAG, "Unlocked connect : @$singleConnectJob  #$upper" )
                    async {
                        suspendCancellableCoroutine<String> { cancellableContinuation ->
                            // make this thread safe by putting this into locked suspend function
                            singleConnect(
                                bleDevice = bleDevice,
                                requiredServices = requiredServices,
                                onConnect = {
                                    Log.e(TAG, "on connected received")

                                    CoroutineScope(Dispatchers.Main).launch {
                                        Log.e(TAG, "invoking on connect: " )
                                        onConnect.invoke()
                                    }
                                },
                                onError = {
                                    CoroutineScope(Dispatchers.Main).launch {
                                        onError.invoke(it)
                                    }
                                },
                                onDisconnect = {
                                    CoroutineScope(Dispatchers.Main).launch {
                                        onDisconnect.invoke()
                                    }
                                    if(cancellableContinuation.isActive)
                                        cancellableContinuation.resume("Disconnected")
                                },
                                address = address,
                                operation = operation
                            )
                        }
                        Log.e(TAG, "-- Connection $conectionNumber -- : @$singleConnectJob #$upper")
                    }.await()
                }
                Log.e(TAG, "-- Connection $conectionNumber -- : @$singleConnectJob #$upper")

            }
        }

//        singleConnect(
//            bleDevice = bleDevice, onConnect = {
//                CoroutineScope(Dispatchers.Main).launch {
//                    onConnect.invoke()
//                }
//            },
//            onError = {
//                CoroutineScope(Dispatchers.Main).launch {
//                    onError.invoke(it)
//                }
//            },
//            onDisconnect = {
//                CoroutineScope(Dispatchers.Main).launch {
//                    onDisconnect.invoke()
//                }
//            },
//            address = address,
//            operation = operation
//        )

    }

    private fun singleConnect(
        bleDevice: BluetoothDevice,
        requiredServices: Map<UUID, List<SubCharHolder>>,
        onConnect: () -> Unit,
        onError: (errorReason: String) -> Unit,
        onDisconnect: () -> Unit,
        address: String,
        operation: String

    ) {
        // create new per device callbacks
        val newDeviceCallbacks = BleKoPerDeviceCallbacks(
            parentJob = parentJob,
            requiredServices = requiredServices,
            device = bleDevice,
            onDeviceShouldDisconnect = { callbacks ->
                val op = "connection state enforced Disconnect"
                Log.e(TAG, "BLE Operation : $op / from state ${callbacks.lifecycleState.name} ${callbacks.errorState}")
                bleKoManager.runOnBleManager(callbacks) { bleKoBleBleManager, onFinished ->
                    bleKoBleBleManager.disconnect()
                    onFinished("Enqueued $op")
                }
            }
        )

        newDeviceCallbacks.onDeviceOnConnected = { callbacks ->
            connectedDeviceCallbacks = callbacks
            onConnect.invoke()
        }
        newDeviceCallbacks.onDeviceOnError = { callbacks, errorReason ->
            onError.invoke(errorReason)
        }
        newDeviceCallbacks.onDeviceOnDisconnected = { callbacks ->
            // somebody already disconnected us and invoked another onDisconnect
            if(connectedDeviceCallbacks != null){
                connectedDeviceCallbacks = null
                Log.e(TAG, "connectDevice from BleKoConnector : onDisconnected  -- normal" )
            }else{
                Log.e(TAG, "connectDevice from BleKoConnector : onDisconnected  -- chained " )
            }
            onDisconnect.invoke()
            killSingleConnection()

        }
        devicesCallbacksMap.put(address, newDeviceCallbacks)
        bleKoManager.runOnBleManager(
            newDeviceCallbacks
        ) { bleKoBleBleManager: BleKoBleBleManager, onFinished: (result: String) -> Unit ->
            bleKoBleBleManager
                .connect(bleDevice)
                .retry(3)
                .enqueue()
            onFinished("Enqueued $operation")
        }
    }

    fun disconnectDevice(address: String,
                         onDisconnect: () -> Unit) {
        val operation = "Disconnect $address"
//        Log.e(TAG, "disconnectDevice from BleKoConnector : isConnected? = ${isConnected(address, onError = {
//            Log.e(
//                TAG, "disconnectDevice from BleKoConnector : isConnected? : ERROR : $it"
//            )}, operation = "test")}")
        if (isConnected(address, {
                connectedDeviceCallbacks?.invokeOnError(it)
            }, operation)) {
            val currentOnDisconnect = connectedDeviceCallbacks!!.onDeviceOnDisconnected
            connectedDeviceCallbacks!!.onDeviceOnDisconnected = {

                connectedDeviceCallbacks!!.onDeviceOnDisconnected = {} // clear the callback to ignore further disconnects

                onDisconnect.invoke()
                currentOnDisconnect.invoke(it)
                connectedDeviceCallbacks = null
                Log.e(TAG, "disconnectDevice from BleKoConnector : onDisconnected  -- already disconnected" )
                killSingleConnection()

            }
            bleKoManager.runOnBleManager(connectedDeviceCallbacks!!) { bleKoBleBleManager, onFinished ->
                bleKoBleBleManager.disconnect().enqueue()
                onFinished("Enqueued $operation")
            }
        }else{
//            Log.e(TAG, "disconnectDevice from BleKoConnector : onDisconnected  -- was not connected " )
            onDisconnect.invoke()
        }

    }

}

