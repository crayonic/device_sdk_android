package space.lupu.bleko.connection

enum class BleKoDevLifecycleState {
    DeviceDisconnecting,
    DeviceDisconnected,
    DeviceConnecting,
    DeviceConnected,
    DeviceNotSupported,
    BondingFailed,
    ServicesDiscovered,
    BondingRequired,
    LinkLossOccurred,
    Bonded,
    DeviceReady,
    Error,
    Unspecified
}