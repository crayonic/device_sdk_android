package space.lupu.bleko.connection

enum class SubCharChannelType {
    read,
    write,
//    write_notify,
    notify
}