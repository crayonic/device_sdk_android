package space.lupu.bleko.connection

import android.bluetooth.BluetoothDevice
import no.nordicsemi.android.ble.callback.DataReceivedCallback
import no.nordicsemi.android.ble.data.Data

class RouteDataReceivedCallback(
    val characteristicString: String,
    val onData:(characteristicString: String, device: BluetoothDevice, data: Data)->Unit
) : DataReceivedCallback{
    override fun onDataReceived(device: BluetoothDevice, data: Data) {
        onData.invoke(characteristicString,device, data)
    }

}