package space.lupu.bleko.connection

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGattCharacteristic
import android.util.Log
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import no.nordicsemi.android.ble.BleManagerCallbacks
import space.lupu.bleko.connection.BleKoDevLifecycleState.*
import space.lupu.koex.lambda.emptyLambda1inUout
import space.lupu.koex.lambda.emptyLambda2inUout
import space.lupu.koex.lambda.emptyLambda3inUout
import java.util.*

class BleKoPerDeviceCallbacks(
    internal val onDeviceConnected_in: (device: BluetoothDevice) -> Unit = emptyLambda1inUout(),
    internal val onDeviceDisconnected_in: (device: BluetoothDevice) -> Unit = emptyLambda1inUout(),
    internal val onError_in: (device: BluetoothDevice, message: String, errorCode: Int) -> Unit = emptyLambda3inUout(),
    internal val onDeviceDisconnecting_in: (device: BluetoothDevice) -> Unit = emptyLambda1inUout(),
    internal val onDeviceNotSupported_in: (device: BluetoothDevice) -> Unit = emptyLambda1inUout(),
    internal val onBondingFailed_in: (device: BluetoothDevice) -> Unit = emptyLambda1inUout(),
    internal val onServicesDiscovered_in: (device: BluetoothDevice, optionalServicesFound: Boolean) -> Unit = emptyLambda2inUout(),
    internal val onBondingRequired_in: (device: BluetoothDevice) -> Unit = emptyLambda1inUout(),
    internal val onLinkLossOccurred_in: (device: BluetoothDevice) -> Unit = emptyLambda1inUout(),
    internal val onBonded_in: (device: BluetoothDevice) -> Unit = emptyLambda1inUout(),
    internal val onDeviceReady_in: (device: BluetoothDevice) -> Unit = emptyLambda1inUout(),
    internal val onDeviceConnecting_in: (device: BluetoothDevice) -> Unit = emptyLambda1inUout(),
    val onDevLifeStateChanged: (currentState: BleKoDevLifecycleState, callbacks: BleKoPerDeviceCallbacks) -> Unit = defaultRouteStateChange(),
    val parentJob:Job = Job(),
    val requiredServices: Map<UUID, List<SubCharHolder>>,
    val device : BluetoothDevice,
    var onDeviceShouldDisconnect : (BleKoPerDeviceCallbacks) -> Unit = {}
) : BleManagerCallbacks {

    val initializedRoutes: HashMap<String, RouteDataReceivedCallback> = hashMapOf()
    val lifecycleMutex = Mutex()
    var lifecycleState: BleKoDevLifecycleState = Unspecified

    var errorState = 0

    val charsRouteCallbackMap: HashMap<String, (ByteArray) -> Unit> = hashMapOf()
    val characteristicsMap: HashMap<String, BluetoothGattCharacteristic> = hashMapOf()

    var onDeviceOnConnected:(BleKoPerDeviceCallbacks) -> Unit = {}

    var onDeviceOnError: (BleKoPerDeviceCallbacks,errorReason:String) -> Unit = emptyLambda2inUout()

    var onDeviceOnDisconnected : (BleKoPerDeviceCallbacks) -> Unit = {}


    fun invokeOnConnected(){

        Log.e("BleCallbacks", "on connected sent ")
        onDeviceOnConnected.invoke(this)
    }

    fun invokeOnDisconnected(){
        onDeviceOnDisconnected.invoke(this)
    }

    fun invokeOnError(errorReason: String){
        onDeviceOnError.invoke(this, errorReason)
    }

    fun changeLifecycleState(
        currentState: BleKoDevLifecycleState
    ) {
        CoroutineScope(Dispatchers.IO+Job(parentJob)).launch{
            lifecycleMutex.withLock {
                lifecycleState = currentState
                withContext(Dispatchers.Main) {
                    Log.e("BleCallbacks", "Device: $device State changed: ${currentState.name}")
                    onDevLifeStateChanged(currentState, this@BleKoPerDeviceCallbacks)
                }
            }
        }
    }

    override fun onDeviceDisconnecting(device: BluetoothDevice) {
        onDeviceDisconnecting_in(device)
        changeLifecycleState(DeviceDisconnecting)
    }

    override fun onDeviceDisconnected(device: BluetoothDevice) {
        onDeviceDisconnected_in(device)
        changeLifecycleState(DeviceDisconnected)
    }

    override fun onDeviceConnected(device: BluetoothDevice) {
        onDeviceConnected_in(device)
        changeLifecycleState(DeviceConnected)
    }

    override fun onDeviceNotSupported(device: BluetoothDevice) {
        onDeviceNotSupported_in(device)
        changeLifecycleState(DeviceNotSupported)
    }

    override fun onBondingFailed(device: BluetoothDevice) {
        onBondingFailed_in(device)
        changeLifecycleState(BondingFailed)
    }

    override fun onServicesDiscovered(device: BluetoothDevice, optionalServicesFound: Boolean) {
        onServicesDiscovered_in(device, optionalServicesFound)
        changeLifecycleState(ServicesDiscovered)
    }

    override fun onBondingRequired(device: BluetoothDevice) {
        onBondingRequired_in(device)
        changeLifecycleState(BondingRequired)
    }

    override fun onLinkLossOccurred(device: BluetoothDevice) {
        onLinkLossOccurred_in(device)
        changeLifecycleState(LinkLossOccurred)
    }

    override fun onBonded(device: BluetoothDevice) {
        onBonded_in(device)
        changeLifecycleState(Bonded)
    }

    override fun onDeviceReady(device: BluetoothDevice) {
        onDeviceReady_in(device)
        changeLifecycleState(DeviceReady)
    }

    override fun onError(device: BluetoothDevice, message: String, errorCode: Int) {
        onError_in(device, message, errorCode)
        errorState =  errorCode
        changeLifecycleState(Error)
    }

    override fun onDeviceConnecting(device: BluetoothDevice) {
        onDeviceConnecting_in(device)
        changeLifecycleState(DeviceConnecting)
    }

    
}

fun defaultRouteStateChange(): (currentState: BleKoDevLifecycleState, callbacks: BleKoPerDeviceCallbacks) -> Unit = {currentState: BleKoDevLifecycleState, callbacks: BleKoPerDeviceCallbacks ->

    when (currentState) {
        DeviceNotSupported, BondingFailed, Error -> {
            callbacks.invokeOnError("Error in BLE connection : " + currentState.name)
            callbacks.onDeviceShouldDisconnect(callbacks)
        }
        DeviceReady -> {
            /*
            * at this place the previous app was calling mGattCallback.enableNotificationsForWriteTX()
            * now it looks like a good point for outside logic to start
            * */
            callbacks.invokeOnConnected()
        }
        DeviceDisconnected -> callbacks.invokeOnDisconnected()
        // ignore all else
        else -> {
        }
    }
}
