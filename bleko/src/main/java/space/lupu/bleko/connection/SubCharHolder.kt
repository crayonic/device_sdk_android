package space.lupu.bleko.connection

import java.util.*

data class SubCharHolder(val charStr: String, val charUUID: UUID, val channelType:SubCharChannelType)