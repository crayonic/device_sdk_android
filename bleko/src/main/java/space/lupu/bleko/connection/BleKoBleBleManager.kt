package space.lupu.bleko.connection

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.content.Context
import android.util.Log
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import no.nordicsemi.android.ble.BleManager
import no.nordicsemi.android.ble.data.Data
import space.lupu.koex.hex.toHexPrefixed0x
import space.lupu.koex.hex.toHexPrefixed0xSeparated
import java.util.*
import kotlin.coroutines.resume

class BleKoBleBleManager(
    runningContext: Context,
    val parentJob: Job = Job()
) : BleManager<BleKoPerDeviceCallbacks>(runningContext) {

    val TAG = "BleKoBleBleManager"

    override fun getGattCallback(): BleManagerGattCallback {
        return gattCallbacks
    }

    val bleManagerMutex = Mutex()
    val instance by lazy { this }

    fun runOnBleManager(
        bleCallbacks: BleKoPerDeviceCallbacks,
        runBlock: (BleKoBleBleManager, onFinished: (result: String) -> Unit) -> Unit
    ) {
        CoroutineScope(Dispatchers.IO + Job(parentJob)).launch {
            withTimeout(1000) {
                bleManagerMutex.withLock {
                    suspendCancellableCoroutine<String> { cont ->
                        // on each operation init callbacks
                        instance.setGattCallbacks(bleCallbacks)
                        // then do the magic with ble manager for this device
                        runBlock.invoke(instance) {
//                            Log.e(TAG, "runOnBleManager onFinished : $it")

                            cont.resume(it)
                        }
                    }
                }
            }
        }
    }

    fun initialize(){
        instance
        Log.e(TAG, "BleKo MGR Initialized")
    }

    fun runOnBleManagerBlocking(
        bleCallbacks: BleKoPerDeviceCallbacks,
        runBlock: (BleKoBleBleManager, onFinished: (String) -> Unit) -> Unit
    ) {
        instance.setGattCallbacks(bleCallbacks)
        runBlock.invoke(this){
//            Log.e(TAG, "runOnBleManagerBlocking : $it")
        }
    }
    ///////////////////////

    override fun shouldClearCacheWhenDisconnected(): Boolean {
        return false
    }

    //////////////////////////

    internal fun readOnConnectedDevice(
        characteristic: BluetoothGattCharacteristic,
        onError: (String) -> Unit,
        onResult: (ByteArray) -> Unit
    ) {
        readCharacteristic(characteristic).with(SimpleDataReceivedCallback(onResult)).fail{device: BluetoothDevice, status: Int ->
            onError("ErrCode:$status, Failed to read characteristic : ${characteristic.uuid} ")
        }.enqueue()
    }

    internal fun writeNotifyOnConnectedDevice(
        characteristicWrite: BluetoothGattCharacteristic,
        characteristicNotify: BluetoothGattCharacteristic,
        sendBytes: ByteArray,
        onError: (String) -> Unit,
        onResult: (ByteArray) -> Unit
    ) {
        // setNotification should have been called in BleManagerGattCallback .initialize() , if not we are doomed


        // at last write
        writeCharacteristic(
            characteristicWrite,
            sendBytes
        ).with { device: BluetoothDevice, data: Data ->
            Log.e(TAG, "BleKo Write charWr: (${characteristicWrite.uuid}) charNot (${characteristicNotify.uuid}) bytes: ${sendBytes.toHexPrefixed0xSeparated()}")
        }.fail { device: BluetoothDevice, status: Int ->
            onError(
                "ErrCode:$status, Failed to write bytes to characteristic : ${characteristicWrite.uuid}\n bytes: ${sendBytes.take(100).toByteArray().toHexPrefixed0xSeparated()}"
            )
        }.enqueue()
    }

    //////////////////////////

    private val gattCallbacks =
        object : BleManagerGattCallback() {
            override fun initialize() {
                Log.e(TAG, "GATT : init $this")
                mCallbacks.requiredServices.forEach { servEntry ->
                    servEntry.value.forEach { subCharKey ->
                        val characteristic =
                            mCallbacks.characteristicsMap.get(subCharKey.charStr)

                        if (characteristic == null) {
                            Log.e(
                                TAG,
                                "Characteristic missing: ServiceChar: ${servEntry.key} RequestedChar: ${subCharKey.charStr}",
                                NullPointerException()
                            )
                            return
                        }

                        val specificRoutingCallback =
                            RouteDataReceivedCallback(subCharKey.charStr) { characteristicString, device, data ->
//                                Log.e( TAG, "Specific routing for char ($characteristic) received bytes: ${data.value?.toHexPrefixed0xSeparated()}")
                                // working with data returned from GATT characteristic operation
                                val deviceRegisteredCallback =
                                    mCallbacks.charsRouteCallbackMap[characteristicString]
                                // propagate data on main thread
//                                Log.e( TAG, "Specific routing for char ($characteristic) lambda to invoke is :${deviceRegisteredCallback?:"NULL"}")
                                deviceRegisteredCallback?.invoke(
                                    data.value ?: byteArrayOf()
                                ) // either incoming bytes or empty byte array
                            }

                        // leaving the below code unoptimized for better understanding
                        when (subCharKey.channelType) {
                            SubCharChannelType.read -> {
//                                readCharacteristic(characteristic).with(specificRoutingCallback)
//                                    .enqueue()
                            }
                            SubCharChannelType.notify -> {
//                                Log.e(TAG, "setNotifications for char ${characteristic.uuid}")
                                mCallbacks.initializedRoutes.put(subCharKey.charStr, specificRoutingCallback)
                                setNotificationCallback(characteristic).with(
                                    specificRoutingCallback
                                )
                                enableNotifications(characteristic).enqueue()
//                                    readCharacteristic(characteristic).with(specificRoutingCallback)
//                                        .enqueue()
                            }
                            SubCharChannelType.write -> {
//                                    writeCharacteristic(characteristic).with(specificRoutingCallback)
//                                        .enqueue()
                            }
                        }

                    }
                }
            }


            override fun onDeviceDisconnected() {
                mCallbacks.onDeviceDisconnected(mCallbacks.device)
            }

            // checking all services trees for open characteristics + if write or write_notify also checks descriptors
            override fun isRequiredServiceSupported(gatt: BluetoothGatt): Boolean {
                Log.d(TAG, "GATT FOUUND characteristics : \n" +
                        "${gatt.services.mapIndexed { index, service ->
                            "\n\nservice[$index]: ${service?.uuid} / \n"+
                                    "${service?.characteristics?.map { "${it?.uuid} \n" }}"
                        }}")
                mCallbacks.requiredServices.forEach { entry ->

                    val service = gatt.getService(entry.key)
                    if (service == null) {
                        Log.e(
                            TAG,
                            "BLE Device ${mCallbacks.device} does not contain required service characteristic servChar: ${entry.key}",
                            NullPointerException()
                        )
                        return false
                    }
                    entry.value.forEach { subCharKey ->
                        val characteristic =
                            service.getCharacteristic(subCharKey.charUUID)
                        if (characteristic == null) {
                            Log.e(
                                TAG,
                                "BLE Device ${mCallbacks.device} does not contain required characteristic servChar: ${entry.key}  reqChar: ${subCharKey.charUUID}",
                                NullPointerException()
                            )
                            return false
                        }


                        val descriptor = when (subCharKey.channelType) {
                            SubCharChannelType.read -> BluetoothGattCharacteristic.PROPERTY_READ
                            SubCharChannelType.write -> BluetoothGattCharacteristic.PROPERTY_WRITE
                            SubCharChannelType.notify -> BluetoothGattCharacteristic.PROPERTY_NOTIFY
//                        else -> 0
                        }
                        val props = characteristic.properties
                        if (props and descriptor == 0) {
                            Log.e(
                                TAG,
                                "BLE Device characteristic descriptor does not match the property READ/WRITE/NOTIFY : servChar: ${entry.key}  reqChar: ${subCharKey.charUUID}",
                                NullPointerException()
                            )
                            return false
                        }
                        mCallbacks.characteristicsMap.put(subCharKey.charStr, characteristic)
                    }

                }
                return true
            }
        }




}