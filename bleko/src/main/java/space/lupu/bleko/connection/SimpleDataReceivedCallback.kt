package space.lupu.bleko.connection

import android.bluetooth.BluetoothDevice
import no.nordicsemi.android.ble.callback.DataReceivedCallback
import no.nordicsemi.android.ble.data.Data

class SimpleDataReceivedCallback(
    val onData:(dataBytes : ByteArray)->Unit
) : DataReceivedCallback{
    override fun onDataReceived(device: BluetoothDevice, data: Data) {
        onData(data.value?: byteArrayOf())
    }
}