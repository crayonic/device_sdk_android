Crayonic Device SDK - Android
========

Provides functional interface for Crayonic Devices, such as L1, Pen or others.  

Features
--------
- List surrounding devices based on specified filter
- Connect and disconnect specific device
- Communicate with specific device upon connection is established

Installation
------------

### Gradle

Add dependency to module level build.gradle:

```groovy
dependencies {
    implementation 'com.crayonic:device-sdk:X.X.X'
}
```

Make sure that you have our repository at project level in build.gradle:

```groovy
allprojects {
    repository {
        maven{ url "https://gitlab.com/crayonic/maven/raw/master/android/stable/" }
    }
}
```

Sample
----------

#### Crayonic service start:
 
```kotlin
val service by lazy { CrayonicService.getRunningInstance(getContext()) } // thread safe initializer
```
#### Basic usage
Filter devices:
```kotlin
val filter = DeviceFilter() // DeviceFilter(vararg tags:String) sets criteria to be met during device search

service.list(filter, onDeviceFound = { device: CrayonicDevice ->
    if(device.name == desiredName){
        // play with the device here  
    }
})
```
Read device values: 
```kotlin
    // read general data on connected device as CrayonicDevice
    device.connect({ 
        device.readBatteryValue()
        device.readModelNumber()
        device.readFirmwareVersion()
    })
```
#### Advanced usage
Run device specific tasks or commands :

```kotlin
    // use more specific functions based on object type
    if(device is CrayonicPen){
        (device as CrayonicPen).connect( 
            onConnect = { device ->
                // run your command on connected device, otherwise DeviceOutOfReachError
                device.runCommandReadPenData(adminPinCode)
            }, 
            onDisconnect = { device ->
                // handle disconnect event
            },
            onError= { device, errorFlag ->
                // handle error propagation to UI 
            }
            
            )
    }else (device is CrayonicL1){
        // handle only onConnect, disconnect and error defaults to empty lambda {}
        (device as CrayonicL1).connect({ device ->
            // implicit cast to CrayonicL1
            device.runTaskReadEvent() // runs whole bunch of commands in the service
        })
    }
```

SDK Implementation
------------
### service

- CrayonicService
    - getRunningInstance : obtain your instance of SDK service 
    - listDevices : search for and list all surrounding devices matching DeviceFilter
    
```kotlin
class CrayonicService():CoreForegroundBindingService(){
    override val ONGOING_NOTIFICATION_ID = 7531
    
    override val ONGOING_NOTIFICATION_CHANNEL_ID = "crayonic_service_device_sdk"
    
    override val ONGOING_NOTIFICATION_CHANNEL_NAME = "Crayonic Device SDK"

    companion object {
        fun getRunningInstance(bindToContext: Context): { .. }
    }

    fun listDevices( filter: DeviceFilter, 
                     onDeviceFound : (device: CrayonicDevice) -> Unit 
    ){ .. }
}
```

### filter

- DeviceFilter
    - Devices are filtered by tags attached to it during listing/scanning.
    - With empty constructor **DeviceFilter()** matches all devices.
    - **matchAnyTags** : At least one of the tags have to be found on device ( logical OR ).  
    - **matchAllTags** : All of these have to be met. ( logical AND )
    - If both lists are used at least one applies among them. ( logical OR ) 
    
```kotlin

class DeviceFilter(
    matchAnyTags:List<String> = listOf(),
    matchAllTags:List<String> = listOf()
):NodeFilter(getRandomizedId()) { .. }
```
    
### devices

- CrayonicDevice
    - errorFlag : use to check if the last interaction was successful ()
    - deviceTags : tags used for filtering

    - readDeviceName : get the whole device name, BLE is advertising shorter version
    - readBatteryValue : juice level in range 0 to 100
    - readModelNumber : model name like Pen or L1
    - readFirmwareVersion : device FW version name, usually in format "2.5"     
    
    - runCommandReadClock : Read device clock in Unix Time

    - runCommandWriteClock : Write device clock in Unix Time

    - runCommandReadPublicKey : TODO

    - runCommandReadEthAddress : TODO

    - runCommandReset : clears device running config and values 
    - runCommandResetToUpdateMode : Clears device running config and values as reset but boots to update mode 
    - runTaskUpdateDevice : Update current firmware with a new one if available. Clears device running config and values 
    
```kotlin
class CrayonicDevice{
    var errorFlag: Error = NoError()
    val deviceTags: List<String>

    fun connect(
        onConnect:(device:CrayonicDevice)->Unit = {},
        onDisconnect:(device:CrayonicDevice)->Unit = {},
        onError:(device:CrayonicDevice)->Unit = {}
    ) { .. }

    fun disconnect(
        onDisconnect:(device:CrayonicDevice)->Unit = {},
        onError:(device:CrayonicDevice)->Unit = {}
    ) { .. }

    fun readDeviceName(onResult:(device:CrayonicDevice, readableValue:String) -> Unit = {}) { .. }
    fun readBatteryValue(onResult:(device:CrayonicDevice, readableValue:String) -> Unit = {}) { .. }
    fun readModelNumber(onResult:(device:CrayonicDevice, readableValue:String) -> Unit = {}) { .. }
    fun readFirmwareVersion(onResult:(device:CrayonicDevice, readableValue:String) -> Unit = {}) { .. }

    fun runCommandReadClock  ( onResult: (device:CrayonicDevice, unixTime: Long) -> Unit = {} ) { .. }
    fun runCommandWriteClock ( adminPin:String, unixTime:Long = currentUtcTime(), onResult: (device:CrayonicDevice) -> Unit = {} ) { .. }

    fun runCommandReadPublicKey ( onResult: (device:CrayonicDevice, bytes:ByteArray) -> Unit = {} ) { .. }
    fun runCommandReadEthAddress( onResult: (device:CrayonicDevice, ethAddress:String) -> Unit = {} ) { .. }

    fun runCommandResetToPowerOff   ( adminPin:String, onResult:(device:CrayonicDevice) -> Unit = {} ){ .. }
    fun runCommandResetToUpdateMode ( adminPin:String, onResult:(device:CrayonicDevice) -> Unit = {} ) { .. }

    fun runTaskUpdateDevice ( adminPin:String, onResult:(device:CrayonicDevice) -> Unit = {} ) { .. }
}
```
    
- CrayonicPen
    - is subclass of CrayonicDevice
    
    - runCommandSetPinCode : TODO

    - runCommandSignEthTx : TODO

    - runTaskInitializePen : TODO

    - runTaskReadData : TODO

```kotlin
class CrayonicPen : CrayonicDevice {

    fun runCommandSetPinCode( adminPin:String, newPin:String, onResult:(device:CrayonicPen) -> Unit = {} )

    fun runCommandSignEthTx( ethTx:ByteArray, onResult: (device:CrayonicPen, bytes:ByteArray) -> Unit = {} )

    fun runTaskInitializePen(adminPin:String, onResult:(device:CrayonicPen) -> Unit = {} )

    fun runTaskReadData(adminPin:String, onResult:(device:CrayonicPen) -> Unit = {} )
}

```
- CrayonicL1
  : CrayonicDevice
        
```kotlin
class CrayonicL1 : CrayonicDevice() {

    fun readTemperature(onResult:(device:CrayonicL1, readableValue:String) -> Unit = {}) { .. }
    fun readHumidity(onResult:(device:CrayonicL1, readableValue:String) -> Unit = {}) { .. }
    fun readLight(onResult:(device:CrayonicL1, readableValue:String) -> Unit = {}) { .. }
    fun readShock(onResult:(device:CrayonicL1, readableValue:String) -> Unit = {}) { .. }
    fun readIncline(onResult:(device:CrayonicL1, readableValue:String) -> Unit = {}) { .. }

    fun runCommandReadAlarmZones(onResult:(device:CrayonicL1, alarmZones:List<CrayonicAlarmZone>) -> Unit = {}) { .. }
    fun runCommandWriteAlarmZones(adminPin: String, alarmZones:List<CrayonicAlarmZone>, onResult:(device:CrayonicL1) -> Unit = {}) { .. }

    fun runCommandReadAssetId(onResult:(device:CrayonicL1, assetId:String) -> Unit = {}) { .. }
    fun runCommandWriteAssetId(adminPin: String, assetId:String, onResult:(device:CrayonicL1) -> Unit = {}) { .. }

    fun runCommandReadAssetId(onResult:(device:CrayonicL1, packageId:String) -> Unit = {}) { .. }
    fun runCommandWriteAssetId(adminPin: String, packageId:String, onResult:(device:CrayonicL1) -> Unit = {}) { .. }

    fun runCommandSetManagementPinCode( adminPin:String, newPin:String, onResult:(device:CrayonicL1) -> Unit = {} ){ .. }

    fun runTaskReadEvents( onResult:(device:CrayonicL1) -> Unit = {} ) { .. }

    fun runTaskInitialization(
        initializationValues : InitValues, // contains bunch of variables needed to setup device
        onResult:(device:CrayonicL1) -> Unit = {} ){ .. }

    data class InitValues(
        var packageId:String,
        var notificationEmail:String,
        var alarmZones:List<CrayonicAlarmZone>,
        var apiAccount:String,
        var apiSecret:String,
        var adminPin:String,
        var newPin:String
    )

    // following commands are used in mass production only    
    fun runTaskFactorySetup(adminPin: String, onResult:(device:CrayonicL1) -> Unit = {}) { .. }
    fun runTaskL1DeviceTest(adminPin: String, onResult:(device:CrayonicL1) -> Unit = {}) { .. }
}
```

    
### errors
- All interaction is ran above some device asynchronously. No error is thrown but rather saved as errorFlag variable inside CrayonicDevice object. If   
- DeviceOutOfReachError : while connecting to unreachable device
 

Document Version
-------
Work in progress version. Content of the SDK may vary until release.

License
--------
```
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

