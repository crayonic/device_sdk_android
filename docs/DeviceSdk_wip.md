Crayonic Device SDK - Android
========

Provides functional interface for Crayonic Devices, such as L1, Pen or others.  

Features
--------
1. List surrounding devices based on specified filter
1. Connect specific device
    - Communicate with specific device upon connection is established
1. Disconnected connected device

Overview
--------------

![list devices](https://gitlab.com/crayonic/tackbit-docs/raw/master/sdk/device_sdk/docs/device_sdk_list_devices.png)

![communication](https://gitlab.com/crayonic/tackbit-docs/raw/master/sdk/device_sdk/docs/device_sdk_communication.png)

![legend](https://gitlab.com/crayonic/tackbit-docs/raw/master/sdk/device_sdk/docs/device_sdk_uml_legend.png)

Installation
------------

### Gradle

Add dependency to module level build.gradle:

```groovy
dependencies {
    implementation 'com.crayonic:device-sdk:X.X.X'
}
```

Make sure that you have our repository at project level in build.gradle:

```groovy
allprojects {
    repository {
        maven{ url "https://gitlab.com/crayonic/maven/raw/master/android/stable/" }
    }
}
```

JAVA Interface
----------------
### service
```java
public interface CrayonicServiceListener {
    public void onDevicesFound(List<CrayonicDevice> devices);
}
```
### device
```java
public interface CrayonicDeviceListener {
    // prevents callback hell with RequestCode and Map of "variableKey" to Objects
    // each variable key has a predefined cast type
    public void onResult(CrayonicDevice device, RequestCode requestKey, Map<String, Object> values);

    public void onConnect(CrayonicDevice device);

    public void onDisconnect(CrayonicDevice device);

    public void onError(CrayonicDevice device, CrayonicRequestCode requestKey, CrayonicDeviceError error);
}
```
### device request keys
```java
public enum CrayonicRequestCode {
    // all devices
    connectDevice,
    disconnectDevice,

    readDeviceName,
    readBatteryLevel,
    readModelNumber,
    readFirmwareVersion,

    runCommandReadClock,
    runCommandWriteClock,
    runCommandReadPublicKey,
    runCommandReadEthAddress,
    runCommandResetToPowerOff,
    runCommandResetToUpdateMode,

    runTaskUpdateDevice,

    // Pen requests
    runCommandSetPinCode,
    runCommandSignEthTx,

    runTaskInitializePen,
    runTaskReadData,

    // L1 requests
    readTemperature,
    readHumidity,
    readLight,
    readShock,
    readIncline,

    runCommandReadAlarmZones,
    runCommandWriteAlarmZones,
    runCommandReadAssetId,
    runCommandWriteAssetId,
    runCommandReadPackageId,
    runCommandWritePackageId,
    runCommandSetManagementPinCode,
    
    runTaskReadEvents,
    runTaskInitialization,
    runTaskFactorySetup,
    runTaskL1DeviceTest,
}

```


Sample
----------

## Java

This sample shows how to:
- list devices
- connect selected device
- read some device member values 
- run device specific task
- disconnect device

Full pseudo code can be found here : [MainActivity](https://gitlab.com/crayonic/tackbit-docs/blob/master/sdk/device_sdk/samples/android/java/MainActivity.java)

![Sample uml](https://gitlab.com/crayonic/tackbit-docs/raw/master/sdk/device_sdk/docs/device_sdk_java_sample.png)

#### Crayonic service start:
 
```java
public class MainActivity extends AppCompatActivity
        implements CrayonicServiceListener, CrayonicDeviceListener {

    CrayonicService crayonicService = null

    Set<CrayonicDevice> foundDevices = new HashSet<CrayonicDevice>();

    CrayonicDevice selectedDevice = null;

    HashMap<CrayonicDevice, String> knownPinCodes = new HashMap<CrayonicDevice,String>();

    @Override
    protected void onResume() {
        if(crayonicService == null)
            crayonicService = CrayonicService.Companion.getRunningInstance(this);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if(crayonicService == null)
            crayonicService.unbindService(this);
        super.onDestroy();
    }

```
#### Device discovery

Search for surrounding devices, using DeviceFilter.

- DeviceFilter
    - ```DeviceFilter( List<String> matchAnyTags,  List<String> matchAllTags)```
    - Devices are filtered by tags attached to it during listing/scanning.
    - With empty constructor **DeviceFilter()** matches all devices.
    - **matchAnyTags** : At least one of the tags have to be found on device ( logical OR ).  
    - **matchAllTags** : All of these have to be met. ( logical AND )
    - If both lists are used at least one applies among them. ( logical OR ) 
    - tags can contain device fileds like :
        - model : "L1" or "Pen"
        - BLE MAC : "AB:CD:EF:12:34:56"
        - operation mode : "normalMode" or "updateMode"

```java
    Set<CrayonicDevice> foundDevices = new HashSet<CrayonicDevice>();

    public void listDevices(){
        DeviceFilter filter = new DeviceFilter(); // matches any reachable device
        crayonicService.listDevices(filter, this);
    }

    @Override
    public void onDevicesFound(List<CrayonicDevice> devices) {
        foundDevices.addAll(devices); // add devices to your collection
    }
```


#### Connect to user selected device
```java
    CrayonicDevice selectedDevice = null;

    private void crayonicDeviceSelectedByUser( CrayonicDevice device ){
        if(selectedDevice != null) selectedDevice.disconnect();

        selectedDevice = device;
        // connect and wait until callback gets here
        device.connect(this);
    }

    // device functionality is available after device gets connected
    @Override
    public void onConnect(CrayonicDevice device) {
        Toast.makeText(this,"Device connected", Toast.LENGTH_SHORT).show();
        // run your command on connected device, otherwise DeviceOutOfReachError
        startRequestingData(device);
    }

    private void startRequestingData(CrayonicDevice device) {
        device.readDeviceName(this);
        device.readBatteryLevel(this);
    }

    @Override
    public void onDisconnect(CrayonicDevice device) {
        Toast.makeText(this,"Device disconnected", Toast.LENGTH_SHORT).show();
    }

```

#### Response handling

```java
    
    HashMap<CrayonicDevice, String> knownPinCodes = getPinCodeDatabase();

    @Override
    public void onResult(CrayonicDevice device, RequestCode requestKey, Map<String, Object> values) {
        // device is null safe
        if(device != selectedDevice) return;

        // update UI
        if(requestKey == RequestCode.readBatteryLevel)
            updateUIreadBatteryLevel( (String) values.get("readableValue"));
        else if(requestKey == RequestCode.readDeviceName)
            updateUIreadDeviceName( (String) values.get("readableValue"));
        else
        // disconnect when task response was received
            device.disconnect(this); 


        // for some operation on device pin code is necessary
        String adminPinCode = knownPinCodes.get(device);

        // filter different type of devices
        //    and run device specific task 
        if(device.getDeviceTags().contains("L1")){
            // read alarms
            ((CrayonicL1) device).runTaskReadEvents(this);
        }else if(device.getDeviceTags().contains("Pen")){
            // read the signing data
            if(adminPinCode != null)
                ((CrayonicPen) device).runTaskReadData(adminPinCode,this);
        }else{
            // try to update unknown device model if we have the PIN
            if(adminPinCode != null)
                device.runTaskUpdateDevice(adminPinCode, this);
        }
    }

    @Override
    public void onError(CrayonicDevice device, CrayonicRequestCode reqKey, CrayonicDeviceError error) {
        // ignore all but current device
        if(device != selectedDevice) return;

        // error handling of callback error instance
        if(error.getErrorType() == CrayonicDeviceErrorType.NoError){
            // all is good
        }else{
            logError(device, error);
        }

        // error handling of device error instance
        CrayonicDeviceError errorFlag = device.getErrorFlag();  
        switch(errorFlag.getErrorType()){
            case NoError:
                // all is good
                break;
            case DeviceOutOfReachError:
                showUiRationaleDeviceOutOfReach(device);
                logError(device, errorFlag);
                break;
            default:
                logError(device, errorFlag);
                break;
        }
    }

```


## Kotlin

#### Crayonic service start:
 
```kotlin
val service by lazy { CrayonicService.getRunningInstance(getContext()) } // thread safe initializer
```
#### Basic usage
Filter devices:
```kotlin
val filter = DeviceFilter() // DeviceFilter sets criteria to be met during device search

service.list(filter, onDeviceFound = { device: CrayonicDevice ->
    if(device.name == desiredName){
        // play with the device here  
    }
})
```
Read device values: 
```kotlin
    // read general data on connected device as CrayonicDevice
    device.connect({ 
        device.readBatteryValue()
        device.readModelNumber()
        device.readFirmwareVersion()
    })
```
#### Advanced usage
Run device specific tasks or commands :

```kotlin
    // use more specific functions based on object type
    if(device is CrayonicPen){
        (device as CrayonicPen).connect( 
            onConnect = { device ->
                // run your command on connected device, otherwise DeviceOutOfReachError
                device.runCommandReadPenData(adminPinCode)
            }, 
            onDisconnect = { device ->
                // handle disconnect event
            },
            onError= { device, errorFlag ->
                // handle error propagation to UI 
            }
            
            )
    }else (device is CrayonicL1){
        // handle only onConnect, disconnect and error defaults to empty lambda {}
        (device as CrayonicL1).connect({ device ->
            // implicit cast to CrayonicL1
            device.runTaskReadEvent() // runs whole bunch of commands in the service
        })
    }
```


Advanced : Kotlin SDK Implementation
------------
### service

- CrayonicService
    - getRunningInstance : obtain your instance of SDK service 
    - listDevices : search for and list all surrounding devices matching DeviceFilter
    
```kotlin
class CrayonicService():CoreForegroundBindingService(){
    override val ONGOING_NOTIFICATION_ID = 7531
    
    override val ONGOING_NOTIFICATION_CHANNEL_ID = "crayonic_service_device_sdk"
    
    override val ONGOING_NOTIFICATION_CHANNEL_NAME = "Crayonic Device SDK"

    companion object {
        fun getRunningInstance(bindToContext: Context): { .. }
    }

    // suits kotlin users
    fun listDevices(filter: DeviceFilter,
                    onDeviceFound : (device: CrayonicDevice) -> Unit
    ){ .. }

    // suits java users
    fun listDevices(filter: DeviceFilter,
                    serviceListener : CrayonicServiceListener
    ){ .. }
}
```

### filter

- DeviceFilter
    - Devices are filtered by tags attached to it during listing/scanning.
    - With empty constructor **DeviceFilter()** matches all devices.
    - **matchAnyTags** : At least one of the tags have to be found on device ( logical OR ).  
    - **matchAllTags** : All of these have to be met. ( logical AND )
    - If both lists are used at least one applies among them. ( logical OR ) 
    
```kotlin

class DeviceFilter(
    matchAnyTags:List<String> = listOf(),
    matchAllTags:List<String> = listOf()
):NodeFilter(getRandomizedId()) { .. }
```
    
### devices

- CrayonicDevice
    - errorFlag : use to check if the last interaction was successful ()
    - deviceTags : tags used for filtering

    - readDeviceName : get the whole device name, BLE is advertising shorter version
    - readBatteryValue : juice level in range 0 to 100
    - readModelNumber : model name like Pen or L1
    - readFirmwareVersion : device FW version name, usually in format "2.5"     
    
    - runCommandReadClock : Read device clock in Unix Time

    - runCommandWriteClock : Write device clock in Unix Time

    - runCommandReadPublicKey 

    - runCommandReadEthAddress

    - runCommandReset : clears device running config and values 
    - runCommandResetToUpdateMode : Clears device running config and values as reset but boots to update mode 
    - runTaskUpdateDevice : Update current firmware with a new one if available. Clears device running config and values 
    
```kotlin

open class CrayonicDevice{
    var errorFlag: CrayonicDeviceError = NoError()
    val deviceTags: List<String>

    // java conveniences
    fun connect(deviceListener: CrayonicDeviceListener) { .. }
    fun connect() = disconnect({},{})

    // kotlin like lambdas
    fun connect(
            onConnect:(device: CrayonicDevice)->Unit = {},
            onDisconnect:(device: CrayonicDevice)->Unit = {},
            onError:(device: CrayonicDevice)->Unit = {}
    ) { .. }

    fun disconnect(deviceListener: CrayonicDeviceListener) { .. }
    fun disconnect() = disconnect({},{})

    fun disconnect(
            onDisconnect:(device: CrayonicDevice)->Unit = {},
            onError:(device: CrayonicDevice)->Unit = {}
    ) { .. }

    fun readDeviceName(deviceListener: CrayonicDeviceListener) { .. }
    fun readDeviceName(onResult:(device: CrayonicDevice, readableValue:String) -> Unit = {}) { .. }

    fun readBatteryLevel(deviceListener: CrayonicDeviceListener) { .. }
    fun readBatteryLevel(onResult:(device: CrayonicDevice, readableValue:String) -> Unit = {}) { .. }

    fun readModelNumber(deviceListener: CrayonicDeviceListener) { .. }
    fun readModelNumber(onResult:(device: CrayonicDevice, readableValue:String) -> Unit = {}) { .. }

    fun readFirmwareVersion(deviceListener: CrayonicDeviceListener) { .. }
    fun readFirmwareVersion(onResult:(device: CrayonicDevice, readableValue:String) -> Unit = {}) { .. }

    fun runCommandReadClock  (deviceListener: CrayonicDeviceListener) { .. }
    fun runCommandReadClock  ( onResult: (device: CrayonicDevice, unixTime: Long) -> Unit = {} ) { .. }

    fun runCommandWriteClock ( adminPin:String, unixTime:Long = currentUtcTime(), deviceListener: CrayonicDeviceListener) { .. }
    fun runCommandWriteClock ( adminPin:String, unixTime:Long = currentUtcTime(), onResult: (device: CrayonicDevice) -> Unit = {} ) { .. }

    fun runCommandReadPublicKey (deviceListener: CrayonicDeviceListener) { .. }
    fun runCommandReadPublicKey ( onResult: (device: CrayonicDevice, bytes:ByteArray) -> Unit = {} ) { .. }

    fun runCommandReadEthAddress (deviceListener: CrayonicDeviceListener) { .. }
    fun runCommandReadEthAddress( onResult: (device: CrayonicDevice, ethAddress:String) -> Unit = {} ) { .. }

    fun runCommandResetToPowerOff   (deviceListener: CrayonicDeviceListener) { .. }
    fun runCommandResetToPowerOff   ( adminPin:String, onResult:(device: CrayonicDevice) -> Unit = {} ){ .. }

    fun runCommandResetToUpdateMode (deviceListener: CrayonicDeviceListener) { .. }
    fun runCommandResetToUpdateMode ( adminPin:String, onResult:(device: CrayonicDevice) -> Unit = {} ) { .. }

    fun runTaskUpdateDevice ( adminPin:String, deviceListener: CrayonicDeviceListener) { .. }
    fun runTaskUpdateDevice ( adminPin:String, onResult:(device: CrayonicDevice) -> Unit = {} ) { .. }
}
```
    
- CrayonicPen
    - is subclass of CrayonicDevice
    
    - runCommandSetPinCode 

    - runCommandSignEthTx 

    - runTaskInitializePen 

    - runTaskReadData 

```kotlin
class CrayonicPen : CrayonicDevice() {

    fun runCommandSetPinCode( adminPin:String, newPin:String, deviceListener: CrayonicDeviceListener) { .. }
    fun runCommandSetPinCode( adminPin:String, newPin:String, onResult:(device: CrayonicPen) -> Unit = {} ) { .. }

    fun runCommandSignEthTx( ethTx:ByteArray, deviceListener: CrayonicDeviceListener) { .. }
    fun runCommandSignEthTx( ethTx:ByteArray, onResult: (device: CrayonicPen, bytes:ByteArray) -> Unit = {} ) { .. }

    fun runTaskInitializePen(adminPin:String, deviceListener: CrayonicDeviceListener) { .. }
    fun runTaskInitializePen(adminPin:String, onResult:(device: CrayonicPen) -> Unit = {} ) { .. }

    fun runTaskReadData(adminPin:String, deviceListener: CrayonicDeviceListener) { .. }
    fun runTaskReadData(adminPin:String, onResult:(device: CrayonicPen) -> Unit = {} ) { .. }
}
```
- CrayonicL1
    - is subclass of CrayonicDevice
    - readTemperature
    - readHumidity
    - readLight
    - readShock
    - readIncline

    - runCommandReadAlarmZones
    - runCommandWriteAlarmZones

    - runCommandReadAssetId
    - runCommandWriteAssetId

    - runCommandReadAssetId
    - runCommandWriteAssetId

    - runCommandSetManagementPinCode

    - runTaskReadEvents

    - runTaskInitialization
        
    - InitValues : formatted input, supports json import/export

    - runTaskFactorySetup
    - runTaskL1DeviceTest


        
```kotlin
class CrayonicL1 : CrayonicDevice() {

    fun readTemperature(onResult:(device:CrayonicL1, readableValue:String) -> Unit = {}) { .. }
    fun readHumidity(onResult:(device:CrayonicL1, readableValue:String) -> Unit = {}) { .. }
    fun readLight(onResult:(device:CrayonicL1, readableValue:String) -> Unit = {}) { .. }
    fun readShock(onResult:(device:CrayonicL1, readableValue:String) -> Unit = {}) { .. }
    fun readIncline(onResult:(device:CrayonicL1, readableValue:String) -> Unit = {}) { .. }

    fun runCommandReadAlarmZones(onResult:(device:CrayonicL1, alarmZones:List<CrayonicAlarmZone>) -> Unit = {}) { .. }
    fun runCommandWriteAlarmZones(adminPin: String, alarmZones:List<CrayonicAlarmZone>, onResult:(device:CrayonicL1) -> Unit = {}) { .. }

    fun runCommandReadAssetId(onResult:(device:CrayonicL1, assetId:String) -> Unit = {}) { .. }
    fun runCommandWriteAssetId(adminPin: String, assetId:String, onResult:(device:CrayonicL1) -> Unit = {}) { .. }

    fun runCommandReadAssetId(onResult:(device:CrayonicL1, packageId:String) -> Unit = {}) { .. }
    fun runCommandWriteAssetId(adminPin: String, packageId:String, onResult:(device:CrayonicL1) -> Unit = {}) { .. }

    fun runCommandSetManagementPinCode( adminPin:String, newPin:String, onResult:(device:CrayonicL1) -> Unit = {} ){ .. }

    fun runTaskReadEvents( onResult:(device:CrayonicL1) -> Unit = {} ) { .. }

    fun runTaskInitialization(
        initializationValues : InitValues, // contains bunch of variables needed to setup device
        onResult:(device:CrayonicL1) -> Unit = {} ){ .. }

    data class InitValues(
        var packageId:String,
        var notificationEmail:String,
        var alarmZones:List<CrayonicAlarmZone>,
        var apiAccount:String,
        var apiSecret:String,
        var adminPin:String,
        var newPin:String
    )

    // following commands are used in mass production only    
    fun runTaskFactorySetup(adminPin: String, onResult:(device:CrayonicL1) -> Unit = {}) { .. }
    fun runTaskL1DeviceTest(adminPin: String, onResult:(device:CrayonicL1) -> Unit = {}) { .. }
}
```

    
### errors
- All interaction is ran above some device asynchronously. No error is thrown but rather saved as errorFlag variable inside CrayonicDevice object. If   
- DeviceOutOfReachError : while connecting to unreachable device
 

Document Version
-------
Work in progress version. Content of the SDK may vary until release.

License
--------
```
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
