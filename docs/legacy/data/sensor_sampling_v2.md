# L1 Data

---

## Delta logging of variety of variably measured types.

Previously we have sampled all types as often as we could at preset sampling interval while saving some power in between samples i.e. 2 second sampling interval made CPU sleep for 1,9 seconds and sample sensors for 0,1 seconds. All sampled data were written in a fixed 16  byte format every 2 seconds inside the buffer.

### PAGE

To achieve much better storage performance while increasing precision of measured data we will keep sampling of all sensors like above however we will only store differences in between current and last written sampled data. The deltas will be stored as part of a *PAGE* and will be recorded only for samples which values has changed in time since the beginning of the page. If sampled data for given sensor never change then that data is only stored as absolute value at the beginning of the page only. Size of each *PAGE* is fixed and must be predefined i.e. 256 bytes.

#### Start of new PAGE

Start of the new *PAGE* begins with *BASELINE* values of measured data in default (max) byte length based on the sample value *TYPE*. Each other sample value of the same *TYPE* in this *PAGE* is *DELTA* value, which is difference from previously accumulated measured value since start of this *PAGE*.


#### End of the PAGE

End of the *PAGE* is strictly at N-th byte (eg. 256-th). This can cause the last record data to be stored only partially. The accumulated value will be the *BASELINE* of the next PAGE.

If there was less then *PAGE* size bytes sampled, all remaining bytes will be repeating ```0x00``` *PADDING*.

### Native Ordering

Every numeric value is stored in Little Endian Byte ordering.

### DELTA

*DELTA* the change between previous accumulated value and current measured value is calculated as numeric difference ( signed int 64 ~~ 8 bytes length ) compressed to minimal number of bytes. Compression removes reocuring upper bytes  ```0x00``` for positive numbers and ```0xFF``` for negative numbers. The first bit is always signed. This way -1 of unsigned int 64 takes only 1 Byte instead of 8. More examples below:

| Signed number | Hexadecimal difference | Compressed |
| --- | --- | --- |
|  0  | 0x 0000 0000 0000 0000 | 0x00 |
|  1  | 0x 0000 0000 0000 0001 | 0x01 |
|  -1  | 0x FFFF FFFF FFFF FFFF | 0xFF |
|  -129  | 0x FFFF FFFF FFFF FF7F | 0xFF7F |
|  129  | 0x 0000 0000 0000 0081 | 0x0081 |

#### Hint: Everything is DELTA

We can assume that first occurance of the measured data in one *PAGE* is *BASELINE* value and any other data point of the same type is *DELTA* value. If we think about it even more, each *PAGE* starts with a new set of *BASELINE* values, meaning the *PAGE* resets values. Clearing values to exactly to ```0``` for each data sample gives us oportunity to use only addition operation (+).

### Type Length Value (TLV)

We will use TLV format to record each data point (including time) with TL stored in one byte (upper 4 bits for *TYPE* and lower 4 for *LENGTH*) -  only 16 types can be defined with each having 16 bytes in size. Smallest sample value will require 2 bytes in the buffer: 1 byte for Type and Length, and 1 byte for value while biggest sample can take up as much as 17 bytes (1 TL + 16 bytes for value).

### Format Overview

1. PAGE (256 Bytes)
	1. TL byte ```04``` - start of new page
	2. Followed by 4 bytes of Time BASELINE
	3. TL byte ```12```
	4. Followed by 2 Bytes of Temperature BASELINE
	3. TL byte ```21```
	4. Followed by 1 Byte of Humidity
	5. ... continues multiple BASELINEs ...
	4. TL byte ```01```
	5. Followed by 1 Byte of Time DELTA
	6. TL byte ```11```
	7. Followed by 1 Byte of Temperature DELTA
	8. ... continues until end of the PAGE ...
2. PAGE (256 Bytes)
	1. TL byte ```04``` - start of new page
	2. Followed by 4 bytes of Time BASELINE
	3. TL byte ```12```
	4. Followed by 2 Bytes of Temperature BASELINE
	3. TL byte ```21```
	4. Followed by 1 Byte of Humidity
	5. ... continues multiple BASELINEs ...
	4. TL byte ```01```
	5. Followed by 1 Byte of Time DELTA
	6. TL byte ```11```
	7. Followed by 1 Byte of Temperature DELTA
	8. ... continues until the last DELTA ...
	9. ... filled in with ```0x00``` *PADDING* as this is the last page read from device

## Supported TYPEs

| TYPE 	| Name                  	| Description                                    	| Unit    	| FACTOR 	|
|------	|-----------------------	|------------------------------------------------	|---------	|-------:	|
| 0    	| Time                  	| Unix timestamp.                                	| Second  	| 1      	|
| 1    	| Temperature           	|                                                	| Celsius 	| 10     	|
| 2    	| Relative humidity     	|                                                	| %       	| 1      	|
| 3    	| Light / Optical Power 	| L2 App generic error.                          	| nW/cm2  	| 1      	|
| 4    	| Shock                 	| Combined magnitude of all 3 acceleration axes. 	| G       	| 100    	|
| 5    	| Incline               	| Array of average accelerations for each axis.  	| G       	| 2048   	|
| 6    	| Longitude             	|                                                	|         	|        	|
| 7    	| Latitude              	|                                                	|         	|        	|
| 8    	| Byte string           	| Binary array                                   	|         	|        	|

### FACTOR of values

Each value has a factor multiplier which moves decimal point to higher order.

For example Shock has FACTOR = 100. Thus for getting the real shock value we read the little endian value of DELTA, divide it by 100 and get real number (float or double), corresponding to measured shock value.

---

### Incline Calculation

Incline consists from 3 average accelerations for axes x, y, z (ordered in array accordingly). Each axis is represented by 2 Bytes signed integer number.

We can simply get incline around each axis from average accelerations of incline array by following equations:

```
PI = Math.PI() // 3.14....

pitch = 180 * atan (aX/sqrt(aY*aY + aZ*aZ))/PI

roll = 180 * atan (aY/sqrt(aX*aX + aZ*aZ))/PI

yaw = 180 * atan (aZ/sqrt(aX*aX + aZ*aZ))/PI
```

More about Yaw, Pitch and Roll in [Flight Dynamics ref.](https://en.wikipedia.org/wiki/Flight_dynamics_(fixed-wing_aircraft)).

---

### Pseudo Parser Code

```
database = {} // DB of objects of all seen sample values

current_record = {
	"time"=0, "temp"=0.0, "hum"=0.0, ...
}

page_size = 256
index = 0
length = 0
delta[16]       // little endian byte array

foreach ( byte in received_bytes ):

	if( (index++ % page_size) == 0 ):
		// new page starts strictly at 'page_size'
		reset_current_record()
		type, length = decode_TL_byte(byte)

	else :
		// inside of the page
		if( length <= 0 ):

			increment_current_value_by_delta(type, *delta)
			clear_delta()
			type, length = decode_TL_byte(byte)

			if(type == 0):
				// new record starts with new timestamp
				save_current_record_to_database()

		else :
			// read value bytes
			byte_array.append(byte)
			length --
```

---

### Examples

#### Example PAGE bytes split to records

Raw data from device, split into rows by measurements. Each row starts with delta time or full 4 byte time for a new Page. End of the page is padded to length of 256 bytes with 00 padding.

```
raw bytes : 049bf5465c12dc00212533e67701413056e9fdcefff907010541d656180043002b00012833991fff01053300e100010a1103010532f38c01051102330d8200010511fe012d1102011e11fe011433b457ff4117010533cc9d0041e8010f11fe010532278b0105327345013c32c0b501193233b6010532995101051102010511fe01461102210342af0056020029ff9bff010521044251ff56e8ffbf0053000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000


--- page : 1 ---
 i: 0 	04  9bf5465c (1548154267)  12 dc00 (220)  21 25 (37)  33 e67701 (96230)  41 30 (48)  56 e9fdcefff907 (-3211799)  	{ @: 1548154267, t: 22, h: 37, l: 9623, s: 0.48 }
 i: 23 	 01 05 (5)  41 d6 (-42)  56 180043002b00 (4390936)  	{ @: 1548154272, t: 22, h: 37, l: 9623, s: 0.06 }
 i: 34 	 01 28 (40)  33 991fff (-57447)  	{ @: 1548154312, t: 22, h: 37, l: 3878.3, s: 0.06 }
 i: 40 	 01 05 (5)  33 00e100 (57600)  	{ @: 1548154317, t: 22, h: 37, l: 9638.3, s: 0.06 }
 i: 46 	 01 0a (10)  11 03 (3)  	{ @: 1548154327, t: 22.3, h: 37, l: 9638.3, s: 0.06 }
 i: 50 	 01 05 (5)  32 f38c (-29453)  	{ @: 1548154332, t: 22.3, h: 37, l: 6693, s: 0.06 }
 i: 55 	 01 05 (5)  11 02 (2)  33 0d8200 (33293)  	{ @: 1548154337, t: 22.5, h: 37, l: 10022.3, s: 0.06 }
 i: 63 	 01 05 (5)  11 fe (-2)  	{ @: 1548154342, t: 22.3, h: 37, l: 10022.3, s: 0.06 }
 i: 67 	 01 2d (45)  11 02 (2)  	{ @: 1548154387, t: 22.5, h: 37, l: 10022.3, s: 0.06 }
 i: 71 	 01 1e (30)  11 fe (-2)  	{ @: 1548154417, t: 22.3, h: 37, l: 10022.3, s: 0.06 }
 i: 75 	 01 14 (20)  33 b457ff (-43084)  41 17 (23)  	{ @: 1548154437, t: 22.3, h: 37, l: 5713.9, s: 0.29 }
 i: 83 	 01 05 (5)  33 cc9d00 (40396)  41 e8 (-24)  	{ @: 1548154442, t: 22.3, h: 37, l: 9753.5, s: 0.05 }
 i: 91 	 01 0f (15)  11 fe (-2)  	{ @: 1548154457, t: 22.1, h: 37, l: 9753.5, s: 0.05 }
 i: 95 	 01 05 (5)  32 278b (-29913)  	{ @: 1548154462, t: 22.1, h: 37, l: 6762.2, s: 0.05 }
 i: 100 	 01 05 (5)  32 7345 (17779)  	{ @: 1548154467, t: 22.1, h: 37, l: 8540.1, s: 0.05 }
 i: 105 	 01 3c (60)  32 c0b5 (-19008)  	{ @: 1548154527, t: 22.1, h: 37, l: 6639.3, s: 0.05 }
 i: 110 	 01 19 (25)  32 33b6 (-18893)  	{ @: 1548154552, t: 22.1, h: 37, l: 4750, s: 0.05 }
 i: 115 	 01 05 (5)  32 9951 (20889)  	{ @: 1548154557, t: 22.1, h: 37, l: 6838.9, s: 0.05 }
 i: 120 	 01 05 (5)  11 02 (2)  	{ @: 1548154562, t: 22.3, h: 37, l: 6838.9, s: 0.05 }
 i: 124 	 01 05 (5)  11 fe (-2)  	{ @: 1548154567, t: 22.1, h: 37, l: 6838.9, s: 0.05 }
 i: 128 	 01 46 (70)  11 02 (2)  21 03 (3)  42 af00 (175)  56 020029ff9bff (-14090238)  	{ @: 1548154637, t: 22.3, h: 40, l: 6838.9, s: 1.8 }
 i: 144 	 01 05 (5)  21 04 (4)  42 51ff (-175)  56 e8ffbf005300 (12582888)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 158 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 159 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 160 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 161 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 162 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 163 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 164 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 165 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 166 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 167 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 168 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 169 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 170 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 171 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 172 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 173 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 174 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 175 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 176 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 177 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 178 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 179 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 180 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 181 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 182 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 183 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 184 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 185 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 186 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 187 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 188 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 189 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 190 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 191 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 192 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 193 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 194 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 195 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 196 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 197 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 198 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 199 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 200 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 201 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 202 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 203 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 204 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 205 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 206 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 207 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 208 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 209 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 210 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 211 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 212 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 213 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 214 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 215 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 216 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 217 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 218 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 219 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 220 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 221 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 222 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 223 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 224 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 225 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 226 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 227 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 228 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 229 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 230 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 231 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 232 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 233 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 234 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 235 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 236 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 237 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 238 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 239 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 240 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 241 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 242 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 243 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 244 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 245 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 246 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 247 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 248 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 249 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 250 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 251 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 252 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 253 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 254 	 00  (0)  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
 i: 255 	 00  	{ @: 1548154642, t: 22.3, h: 44, l: 6838.9, s: 0.05 }
```
