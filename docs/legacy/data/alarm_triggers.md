# Advertising Alarms Triggers

## Critical State

To ensure safe delivery package should never reach critical state.

We implement mechanism that monitors current sensor measurements for critical values (outside of standard range).

In case delivery package was opened or damaged during transfer (critical state) device starts advertising to notify user of such event.

## Alarm Triggers

**Alarm is triggered** if current measured value is **outside of the range <MinCrit, MaxCrit>**

## Min max thresholds - Critical values

To set standard range we set thresholds for minimal and maximal critical values specific data structure (MinMax) is used.

### Threshold Data Structure

| Order 	| Name                  	| Description                                                	| Unit    	| FACTOR 	| Bytes Length 	| Un/Signed 	|
|-------	|-----------------------	|-----------------------------------------------------------	|---------	|-------:	|--------------	|-----------	|
| 0     	| Time                  	| min: snooze time after alarm; max: Expiration Unix timestamp.| Second  	| 1      	| 4            	| unsigned  	|
| 1     	| Temperature           	|                                                             	| Celsius 	| 10     	| 2            	| signed    	|
| 2     	| Relative humidity     	|                                                	            | %       	| 1      	| 2           	| unsigned  	|
| 3     	| Light / Optical Power 	| L2 App generic error.                                     	| nW/cm2  	| 1      	| 4            	| unsigned  	|
| 4     	| Shock                 	| Combined magnitude of all 3 acceleration axes. 	            | G       	| 100    	| 2            	| unsigned  	|
| 5     	| Incline X             	| Avg. acceleration, 90 degrees == 2048            	            | degrees  	| 90/2048 	| 2            	| signed    	|
| 6     	| Incline Y             	| Avg. acceleration, 90 degrees == 2048                         | degrees  	| 90/2048  	| 2            	| signed    	|
| 7     	| Incline Z             	| Avg. acceleration, 90 degrees == 2048                         | degrees  	| 90/2048  	| 2            	| signed    	|
|       	|                       	|                                                	            |         	|        	| 20B in total 	|           	|


*NOTE* - Every numeric value is integer number in Little endian ordering.

*NOTE* - Some values are with FACTOR number. Meaning users real number value has to be multiplied by this factor to keep the decimal places intact. All valueas are sent as integer numbers.

### Combining to MinMax Critical value structure

Concatenating 2 structures we get final structure that is send as payload to L1 during Provisioning. Total bytes length is 40 Bytes. See example :

#### Example

**Users Json**
```json
{
	"min":
	{
		"time":0,
		"temperature":-48.0,
		"humidity":25,
		"optical_power":0.0,
		"shock":0.0,
		"incline_x":-180.0,
		"incline_y":-180.0,
		"incline_z":-180.0
	},
	"max":
	{
		"time":4294967295,
		"temperature":79.5,
		"humidity":95,
		"optical_power":10000.0,
		"shock":25.5,
		"incline_x":180.0,
		"incline_y":180.0,
		"incline_z":180.0
	}
}
```

**MinMax Bytes for L1**

Bytes are ordered Little-endian. The least significant byte (LSB) value is at the lowest address.

Signed values have first bit in MSB == 1

```hex
00 00 00 00   20 fe   19 00   00 00 00 00   00 00   4c ff   4c ff   4c ff 
ff ff ff ff   1b 03   5f 00   10 27 00 00   ff 00   b4 00   b4 00   b4 00
```

####APPEX 1 : targetting C structure
```
 // target structure
	typedef struct
		{
			uint32_t  time;      // unix timestamp   uint 0 uint max
			int16_t   temp;      // < −3276.7, +3276.7 > °C  short min max
			uint16_t   humid;     // < 0, 100 >      --   0 - 255
			uint32_t  lux;       //  < 0, 1  ... see sensors calc
			uint16_t   shock;     // < 0, 25.5 > G ~ number of 0,255
			int16_t   incline[3]; // -180 to 180 degree
		} sensors_data_t;

```

