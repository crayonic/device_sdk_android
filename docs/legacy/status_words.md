# Status Words

Response to APDU command from recepient. Following Status words are supported by L1 devices:

| SW Hex | OK?     | Name                          | Description                                                                      |
|--------|---------|-------------------------------|----------------------------------------------------------------------------------|
| 9000   | Success | OK                            | Standard confirmation of successful APDU command execution.                      |
| 90FF   | Success | OK-wait                       | APDU Command follows as per [2 way apdu protocol](./protocols/two_way_apdu.md)   |
| 7000   | Success | Undefined Success             |                                                                                  |
| FFFF   | Error   | Application Error             | L2 App generic error.                                                            |
| 6C00   | Error   | Wrong length / Aborted        |                                                                                  |
| 6700   | Error   | Wrong key length              |                                                                                  |
| 7100   | Error   | Unexpected Input              | Mismatch of APDU header bytes (eg. Unknown command) or data bytes (eg. CRC fail) |
| 7FFF   | Error   | Undefined Error               |                                                                                  |
| 7F00   | Error   | Encryption Error              |                                                                                  |
| 6400   | Error   | Timeout                       |                                                                                  |
| 6983   | Error   | Authentication Method Blocked |                                                                                  |
| 6982   | Error   | Security Status Not Satisfied |                                                                                  |
| 6401   | Error   | User Aborted                  |                                                                                  |
| 6B00   | Error   | Wrong Parameters              |                                                                                  |
| 6300   | Error   | Warning                       |                                                                                  |
