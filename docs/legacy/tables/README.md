# Info

This folder acts as repository for table files used to generate various tables in documentation. 

## Usage for file of type *.tgn

1. Open [table generator webpage](http://www.tablesgenerator.com/markdown_tables#)
2. Load saved table : ``` "File" > "Load table..." ```
3. Edit 
4. Save table : ``` "File" > "Save table..." ```
5. Push to git ...

