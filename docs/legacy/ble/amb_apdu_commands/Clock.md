# Read and Set Clock

## L1 Implementation

###Read Clock
Get current GMT unix time.

#### Request:
*APDU Read Record*
* P1 = 0x0D
* P2 = 0x00
* Le = 0x04 ( expected is 4 byte word )

#### Response:
* (4 byte word) 32b unsigned integer word for unix time (seconds) in little endian format
* 2B Status word : success ( 0x9000 ) or error

### Set Clock
Set GTM unix time to device.

#### Request:
*APDU Write Record*
* P1 = 0x0D
* P2 = 0x00
* Lc = 0x04 
* (4 byte word) Data = unix time in seconds in little endian format

#### Response:
* 2B Status word : success ( 0x9000 ) or error

---

## L2 Implementation
#### Path :
```Ambrosus-L2-GW/app/src/main/java/com/crayonic/ambrosus/gateway/comm/bt/ble_manager/model```
#### Class :
```CLASS_FIELD```

---

#### Received data format :
DETAILS_OF_DATA_FORMAT_FIELD

---

## Integration
Already integrated in class ```INTEGRATED_IN_CLASS_FILED```. Received data are converted to RESULT_FORMAT.

## Usage
See the steps and example in [Usage of APDU Model](../README.md#usage-of-apdu-model)
