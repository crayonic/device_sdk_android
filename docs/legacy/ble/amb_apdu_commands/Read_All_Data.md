# Read All Data
This command is using L1 implementation <b>Read Buffer Data</b>. APDU command takes the length of data from name of device. Expects response of max 65520 (due to extended APDU 2 byte length limitation). Bytes recevied from L1 device are 16 Bytes long structures (see [AMB_DataStructure](../l2_data_formats/AMB_DataStructure.md) ).

## L1 Implementation

### Read Buffered Data
Reads raw data from the measurement buffer without deleting any (mostly used for debugging
purposes).
#### Request:
P1 = 0x00, P2 = 0x00, Le = [advertised buffer size but maximum 65535 at one time]
#### Response:
Raw measured data from buffer


## L2 Implementation
#### Path :
```Ambrosus-L2-GW/app/src/main/java/com/crayonic/ambrosus/gateway/comm/bt/ble_manager/model```
#### Class :
```ReadData_ApduBleModel```
#### Received data format :
Data are received as byte stream in ```ByteArray```. Each 16 bytes represent data structure ```AMB_DataStructure```.

## Integration
Already integrated in class ```APDUReadDataCommand```. Received data are converted to list of ```AMB_DataStructure``` instances.

#### Custom integration
1. Instantiate ```ReadData_ApduBleModel```
1. Register observers to obtain data or other responses asyncronously
1. Run the model with function : ```runCommand( device : L1Device )```
1. Unregister observers after model lived out its usefulness

## Usage
#### Send APDU command and handle interactions  :
Only basic interactions are handled below, like data and status word. For all other observable items check definition in ApduBleModel
```
// instantiate
val apduBleModel = ReadData_ApduBleModel(activity)

// handle response status word
apduBleModel.apduResponse()?.observe(activity, Observer { apduResponse ->
            apduResponse?.let {
                lastResponseAPDU = it
                if (!apduResponse.isValid()) {
                    // something has failed - we can check for known status words and map the error
                } else {
                    // apdu OK "0x9000"
                }
            }
})

// handle data
apduBleModel.dataReceived()?.observe(activity, Observer { data ->
            data?.let {
                // process the data
                // e.g. parsing them
                val readableString = dataReceived.toListOfAMB_DataStructure().toStringTable()
            }
})

// do something after disconnect
apduBleModel.hasBeenDisconnected()?.observe(activity, Observer { isDisconnected ->
            isDisconnected?.let{
                // was it really disconnected?
            }
})

// run the model for target L1Device
val hasStarted = apduBleModel?.runForDevice(l1BleDevice!!) ?: false
if (!hasStarted ) {
    // Failed to start APDU Model - check Android BLE Stack or L1 Device if powered on
}

// free the observers
removeObserversFromBefore(apduBleModel)


```