# Read Event Process
This process consist of 4 APDU commands on both L1 and L2 and 1 HTTP Post request on L2. 

####Process overview
1. **L2** initiates process by sending [AMB Event Start](#amb-event-start)
1. **L1** executes [AMB Event Start](#amb-event-start) upon receiving
1. **L2** reads expected length by sending [AMB Event Get Length](#amb-event-get-length)
1. **L1** executes [AMB Event Get Length](#amb-event-get-length) upon receiving
1. **L2** reads data by sending [AMB Event Read](#amb-event-read)
1. **L1** executes [AMB Event Read](#amb-event-read) upon receiving
1. **L2** **OPTIONAL:** processes data and does HTTP Post to create event on Ambrosus Api
1. **L2** finishes process by sending [AMB Event Finish](#amb-event-finish)
1. **L1** executes [AMB Event Finish](#amb-event-finish) upon receiving

![Event Start -> Event Read Length -> Event Read Data -> HTTP Post (optional) -> Event Finish ](../apdu_reaad_event_process.jpg  "Process overview")

## L1 Implementation
**NOTE:** Any measuring will be stopped between generation of the event json [AMB Event Start](#amb-event-start) and confirmation that its read in [AMB Event Finish](#amb-event-finish).

Command should be executed as follows below. 

###AMB Event Start
**APDU Write Record**

Prepares and signs Ambrosus event to be delivered over the BTLE.
**NOTE:** This command will stop L1 measurements for 180 seconds or until [AMB Event Finish](#amb-event-finish) command is executed.
####Request:
P1 = 0x01, P2 = 0x00, Lc = 0x0 (no data to write) , Data = (none)
####Response:
Status word 9000 or error if event cannot be generated i.e. no provisioning happened before
this or no data in measured buffer present.

---
###AMB Event Get Length
**APDU Read Record**
Gets length of prepared AMB event to be passed into AMB Event Read command.
Returns error if no event started.
####Request:
P1 = 0x01, P2 = 0x02, Le = 0x04 (expected is 4 byte uint32 word in little endian format)
####Response:
Returns size of event for reading in unsigned 32B integer in little endian.

---

###AMB Event Read
**APDU Read Record**
Read L1 generated event in [L1 Event Json Format](#l1-event-json-format). Before reading event, make sure
the even is first generated and event size is known. Data will be deleted from buffer ONLY upon
successful confirmation from the client. 

**NOTE:** Any measuring will be stopped between [AMB Event Start](#amb-event-start) and confirmation that its read in [AMB Event Finish](#amb-event-finish).
Returns error if no event started.
####Request:
P1 = 0x01, P2 = 0x00, Le = [event size as returned by AMB event get length command]
####Response:
Returns signed JSON event according to Ambrosus spec with binary encoded data to save packet size. In the signed event json an Hexadecimal representation of these binary data are used. More details in [L1 Event Json Format](#l1-event-json-format)

---

###AMB Event Finish
**APDU Write Record**
Confirms data has been read (and submitted to Ambrosus). Data are deleted only if Request data match the eventId created on AMB Event Start, otherwise data are left intact on device. Measuring will be enabled to continue after execution of this command.

####Request:
P1 = 0x01, P2 = 0x01, Lc = 0x20 (32 bytes - eventId of the event json), Data = 32 bytes of (later signed) hash as confirmation that data has been written to blockchain on L2 gateway.

####Response:
Status word OK or error if hash does not match data sent by L1

---

### L1 Event Json Format
Compression is done before sending data from L1. By default on reading buffered data only bytes are received. But in case of event Json, data are represented in hexadecimal format. In this format data are also signed. This procedure prolongs to twice as long just because of hexadecimal formatting. L1 implements simple compression to save data on transmission. Text data from Json are sent in character represented bytes and raw data are sent in raw bytes (conversion needed). 

**NOTE:** Json event generated and signed during [AMB Event Start](#amb-event-start) ( see [Json generated and signed on L1](#json-generated-and-signed-on-l1)) differes from data received after [AMB Event Read](#amb-event-read) ( see [Data received sent from L1](#data-received-sent-from-l1)). 

---


####Json generated and signed on L1
Signed Json event according to [Ambrosus Api Docs](https://ambrosus.docs.apiary.io/#reference/events/assetsassetidevents/create-an-event)
All *content*>*data*>*[0]*>*l1_raw* data are in hexadecimal representation with lowercased letters. For conversion to usable gathered data see [Conversion of raw_l1 to readable data](#conversion-of-raw_l1-to-readable-data)

---


####Data received sent from L1
* Header with lengths of character based parts (prefix,suffix) is prepended
* Data *content*>*data*>*[0]*>*l1_raw* from Signed Json are sent from L1 in binary form.
* Hash of signed message is appended  

Bytes sent from L1 consist from 5 parts
1. Header with two text numbers separated by ':'. Numbers represent length of prefix and suffix bytes. Example :```110:513```
1. Prefix bytes each byte represents character of Json. Length specified in header. Starting with character '{'. 
1. Data in binary format, each 16B represent one record structured as   
1. Suffix bytes each byte represents character of Json. Length suffix specified in header - 66 for hash below. Ending with character '}' 
1. 66B bytes characters in ETH hash format. Used for signature verification on L2. Represents hash of *content*>*idData* from signed event Json.

---

#### Parsing and usage on L2

After receiving data from [AMB Event Read](#amb-event-read) apply:
1. Read length of prefix and suffix from header
2. Read Chars of prefix starting with '{'. Length of prefix.
2. Read ByteArray of data and convert it to lowercased hexadecimal. Length of received data minus prefix and minus suffix length.
2. Read Chars of Json part of suffix starting with '}'. Length of suffix minus 66.
2. Read Chars of ETH Hash used in signing the Json.  Length of 66.

Parse data option A:
* get string representation of Event Json
* get eventId as CRC for deletion of data
```kotlin
val parsedData = AMB_L1_EventData( rawDataFromL1 )

// event
val eventJson : String = parsedData.getEvent()

//crc for data deletion
val crc : ByteArray    = parsedData.getCheckHash()
```
Parse data option B (recommended):
* verify signature
* validate event json
* get Json event
* get data ready for HTTP Post
* get eventId as CRC for deletion of data
* get URL for HTTP Post
```kotlin
val parsedAndVerifiedData = AMB_L1GeneratedPostEventJsonModel( rawDataFromL1 )

// event
val eventJson : JSONObject   = parsedAndVerifiedData.toJson()

// data for http
val eventJsonString : String = parsedAndVerifiedData.getData()

// crc for deletion of data
val crc : ByteArray          = parsedAndVerifiedData.getCRC()
```

#### Conversion of raw_l1 to readable data
The member *content*>*data*>*[0]*>*l1_raw* in Event Json contains hexadecimal format of raw L1 data. 

To retrieve readable L1 gathered data records do : 
1. Taking the signed Json  
1. Convert hexadecimal  *content*>*data*>*[0]*>*l1_raw* to ByteArray
1. Convert bytes to list of usable data models of AMB 16B Structure. More details in [AMB_DataStructure](../l2_data_formats/AMB_DataStructure.md)

---

## L2 Implementation

####Path
```Ambrosus-L2-GW/app/src/main/java/com/crayonic/ambrosus/gateway/comm/bt/ble_manager/controller```
####Class
```ReadEvent_ApduProcess```
####HTTP Post is optional
Flag **isHttpPostRequired** is defined in constructor of this process. If you dont want to post the data during Read Event Process, simply set this flag to false and create event HTTP Post to AMB Api will be skipped.

####Received data format
After the process is ran successfully multiple formats are offered to manage resulting event json data.
* getData() : ByteArray - Returns ByteArray(0) if process failed, otherwise raw received ByteArray data.
* public eventJsonModel : AMB_L1GeneratedPostEventJsonModel? - Returns data model with parsed raw data to event.

####Data deletion from L1
On **L2** Event Process success, data are deleted from L1 in function ```execApduEventFinish(crc: ByteArray)```. If there was any issue/error in process steps only 32B of zeros are sent to L1, leaving data on L1 intact.

##Integration
Already integrated in class ```APDUProcessReadEventCommand``` or in GW mode of L2 class ```L1DevicesFragment```.

####Custom integration
1. Instantiate ```ReadEvent_ApduProcess```
1. Implement ```ApduProcessCallbacks``` 
1. Set target device ```setTargetDevice( l1Device!! )```
1. Run with function : ```run( apduProcessCallbacks )```
