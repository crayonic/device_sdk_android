# Bluetooth Communication

## Overview
#### BT Gatt

To send bytes over Android BLE stack BLE Gatt callbacks are overriden in ApduManager. Specific bluetooth characteristic is used for proprietary service on embeded devices. More in section BT Gatt Charactristics[BT Gatt Charactristics](#bt-gatt-charactristics) .

#### APDU Protocol

APDU protocol is specified in ISO7816. On low level APDU protocol is used for communication with ebeded devices. More in section [APDU R/W Commands](#apdu-rw-commands) APDU R/W Commands.


---
## Implementation
 ![](./apdu_ble_overview.png)

#### APDU Model

Both APDU Manager and APDU DataReceiver are instantiated and called from APDU Model. Model abstracts any APDU Command, like Read Record or Write Record.

#### APDU Manager

Sends bytes over the BLE Gatt. Reacts to Gatt callbacks to current calling APDU BLE Model

#### APDU Data Receiver

Receives bytes until expected length is received. Afterwards are first 2 Bytes considered to be APDU response. Drop everything else. Reacts over callbacks.



## Ambrosus Spec

### BT Gatt Charactristics
---
#### Ambrosus L1 Device (AMBL1)

By default AMB L1 devices operates as BLE Gatt Server. 

Broadcasting name is concatenation of :
1. Device make "AMBL1"
2. <i>if data gathered</i> - ":"
3. <i>if data gathered</i> - number of bytes stored in and ready to be read from AMBL1

---
#### AMBL1 Service

Characteristic UUID:
```hex
9eb70001-8c04-4c98-ae44-2ca32bfa549a
```
Custom BLE service characteristic is used to communicate with AMBL1 embed devices. AMBL1 device receives byse on Write characteristic and replies over Notification characteristic. To ensure two way communication enable notifications on Notification characteristic (Android BLE Gatt).

---
#### AMBL1 Write

Characteristic UUID:
```hex
9eb70002-8c04-4c98-ae44-2ca32bfa549a
```
Send bytes to AMBL1 device.

---
#### AMBL1 Notification

Characteristic UUID:
```hex
9eb70003-8c04-4c98-ae44-2ca32bfa549a
```
Receive bytes from AMBL1 device.

---
#### DFU Service

While in Firmware Update mode AMBL1 broadcasts name <b>"DfuTarg"</b>. During this period device waits for DFU Update as specified in [Nordic Device Firmware Update process](http://infocenter.nordicsemi.com/index.jsp?topic=%2Fcom.nordic.infocenter.sdk5.v15.0.0%2Flib_bootloader_dfu_process.html)

---
### APDU R/W Commands



---
#### Ambrosus L1 APDU Communication Protocol (AMB_APDU)

AMBL1 is programmed to accept only well formatted byte streams in compliance with APDU standard as in ISO7816. Each AMB_APDU have fixed fields identifing type of command and variable fields.

More specifically we have implemented business logic of L1 on top of two APDU commands READ_RECORD and WRITE_RECORD where record number in P1 is the deciding parameter which setting or buffer should be read or written to.

---

#### APDU READ_RECORD
(7 bytes long) APDU contents
 
1. **1B** ```CLA```  0xFF 
1. **1B** ```INS```  0xB2 
1. **1B** ```P1```  Record identifier (0x00 - 0xFF) 
1. **1B** ```P2```  Sub record (0x00 - 0xFF) 
1. **0B** ```Lc``` field  Empty (skip) 
1. **0B** ```Data``` field Empty (skip) 
1. **3B** ```Le``` field  Number of bytes to be read in extended APDU format in order to read a maximum of 65535 bytes i.e. 0x00FFFF. First byte of Lc is always 0x00 ( using Extended APDU ). 

|  1.    |  2.    | 3.    | 4.    | 7.    | 7.    | 7.    |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ```CLA```   | ```INS```   | ```P1```    | ```P2```    | ```Le0```   | ```Le1```   | ```Le2```   |
  
 
 **APDU response**
1. Le Bytes long response (Note: the device currently has no timeout so if you request too many bytes to read this communication may never end until disconect)
1. 2 Bytes **Status word**
  * 0x9000 = OK / success
  * 0xXXXX = other / error 
  * see Status words table for all values

**Example :** 
Read 2 bytes of data form measurement buffer


| ```CLA```   | ```INS```   | ```P1```    | ```P2```    | ```Le0```   | ```Le1```   | ```Le2```   |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | 
| ```FF```    | ```B2```    | ```00```    | ```00```    | ```00```    | ```00```    | ```02```    |

* *Request:*
```
FF B2 00 00 00 00 02
```
* *Response:* 
```
01 FF 90 00
```

---

#### APDU WRITE_RECORD 
(7 + Lc Bytes long) APDU contents
 
1. **1B** ```CLA```  0xFF 
1. **1B** ```INS```  0xB2 
1. **1B** ```P1```  Record identifier (0x00 - 0xFF) 
1. **1B** ```P2```  Sub record (0x00 - 0xFF) 
1. **0B** ```Lc``` Length of the subsequent data field in extended APDU format (3 Bytes) to allow up to 65535 bytes to be written by one WRITE command. First byte of Lc is always 0x00 ( using Extended APDU ). 
1. **NB** ```Data```  Record to be written of Lc Bytes ( specified in Lc field )
1. **3B** ```Le```  Empty (Only 2 Bytes of SW word is expected in return)

| 1.    | 2.    | 3.    | 4.    | 5.    | 5.    | 5.    | 6. | 6. | 6. |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |:---: |:---: |:---: | 
| ```CLA```   | ```INS```   | ```P1```    | ```P2```    | ```Lc0```   | ```Lc1```   | ```Lc2```  | ```Data 0``` | ```Data ...``` | ```Data Lc-1``` |   

  
 
**APDU response**
1. 2 Bytes **Status word** only
* 0x9000 = OK / success
* 0xXXXX = other / error 
* see Status words table for all values

**Example :** 
Set sampling to 2 second interval. Write value of 2 to sampling setting as 4 byte word (notice little endian format that represents unsigned 32b integer word in the data field)

| ```CLA```   | ```INS```   | ```P1```    | ```P2```    | ```Lc0```   | ```Lc1```   | ```Lc2```  | ```Data 0``` | ```Data 1``` | ```Data 2``` | ```Data 3``` |   
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---:  | :---:  | :---:  | :---:  |
|  ```FF```    | ```D2```    | ```0C```    | ```00```    | ```00```    | ```00```    | ```04```    | ```02```     | ```00```     | ```00```     | ```00```     |

   * *Request:*
```
FF D2 0C 00 00 00 04 02 00 00 00
```
   * *Response:* 
```
90 00
```

---
#### AMB_APDU commands
Implemented on L1 and L2. Specific implementation in detail (including data format) can be found in separate documents.

<b>Read Only (Read)</b>: Reads bytes formatted as APDU Read Record
1. [Read All Data](amb_apdu_commands/Read_All_Data.md)
1. Read Sampling rate
1. Read Clock
1. Read Data Low Watermark
1. Read Data High Watermark
1. Read Public Key
1. Read Account Id - Eth Address
1. Read Asset Id
1. Read Event Length (2nd in [Event Process](amb_apdu_commands/Event_Read_Process.md#amb-event-get-length))
1. Read Event Data (3rd in [Event Process](amb_apdu_commands/Event_Read_Process.md#amb-event-read))

<b>Administration write (Set)</b> : Writes bytes formatted as APDU Write Record
1. Set Reset
1. Set Reset to Firmware Update mode
1. Set Sampling rate
1. Set Clock
1. Set Data Low Watermark
1. Set Data High Watermark
1. Set Generate Keys
1. Set Asset Id
1. Set Event Start (1st in [Event Process](amb_apdu_commands/Event_Read_Process.md#amb-event-start))
1. Set Event Finish  (4th in [Event Process](amb_apdu_commands/Event_Read_Process.md#amb-event-finish))

<b>[Event Process](amb_apdu_commands/Event_Read_Process.md) (Process)</b> : Sequence of 4 commands
1. Set Event Start (1st in [Event Process](amb_apdu_commands/Event_Read_Process.md#amb-event-start))
1. Read Event Length (2nd in [Event Process](amb_apdu_commands/Event_Read_Process.md#amb-event-get-length))
1. Read Event Data (3rd in [Event Process](amb_apdu_commands/Event_Read_Process.md#amb-event-read))
1. Set Event Finish  (4th in [Event Process](amb_apdu_commands/Event_Read_Process.md#amb-event-finish))



---
## Integration of APDU Model
Each APDU model serves as one communication with L1. Meaning after each APDU sent to device L1 both L1 and L2 disconnect from BLE. L1 is waiting for disconnect to be initiated from L2. This whole procedure is asyncronous thus android arch LiveObjects and VievModel infrastructure of Android arch is used to make the background observable on frontend without taking care of the UI thread interrupts.
 ![](./observing_model.jpg)

#### APDU Process
For running more than one APDU command in succession use APDU process.
 ![](./apdu_process.jpg)

---
#### UI Command
As an UI integration in L2 App an command (in command center) is used to run one specific APDU model or process. Check out ```APDURecordBaseCommand``` and its children classes for APDU Model integration.

---
#### UI Process
Similar to UI Command a process can be ran from command (in command center) as well. Check out ```APDUProcessBaseCommand``` and its children classes for more than one APDU Model integration.

---
## Usage of APDU Model

#### Read Record APDU command:
1. Override ReadRecordBase_ApduBleModel in custom model.
2. Override method provideReadRecordApduBytes(expectedLength: Int) - used to specify APDU Command bytes to send over APDU Manager
3. Override method provideExpectedLength() - used in APDU Data Receiver to set expected length before BLE transmission
4. Implement custom logic
5. Instantiate
6. Create and register observers fro handling async response of APDU Manager
7. Run with runForDevice(device: BluetoothDevice)
8. Unregister observers as part of cleanup

<b>Example Implementation</b>
```
class ReadAssetId_ApduBleModel(activity: FragmentActivity) : ReadRecordBase_ApduBleModel(activity) {
    override fun postProcessReceivedData(allData: ByteArray): ByteArray {
        return  allData
    }

    override fun provideReadRecordApduBytes(expectedLength: Int): ByteArray? {
        return AMB_CommunicationProtocol.apduReadRecord(
                AMB_CommunicationProtocol.HEX_APDU_HEAD_READ_ASSET_ID, expectedLength)
    }

    override fun provideExpectedLength(): Int {
        return AMB_CommunicationProtocol.AMBL1_ASSET_ID_BYTES_LENGTH //32 bytes
    }
}
```

<b>Example Usage</b>
```
	// instantiate
        val apduBleModel = SetTime_ApduBleModel(this)
        
        // handle most important responses in observers
        // handle response status word
        apduBleModel.apduResponse()?.observe(activity, Observer { apduResponse ->
                    apduResponse?.let {
                        lastResponseAPDU = it
                        if (!apduResponse.isValid()) {
                            // something has failed - we can check for known status words and map the error
                        } else {
                            // apdu OK "0x9000"
                        }
                    }
        })

        // handle data
        apduBleModel.dataReceived()?.observe(activity, Observer { data ->
                    data?.let {
                        // process the data
                        // e.g. parsing them
                        val readableString = dataReceived.toHexNum() // 0x00..00 - ETH hex format 
                    }
        })

        // do something after disconnect
        apduBleModel.hasBeenDisconnected()?.observe(activity, Observer { isDisconnected ->
                    isDisconnected?.let{
                        // was it really disconnected?
                    }
        })
        
        // run the model
        val success = apduBleModel.runForDevice( l1BleDevice!! )
        if (!success){
               // failure "Failed to open communication with device")
        }
        
        // free the observers
        removeObserversFromBefore(apduBleModel)

```
---
#### Write Record APDU command:
1. Override WriteRecordBase_ApduBleModel in custom model with T as type template, e.g. Int.
3. Override method validate(valueToBeWritten: T ) - used to validate input before crateing APDU bytes
2. Override method provideWriteRecordApduBytes(valueToBeWritten: T ) - used to specify APDU Command bytes to send over APDU Manager
4. Implement custom logic
5. Instantiate
6. Create and register observers fro handling async response of APDU Manager
7. Run with runForDevice(device: BluetoothDevice, valueToBeWritten: T )
8. Unregister observers as part of cleanup

<b>Example Implementation</b>
```
class SetTime_ApduBleModel(activity:FragmentActivity) : WriteRecordBase_ApduBleModel<Long>(activity) {
    override val DEFAULT_ARGUMENT_VALUE: Long = 0

    override fun validate(valueToBeWritten: Long): Boolean {
        if(valueToBeWritten >= DEFAULT_ARGUMENT_VALUE){
            return true
        }
        return false
    }

    override fun provideWriteRecordApduBytes(valueToBeWritten: Long): ByteArray? {
        return AMB_CommunicationProtocol.APDUBytesWriteRecordTime(
                HexUtil.long2TimeBytes(valueToBeWritten)
        )
    }
}
```
<b>Example Usage</b>
```
	// instantiate
        val apduBleModel = SetTime_ApduBleModel(this)
        
        // handle most important responses in observers
        // handle response status word
        apduBleModel.apduResponse()?.observe(activity, Observer { apduResponse ->
                    apduResponse?.let {
                        lastResponseAPDU = it
                        if (!apduResponse.isValid()) {
                            // something has failed - we can check for known status words and map the error
                        } else {
                            // apdu OK "0x9000"
                        }
                    }
        })

        // do something after disconnect
        apduBleModel.hasBeenDisconnected()?.observe(activity, Observer { isDisconnected ->
                    isDisconnected?.let{
                        // was it really disconnected?
                    }
        })
        
        // run the model
        val success = apduBleModel.runForDevice( l1BleDevice!!, TimeUtil.currentTime() )
        if (!success){
               // failure "Failed to open communication with device")
        }
        
        // free the observers
        removeObserversFromBefore(apduBleModel)

```

---
#### All observable values of APDU Model :

1. connectionLog ```LiveData<String?> ``` - used for logging purposes

1. device ```LiveData<L1Device?>``` - change of target device

1. apduResponse ```LiveData<APDUResponseExtended?>``` - response APDU status word, received after all data has been received

1. dataOutgoing  ```LiveData<ByteArray?>``` - bytes sent over BLE Stack to L1

1. dataReceived ```LiveData<ByteArray?>``` - bytes received from BLE Stack without APDU status word

1. dataPartReceived ```LiveData<Array<Int>?>``` - An array of integers representing partial communication in progress. Array is ordered like : Current part size, Total sum of previous parts, Expected final size. ```APDUDataReceiver``` contains helper functions to parse this array and calculate percentage also.

1. isRepeatingAfterJumboPayload ```LiveData<Boolean>``` - Flag is true if data are longer than APDU packet (64k-1) called jumbo packet

1. isRepeating ```LiveData<Boolean>``` - Flag is true if this model has ran again without calling runForDevice(..) , e.g. in caseof jumbo packet

1. hasBeenExecuted  ```LiveData<Boolean>``` - Flag is true after model was executed with runForDevice(..) function.

1. hasBeenDisconnected ```LiveData<Boolean>``` - Flag is tru after device disconnects from BLE Stack.

1. isRunning ```LiveData<Boolean>``` - Flag is true after hasBeenExecuted and before last hasBeenDisconnected (there may be more disconnects in case of jumbo packets)

---