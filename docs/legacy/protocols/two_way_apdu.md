## 2-way communication 
To sustain two way communication, we perform messages ping-pong by responding with special Status Word (SW). 

This can be done with responding to any APDU command with one of following SW:
1. OK === 90 00 - executed correctly, B waits for another messages or disconnect
2. OK-WAIT === 90 FF - executed correcty, B will send some message back, e.g. APDU command
3. ANY ERROR SW - incorrect execution or state, leds to disconnect 


##### Example
Lets assume case Alice (A) want to speak with Bob (B). 

![ ](./img/diagram_2wayAPDU.jpg  "2-way APDU communication")

1. A  sends any APDU command to B.
2. B processes the command and responds with OK-WAIT.
3. A received OK-WAIT and waits.
4. B sends APDU command to A.
5. A processes the command and responds with OK-WAIT.
6. B  received OK and waits.
7. A sends disconnect to B and disconnects.
8. B disconnects.

### Message exchange APDUS
Two devices can exchange messages when supporting the Two way apdu communication and the same RX/TX BLE service. This group of APDU commands have P1 = `FX`.
1. APDU Text Message
1. APDU Bytes Message

#### APDU Text Message
* APDU write record command
* Carries payload with ASCII text representation
* Correct response SW is OK - `9000` or OK-WAIT - `90FF`

| CLA | INS | P1 | P2 | LE 00 | LE 01 - 02 | Data |
| --- | --- | --- | --- | --- | --- | --- |
| `FF` | `D2` | `FA` | `00` | `00` | `Data length` | `ASCII text` |

#### APDU Bytes Message
* APDU write record command
* Carries payload with bytes array. Previous interaction should imply usage of such bytes
* Correct response SW is OK - `9000` or OK-WAIT - `90FF`

| CLA | INS | P1 | P2 | LE 00 | LE 01 - 02 | Data |
| --- | --- | --- | --- | --- | --- | --- |
| `FF` | `D2` | `FB` | `00` | `00` | `Data length` | `Bytes array` |