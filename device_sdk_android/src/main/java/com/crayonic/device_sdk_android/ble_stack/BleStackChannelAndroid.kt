package com.crayonic.device_sdk_android.ble_stack

import android.util.Log
import com.crayonic.device_sdk.ble.BleDeviceCharacteristics.BleChars
import com.crayonic.device_sdk.ble.BleDeviceCharacteristics.L1_BleReqCharsSetup
import com.crayonic.device_sdk.ble.BleDeviceCharacteristics.Pen_BleReqCharsSetup
import com.crayonic.device_sdk.ble.BleDeviceRequiredCharSetup
import com.crayonic.device_sdk.ble.BleStackModule
import com.crayonic.device_sdk.ble.FoundBleDevice
import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.util.currentUnixTime
import com.crayonic.device_sdk.util.seconds_20
import com.crayonic.device_sdk.util.seconds_30
import com.crayonic.device_sdk.util.seconds_5
import com.crayonic.device_sdk_android.core.CrayonicDevicesService
import kotlinx.coroutines.Job
import no.nordicsemi.android.support.v18.scanner.ScanCallback
import no.nordicsemi.android.support.v18.scanner.ScanResult
import space.lupu.bleko.connection.BleKoConnector
import space.lupu.bleko.connection.SubCharHolder
import space.lupu.bleko.scanner.BleKoFriendlyResult
import space.lupu.bleko.scanner.BleKoScanner
import space.lupu.bleko.utils.formServiceCharacteristicMapEntry
import space.lupu.koex.hex.toHexSeparated
import java.util.*

class  BleStackChannelAndroid:BleStackModule.BleStackChannel(BleStackModuleAndroid.tags){
    val TAG = "BleStackChannelAndroid"
    val debugMode = false

    val blekoConnector: BleKoConnector by lazy{
        Log.e(TAG, "Initialized by lazy")
        BleKoConnector(
            context = CrayonicDevicesService.coreModule.service,
            parentJob = CrayonicDevicesService.coreModule.service.getNewBleStackJob()
        )
    }
    var connectedFriend: BleKoFriendlyResult? = null

    private fun BleDeviceRequiredCharSetup.toRequiredServicesMap():Map<UUID, List<SubCharHolder>> {
        val resultMap = hashMapOf<UUID, List<SubCharHolder>>()

        requireServices.forEach { serviceHolder ->
            val entry = formServiceCharacteristicMapEntry(
                serviceHolder.service,
                serviceHolder.readChars,
                serviceHolder.writeChars,
                serviceHolder.notifyChars
            )
            if(entry != null)
                resultMap.put(entry.first, entry.second)
        }

        return resultMap
    }

    var blekoScanner: BleKoScanner = BleKoScanner()
    val scanResults = hashMapOf<String,  BleKoFriendlyResult>()
    val foundDevices = hashMapOf<String,  FoundBleDevice>()


    ////////////////////////////////
    // Connect

    override fun connect(
        bleAddress: String,
        onConnected: () -> Unit,
        onError: (reason: String) -> Unit,
        onDisconnected: () -> Unit
    ) {
        val foundDevice = foundDevices[bleAddress]
        val friend = scanResults[bleAddress]
        val friendlyDevice = friend?.bleDevice


        // TODO : check timestamp of the scan result : if older than 30 sec rescan

        when{
            foundDevice == null || friend == null || friendlyDevice == null-> {
                onError("BLE Device $bleAddress not found : rescan then try to connect")
            }
            foundDevice.isL1() || foundDevice.isPen() -> {

                // retrieve the gatt init setup
                val requiredCharSetup:  Map<UUID, List<SubCharHolder>> = when {
                    foundDevice.isL1() -> L1_BleReqCharsSetup.toRequiredServicesMap()
                    foundDevice.isPen() -> Pen_BleReqCharsSetup.toRequiredServicesMap()
                    else -> mapOf<UUID, List<SubCharHolder>>()
                }

                blekoConnector.connectDevice(
//                    bleDevice = friendlyDevice,
                    address = bleAddress,
                    requiredServices = requiredCharSetup,
                    onConnect = {
                        connectedFriend = friend
                        onConnected.invoke()
                    },
                    onDisconnect = {
                        logOnDebug("connect ($bleAddress)-> onDisconnect ")
                        // remove conn device device
                        connectedFriend = null
                    },
                    onError = {
                        logOnDebug("connect ($bleAddress)-> onError : $it")
                        onError.invoke(it)
                    }

                )
            }

        }
    }

    override fun disconnect(bleAddress: String, onDisconnected: () -> Unit) {
        val foundDevice = getFoundDeviceFromConnected(bleAddress)
        when{
            foundDevice == null -> {
                /* no such device is connected */
                onDisconnected.invoke()
            }
            foundDevice.isL1() || foundDevice.isPen() -> {
                blekoConnector.disconnectDevice(foundDevice.address) {
                    logOnDebug("disconnect ($bleAddress)-> onDisconnect : normally")
                    // remove conn device device
                    connectedFriend = null
                    onDisconnected.invoke()
                }
            }

        }
    }


    override fun readCharacteristic(
        bleAddress: String,
        serviceChar: String,
        onFail: (reason: String) -> Unit,
        onResult: (ByteArray) -> Unit
    ) {
        val friend = getConnectedFriend(bleAddress)
        val foundDevice = getFoundDeviceFromConnected(bleAddress)
        logOnDebug("read ($bleAddress)-> with Characteristic: $serviceChar")
        when{
            foundDevice == null || friend == null -> {/* No Op */}
            foundDevice.isL1() || foundDevice.isPen()-> {
                blekoConnector.read(
                    address = foundDevice.address,
                    characteristicReadStr = serviceChar,
                    onError = {
                        logOnDebug("read ($bleAddress)-> onError : $it , with Characteristic: $serviceChar")
                        onFail.invoke(it)
                    }
                    ){ data ->
                    logOnDebug("read ($bleAddress)-> onResult : with Characteristic: $serviceChar, data[${data.size}]: \n${data.toHexSeparated()}")
                    onResult.invoke(data)
                }
            }
        }
    }

    override fun writeCharacteristic(
        bleAddress: String,
        serviceCharSend: String,
        serviceCharReceive: String,
        onFail: (reason: String) -> Unit,
        bytesToSend: ByteArray,
        onResult: (ByteArray) -> Unit
    ) {
        val friend = getConnectedFriend(bleAddress)
        val foundDevice = getFoundDeviceFromConnected(bleAddress)
        logOnDebug("write ($bleAddress)-> with Characteristic: $serviceCharSend")
        when{
            foundDevice == null || friend == null -> {/* No Op */}
            foundDevice.isL1() || foundDevice.isPen() -> {
                blekoConnector.writeWithNotify(
                    address = foundDevice.address,
                    characteristicWriteStr = serviceCharSend,
                    characteristicNotifyStr= serviceCharReceive,
                    onError = {
                        logOnDebug("write ($bleAddress)-> onError : $it , with Write Characteristic: $serviceCharSend , with Notify Characteristic: $serviceCharReceive ")
                        onFail.invoke(it)
                    },
                    sendBytes = bytesToSend
                ){ data ->
                    logOnDebug("write ($bleAddress)-> onResult : with Write Characteristic: $serviceCharSend , with Notify Characteristic: $serviceCharReceive , data[${data.size}]: \n${data.toHexSeparated()}")
                    onResult.invoke(data)
                }
            }

        }
    }

    fun getConnectedFriend(bleAddress: String):BleKoFriendlyResult? = when {
        connectedFriend?.macAddress.equals(bleAddress,true) -> connectedFriend
        else -> null
    }

    fun getFoundDeviceFromConnected(bleAddress: String):FoundBleDevice?  {
        val friend = when {
            connectedFriend?.macAddress.equals(bleAddress,true) -> connectedFriend
            else -> null
        }
        val foundDevice = foundDevices[friend?.macAddress?:""]
        return foundDevice
    }

    ///////////////////////////////
    // Scanning

    val onScanResultRoutes = arrayListOf<(foundBleDevs: List<FoundBleDevice>) -> Unit>()
    var activeScanTag = ""
    val scanTimeout = seconds_30
    var scanCallbackL1: ScanCallback? = null
    var scanCallbackPen: ScanCallback? = null
    var activeScanTimestamp = 0L

    // Time for how long will be the scan result kept in cache in milli seconds
    val keepScanTimeMs = seconds_5

    private fun filterOutOldScanResults(){
        val currentTimeMs = currentUnixTime().times(1_000) // we are taking rounded time in seconds
        val removeRecords = scanResults.filter { entry ->
            val cond = entry.value.timestampMs+keepScanTimeMs <= currentTimeMs
//            if(cond) Log.e(TAG, "Removing scan result for  ${entry.key} ${entry.value}" )
            cond
        }.keys
        removeRecords.forEach {

            scanResults.remove(it)
            foundDevices.remove(it)
        }
    }


    override fun findBleDevices(onFound: (foundBleDevs: List<FoundBleDevice>) -> Unit) {
        filterOutOldScanResults()

        val startedAt = currentUnixTime()
        if (activeScanTimestamp +scanTimeout < startedAt){
            // active scan has already timed out, we have to reset it
            activeScanTag = ""
            scanCallbackL1 = null
            scanCallbackPen = null
            activeScanTimestamp = startedAt
            onScanResultRoutes.clear()
        }

        var scanTag = activeScanTag
        if (activeScanTag.isBlank()){
            // create new
            scanTag = Single.randomId("scan_${scanTimeout.div(1000)}_sec")
        }

        onScanResultRoutes.add(onFound)

        val scanL1= scanCallbackL1?: blekoScanner.startScan(
            stringServiceCharacteristic = BleChars.L1_mainService,
            bleDeviceName = "",
            macAddress = "",
            timeoutMs = scanTimeout,
            onScanResult = {callbackType: Int, result: ScanResult ->
                val timeLeft = currentUnixTime() - startedAt
//                logOnDebug("onScanResult (L1): scan_id: $scanTag / unixTime left : $timeLeft\n address: ${result.device.address}, name: ${result.device.name}, result: $result, callbackType: $callbackType")
            },
            onScanFailed = {
                logOnDebug("onScanFailed (L1): $it")
            },
            onBatchScanResults = {results->
                results.map { result->
                    val timeLeft = currentUnixTime() - startedAt
                    filterOutOldScanResults()

                    //logOnDebug("onBatchScanResults (L1): scan_id: $scanTag / unixTime left : $timeLeft\n address: ${result.device.address}, name: ${result.device.name}, result: $result")
                    val friendlyResult = BleKoFriendlyResult.fromScanResult(result)
                    scanResults.put(friendlyResult.macAddress, friendlyResult)
                    foundDevices.put(friendlyResult.macAddress, friendlyResult.toL1Device())
//                    Log.e(TAG, "onBatchScanResults foundDevices: ${foundDevices.count()} ${foundDevices.values.joinToString()}")
//                    Log.e(TAG, "onBatchScanResults scanResults: ${scanResults.count()} ")
                }
                if(foundDevices.isNotEmpty())
                    routeOnScannerResult(foundDevices.values.toList())
            },
            parentJob = CrayonicDevicesService.coreModule.service.getNewJob(Single.randomId("scanL1-${System.currentTimeMillis()}"))
        )


        val scanPen = scanCallbackPen?: blekoScanner.startScan(
            stringServiceCharacteristic = BleChars.Pen_mainService,
            bleDeviceName = "",
            macAddress = "",
            timeoutMs = scanTimeout,
            onScanResult = {callbackType: Int, result: ScanResult ->
                val timeLeft = currentUnixTime() - startedAt

                logOnDebug("onScanResult (pen): scan_id: $scanTag / unixTime left : $timeLeft\n address: ${result.device.address}, name: ${result.device.name}, result: $result, callbackType: $callbackType")

            },
            onScanFailed = {
                logOnDebug("onScanFailed (pen): $it")
            },
            onBatchScanResults = {results->
                results.map { result->
                    val timeLeft = currentUnixTime() - startedAt
                    filterOutOldScanResults()

                    //logOnDebug("onBatchScanResults (pen): scan_id: $scanTag / unixTime left : $timeLeft\n address: ${result.device.address}, name: ${result.device.name}, result: $result")
                    val friendlyResult = BleKoFriendlyResult.fromScanResult(result)
                    scanResults.put(friendlyResult.macAddress, friendlyResult)
                    foundDevices.put(friendlyResult.macAddress, friendlyResult.toPenDevice())
                }
                if(foundDevices.isNotEmpty())
                    routeOnScannerResult(foundDevices.values.toList())
            },
            parentJob = CrayonicDevicesService.coreModule.service.getNewJob(Single.randomId("scanPen-${System.currentTimeMillis()}"))
        )
        logOnDebug("started scanning for L1 devices : $scanL1")
        logOnDebug("started scanning for Pen devices : $scanPen")
    }

    private fun routeOnScannerResult(listOfFoundDevices: List<FoundBleDevice>) {
//        Log.e(TAG,  "routeOnScannerResult: listOfFoundDevices[${listOfFoundDevices.size}] ${listOfFoundDevices.joinToString()}" )
        onScanResultRoutes.forEach {
            it.invoke(listOfFoundDevices)
        }
    }

    override fun stopAllSearches() {
        blekoScanner.stopAllScans()
        scanCallbackL1 = null
        scanCallbackPen = null
        onScanResultRoutes.clear()
    }

    fun BleKoFriendlyResult.toL1Device()=
        FoundBleDevice(
            macAddress,
            bleName,
            "",
            "",
            BleChars.L1_mainService
        )


    fun BleKoFriendlyResult.toPenDevice()=
        FoundBleDevice(
            macAddress,
            bleName,
            "",
            "",
            BleChars.Pen_mainService
        )


    /////////////////////////////
    //

    fun logOnDebug(message:String){
        if(debugMode)
        Log.e(TAG, message )
    }

}