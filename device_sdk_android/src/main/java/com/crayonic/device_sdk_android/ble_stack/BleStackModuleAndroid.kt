package com.crayonic.device_sdk_android.ble_stack

import com.crayonic.device_sdk.ble.BleStackModule
import com.crayonic.device_sdk.core.Single

class BleStackModuleAndroid :BleStackModule<BleStackChannelAndroid>(
    BleStackChannelAndroid(),
    200,
    tags
){

    companion object{
        val tags = listOf("android","gatt","scanner","connect")
    }
}