package com.crayonic.device_sdk_android.core.view

import android.os.Bundle
import androidx.lifecycle.Observer
import com.crayonic.device_sdk_android.R
import com.crayonic.device_sdk_android.core.CrayonicDevicesService
import com.crayonic.device_sdk_android.utils.nfc.NFCConsumingActivity
import kotlinx.android.synthetic.main.activity_service_info.*
import kotlinx.android.synthetic.main.content_service_info.*


class ServiceInfoActivity : NFCConsumingActivity() {

    lateinit var communicationViewModel: CommunicationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_info)
        setSupportActionBar(toolbar)

        toolbar.title = "${StatusInfo.modelName} service"

        service_name.text = "${StatusInfo.modelName} service v${StatusInfo.serviceVersion}"
        application_info_text.text = StatusInfo.formattedGWAllInfo()

        button_stop.setOnClickListener {
            sendStopToService()
        }

        button_open_app.setOnClickListener {
//            IntentsUtil.backToPackageListActivity(this)
            //
        }

        communicationViewModel = CrayonicDevicesService.coreModule.service.getCurrentStatusViewModel()

        communicationViewModel.getLiveInfo().observe(this, Observer { info ->
            communication_status_title.text = info.currentTask
            communication_status_text.text =
                createStatusText(
                    communicationViewModel
                )
        })
        communication_status_title.text = "Idle"
        communication_status_text.text =
            createStatusText(
                communicationViewModel
            )

    }


    private fun sendStopToService() {
        CrayonicDevicesService
    }


    companion object {
        fun createStatusText(viewModel: CommunicationViewModel): String {
            val info = viewModel.getLiveInfo().value
            val device = viewModel.getLiveConnectedDevice().value
            val deviceAddress = viewModel.getLiveHwAddress().value ?: ""
            return "Service info : $info" +
                    "Last seen device : $deviceAddress $device "

        }

    }
}
