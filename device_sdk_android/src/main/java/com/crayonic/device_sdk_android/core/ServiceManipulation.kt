package com.crayonic.device_sdk_android.core

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.crayonic.device_sdk_android.BuildConfig
import com.crayonic.device_sdk_android.kotojava.CrayonicServiceListener
import kotlinx.coroutines.*
import kotlin.coroutines.resume

abstract class ServiceManipulation {

    private val TAG = "ServiceManipulation"

    internal val debugMode = BuildConfig.DEBUG

    internal var serviceConnector: CrayonicDevicesService.Connector =
        CrayonicDevicesService.Connector()

    @SuppressLint("StaticFieldLeak")
    internal lateinit var coreModule: CoreModule

    ///////////////////////////////////////////////////////

    internal fun suicideService() {
//            if (::coreModule.isInitialized) {
        if (::coreModule.isInitialized) {
            coreModule.stop()
            serviceLog("Companion.  :.( service suicide :.( ")
        }
    }

    internal fun stopServiceWhenWorkIsDone() {
//            if (::coreModule.isInitialized) {
        if (::coreModule.isInitialized) {
            coreModule.unbind()
            coreModule.selfStopWhenReady()
            serviceLog("Companion.stopServiceWhenWorkIsDone")
        }
    }
    /////////////////////////////////////////////

    private fun runService(initialContext: Context) {
        serviceConnector = CrayonicDevicesService.Connector()

        serviceConnector.startInForeground(initialContext)
    }

    private fun startAndBind(
        bindToContext: Context,
        onServiceError: (Exception) -> Unit,
        onServiceAttached: (service: CrayonicDevicesService) -> Unit
    ) {
        var service: CrayonicDevicesService? = null
        val job = Job()
        CoroutineScope(Dispatchers.IO + job).launch {
            withTimeout(4000L) {
                serviceLog("Starting foreground service (BBFGS), with timeout 4 sec")
                service = suspendCancellableCoroutine<CrayonicDevicesService> { cont ->
                    serviceConnector.startForegroundAndBind(bindToContext) {
                        serviceLog("Service bound : $service")


                        if (!::coreModule.isInitialized) {
                            coreModule = CoreModule(bindToContext, it, serviceConnector)
                            coreModule.service.registeredOnServiceError = onServiceError
                            coreModule.service.initContextDependent() // secondary initialization point

                        } else if (!coreModule.isServiceStillRunning()) {
                            coreModule = CoreModule(bindToContext, it, serviceConnector)
                            coreModule.service.registeredOnServiceError = onServiceError
                        }



                        serviceLog("Service in core : ${coreModule.service}")

//                                it.startForeground(it.ONGOING_NOTIFICATION_ID, it.getNotification())
                        cont.resume(it)
                    }
                }
            }
            CoroutineScope(Dispatchers.Main).launch {
                if (service == null) {
                    onServiceError(CoreModule.ServiceBindingError())
                } else {
                    onServiceAttached(service!!)
                }
            }
            job.cancel()
        }

    }

    ///////////////////////////////////////////////////////

    fun getInstance(bindToContext: Context, serviceListener: CrayonicServiceListener) {
        getInstance(
            bindToContext = bindToContext,
            onServiceError = { serviceListener.onServiceError(it) },
            onServiceAttached = { serviceListener.onServiceAttached(it) }

        )
    }

    fun getInstance(
        bindToContext: Context,
        onServiceError: (Exception) -> Unit,
        onServiceAttached: (service: CrayonicDevicesService) -> Unit
    ) {
        if (::coreModule.isInitialized && coreModule.isServiceStillRunning()) {
            serviceLog("-------- BIND: onAttached ")
            coreModule.bind()
            onServiceAttached(coreModule.service)
        } else {
            serviceLog("-------- NEW ")
            runService(bindToContext)
            startAndBind(
                bindToContext = bindToContext,
                onServiceError = { exception: Exception ->
                    onServiceError.invoke(exception)
                    serviceLog("Service Error : ${exception.localizedMessage} ${exception.stackTrace}")
                    if (::coreModule.isInitialized)
                        coreModule.service.killSwitch()
                },
                onServiceAttached = onServiceAttached
            )
        }
    }

    //////////////////////////////////
    internal fun serviceLog(message: String) {
        if (debugMode)
            Log.d(TAG, message)
    }
}
