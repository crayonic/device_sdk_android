package com.crayonic.device_sdk_android.core

import android.content.Context
import android.util.Log
import com.crayonic.device_sdk_android.BuildConfig
import kotlinx.coroutines.*

internal data class  CoreModule (
    val initialContext:Context,
    val service: CrayonicDevicesService,
    val serviceConnector: CrayonicDevicesService.Connector
){
    val debugMode = BuildConfig.DEBUG
        
    var isServiceWorking = false
        set(value) {
            if(hasFreedom)
                field = value
        }

    var isServiceBound = false
        set(value) {
            if(hasFreedom)
                field = value
        }

    var hasFreedom = true
        set(value) {
            if(hasFreedom)
                field = value
        }



    val suicideJob = Job()

    var hitmanMission = Job(suicideJob).apply { cancel() }

    init {
        isServiceBound = true
        dummyLoad()
    }

    fun dummyLoad(){
        val worker = Job(suicideJob)
        CoroutineScope(Dispatchers.IO + worker).launch {
            serviceStory(TAG, "worker - hard work started")
            work()
            delay(15000)
            serviceStory(TAG, "worker - hard work is done")
            done()
            selfStopWhenReady()
        }
    }

    fun bind(): Boolean {
        isServiceBound = true
        return hasFreedom
    }

    fun unbind(): Boolean {
        isServiceBound = false
        return hasFreedom
    }

    fun work(): Boolean {
        isServiceWorking = true
        return hasFreedom
    }

    fun done(): Boolean {
        isServiceWorking = false
        return hasFreedom
    }

    fun stop(): Boolean {
        if(hasFreedom) {
            putOnLockdown()
            suicideJob.cancel()
            service.serviceJob.cancel()
            return service.stop(serviceConnector.connection)
        }

        return false
    }

    private fun putOnLockdown() {
        serviceStory(TAG, "putOnLockdown : before : hasFreedom :$hasFreedom, isServiceBound: $isServiceBound, isServiceWorking: $isServiceWorking, ")
        hasFreedom = false
        isServiceBound = false
        isServiceWorking = false
        serviceStory(TAG, "putOnLockdown : after: hasFreedom :$hasFreedom, isServiceBound: $isServiceBound, isServiceWorking: $isServiceWorking, ")
    }

    fun isServiceStillRunning(): Boolean {
        return hasFreedom
    }


    fun selfStopWhenReady() {
        if(hitmanMission.isCancelled || hitmanMission.isCompleted) {
            hitmanMission = Job(suicideJob)
            CoroutineScope(Dispatchers.IO + hitmanMission).launch {
                serviceStory(TAG, "hitmanMission - started")
                while (isServiceWorking) {
                    serviceStory(
                        TAG,
                        "hitmanMission - Hitman is waiting for target to stop working"
                    )
                    delay(5000)
                }
                serviceStory(
                    TAG,
                    "hitmanMission - Hitman checks if he has somebody to live for? ${if (isServiceBound) "True Service is bound" else ""}"
                )
                if (!isServiceBound && hasFreedom) {
                    serviceStory(
                        TAG,
                        "hitmanMission - Hitman commits suicide. There was no Service bound to live for "
                    )
                    stop()
                }
                hitmanMission.cancel()
            } // end of coroutine scope
        }
    }

    private fun serviceStory(tag: String, message: String) {
        if(debugMode)
            Log.e(tag,message)
    }

    data class Setup(
        val initialContext: Context,
        val service : CrayonicDevicesService,
        val serviceConnector : CrayonicDevicesService.Connector
    )

    companion object
//        : SingletonInitializer<CoreModule, Setup>(::CoreModule)
    {
        val TAG = "CoreModule"
        const val SERVICE_BIND_TIMEOUT = 5000L
    }

    class ServiceBindingError : Exception {
        companion object {
            const val msg = "Service could not be bound before TIMEOUT of $SERVICE_BIND_TIMEOUT sec"
        }

        constructor(tag: String) : super("[${tag}] : $msg")
        constructor() : super(msg)
    }

    class InitContextError : Exception {
        companion object {
            const val msg = "Missing application context / initialized incorrectly"
        }

        constructor(tag: String) : super("[${tag}] : $msg")
        constructor() : super(msg)
    }

}