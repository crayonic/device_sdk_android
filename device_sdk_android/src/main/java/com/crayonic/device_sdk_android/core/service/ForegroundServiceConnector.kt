package com.crayonic.device_sdk_android.core.service

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.Build
import android.os.IBinder
import android.util.Log

abstract class ForegroundServiceConnector<LocalService: BaseBindingForegroundService>: BaseServiceConnector<LocalService>() {
    private val TAG = "FgServiceConnector"

    override var onBoundAction : (LocalService)-> Unit = {}

    override val connection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, iBinder: IBinder) {
            val binder = iBinder as BaseBindingService.LocalBinder
            this@ForegroundServiceConnector.service = binder.getService() as LocalService

            this@ForegroundServiceConnector.service?.let{
                onBoundAction.invoke(it)
            }
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            this@ForegroundServiceConnector.service = null
        }
    }

    fun isInForeground():Boolean = service?.isInForeground?:false

    fun startInForeground(context: Context): Boolean {
//        if(service != null)
//            service.lifecycle.currentState
        try {
            newIntent(context).also { intent ->
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(intent)
                }else{
                    context.startService(intent)
                }
            }
        }catch (e:Exception){
            Log.e(TAG, "ERR : startForegroundAndBind", e)
            return false
        }
        return true
    }

    fun startForegroundAndBind(context: Context, onBound : (LocalService) -> Unit ): Boolean {
        if(startInForeground(context))
            return bind(context, onBound)
        return false
    }

    // call only if we want to kill the service
    fun stopInForeground(): Boolean{
        if(service == null) {
            Log.e(TAG, "BBFSC ERR : stopInForeground - No service to stop, service is null.")
            return false
        }
        return service!!.stop(this.connection)

    }
    // call only if we want to kill the service
    fun stopInForeground(boundContext: Context): Boolean{
        if(service == null) {
            Log.e(TAG, "BBFSC ERR : stopInForeground - No service to stop, service is null.")
            return false
        }
        return service!!.stop(boundContext, this.connection)

    }
}

//inline fun <reified T : Any> newIntent(context: Context): Intent = Intent(context, T::class.java)