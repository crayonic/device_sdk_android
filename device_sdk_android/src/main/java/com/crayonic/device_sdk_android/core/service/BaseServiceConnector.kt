package com.crayonic.device_sdk_android.core.service

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log

abstract class BaseServiceConnector<LocalService: BaseBindingService>{
    private val TAG = "BaseServiceConnector"
    var service: LocalService? = null
    var isBound: Boolean = false

    open var onBoundAction : (LocalService)-> Unit = {}

    open val connection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as BaseBindingService.LocalBinder
            this@BaseServiceConnector.service = binder.getService() as LocalService

            this@BaseServiceConnector.service?.let{
                onBoundAction.invoke(it)
                isBound = true
            }
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            this@BaseServiceConnector.service = null
            isBound = false
        }
    }

    abstract fun newIntent(context: Context):Intent

//  EXAMPLE : Your impl. should look like..
//    override fun newIntent(context: Context):Intent{
//        return Intent(context, BaseBindingService::class.java)
//    }

    // bind to activity in onStart
    fun bind(context: Context): Boolean{
        try {
            newIntent(context).also { intent ->
                context.bindService(intent, connection, Context.BIND_AUTO_CREATE)
            }
        }catch (e:Exception){
            Log.e(TAG, "ERR : bind", e)
            return false
        }
        return true
    }

    // bind to activity in onStart
    fun bind(context: Context, onBound : (LocalService) -> Unit ): Boolean {
        onBoundAction = onBound
        return bind(context)
    }

    // unbind from activity in onStop
    fun unbind(context: Context): Boolean{
        try {
            context.unbindService(connection)
            isBound = false
        }catch (e:Exception){
            Log.e(TAG, "ERR : unbind", e)
            return false
        }
        return true
    }

    fun stop(context: Context): Boolean {
        Log.i(TAG, "Service stopped");
        try {
            context.stopService(newIntent(context))
        }catch (e:Exception){
            Log.e(TAG, "ERR : stop", e)
            return false
        }
        return true
    }

}