package com.crayonic.device_sdk_android.core.service

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Binder
import android.os.IBinder
import android.util.Log
import androidx.lifecycle.LifecycleService

open class BaseBindingService: LifecycleService() {
    // Binder given to clients
    internal val binder = LocalBinder()
    var isBound: Boolean = false

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        // Return this instance of LocalService so clients can call public methods
        fun getService(): BaseBindingService = this@BaseBindingService
    }

    @SuppressLint("MissingSuperCall")
    override fun onBind(intent: Intent): IBinder {
        Log.i("BBS", "onBind : intent:${intent.toString()}")
        isBound = true
//        return super.onBind(intent)
        return binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.i("BBS", "onUnbind : intent:${intent.toString()}")
        isBound = false
        return super.onUnbind(intent)
    }

    fun stop(connection: ServiceConnection):Boolean{
        return stop(this, connection)
    }

    // correct way to stop the service even if its bound
    open fun stop(context: Context?, connection: ServiceConnection):Boolean{
        Log.i("BBS", "stop()")
        // unbind
        try {
            if (isBound){
                if(context == null){
                    Log.e("BBS", "stop() - failed to unbindService(connection) : bound context not provided")
                }else{
                    context.unbindService(connection)
                    isBound = false
                }
            }
        } catch (e: Exception) {
            Log.e("BBS", "stop() - failed to unbindService(connection) : conection:${connection}", e)
        }

        // stop all work if necessary
        onBeforeServiceStopped()

        // stop
        try {
            stopSelf()
        } catch (e: Exception) {
            Log.e("BBS", "stop() - failed to stopSelf service", e)
            return false
        }
        return true
    }

    /**
     * Override to stop all the wrok that should end before service is stopped by system
     */
    open fun onBeforeServiceStopped(){}
}