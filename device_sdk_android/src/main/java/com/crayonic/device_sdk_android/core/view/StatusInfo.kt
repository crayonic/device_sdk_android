package com.crayonic.device_sdk_android.core.view

import com.crayonic.device_sdk_android.R
import com.crayonic.device_sdk_android.core.CrayonicDevicesService

object StatusInfo{
    val modelName: String = wrapGetResourceString(R.string.model_name) // generated on gradle build
    val modelVersion: String = wrapGetResourceString(R.string.version_string) // generated on gradle build
    val currentFwRevision_l1 = wrapGetResourceString(R.string.fw_revision_string_l1) // generated on gradle build
    val currentFwRevision_l1m = wrapGetResourceString(R.string.fw_revision_string_l1m) // generated on gradle build
    val serviceVersion = wrapGetResourceString(R.string.service_version) // generated on gradle build


//    val features: String = wrapGetResourceString(R.string.version_features) // generated on gradle build
//    val git_commit: String =  wrapGetResourceString(R.string.git_commit_hash) // generated on gradle build
//    val deviceUUID: String = BaseApplication.uuid


    fun getUpdateFileName():String{
        return getUpdateFileName("default", false)
    }

    private fun wrapGetResourceString(resId : Int): String {
        return CrayonicDevicesService.coreModule.service.applicationContext.resources.getString(resId)
    }

    fun getAppVersionString():String = "$modelName $modelVersion"

    fun getUpdateFileName(modelRevision:String, isAppOnlyPackage:Boolean):String{
//        val defaultFallback_bootloader = wrapGetResourceString(R.string.fw_A_user_file_bl_sd_app__l1)
//        val defaultFallback_app_only = wrapGetResourceString(R.string.fw_C_user_file_app_only__l1)
//        if(isAppOnlyPackage){
//            // app only
//            return when(modelRevision.toUpperCase()){
//                "L1" -> wrapGetResourceString(R.string.fw_C_user_file_app_only__l1)
//                "L1M" -> wrapGetResourceString(R.string.fw_D_user_file_app_only__l1m)
//                else -> defaultFallback_app_only
//            }
//        }else{
//            // app sd bl
//            return when(modelRevision.toUpperCase()){
//                "L1" -> wrapGetResourceString(R.string.fw_A_user_file_bl_sd_app__l1)
//                "L1M" -> wrapGetResourceString(R.string.fw_B_user_file_bl_sd_app__l1m)
//                else -> defaultFallback_bootloader
//            }
//        }
        return ""
    }

//    private fun wrapRawFileFromString(gradleResource : Int): Int {
//        return getRawFileFromString(CrayonicDevicesService.getInstance(), wrapGetResourceString(gradleResource))
//    }

    fun getUpdateRawResource(modelRevision:String, isAppOnlyPackage:Boolean):Int{
//        val defaultFallback_bootloader =  wrapRawFileFromString(R.string.fw_A_raw_resource_app_bl_l1)
//        val defaultFallback_app_only =  wrapRawFileFromString(R.string.fw_C_raw_resource_app_only_l1)
//        if(isAppOnlyPackage){
//            return when(modelRevision.toUpperCase()){
//                "L1" -> wrapRawFileFromString(R.string.fw_C_raw_resource_app_only_l1)
//                "L1M" -> wrapRawFileFromString(R.string.fw_D_raw_resource_app_only_l1m)
//                else -> defaultFallback_app_only
//            }
//        }else{
//            return when(modelRevision.toUpperCase()){
//                "L1" -> wrapRawFileFromString(R.string.fw_A_raw_resource_app_bl_l1)
//                "L1M" -> wrapRawFileFromString(R.string.fw_B_raw_resource_app_bl_l1m)
//                else -> defaultFallback_bootloader
//            }
//        }
        return 0
    }


//    fun batteryLevel() :String{
//        val bm = BaseApplication.instance.getSystemService(BATTERY_SERVICE) as BatteryManager
//        val batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
//        return " "+batLevel+"%"
//    }

//    fun batteryLevelNumber() :Int{
//        val bm = BaseApplication.instance.getSystemService(BATTERY_SERVICE) as BatteryManager
//        val batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
//        return batLevel
//    }
//
//    fun humanReadableUptime(): String {
//        return TimeUtil.humanReadableUptime()
//    }
//
//    fun currentFirmware():String{
//        return GWPreferences.getFirmwareVersion(BaseApplication.instance)
//    }


    fun formattedGWAllInfo(): String{
        return  "Status :\n"+
//                "Phone Battery : ${batteryLevel()}"+"\n"+
//                "Phone ID : ${deviceUUID}"+"\n"+
                "Phone App : ${modelName} ${modelVersion} " +
//                "${git_commit}"
                "\n"+
                "Latest L1 Firmware : ${currentFwRevision_l1} " +

//                        "${currentFirmware()}"+
                "\n"
//        +
//                "User data created ${TimeUtil.humanReadableDateMillis(GWPreferences.getFirstRunTimestamp())}"
    }

}
class StatusInfoUtil{
    companion object{
        fun getUpdateFileName(modelRevision:String, isAppOnlyPackage:Boolean) = StatusInfo.getUpdateFileName(modelRevision, isAppOnlyPackage)
        fun getUpdateRawResource(modelRevision:String, isAppOnlyPackage:Boolean):Int = StatusInfo.getUpdateRawResource(modelRevision = modelRevision, isAppOnlyPackage = isAppOnlyPackage)
    }
}