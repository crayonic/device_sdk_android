package com.crayonic.device_sdk_android.core

import android.Manifest
import android.app.PendingIntent
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.content.PermissionChecker
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import com.crayonic.device_sdk.ble.BleDevicesModule
import com.crayonic.device_sdk.core.Controller
import com.crayonic.device_sdk.device.DeviceFilter
import com.crayonic.device_sdk.http.l1_json_api.L1JsonApi
import com.crayonic.device_sdk.device.CrayonicDevice
import com.crayonic.device_sdk.util.periodicCheck
import com.crayonic.device_sdk_android.ble_stack.BleStackModuleAndroid
import com.crayonic.device_sdk_android.core.service.BaseBindingForegroundService
import com.crayonic.device_sdk_android.core.service.ForegroundServiceConnector
import com.crayonic.device_sdk_android.core.view.CommunicationViewModel
import com.crayonic.device_sdk_android.core.view.ServiceInfoActivity
import com.crayonic.device_sdk_android.kotojava.CrayonicServiceListener
import com.crayonic.device_sdk_android.utils.TimeUtil
import kotlinx.coroutines.Job
import space.lupu.koex.lambda.emptyLambda2inUout

class CrayonicDevicesService : BaseBindingForegroundService() {
    override val ONGOING_NOTIFICATION_ID = 7531
    override val ONGOING_NOTIFICATION_CHANNEL_ID = "crayonic_service_device_sdk"
    override val ONGOING_NOTIFICATION_CHANNEL_NAME = "Crayonic Device SDK"

    internal val serviceJob = Job()

    init {
        Log.e(TAG, "init $this")
    }

    lateinit var registeredOnServiceError: (Exception) -> Unit

    internal fun initContextDependent() {
        val blstk = BleStackModuleAndroid()
        controller.konnector.attachModule(
            blstk
        )
        blstk.channel.blekoConnector.initialize()
        runPeriodicWorkChecks()

        checkForPermissions()
        checkForBleSupport()
    }

    private fun checkForPermissions() {
        try {
            val permissions = arrayListOf(
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN,
//                Manifest.permission.ACCESS_COARSE_LOCATION, // this is a runtime permission
//                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.INTERNET
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                permissions.add(Manifest.permission.FOREGROUND_SERVICE)
            }
            permissions.forEach { perm ->
                if (PermissionChecker.checkSelfPermission(this, perm) != PERMISSION_GRANTED) {
                    throw SecurityException("Permission is missing : $perm")
                }
            }
        } catch (e: Exception) {
            registeredOnServiceError.invoke(e)
        }
    }

    private fun checkForBleSupport() {
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val bleAdapter = bluetoothManager.adapter
        if (bleAdapter == null) {
            registeredOnServiceError.invoke(Exception("BLE is not supported on this device"))
            killSwitch()
        } else if (!bleAdapter.isEnabled) {
            registeredOnServiceError.invoke(
                Exception(
                    "BLE is disabled: enable it in user settings or aproove requested permissions"
                )
            )
            killSwitch()
        }
        try {
//            listDevices(DeviceFilter("somethingThatWillNeverMatchASoul"), emptyLambda2inUout())
        } catch (e: Exception) {
                registeredOnServiceError.invoke(
                    Exception(
                        "Service Error : listDevices failed ",
                        e
                    )
                )
        }
    }

    //---------------------------------------------

    private val controller: Controller by lazy { initController() }
    private fun initController() =
        Controller.getInstance(
            Controller.Setup(
                listOf(
                    L1JsonApi("https://gateway-test.ambrosus.com"),
                    BleDevicesModule()
                )
            )
        )


    //---------------------------------------------

    private fun runPeriodicWorkChecks() {
        periodicCheck(
            serviceJob,
            isStillWorkingOnSomething(), // if true repeat , kill service otherwise
            5000,
            {
                suicideService() // kill service
            }
        )
    }

    private fun isStillWorkingOnSomething(): () -> Boolean = {
        var checkResult_work = true
        var checkResult_bound = true
        try {
            val removed = controller.cleanupJobsChache()
            if (debugMode) {
                serviceLog("Removed $removed jobs from cache")
                controller.jobsCache.forEach { entry ->
                    val job = entry.value
                    serviceLog("jobCache : ${entry.key} - $job isCompleted ${job.isCompleted} isActive ${job.isActive} isCancelled ${job.isCancelled} childrens [${job.children.count()}]")
                }

                if (controller.jobsCache.count() > 1000) {
                    Log.e(
                        TAG,
                        "TOO MANY JOBS OPENED: Killing service, try using the service more carefully. Think about users resources."
                    )
                    killSwitch()
                }
            }

            val bleStackJob = controller.jobsCache.get(bleStackJobTag)
            val activeJobs = controller.job_parent.children.filter { job: Job -> job.isActive && !job.equals(bleStackJob) }.toList()
            checkResult_work = activeJobs.isNotEmpty()
            if(checkResult_work) coreModule.work() else coreModule.done()

            checkResult_bound = coreModule.isServiceBound
        } catch (upae: UninitializedPropertyAccessException) {
            Log.e(TAG, "Service initialization failed : it has to be killed", upae)
            checkResult_work = false
            checkResult_bound = false
        } catch (e: Exception) {
            Log.e(
                TAG,
                "Consumed exception in service periodic work check. Accessing value before it is initialized?",
                e
            )
        }
        val result = checkResult_work || checkResult_bound
        serviceLog("periodic work check : ${if (result) "REPEAT" else "KILL SERVICE"} : Is still working ? $checkResult_work, Is still being used by other context ? $checkResult_bound ")
        result
    }
    //----------------------------------------------

    val startedAt = TimeUtil.currentTimeMilliseconds()
    fun getUptimeMs(): Long {
        return (TimeUtil.currentTimeMilliseconds() - startedAt)
    }

    fun getUptimeHumanReadable(): String {
        return TimeUtil.readableTimeYDHMS(getUptimeMs().div(1000))
    }

    //---------------------------------------------

    private var communicationViewModel: CommunicationViewModel? = null
    fun getCurrentStatusViewModel() = getCommViewModel()
    protected fun getCommViewModel(): CommunicationViewModel {
        if (communicationViewModel == null) {
            communicationViewModel = CommunicationViewModel()
            clearCommViewModel()
        }
        return communicationViewModel!!
    }

    protected fun clearCommViewModel() {
        Log.i(TAG, "Clearing communication data to defaults")
        getCommViewModel().reset()
    }

    //---------------------------------------------
    fun listDevices(
        filter: DeviceFilter,
        onDevicesFound: (originFilter: DeviceFilter, List<CrayonicDevice>) -> Unit
    ) = internal_listDevices(filter, onDevicesFound = onDevicesFound)

    fun listDevices(
        filter: DeviceFilter,
        serviceListener: CrayonicServiceListener
    ) = internal_listDevices(filter, serviceListener = serviceListener)

    private fun internal_listDevices(
        filter: DeviceFilter,
        serviceListener: CrayonicServiceListener? = null,
        onDevicesFound: (originFilter: DeviceFilter, List<CrayonicDevice>) -> Unit = { _, _ -> }
    ) {
        try {
            // Do the stuff with permissions safely
            if (serviceListener == null) {
//                Log.e(TAG, "controller.listDevices - no serviceListener")
                controller.listDevices(filter, onDevicesFound = onDevicesFound)
            } else {
//                Log.e(TAG, "controller.listDevices - with listener")
                controller.listDevices(filter) { originFilter, list ->
                    serviceListener.onDevicesFound(originFilter, list)
                }
            }
        } catch (e: Exception) {
            registeredOnServiceError.invoke(e)
        }
    }

    fun clearDevicesCache(){
        controller.clearDevicesCache()
    }

    fun stopListingFor(filter: DeviceFilter){
        controller.stopListingFor(filter)
    }

    fun stopAllDeviceListings(){
        controller.stopAllDeviceListings()
    }

    //---------------------------------------------
    override fun newPendingIntent(): PendingIntent {
//        return Intent(this, callingActivity).let { notificationIntent ->
        return Intent(this, ServiceInfoActivity::class.java).let { notificationIntent ->
            PendingIntent.getActivity(this, 0, notificationIntent, 0)
        }
    }


    //---------------------------------------------

    fun unbind() = stopServiceWhenWorkIsDone()

    fun killSwitch() = suicideService()

    override fun onBeforeServiceStopped() {
        super.onBeforeServiceStopped()
        endServiceLifecycle()
    }

    private fun endServiceLifecycle() {
        controller.stopEverything() // calls cancel on all the coroutine jobs inside library
        serviceJob.cancel() // stop the work in this service  if there is still any
    }

    fun getNewJob(tag:String = "Service.getNewJob(undefined)"): Job = controller.getNewJob(tag)
    fun getNewBleStackJob(): Job = controller.getNewJob(bleStackJobTag)

    //---------------------------------------------

    class Connector : ForegroundServiceConnector<CrayonicDevicesService>() {
        override fun newIntent(context: Context): Intent {
            return Intent(context, CrayonicDevicesService::class.java)
        }
    }

    companion object : ServiceManipulation() {
        val TAG = "CrayonicDevicesService"
        val bleStackJobTag = "blekoConnectorJob"
    }
}