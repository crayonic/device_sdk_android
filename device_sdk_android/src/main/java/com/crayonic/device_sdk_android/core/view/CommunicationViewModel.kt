package com.crayonic.device_sdk_android.core.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.crayonic.device_sdk.ble.FoundBleDevice

class CommunicationViewModel: ViewModel() {

    private val info: MutableLiveData<CommunicationInfoLive> = MutableLiveData()
    fun getLiveInfo(): LiveData<CommunicationInfoLive> = info
    fun postInfo(postValue:CommunicationInfoLive) = info.postValue(postValue)

//    private val searchParams: MutableLiveData<DeviceScanningController.SearchParams> = MutableLiveData()
//    fun getLiveSearchParams(): LiveData<DeviceScanningController.SearchParams> = searchParams
//    fun postSearchParams(postValue: DeviceScanningController.SearchParams) = searchParams.postValue(postValue)

    private val hwAddress: MutableLiveData<String> = MutableLiveData()
    fun getLiveHwAddress(): LiveData<String> = hwAddress
    fun postHwAddress(postValue:String) = hwAddress.postValue(postValue)

//    private val btDevInfo: MutableLiveData<BtDevInfo> = MutableLiveData()
//    fun getLiveBtDevInfo(): LiveData<BtDevInfo> = btDevInfo
//    fun postBtDevInfo(postValue:BtDevInfo) = btDevInfo.postValue(postValue)

    private val connectedDevice: MutableLiveData<FoundBleDevice> = MutableLiveData()
    fun getLiveConnectedDevice(): LiveData<FoundBleDevice> = connectedDevice
    fun postConnectedDevice(postValue:FoundBleDevice) = connectedDevice.postValue(postValue)

    private val error: MutableLiveData<Exception> = MutableLiveData()
    fun getLiveError(): LiveData<Exception> = error
    fun postError(postValue:Exception) = error.postValue(postValue)


    fun reset() {
        info.value = CommunicationInfoLive()
//        searchParams.value = DeviceScanningController.SearchParams.invalidSearchParams()
        hwAddress.value = ""
//        btDevInfo.value = BtDevInfo()
        connectedDevice.value = FoundBleDevice()
        error.value = Ready()
//        postInfo(CommunicationInfoLive()) // reset
//        postSearchParams(DeviceScanningController.SearchParams.invalidSearchParams()) // reset
//        postHwAddress("") // reset
//        postBtDevInfo(BtDevInfo()) // reset
//        postConnectedDevice(L1Device("","")) // reset
//        postError(Ready()) // reset
    }

    init {
        reset()
    }

    fun getErrorValueOrDefault(default:Exception):Exception{
        return getLiveError().value?:default
    }

    fun getErrorOrNoError(): Exception {
        return getErrorValueOrDefault(NoError())
    }

    fun isAllWorkDone():Boolean = getErrorOrNoError() is AllWorkDone
    fun isReadyToStarANew():Boolean {
        val error = getErrorOrNoError()
        return error is Ready || error is AllWorkDone || error is DeviceDisconnected || error is NoDeviceFound
    }
    fun isDisconnected():Boolean {
        val error = getErrorOrNoError()
        return error is DeviceDisconnected
    }

    class Ready():Exception("Ready for new communication")
    class NoError():Exception("No error detected")
    class Timeout(timeoutReason:String):Exception("Timeout : $timeoutReason")
    class AllWorkDone(postMortemMessage:String):Exception("All work done - finished with message: $postMortemMessage")
    class NoDeviceFound(searchParams:String):Exception("Scanner found no device for search params : $searchParams")
    class DeviceDisconnected(deviceDescription:String):Exception("Device disconnected (this may standard behavior) : $deviceDescription")
    class ConnectionError(errorMessage:String):Exception("Connection Error :\n$errorMessage")

}