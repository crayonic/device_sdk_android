package com.crayonic.device_sdk_android.core.view

import com.crayonic.device_sdk.util.getAsBooleanOrNull
import com.crayonic.device_sdk.util.getAsIntOrNull
import com.crayonic.device_sdk.util.getAsStringOrNull
import com.crayonic.device_sdk.util.toShortJsonString
import com.google.gson.JsonObject
import com.google.gson.JsonParser

data class CommunicationInfoLive(
    var currentTask:String,
    var currentTaskDetail:String,
    var currentTaskPercentage:Int,
    var overallPercentage:Int,
    var isError:Boolean,
    var timedOutReason:String
){
    constructor(currentTask:String, currentTaskDetail:String, currentTaskPercentage:Int, overallPercentage:Int):this(
        currentTask, currentTaskDetail, currentTaskPercentage, overallPercentage, default_is_error_state, default_no_timeout
    )

    constructor():this(default_currentTask, default_currentTaskDetail, default_currentTask_percentage, default_overall_percentage, default_is_error_state, default_no_timeout)

    override fun toString():String{
        return toJson().toShortJsonString()
    }

    fun isTimedOut():Boolean{
        return !timedOutReason.equals(NO_TIMEOUT_VALUE)
    }

    fun toJson(): JsonObject {
        val json = JsonObject().apply {
            addProperty(json_key_currentTask, currentTask)
            addProperty(json_key_currentTaskDetail, currentTaskDetail)
            addProperty(json_key_currentTask_percentage, currentTaskPercentage)
            addProperty(json_key_overall_percentage, overallPercentage)
            addProperty(json_key_is_error, isError)
            addProperty(json_key_timed_out_reason, timedOutReason)
        }
        return json
    }

    companion object {
        fun fromJson(json: JsonObject): CommunicationInfoLive {
            return safeProvisioningInfo(
                json.getAsStringOrNull(json_key_currentTask),
                json.getAsStringOrNull(json_key_currentTaskDetail),
                json.getAsIntOrNull(json_key_currentTask_percentage),
                json.getAsIntOrNull(json_key_overall_percentage),
                json.getAsBooleanOrNull(json_key_is_error)?:default_is_error_state,
                json.getAsStringOrNull(json_key_timed_out_reason)?: default_no_timeout
            )
        }

        fun fromJsonStr(text: String): CommunicationInfoLive {
            val json = JsonParser().parse(text)as JsonObject
            return fromJson(json)
        }

        fun safeProvisioningInfo(
            currentTask:String?,
            currentTaskDetail:String?,
            currentTaskPercentage:Int?,
            overallPercentage:Int?, isError: Boolean, timedOutReason:String?): CommunicationInfoLive {
            return CommunicationInfoLive(
                currentTask ?:  default_currentTask,
                currentTaskDetail ?: default_currentTaskDetail,
                currentTaskPercentage ?: default_currentTask_percentage,
                overallPercentage ?: default_overall_percentage,
                isError,
                timedOutReason ?: default_no_timeout
            )
        }
        fun safeProvisioningInfo(
            oldInfo: CommunicationInfoLive?,
            currentTask:String?,
            currentTaskDetail:String?,
            currentTaskPercentage:Int?,
            overallPercentage:Int?, isError: Boolean, timedOutReason:String?): CommunicationInfoLive {
            return CommunicationInfoLive(
                currentTask ?: oldInfo?.currentTask ?: default_currentTask,
                currentTaskDetail ?: oldInfo?.currentTaskDetail ?: default_currentTaskDetail,
                currentTaskPercentage ?: oldInfo?.currentTaskPercentage ?: default_currentTask_percentage,
                overallPercentage ?: oldInfo?.overallPercentage ?: default_overall_percentage,
                isError,
                timedOutReason ?: oldInfo?.timedOutReason ?: default_no_timeout
            )
        }

        val default_currentTask = "Idle"
        val default_currentTaskDetail = "N/A"
        val default_currentTask_percentage = 0
        val default_overall_percentage = 0
        val default_is_error_state = false
        val NO_TIMEOUT_VALUE = ""
        val default_no_timeout = NO_TIMEOUT_VALUE

        val json_key_currentTask = "currentTask"
        val json_key_currentTaskDetail = "currentTaskDetail"
        val json_key_currentTask_percentage = "currentTask_percentage"
        val json_key_overall_percentage = "overall_percentage"
        val json_key_is_error = "is_error"

        val json_key_timed_out_reason = "timed_out_reason"
    }
}