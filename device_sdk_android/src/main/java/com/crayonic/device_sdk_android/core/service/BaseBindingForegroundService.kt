package com.crayonic.device_sdk_android.core.service

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.crayonic.device_sdk_android.BuildConfig

abstract class BaseBindingForegroundService: BaseBindingService(){

//    private val mDispatcher = ServiceLifecycleDispatcher(this)

//    open val ONGOING_NOTIFICATION_ID = 257
//    open val ONGOING_NOTIFICATION_CHANNEL_ID = "bbfgs_channel"
//    open val ONGOING_NOTIFICATION_CHANNEL_NAME = "BaseBindingForegroundServiceChannel"
    abstract val ONGOING_NOTIFICATION_ID :Int
    abstract val ONGOING_NOTIFICATION_CHANNEL_ID:String
    abstract val ONGOING_NOTIFICATION_CHANNEL_NAME:String
    var isInForeground: Boolean = false



    fun getNotificationChannelId(): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannelForOreo(ONGOING_NOTIFICATION_CHANNEL_ID, ONGOING_NOTIFICATION_CHANNEL_NAME)
        } else {
            // If earlier version channel ID is not used
            // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
            ""
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    open fun createNotificationChannelForOreo(channelId: String, channelName: String):String{
        val chan = NotificationChannel(channelId,
            channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = Color.GREEN
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    open fun getNotification(): Notification {
        return NotificationCompat.Builder(this, getNotificationChannelId())
            .setContentTitle("BBFGS")
            .setContentText("BBFGS is running in fg/bg")
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setContentIntent(newPendingIntent())
            .setTicker("Base Binding Foreground Service is running in background")// for visually impaired
            .build()

    }

    abstract protected fun newPendingIntent():PendingIntent
//    EXAMPLE :
//    override fun newPendingIntent():Intent{
//        return Intent(this, MainActivity::class.java).let { notificationIntent ->
//            PendingIntent.getActivity(this, 0, notificationIntent, 0)
//        }
//
//    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        logD("BBFGS", "onStartCommand : intent:${intent.toString()}, notification : $ONGOING_NOTIFICATION_ID ")
        startForeground(ONGOING_NOTIFICATION_ID, getNotification())
        return super.onStartCommand(intent, flags, startId)
//        return START_NOT_STICKY
    }

    override fun onStart(intent: Intent?, startId: Int) {
        logD("BBFGS", "onStart : intent:${intent.toString()}")
        isInForeground = true
//        mDispatcher.onServicePreSuperOnStart()
        super.onStart(intent, startId)
    }

    override fun onLowMemory() {
        logD("BBFGS", "onLowMemory")
        super.onLowMemory()
    }

    override fun onCreate() {
        logD("BBFGS", "onCreate")
//        mDispatcher.onServicePreSuperOnCreate()
        super.onCreate()
    }

    override fun onDestroy() {
        logD("BBFGS", "onDestroy")
        isInForeground = false
//        mDispatcher.onServicePreSuperOnDestroy()
        super.onDestroy()
    }

    @SuppressLint("MissingSuperCall")
    override fun onBind(intent: Intent): IBinder {
        logD("BBFGS", "onBind : intent:${intent.toString()}")
        isBound = true
//        return super.onBind(intent)
        return binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        logD("BBFGS", "onUnbind : intent:${intent.toString()}")
        isBound = false
        return super.onUnbind(intent)
    }

    override fun onRebind(intent: Intent?) {
        logD("BBFGS", "onRebind : intent:${intent.toString()}")
        return super.onRebind(intent)
    }

    // correct way to stop the service even if its bound
    override fun stop(context: Context?, connection: ServiceConnection):Boolean{
        logD("BBFGS", "stop()")
        // unbind
        try {
            if (isBound){
                if(context == null){
                    logE("BBFGS", "stop() - failed to unbindService(connection) : bound context not provided")
                }else{
                    context.unbindService(connection)
                    isBound = false
                }
            }
        } catch (e: Exception) {
            logE("BBFGS", "stop() - failed to unbindService(connection) : conection:${connection}", e)
        }

        // stop all work if necessary
        try {
            onBeforeServiceStopped()
        }catch (e:Exception){
            logE("BBFGS", "Failed to continue in service stop : onBeforeServiceStopped - thrown an exception", e)
            throw e
        }

        // stopForeground
        try {
            if (isInForeground){
                stopForeground(true)
            }
        } catch (e: Exception) {
            logE("BBFGS", "stop() - failed to stopForeground", e)
        }

        // stop
        try {
            stopSelf()
        } catch (e: Exception) {
            logE("BBFGS", "stop() - failed to stopSelf service", e)
            return false
        }
        return true
    }

    private fun logD(tag: String, msg: String) {
        if(BuildConfig.DEBUG)
            Log.d(tag, msg)
    }

    private fun logE(tag: String, msg: String, e: Exception? =null) {
        if(BuildConfig.DEBUG)
            if(e == null ) Log.e(tag, msg)
            else Log.e(tag, msg, e)
    }
}