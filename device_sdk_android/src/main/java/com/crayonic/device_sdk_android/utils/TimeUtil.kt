package com.crayonic.device_sdk_android.utils

import android.annotation.TargetApi
import android.content.Context
import android.os.*
import android.text.format.DateFormat
import android.util.Log
import com.crayonic.device_sdk.core.Single
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Semaphore
import java.util.concurrent.TimeoutException


object TimeUtil{
    val TAG = "TimeUtil"

    fun HUMAN_READABLE_SIMPLE_FORMAT(): SimpleDateFormat {
        return SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    }

    fun DATABASE_READABLE_SIMPLE_FORMAT(): SimpleDateFormat {
        return SimpleDateFormat("yyyy-MM-dd_HH-mm-ss", Locale.getDefault())
    }
    fun DATABASE_READABLE_SIMPLE_FORMAT_MS(): SimpleDateFormat {
        return SimpleDateFormat("yyyy-MM-dd_HH-mm-ss-SSS", Locale.getDefault())
    }

    /**
     * Current System unixTime in seconds
     */
    fun currentTimeSeconds():Long{
        return System.currentTimeMillis()/1000
    }

    /**
     * Current System unixTime in milliseconds
     */
    fun currentTimeMilliseconds():Long{
        return System.currentTimeMillis()
    }

    /**
     * Current System unixTime in milliseconds
     */
    fun currentDate():Date{
        return Date(currentTimeMilliseconds())
    }

    /**
     * Human Readable unixTime in format "yyyy-MM-dd HH:mm:ss"
     */
    fun humanReadableCurrentDate(): String {
        return humanReadableDateMillis(currentTimeMilliseconds())
    }

    fun currentDateFormatted(format : SimpleDateFormat):String {
        return format.format(currentDate())
    }

    /**
     * Human Readable unixTime in format "yyyy-MM-dd HH:mm:ss"
     */
    fun humanReadableTime(timeInSeconds : Long): String {
        return humanReadableDateMillis(timeInSeconds * 1000)
    }

    fun getModifiedDate(locale: Locale, modified: Long): String {
        var dateFormat: SimpleDateFormat? = null

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            dateFormat = SimpleDateFormat(getDateFormat(locale))
        } else {
            dateFormat = SimpleDateFormat("MMM/dd/yyyy hh:mm:ss aa")
        }

        return dateFormat.format(Date(modified))
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    fun getDateFormat(locale: Locale): String {
        return DateFormat.getBestDateTimePattern(locale, "MM/dd/yyyy hh:mm:ss aa")
    }

    fun getModifiedDate(modified: Long): String {
        return getModifiedDate(Locale.getDefault(), modified)
    }
    /**
     * Human Readable unixTime in format "yyyy-MM-dd HH:mm:ss"
     */
    fun humanReadableLocaleTime(timeInSeconds : Long): String {
        return getModifiedDate(timeInSeconds*1000)
    }

    fun humanReadableDateMillis(timeMilliseconds : Long): String {
        val date = Date(timeMilliseconds)
        val readable = "Date: ${HUMAN_READABLE_SIMPLE_FORMAT().format(date)}"
        return readable
    }
    fun humanReadableDate(date:Date = Date(currentTimeMilliseconds())): String =
        "Date: ${HUMAN_READABLE_SIMPLE_FORMAT().format(date)}"

    fun databaseReadableDateMillis(timeMilliseconds : Long): String =
        DATABASE_READABLE_SIMPLE_FORMAT_MS().format(Date(timeMilliseconds))

    fun databaseReadableDateSeconds(timeSeconds : Long): String {
        val date = Date(timeSeconds * 1000)
        val readable = DATABASE_READABLE_SIMPLE_FORMAT().format(date)
//        Log.d(TAG, "Database readable unixTime : "+readable)
        return readable
    }

    fun databaseTimestamp(): String {
        return databaseReadableDateMillis(currentTimeMilliseconds())
    }


    fun isValidDatabaseFormat(dateString: String):Boolean{
        val dbFormat = DATABASE_READABLE_SIMPLE_FORMAT()
        val re = Regex("^\\d{4}-\\d{2}-\\d{2}_\\d{2}-\\d{2}-\\d{2}$")
        if(re.matches(dateString)){
            try {
                dbFormat.parse(dateString)
                return true
            }catch (e:Exception){
                return false
            }
        }
        return false
    }

    /**
     * Parse formated string to Date class
     * format : yyyy-MM-dd_HH-mm-ss
     * @return Date of epoch === Date(0) if the pattern is not formatted correctly
     * @param dateString
     */
    fun parseDatabaseFormatDate(dateString:String):Date{
        val dbFormat = DATABASE_READABLE_SIMPLE_FORMAT()
        val re = Regex("^\\d{4}-\\d{2}-\\d{2}_\\d{2}-\\d{2}-\\d{2}$")
        if(re.matches(dateString)){
            try {
                val dateFromString = dbFormat.parse(dateString)
                return dateFromString
            }catch (e:Exception){
                Log.d(TAG, "Could not parse Date from string : "+dateString)
            }
        }
        return Date(0)
    }
    /**
     * Parse formatted string to Date class
     * format : yyyy-MM-dd HH:mm:ss
     * @return Date of epoch === Date(0) if the pattern is not formatted correctly
     * @param dateString
     */
    fun parseHumanReadableFormatDate(dateString:String):Date{
        val dbFormat = HUMAN_READABLE_SIMPLE_FORMAT()
        val re = Regex("^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}$")
        if(re.matches(dateString)){
            try {
                val dateFromString = dbFormat.parse(dateString)
                return dateFromString
            }catch (e:Exception){
                Log.d(TAG, "Could not parse Date from string : "+dateString)
            }
        }
        return Date(0)
    }

    /**
     * Parse formated string to millis from EPOCH
     * format : yyyy-MM-dd_HH-mm-ss
     * @param dateString
     */
    fun parseDatabaseFormatMillis(dateString:String):Long{
        return parseDatabaseFormatDate(dateString).time
    }

    /**
     * Parse formated string to millis from EPOCH
     * format : yyyy-MM-dd_HH-mm-ss
     * @param dateString
     */
    fun parseHumanReadableFormatMillis(dateString:String):Long{
        return parseHumanReadableFormatDate(dateString).time
    }

    /**
     * Parse formated string to seconds from EPOCH
     * format : yyyy-MM-dd_HH-mm-ss
     * @param dateString
     */
    fun parseDatabaseFormatSeconds(dateString:String):Long{
        return parseDatabaseFormatMillis(dateString) /1000
    }

    /**
     * Parse formated string to seconds from EPOCH
     * format : yyyy-MM-dd_HH-mm-ss
     * @param dateString
     */
    fun parseHumanReadableFormatSeconds(dateString:String):Long{
        return parseHumanReadableFormatMillis(dateString) /1000
    }
    /**
     * Current Date unixTime
     * Returns the number of milliseconds since January 1, 1970, 00:00:00 GMT represented by this Date object.
     */
    fun currentTime(): Long {
        // current date
        return Date().time / 1000
    }

//    /**
//     * Human Readable uptime from last
//     */
//    fun humanReadableUptime(): String {
//        return FormattingUtil.humanReadableMillisTime(GWPreferences.getUptimeMillis())
//    }

    /**
     * Throw an TimeoutException on timeout
     * You should catch and distinguish the timeout exceptions by tag.
     * To separate the tag from message an '|' character is used as separator.
     * @return current millis of timeout start
     */
    fun startThrowingTimeout(timeout: Long, message: String, tag:String): Long {
        LambdaTimeout(timeout, 1000, onTimeout = { throw TimeoutException(tag+"|"+message) }, onTick = {  }).start()
        return currentTimeMilliseconds()
    }

    /**
     * Start lambda function on timeout
     * @return current millis of timeout start
     */
    fun startWithDelay(timeout: Long, onTimeout: (LambdaTimeout) -> Unit): Long {
        LambdaTimeout(timeout, 1000, onTimeout, onTick = {  }).start()
        return currentTimeMilliseconds()
    }

    /**
     * Start lambda function on timeout
     * @return current millis of timeout start
     */
    fun startWithDelay(context: Context, timeout: Long, onTimeout: (LambdaTimeoutOnHandler) -> Unit): Long {
        LambdaTimeoutOnHandler(uniqueTag1000("startWithDelay"), timeout, onTimeout, context )
        return currentTimeMilliseconds()
    }

    /**
     * Run Unit on timeout pass
     * Check the tag of TaggedLambdaTimeout instance returned in onTimeout if the timeout is still applicable
     * @return current millis of timeout start
     */
    fun startTimeout( tag:String, timeout: Long, onTimeout: (TaggedLambdaTimeout) -> Unit): Long {
        TaggedLambdaTimeout(tag, timeout, 1000, onTimeout = onTimeout, onTick = {  }).start()
        return currentTimeMilliseconds()
    }

    /**
     * Create unique ID that distinguishes same tag objects from the one at unixTime timeout started.
     * This function is used closesely with timeouts thus it is placed in this file.
     * @param tag - prefix of the unique id
     */
    fun uniqueTag1000(tag:String):String{
        return Single.randomId(tag,slidingLength = 3)
    }

    val year = 365 * 24 * 60 * 60L
    val day = 24 * 60 * 60L
    val hour = 60 * 60L
    val minute = 60L

    fun readableTimeYDHMS(timeInSeconds: Long): String {
        val timeAsDouble = timeInSeconds.toDouble()

        val years = timeAsDouble.div(year).toInt()
        val days = timeAsDouble.rem(year).div(day).toInt()
        val hours = timeAsDouble.rem(day).div(hour).toInt()
        val minutes = timeAsDouble.rem(hour).div(minute).toInt()
        val seconds = timeAsDouble.rem(minute).toInt()

        if (timeInSeconds >= year) {
//            return String.format("%02d:%02d:%02d", secs / 3600, (secs % 3600) / 60, secs % 60)
            if (days > 0)
                return "${years}y ${days}d"
            else
                return "${years}y"
        } else if (timeInSeconds >= day) {
            if (hours > 0)
                return "${days}d ${hours}h"
            else
                return "${days}d"
        } else if (timeInSeconds >= hour) {
            if (seconds > 0)
                return "${hours}h ${minutes}m ${seconds}s"
            else if (minutes > 0)
                return "${hours}h ${minutes}m"
            else
                return "${hours}h"
        } else if (timeInSeconds >= minute) {
            if (seconds > 0)
                return "${minutes}m ${seconds}s"
            else
                return "${minutes}m"
        } else {
            return "${seconds}s"
        }
    }
}

class LambdaTimeout(val timeoutInMs:Long, val tickInMs:Long, val onTimeout:(LambdaTimeout) -> Unit, val onTick:() -> Unit):CountDownTimer(timeoutInMs, tickInMs){
    override fun onFinish() {
        onTimeout(this)
    }

    override fun onTick(tickInMs: Long) {
        onTick()
    }
}

class LambdaTimeoutOnHandler(val tag:String, val timeoutInMs:Long,
                    val onTimeout:(LambdaTimeoutOnHandler) -> Unit,
                    val context: Context
){
    val handler = CustomHandler(tag)
    init {
        handler.postDelayed( { onTimeout(this) }, timeoutInMs )
    }

}

class TaggedLambdaTimeout(val tag:String,
                          val timeoutInMs:Long,
                          val tickInMs:Long,
                          val onTimeout:(TaggedLambdaTimeout) -> Unit,
                          val onTick:() -> Unit):CountDownTimer(timeoutInMs, tickInMs){
    override fun onFinish() {
        onTimeout(this)
    }

    override fun onTick(p0: Long) {
        onTick()
    }

}

class CustomHandler protected constructor(handlerName: String, handlerPriority: Int) : Handler(startHandlerThread(handlerName, handlerPriority)) {

    constructor(name: String) : this(name, Process.THREAD_PRIORITY_BACKGROUND) {}

    fun quit() {
        looper.quit()
    }

    companion object {
        private fun startHandlerThread(name: String, priority: Int): Looper {
            val semaphore = Semaphore(0)
            val handlerThread = object : HandlerThread(name, priority) {
                protected override fun onLooperPrepared() {
                    semaphore.release()
                }
            }
            handlerThread.start()
            semaphore.acquireUninterruptibly()
            return handlerThread.getLooper()
        }

    }

}