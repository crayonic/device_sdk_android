package com.crayonic.device_sdk_android.utils

import android.util.Base64

fun ByteArray.base64Encode(): String {
    return Base64.encodeToString(this, Base64.NO_WRAP.or(Base64.URL_SAFE).or(Base64.NO_PADDING));
}

fun ByteArray.base64Encode(typeFlags: Int): String {
    return Base64.encodeToString(this, typeFlags);
}

fun String.base64Decode(): ByteArray? {
    return Base64.decode(this, Base64.NO_WRAP.or(Base64.URL_SAFE).or(Base64.NO_PADDING))
}

fun String.base64Decode(typeFlags: Int): ByteArray? {
    return Base64.decode(this, typeFlags)
}
