package com.crayonic.device_sdk_android.utils

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import androidx.fragment.app.FragmentActivity

fun String.copyToClipboard(activity: FragmentActivity){
    val clipboard = activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clip = ClipData.newPlainText("clip", this)
    clipboard.setPrimaryClip(clip)
}