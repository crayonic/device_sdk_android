package com.crayonic.device_sdk_android.utils.nfc

import android.content.Intent
import android.nfc.NfcAdapter
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

abstract class NFCConsumingActivity() : AppCompatActivity(){

    private var nfcAdapter: NfcAdapter? = null
    private var isNfcEnabled = false
    private var isBetweenOnResumeAndOnPause = false

    override fun onResume() {
        Log.d("NFCDebug", "onResume: ")
        isBetweenOnResumeAndOnPause = true
        Log.e("NFCDebug", "isBetweenOnResumeAndOnPause = true")
        setupNfc()
        super.onResume()
    }

    override fun onPause() {
        Log.d("NFCDebug", "onPause: ")
        Log.e("NFCDebug", "un hookup")
        NFCUtil.unHookupNfcOnForegroundAfterTagReceived(this, nfcAdapter, isBetweenOnResumeAndOnPause)
        isBetweenOnResumeAndOnPause = false
        Log.e("NFCDebug", "isBetweenOnResumeAndOnPause = false")

        super.onPause()
    }

    private fun setupNfc() {
        Log.d("NFCDebug", "setupNfc")
        isNfcEnabled = NFCUtil.checkNFCisEnabled(this)
        Log.e("NFCDebug", if (isNfcEnabled) {"nfc enabled"} else {"nfc disabled"})
        if (isNfcEnabled) {
            Log.e("NFCDebug", "hookup")
            nfcAdapter = NFCUtil.hookupNfcOnForegroundForL1TextAddress(this, isBetweenOnResumeAndOnPause)
        }
    }


    override fun onNewIntent(intent: Intent?) {
        Log.e("NFCDebug", "onNewIntent\n\n${intent?.let { NFCUtil.debugIntent(it) }}")
        super.onNewIntent(intent)
        if (intent != null) {
            setIntent(intent)
            handleNfcIntent(intent)
        }
    }

    private fun handleNfcIntent(intent: Intent) {
//        Log.d("NFCDebug", "handleIntent")
        // do not listen to nfc for a while
        if (isNfcEnabled) {

            NFCUtil.unHookupNfcOnForegroundAfterTagReceived(this, nfcAdapter, isBetweenOnResumeAndOnPause)

            onNfcIntentFound(intent)

            setupNfc()

        }
    }

    open fun onNfcIntentFound(intent: Intent){}
}