package com.crayonic.device_sdk_android.utils

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentActivity
import java.io.File

/**
 * Created by lukassos on 19.3.2018.
 */
class IntentsUtil {
    companion object {
//        fun runPrintJob(activity: AppCompatActivity, title: String, image: Bitmap) {
//            FileUtil.doPhotoPrint(activity, image, title)
//        }

        //        fun runShareImage (activity: AppCompatActivity, title: String, image: Bitmap){
//
//            val path = MediaStore.Images.Media.insertImage(BaseApplication.instance.getContentResolver(), image, title, null)
//            val uri = Uri.parse(path)
//
//            val intent = Intent(Intent.ACTION_SEND)
//            intent.type = "image/jpeg"
//            intent.putExtra(Intent.EXTRA_STREAM, uri)
//            startActivity(activity.applicationContext, Intent.createChooser(intent, "Share Image"), null)
//            activity.startActivity(intent)
//        }
        fun runSharePlainText(activity: Activity, body: String, title: String?, subject: String?) {
            val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            title?.let {
                sharingIntent.putExtra(android.content.Intent.EXTRA_TITLE, title)
            }
            subject?.let {
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject)
            }
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body)

            activity.startActivity(Intent.createChooser(sharingIntent, "Share File Using"))
        }

        fun runShareFile(activity: Activity, filename: String, file: File): Boolean {
            try {
                val fileUri = FileProvider.getUriForFile(
                    activity,
                    ".fileprovider",
                    file
                )

                val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
                sharingIntent.apply {
                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                        addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION.or(Intent.FLAG_GRANT_WRITE_URI_PERMISSION))
                        clipData = ClipData.newRawUri("", fileUri)
                    }

                    // any file
                    type = "*/*"
                    // file content
                    putExtra(android.content.Intent.EXTRA_STREAM, fileUri)
                    // email subject
                    putExtra(android.content.Intent.EXTRA_SUBJECT, "Shared file " + filename)
                    // email body
                    putExtra(
                        android.content.Intent.EXTRA_TEXT,
                        "Hello\nSending you file from Crayonic SDK.\nSee attached " + filename
                    )

                }
                activity.startActivity(
                    Intent.createChooser(sharingIntent, "Share File Using")
                )
                return true
            } catch (ile: IllegalArgumentException) {
                Log.e(
                    "File Sharing",
                    "Selected file cannot be shared: " +
                            filename
                )
            } catch (e: Exception) {
                Log.d("File Sharing", "sharing failed: " + e.message)
            }
            return false
        }


        fun runSettingsSystemNFC(activity: Activity) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                val intent = Intent(Settings.ACTION_NFC_SETTINGS)
                activity.startActivity(intent)
            } else {
                val intent = Intent(Settings.ACTION_WIRELESS_SETTINGS)
                activity.startActivity(intent)
            }
        }

        fun sharableAlertWithText(
            title: String,
            text: String,
            activity: FragmentActivity,
            onFinish: () -> Unit
        ) {
            AlertDialog.Builder(activity).setTitle(title)
                .setMessage(text)
                .setPositiveButton("OK") { dialog, which ->
                    onFinish()

                }.setNeutralButton("Copy") { dialog, which ->
                    val clipboard =
                        activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    val clip = ClipData.newPlainText(title, text)
                    clipboard.setPrimaryClip(clip)
                    Toast.makeText(activity, "Text copied to clipboard ", Toast.LENGTH_SHORT).show()

                }.setNegativeButton("Share") { dialog, which ->
//                    FileUtil.saveAndShareLogFile(text, "sharableAlert", activity)
                }.create().show()
        }


        fun shutDown(delayMillis: Long) {
            TimeUtil.startWithDelay(delayMillis) {
                shutDownNow()
            }
        }

        fun shutDownNow() {
//            CrayonicDevicesService.getInstance().closeAllActivities() // only if tracking activities
            System.exit(0)
        }


        fun openWebBrowser(activity: Activity, baseUrl: String) {
            val webpage = Uri.parse(baseUrl)
            val openURLintent = Intent(android.content.Intent.ACTION_VIEW, webpage)
            activity.startActivity(openURLintent)
        }
    }
}

