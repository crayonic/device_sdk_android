package com.crayonic.device_sdk_android.utils.nfc

import android.annotation.SuppressLint
import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.IntentFilter.MalformedMimeTypeException
import android.net.Uri
import android.nfc.*
import android.nfc.NfcAdapter.*
import android.nfc.tech.IsoDep
import android.nfc.tech.Ndef
import android.nfc.tech.NdefFormatable
import android.nfc.tech.NfcA
import android.util.Log
import java.io.IOException
import java.nio.charset.Charset


object NFCUtil{

    val NDEF_TEXT_ADDRESS_PREFIXES = arrayOf("http://www","https://www","en","http://","https://","ambrosus.crayonic.com","/device/l1/")

    fun isNdefDiscovered(intent:Intent): Boolean {
        return intent.action?.equals(ACTION_NDEF_DISCOVERED)?:false
    }
    fun isTechDiscovered(intent:Intent): Boolean {
        return intent.action?.equals(ACTION_TECH_DISCOVERED)?:false
    }
    fun isTagDiscovered(intent:Intent): Boolean {
        return intent.action?.equals(ACTION_TAG_DISCOVERED)?:false
    }
    fun isNdefDiscoveredIntent(intent:Intent): Boolean {
        return intent.action?.equals("android.nfc.action.NDEF_DISCOVERED")?:false
    }

    fun nfcAdapter(context: Context):NfcAdapter?{
        return NfcAdapter.getDefaultAdapter(context)
    }

    fun isNFCSupportedOnPhone(context: Context):Boolean{
        return nfcAdapter(context) != null
    }

    fun checkNFCisEnabled(context: Context):Boolean{
        val nfcAdapter = nfcAdapter( context )
        nfcAdapter?.let {
            return it.isEnabled
        }
        return false
    }

    /**
     * Parse data form NFC
     * Opens L1 Detail Activity if parsed correctly
     */
    fun parseNFCInfo(intent: Intent):Boolean {
        // received some data over intent, possibly from NFC

        return false
    }

    fun ndefIntentFilter(): IntentFilter {
        val ndef = IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED)
        try {
            ndef.addDataType("*/*")    /* Handles all MIME based dispatches.
                                       You should specify only the ones that you need. */
        } catch (e: MalformedMimeTypeException) {
            throw RuntimeException("fail", e)
        }

        return ndef
    }

    fun ndefTagFromIntent(intent: Intent): Tag {
        val tagFromIntent : Tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG)
        return tagFromIntent
    }

    fun debugIntent(intent: Intent){
        val extrasString = StringBuilder()
        intent.extras?.keySet()?.forEach { key ->
            extrasString.append(key).append(" = ${intent.extras?.get(key)}").append("\n")
        }
        val ndef_msgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
        val nm_str = StringBuilder()
        nm_str.append("\n")
        ndef_msgs?.forEach { parcel->
            (parcel as NdefMessage).records.forEachIndexed { i, ndefRecord->
                nm_str.append("record [${i}]: ").append(ndefRecord.toString()).append("\n")
            }
        }
        Log.d("NFCDebug", "intent - " +
                "\n/ action: ${intent.action} " +
                "\n/ data: ${intent.dataString}" +
                "\n/ extras: \n${extrasString}\n" +
                "\n/ EXTRA_NDEF_MESSAGES : \n${nm_str}\n" +
                "")
    }

    // from:  https://stackoverflow.com/questions/52763896/android-kotlin-how-to-read-nfc-tag-inside-activity
    // put it in onCreate and after failed tag read

    // null if not hooked up
    @SuppressLint("MissingPermission")
    fun hookupNfcOnForeground(activity:Activity, intentFilters:Array<IntentFilter>, supportedTechTypes:Array<Array<String>>, isBetweenOnResumeAndOnPause: Boolean): NfcAdapter?{
        if(isBetweenOnResumeAndOnPause) {
            val adapter = NfcAdapter.getDefaultAdapter(activity)
            adapter?.let { nfcAdapter ->
                // An Intent to start your current Activity. Flag to singleTop
                // to imply that it should only be delivered to the current
                // instance rather than starting a new instance of the Activity.
                val launchIntent = Intent(activity, activity.javaClass)
                launchIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)

                // Supply this launch intent as the PendingIntent, set to cancel
                // one if it's already in progress. It never should be.
                val pendingIntent = PendingIntent.getActivity(
                        activity, 0, launchIntent, PendingIntent.FLAG_CANCEL_CURRENT
                )

                // And enable your Activity to receive NFC events. Note that there
                // is no need to manually disable dispatch in onPause() as the system
                // very strictly performs this for you. You only need to disable
                // dispatch if you don't want to receive tags while resumed.
                nfcAdapter.enableForegroundDispatch(
                        activity, pendingIntent, intentFilters, supportedTechTypes
                )
            }
            return adapter
        }
        return null
    }
    // from:  https://stackoverflow.com/questions/52763896/android-kotlin-how-to-read-nfc-tag-inside-activity
    // put it in onCreate and after failed tag read
    fun hookupNfcOnForegroundForL1TextAddress(activity:Activity, isBetweenOnResumeAndOnPause:Boolean): NfcAdapter?{
        // Define your filters and desired technology types

        // current L1 sends address as text in Ndef / NfcA
        // example tag :
        /*
                Tag type : ISO 14443-3A  / NXP MIFARE Ultralight
                Tech : NfcA, Ndef
                Serial number 5f:f2:45:aa:54:20:a0
                ATQA : 0x0044
                Data format : NFC Forum Type 2
                Size:  67/70
                Writable / NO
                Can be made Read only : Yes
                Record[0] : UTF-8 (en) : text/plain
                     http://ambrosus.crayonic.com/device/l1/C7-3A-98-C0-1D-A4
         */
        // example intent:
        /*
        / action: android.nfc.action.TECH_DISCOVERED
        / data: null
        / extras:
            android.nfc.extra.NDEF_MESSAGES = [Landroid.os.Parcelable;@c08ecaa
            android.nfc.extra.ID = [B@d64c09b
            android.nfc.extra.TAG = TAG: Tech [android.nfc.tech.NfcA, android.nfc.tech.Ndef]
         */

//        Log.e("NFCDebug", "nfcUtils - hookup")
        val filters = arrayOf(
                IntentFilter(ACTION_TECH_DISCOVERED),
                IntentFilter(ACTION_NDEF_DISCOVERED),
                IntentFilter(ACTION_TAG_DISCOVERED)
        )
        val techTypes = arrayOf(arrayOf(NfcA::class.java.name, Ndef::class.java.name))
//        val techTypes = arrayOf(arrayOf(NfcA::class.java.name))
//        val techTypes = arrayOf(arrayOf(Ndef::class.java.name))
        return hookupNfcOnForeground(activity,filters,techTypes, isBetweenOnResumeAndOnPause)
    }

    // put it into tag received
    @SuppressLint("MissingPermission")
    fun unHookupNfcOnForegroundAfterTagReceived(activity:Activity, nfcAdapter:NfcAdapter?, isBetweenOnResumeAndOnPause:Boolean){
//        Log.e("NFCDebug", "nfcUtils - un hookup")
        if(isBetweenOnResumeAndOnPause)
            nfcAdapter?.disableForegroundDispatch(activity)
    }

    /**
     * Parse L1 BLE address from ndefRecord
     * @param ndefRecord
     */
    @SuppressLint("DefaultLocale")
    fun deviceAddressFromNdefRecord(ndefRecord:NdefRecord):String?{
        // try the uri
        val uri = ndefRecord.toUriOrNull()
        if(uri != null){
            return uri.lastPathSegment?.replace("-",":")?.toUpperCase()
        }
        val text = ndefRecord.payload.toString(Charset.defaultCharset())
        var cleanText = ""
        text.forEach { c: Char ->
            if(c.toInt() > 32){
                cleanText+=c
            }
        }
        NDEF_TEXT_ADDRESS_PREFIXES.forEach {
            cleanText = cleanText.removePrefix(it)
        }
        // try uri in text
        val uriInText = cleanText.replace("-",":").toUpperCase()
        if(uriInText.isNotBlank())
            return uriInText
        // try plain text
        val text2 = ndefRecord.payload.toString(Charset.defaultCharset())
        val index = text.indexOf("/device/l1/")+"/device/l1/".length
        val address = text.substring(index,index+17).replace("-",":")
        if (address.isNotBlank())
            return address

        // fallback
        return null
    }

    fun deviceAddressFromIntent(intent:Intent):String?{
        if (isTechDiscovered(intent) || isNdefDiscovered(intent) || isTagDiscovered(intent)) {
            val rawMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
            rawMessages?.forEach { parcel->
                (parcel as NdefMessage).records.forEach {ndefRecord->
                    deviceAddressFromNdefRecord(ndefRecord)?.let{address ->
                        return address
                    }
                }
            }
        }
        return null
    }


    fun createNFCMessage(payload: String, intent: Intent?): Boolean {

        val pathPrefix = "L1:"
        val nfcRecord = NdefRecord(NdefRecord.TNF_EXTERNAL_TYPE, pathPrefix.toByteArray(), ByteArray(0), payload.toByteArray())
        val nfcMessage = NdefMessage(arrayOf(nfcRecord))
        intent?.let {
            val tag = it.getParcelableExtra<Tag>(NfcAdapter.EXTRA_TAG)
            return writeMessageToTag(nfcMessage, tag)
        }
        return false
    }

    fun retrieveNFCMessage(intent: Intent?): String? {
        intent?.let {
            if (NfcAdapter.ACTION_NDEF_DISCOVERED == intent.action) {
                val nDefMessages = getNDefMessages(intent)
                nDefMessages[0].records?.let {
                    it.forEach {
                        it?.payload.let {
                            it?.let {
                                return String(it)

                            }
                        }
                    }
                }

            } else {
                return null
            }
        }
        return null
    }


    private fun getNDefMessages(intent: Intent): Array<NdefMessage> {

        val rawMessage = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
        rawMessage?.let {
            return rawMessage.map {
                it as NdefMessage
            }.toTypedArray()
        }
        // Unknown tag type
        val empty = byteArrayOf()
        val record = NdefRecord(NdefRecord.TNF_UNKNOWN, empty, empty, empty)
        val msg = NdefMessage(arrayOf(record))
        return arrayOf(msg)
    }

    fun disableNFCInForeground(nfcAdapter: NfcAdapter, activity: Activity) {
        nfcAdapter.disableForegroundDispatch(activity)
    }

    fun <T> enableNFCInForeground(nfcAdapter: NfcAdapter, activity: Activity, classType: Class<T>) {
        val pendingIntent = PendingIntent.getActivity(activity, 0,
                Intent(activity, classType).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0)
        val nfcIntentFilter = IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED)
        val filters = arrayOf(nfcIntentFilter)

        val TechLists = arrayOf(arrayOf(Ndef::class.java.name), arrayOf(NdefFormatable::class.java.name))

        nfcAdapter.enableForegroundDispatch(activity, pendingIntent, filters, TechLists)
    }


    private fun writeMessageToTag(nfcMessage: NdefMessage, tag: Tag?): Boolean {

        try {
            val nDefTag = Ndef.get(tag)

            nDefTag?.let {
                it.connect()
                if (it.maxSize < nfcMessage.toByteArray().size) {
                    //Message to large to write to NFC tag
                    return false
                }
                if (it.isWritable) {
                    it.writeNdefMessage(nfcMessage)
                    it.close()
                    //Message is written to tag
                    return true
                } else {
                    //NFC tag is read-only
                    return false
                }
            }

            val nDefFormatableTag = NdefFormatable.get(tag)

            nDefFormatableTag?.let {
                try {
                    it.connect()
                    it.format(nfcMessage)
                    it.close()
                    //The data is written to the tag
                    return true
                } catch (e: IOException) {
                    //Failed to format tag
                    return false
                }
            }
            //NDEF is not supported
            return false

        } catch (e: Exception) {
            //Write operation has failed
        }
        return false
    }
}


fun NdefRecord.toUriOrNull():Uri?{
    try {
        return this.toUri()
    }catch (e:Exception){
    }
    return null
}

fun NdefRecord.toStringOrNull():String?{
    try {
        return this.toString()
    }catch (e:Exception){
    }
    return null
}
