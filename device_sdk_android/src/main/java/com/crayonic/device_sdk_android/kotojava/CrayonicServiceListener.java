package com.crayonic.device_sdk_android.kotojava;

import com.crayonic.device_sdk.device.DeviceFilter;
import com.crayonic.device_sdk.device.CrayonicDevice;
import com.crayonic.device_sdk_android.core.CrayonicDevicesService;

import java.util.List;

public interface CrayonicServiceListener {
    public void onDevicesFound(DeviceFilter originFilter, List<CrayonicDevice> devices);
    public void onServiceAttached(CrayonicDevicesService crayonicService);
    public void onServiceError(Exception exception);
}
