# Project Title

Device SDK Android 

Platform dependent part of communication control for Crayonic hardware devices

### Prerequisites

Add dependency to your module build gradle. 

### Usage

There is a sample module that simulates usage of this library.

See the sample Activities for hint on how to implement this SDK. 

## Running the tests

No tests included in this pre-release version. 

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 
