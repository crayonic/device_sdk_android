# Project Title

Device SDK 

Platform independent part of communication control for Crayonic hardware devices

### Prerequisites

Add dependency to your module build gradle. 

### Usage

There is a sample module that simulates usage of this library. 

This exact library is used internally as a dependency of another project library.  

## Running the tests

No tests included in this pre-release version. 

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 
