package com.crayonic.device_sdk.memory

import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.util.TagsEnum

class RunningCache : MemoryModule<RunningCache.CacheChannel>(
    CacheChannel(),
    Single.cacheId("running-cache"),
    0,
    specificTags = listOf(TagsEnum.cache.name)
) {

    class CacheChannel :
        MemoryChannel(
            Single.cacheId(Single.cacheId("running-cache_channel")),
            TagsEnum.cache.name
        ), SaveLoadInterface {


        override fun <T> save(id: String, value: T, onDone: (success: Boolean) -> Unit) {
            Single.cacheObject(id, value)
            onDone(true)
        }

        @Suppress("UNCHECKED_CAST")
        override fun <T> load(id: String, onDone: (success: Boolean, instance: T?) -> Unit) {
            val obj = Single.retrieveObject(id)
            try {
                onDone(true, obj as T?)
            } catch (e: Exception) {
                onDone(false, null)
            }

        }
    }
}