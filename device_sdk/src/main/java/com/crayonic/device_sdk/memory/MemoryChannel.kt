package com.crayonic.device_sdk.memory

import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.util.TagsEnum
import com.crayonic.device_sdk.util.initializeTags
import space.lupu.konnector.impl.one_node_module.FunctionalityChannel

open class MemoryChannel(
    id: String = defaultId(),
    tags: List<String>
) : FunctionalityChannel(
    id,
    tags = tags.toList().initializeTags(
        listOf(
            TagsEnum.channel,
            TagsEnum.memory_channel,
            TagsEnum.memory
        )
    )
) {
    constructor(id: String = defaultId(), vararg tags: String) : this(id, tags.toList())

    companion object {
        fun defaultId()= Single.cacheId(TagsEnum.memory_channel.name)
    }
}