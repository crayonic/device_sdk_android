package com.crayonic.device_sdk.memory

import com.crayonic.device_sdk.util.TagsEnum
import com.crayonic.device_sdk.util.appendToList
import com.crayonic.device_sdk.util.initializeTags
import space.lupu.konnector.Module
import space.lupu.konnector.impl.one_node_module.OneNodeModule

open class MemoryModule<CH:MemoryChannel>(
    channel:CH,
    id: String,
    modulePriority: Int,
    specificTags: List<String>
) : OneNodeModule<CH>(
    channel,
    id,
    modulePriority,
    specificTags.appendToList(TagsEnum.module, TagsEnum.memory_module, TagsEnum.memory),
    specificTags.appendToList(TagsEnum.provider, TagsEnum.memory_provider, TagsEnum.memory),
    specificTags.appendToList(TagsEnum.node, TagsEnum.memory_node, TagsEnum.memory)
)