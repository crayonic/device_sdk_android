package com.crayonic.device_sdk.memory

interface SaveLoadInterface{
    fun <T> save(id:String, value:T, onDone:(success:Boolean)->Unit)
    fun <T> load(id: String, onDone:(success:Boolean, instance:T?)->Unit)
}