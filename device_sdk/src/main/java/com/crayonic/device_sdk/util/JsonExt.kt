package com.crayonic.device_sdk.util

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import space.lupu.koex.strings.removeLineEndsAndSpaces
import space.lupu.koex.strings.times
import kotlin.math.max
import kotlin.math.min

fun String?.toJson():JsonObject{
    if(this == null)
        return JsonParser.parseString("{}").asJsonObject

    try {
        return JsonParser.parseString(this).asJsonObject
    }catch (e:Exception){
        return JsonParser.parseString("{}").asJsonObject
    }
}

fun String?.toJsonArray(): JsonArray {
    if(this == null)
        return JsonArray()
    try {
        return JsonParser.parseString(this).asJsonArray
    }catch (e:Exception){
        return JsonArray()
    }
}



//
//fun String?.toJSON(): JSONObject {
//    return JSONObject(this.toJson().toString())
//}
//
//fun JSONArray.toShortJsonString(): String {
//    return this.toString(0).removeCRLFandSPACE()
//}
//
//fun JSONObject?.toShortJsonString(): String {
//    if (this == null)
//        return "{}"
//    return this.toString(0).removeCRLFandSPACE()
//}
//
//fun JSONObject.sortByKeys(): JSONObject {
//    val inJson = this
//    val keys = arrayListOf<String>()
//    inJson.keys().forEach { key ->
//        keys.add(key)
//    }
//    val outJson = JSONObject()
//    keys.apply {
//        sort()
//        forEach { k ->
//            outJson.put(k, inJson.get(k))
//        }
//    }
//    return outJson
//}

fun List<*>.toJsonArrayLikeString():String{
    val sb = StringBuilder("[\n")
    val lastIndex = size - 1
    forEachIndexed {i,  obj ->
        sb.append(obj.toString())
        if (i < lastIndex)
            sb.append(",\n")
        else
            sb.append("\n")
    }
    sb.append("]")
    return sb.toString()
}

fun JsonObject?.sortByKeys(): JsonObject {
    if (this == null)
        return JsonObject()

    val keys = arrayListOf<String>()
    this.entrySet().forEach { pair ->
        keys.add(pair.key)
    }
    val out = JsonObject()
    keys.sorted().forEach { sortedKey ->
        out.add(sortedKey, this[sortedKey])
    }
    return out
}

fun JsonObject?.toShortJsonString(): String {
    if (this == null)
        return "{}"
    return this.toString().removeLineEndsAndSpaces()
}

fun JsonObject.getAsJsonObjectOrNull(memberName : String): JsonObject? {
    if (this.has(memberName))
        return get(memberName).asJsonObject
    return null
}
fun JsonObject.getAsLongOrNull(memberName : String): Long?{
    if (this.has(memberName))
        return get(memberName).asLong
    return null
}
fun JsonObject.getAsDoubleOrNull(memberName : String): Double?{
    if (this.has(memberName))
        return get(memberName).asDouble
    return null
}
fun JsonObject.getAsIntOrNull(memberName : String): Int?{
    if (this.has(memberName))
        return get(memberName).asInt
    return null
}
fun JsonObject.getAsBooleanOrNull(memberName : String): Boolean?{
    if (this.has(memberName))
        return get(memberName).asBoolean
    return null
}
fun JsonObject.getAsStringOrNull(memberName : String): String?{
    if (this.has(memberName))
        return get(memberName).asString
    return null
}

fun String?.equalJsonMembers(otherStr: String?): Boolean = when {
    this == null -> otherStr == null
    otherStr == null -> false
    this == otherStr -> true
    else -> JsonParser.parseString(this).asJsonObject.equals(
        JsonParser.parseString(
            otherStr
        ).asJsonObject
    )
}

val allowedJsonSpecialChars = listOf('\'','\\','"','[',']','{','}','.',',',';',':','=','+','-','_')

fun Char.isJsonAllowed(): Boolean {
    return isLetterOrDigit() || isWhitespace() || allowedJsonSpecialChars.contains(this)
}

fun ByteArray.isJsonAllowedStringOnly(): Boolean {
    var check = isNotEmpty()
    forEach {
        if (check){
            check = it.toChar().isJsonAllowed()
        }
    }
    return check
}

fun String.isJsonAllowedStringOnly(): Boolean {
    var check = isNotEmpty()
    forEach {
        if (check){
            check = it.isJsonAllowed()
        }
    }
    return check
}


/**
 * Returns -1 if no error found
 */
fun validateJsonString(jsonString:String):Int{
    try{
        JsonParser.parseString(jsonString)
    }catch (me: Exception){

        try {
            var failedAtPos = -1
            var isCol = false
            me.localizedMessage.split(" ")
                .forEach {
                    if(it.contentEquals("column")) {
                        isCol = true

                    }else if(isCol){
                        failedAtPos = it.toInt()
                        isCol = false
                        return@forEach
                    }
                }
            if(failedAtPos >= 0) {
                println("JsonValidator - Malformed at position $failedAtPos character \'${jsonString[failedAtPos]}\'\n see surroundings : \n" +
                        "${jsonString.substring(max(failedAtPos-100, 0), min(jsonString.length, failedAtPos + 100))}\n"+
                        " ".times(95)+"^^^^^"
                )
                return failedAtPos
            }
        } catch (e: Exception) {
            println("JsonValidator - Failed to get index of the error from previous error message \"${me.localizedMessage}\" + ${e.stackTrace}")
        }

    }
    return -1
}