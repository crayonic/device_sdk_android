package com.crayonic.device_sdk.util

import space.lupu.kapdu.transmission.TransmissionProgress

//class Progress

class Progress(currentChunkSize: Int, sumTotalSoFar: Int, expectedFinalLength: Int) :
    TransmissionProgress(
        currentChunkSize = currentChunkSize,
        sumTotalSoFar = sumTotalSoFar, expectedFinalLength = expectedFinalLength
    ) {
    constructor(transmissionProgress: TransmissionProgress) : this(
        transmissionProgress.currentChunkSize,
        transmissionProgress.sumTotalSoFar,
        transmissionProgress.expectedFinalLength
    )
    fun perc() = percentage()
}