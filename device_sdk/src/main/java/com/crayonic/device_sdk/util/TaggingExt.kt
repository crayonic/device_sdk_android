package com.crayonic.device_sdk.util

import space.lupu.konnector.tags.Tag
import space.lupu.konnector.tags.Tags

fun Collection<String>.initializeTags(vararg additionalTags: TagsEnum): Tags =
    Tags(this.map { s: String -> Tag(s) }.toMutableList().apply {
        addAll(additionalTags.map { tagsEnum -> Tag(tagsEnum.name) })
    })

fun Collection<String>.initializeTags(additionalTags: Collection<TagsEnum>): Tags =
    Tags(this.map { s: String -> Tag(s) }.toMutableList().apply {
        addAll(additionalTags.map { tagsEnum -> Tag(tagsEnum.name) })
    })

fun Collection<String>.initializeTags(vararg additionalTags: String): Tags =
    Tags(this.map { s: String -> Tag(s) }.toMutableList().apply {
        addAll(additionalTags.map { s -> Tag(s) })
    })

fun Collection<String>.appendToList(vararg additionalTags: String): List<String> =
    toMutableList().apply { addAll(additionalTags) }

fun Collection<String>.appendToList(vararg additionalTags: TagsEnum): List<String> =
    toMutableList().apply {
        addAll(additionalTags.map { tagsEnum -> tagsEnum.name })
    }

fun Collection<TagsEnum>.toTags()=
    listOf<String>().initializeTags(this)

enum class TagsEnum{
    memory_module, memory_provider, memory_node, memory_channel, memory,
    cache, database, file_io, keystore, key_value,
    device_module, device_provider, device_node, device_channel, device,
    ble, l1, pen, apdu,
    ble_stack_module, ble_stack_provider, ble_stack_node, ble_stack_channel, ble_stack,
    update_device_module, update_device_provider, update_device_node, update_device_channel, update_device,
    dfu,
    json_api_module, json_api_provider, json_api_node, json_api_channel, json_api,
    amb,
    crypto_module, crypto_provider, crypto_node, crypto_channel, crypto,
    signal, keccak, ecdsa,
    channel, node, provider, module
}
