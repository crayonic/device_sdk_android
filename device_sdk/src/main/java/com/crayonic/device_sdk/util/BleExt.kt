package com.crayonic.device_sdk.util

import com.crayonic.device_sdk.ble.BleDeviceNode
import com.crayonic.device_sdk.core.Controller
import com.crayonic.device_sdk.ble.BleStackModule
import com.crayonic.device_sdk.ble.FoundBleDevice
import com.crayonic.device_sdk.device.CrayonicDeviceNode


fun runOnBleStack(onFail:(reason:String)-> Unit, action:(bleStack: BleStackModule<*>)->Unit):Boolean{
    val bleStack =
    try {
        Controller.getInstance().takeBestModuleForTags(TagsEnum.ble_stack.name)
    }catch (e:Exception){
        onFail("Ble Stack module cannot be accessed : ${e.localizedMessage} \n ${e.printStackTrace()}")
        return false
    }
    when (bleStack) {
        null -> {
            onFail("Ble Stack module is missing")
            return false
        }
        is BleStackModule<*> -> {
            action(bleStack)
            return true
        }
        else -> {
            onFail("Module with tag ${TagsEnum.ble_stack.name} is not a Ble Stack ? ")
            return false
        }
    }
}


