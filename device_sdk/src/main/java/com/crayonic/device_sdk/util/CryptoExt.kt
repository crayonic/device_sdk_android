@file:Suppress("unused")

package com.crayonic.device_sdk.util

import com.crayonic.device_sdk.core.Single.zeros32
import org.kethereum.crypto.signMessageHash
import org.kethereum.crypto.toECKeyPair
import org.kethereum.extensions.toBigInteger
import org.kethereum.keccakshortcut.keccak
import org.kethereum.model.PrivateKey
import org.kethereum.model.PublicKey
import org.kethereum.model.SignatureData
import space.lupu.koex.hex.Hex
import space.lupu.koex.hex.toHex
import space.lupu.koex.numbers.toByteArray
import java.nio.charset.Charset


fun ByteArray.toKeccak(): ByteArray = this.keccak()
//{
//    val digest = Keccak256()
//    digest.update(this)
//    return digest.digest()
//}

fun String.toKeccak(): ByteArray {
//    Log.e("CryptoExtension", "String.toKeccak: bytes : ${this.toByteArray().toHex()}")
    return this.toByteArray(Charset.defaultCharset()).toKeccak()
}

/**
 *  keccak256( this ) -> "0x0.."
 */
fun String.toKeccakHex(delimiter: String="", includePrefix0x:Boolean = false): String {
    return this.toKeccak().toHex(delimiter, includePrefix0x)
}

/**
 *  keccak256( this.prependedByEthSigMsg ) -> "0x0.."
 */
fun String?.toEthKeccakHex(delimiter: String="", includePrefix0x:Boolean = true): String {
    if (this == null)
        return zeros32.toHex(delimiter, includePrefix0x) // ByteArray(32).toHex(delimiter, includePrefix0x) with static value import
    return this.prefixWithEthSignedMessage().toKeccakHex(delimiter, includePrefix0x)
}

/**
 *  keccak256( this.prependedByEthSigMsg ) -> "0x0.."
 */
fun String?.toEthKeccak(): ByteArray {
    if (this == null)
        return ByteArray(32)
    return this.prefixWithEthSignedMessage().toKeccak()
}

/**
 * Sign the text with ETH like EC signature
 */
fun String.toEthSignature(secret: String): String =
    toEthSignatureHexString(secret.toECKeyFromPrivate(), true)

fun String.toECKeyFromPrivate(): PrivateKey = PrivateKey(this)

fun String.toPublicKeyString(): PublicKey = PublicKey(this)

fun String.toEthSignatureData(privateKey: PrivateKey): SignatureData {
    // like python impl
    // prefix eth message -> keccak256 -> HASH
    // use private key to sign -> signature
    // to last byte ( V ) add 27

    val msg_real_hash = this.prefixWithEthSignedMessage().toKeccak()// by default UTF-8

    val signature: SignatureData = signMessageHash(msg_real_hash, privateKey.toECKeyPair())
    // signature looks like :
    // 0x19444b9eead0394ca5041068d4ec50af5fd593d8eed145101ccfb5a9ac4cae2772e4890d3ab912229a1c37e2efed35077c3630ebf7359110477090b99ba242321c
    return signature
}

fun String.toEthSignatureHexString(privateKey: PrivateKey, includePrefix0x: Boolean):String =
    (if(includePrefix0x) "0x" else "") + toEthSignatureData(privateKey)

fun String.toEthSignatureHex(privateKey: PrivateKey, includePrefix0x: Boolean):Hex =
    Hex(toEthSignatureHexString(privateKey, includePrefix0x))


fun ByteArray.add27ToLastByte(): ByteArray {
    if(this.size != 65)
        return this
    return this.apply {
        val v = this.last()
        if(v < 27.toByte() ){
            if(v == 0.toByte() ){
                set(65, 27.toByte())
            }else if(v==1.toByte()){
                set(65, 28.toByte())
            }
        }
    }
}

private fun String.hexReplaceLastByte(replacement: Byte): String {
    if (this.length > 1)
        return this.replaceRange(this.length - 2, this.length, replacement.toHex())
    else
        return this
}

/**
 * Prepend ETH SigMsg preffix to this -> String
 */
fun String.prefixWithEthSignedMessage(): String {
    return (PREFIX_ETH_SIGNED_MSG + this.length + this)
}

val PREFIX_ETH_SIGNED_MSG = "\u0019Ethereum Signed Message:\n"

/**
 * Extension to derive ETH deviceAddress from public key (used in ECDSA)
 * Steps taken :
 * 1. hash of 64B public key -> keccak256(pub)
 * 2. take last 20B of keccak256(pub) -> output
 * @return bytes of ethereum deviceAddress
 */
fun ByteArray?.pubToEthAddress(): ByteArray {
    if (this == null)
        return ByteArray(0)

    val hash: ByteArray
    if (size == 65)
        hash = copyOfRange(1, 65).toKeccak()
    else if (size == 64)
        hash = this.toKeccak()
    else
        return ByteArray(0)

    return hash.copyOfRange(hash.size - 20, hash.size)
}

fun ByteArray.ethAddressToChecksumAddress(): String {
    return this.toHex().ethAddressToChecksumAddress()
}

//fun ECKey.privateToChecksumAddress(): String {
//    return this.pubKey.pubToEthAddress().addressToChecksumAddress()
//}
//
//fun ECKey.toEthAddress(): ByteArray {
//    return this.pubKey.pubToEthAddress()
//}


/**
 * Extension to derive checksum ETH deviceAddress from 20 bytes of ETH deviceAddress
 * Result is case sensitive
 *
 * Spec :
 * @see https://github.com/ethereum/EIPs/blob/master/EIPS/eip-55.md
 *
 * Previous implementations:
 * @see https://github.com/web3j/web3j/blob/master/crypto/src/main/java/org/web3j/crypto/Keys.java
 *
 * @return case sensitive string of checksum ethereum deviceAddress
 */
fun String.ethAddressToChecksumAddress(): String {
    val lowercaseAddress = Hex(this.toLowerCase()).toString() // removes 0x and validates hex
    val addressHash = lowercaseAddress.toKeccakHex(includePrefix0x = false)

    val result = StringBuilder(lowercaseAddress.length + 2)

    result.append("0x")

    for (i in 0 until lowercaseAddress.length) {
        if (Integer.parseInt(addressHash.get(i).toString(), 16) >= 8) {
            result.append(lowercaseAddress.get(i).toString().toUpperCase())
        } else {
            result.append(lowercaseAddress.get(i))
        }
    }

    return result.toString()
}

fun String.messageHashCheck(messageHashHex:String):Boolean =
    toEthKeccakHex().equals(messageHashHex, ignoreCase = true)

fun ByteArray.toRSVSignatureData(): SignatureData? {
    if (this.size == 65) {
        val r = this.copyOfRange(0, 32).toBigInteger()
        val s = this.copyOfRange(32, 64).toBigInteger()
        val v = this.last().toByteArray().toBigInteger()
        return SignatureData(r,s,v)
    }
//    else if (this.size == 64){
//        val r = this.copyOfRange(0, 32)
//        val s = this.copyOfRange(32, 64)
//        return ECKey.ECDSASignature.fromComponents(r, s, 27.toByte())
//    }
    else {
        return null
    }
}
//
//fun ByteArray.toECDSASignatureFromRS(): ECKey.ECDSASignature? {
//    return this.toECDSASignatureFromRS(0x1b.toByte())
//}
//
//fun ByteArray.toECDSASignatureFromRS(v:Byte): ECKey.ECDSASignature? {
//    if (this.size == 64) {
//        val r = this.copyOfRange(0, 32)
//        val s = this.copyOfRange(32, 64)
//        return ECKey.ECDSASignature.fromComponents(r, s, v)
//    } else {
//        return null
//    }
//}
//
//fun ByteArray.toECDSASignatureFromSRV(): ECKey.ECDSASignature? {
//    if (this.size == 65) {
//        val s = this.copyOfRange(0, 32)
//        val r = this.copyOfRange(32, 64)
//        val v = this.last()
//        return ECKey.ECDSASignature.fromComponents(r, s, v)
//    }
////    else if (this.size == 64){
////        val s = this.copyOfRange(0, 32)
////        val r = this.copyOfRange(32, 64)
////        return ECKey.ECDSASignature.fromComponents(r, s, 27.toByte())
////    }
//    else {
//        return null
//    }
//}
//
//fun ByteArray.toECDSASignatureFromSR(): ECKey.ECDSASignature? {
//    if (this.size == 64) {
//        val s = this.copyOfRange(0, 32)
//        val r = this.copyOfRange(32, 64)
//        return ECKey.ECDSASignature.fromComponents(r, s, 27.toByte())
//    } else {
//        return null
//    }
//}
//
//fun String.hex2ecdsaSignatureRSV(): ECKey.ECDSASignature? {
//    return this.hexStringToByteArray().toECDSASignatureFromRSV()
//}
//
//fun String.hex2ecdsaSignatureSRV(): ECKey.ECDSASignature? {
//    return this.hexStringToByteArray().toECDSASignatureFromSRV()
//}
//
//fun ByteArray.toECDSASignatureValidationRSV(): Boolean {
//    val sig = this.toECDSASignatureFromRSV()
//    if (sig == null) {
//        return false
//    } else {
//        return sig.validateComponents()
//    }
//}
//
//fun ByteArray.toECDSASignatureValidationSRV(): Boolean {
//    val sig = this.toECDSASignatureFromSRV()
//    if (sig == null) {
//        return false
//    } else {
//        return sig.validateComponents()
//    }
//}
//
//fun ECKey.ECDSASignature?.componentsValidation(): Boolean {
//    if (this == null) {
//        return false
//    } else {
//        return this.validateComponents()
//    }
//}
//
//fun ByteArray.verifyMessageInSignatureExhaustive(ecdsaSig: ECKey.ECDSASignature, anyEthAddress: String): Boolean {
//    val ethAddHexNum = anyEthAddress.toLowerCase()
//    var publicKey: ByteArray = ByteArray(0)
//    for (i in 0..3) {
//        publicKey = ECKey.recoverPubBytesFromSignature(i, ecdsaSig, this) ?: ByteArray(0)
//        if (publicKey.size != 0) {
//            // possible match
//            if (ethAddHexNum.equals(publicKey.pubToEthAddress())) {
//                return ECKey.verify(this, ecdsaSig, publicKey)
//            }
//        }
//    }
//    return false
//}
//
//fun ECKey.ECDSASignature?.recoverCorrectSignatureV(messageHash: ByteArray, anyEthAddress: String): ECKey.ECDSASignature? {
//    if (this==null)
//        return null
//    val ethAddHexNum = anyEthAddress.toLowerCase()
//    var publicKey = ByteArray(0)
//    var i = 0
//    while (i < 4) {
//        publicKey = ECKey.recoverPubBytesFromSignature(i, this, messageHash) ?: ByteArray(0)
//        if (publicKey.size >= 64) {
//            // possible match
//            val ethAddFromPubKey = publicKey.pubToEthAddress().toHexNum()
//            if (ethAddHexNum.equals(ethAddFromPubKey, ignoreCase = true)) {
//                return ECKey.ECDSASignature.fromComponents(this.r.toByteArray(), this.s.toByteArray(), (i + 27).toByte())
//            }
//        }
//        i++
//    }
//    return null
//}
//
//fun ECKey.ECDSASignature?.toByteArrayWithV(): ByteArray {
//    if (this == null)
//        return ByteArray(65)
//    var rBytes = r.toByteArray()
//    if (rBytes.size > 32) {
//        rBytes = rBytes.copyOfRange(rBytes.size - 32, rBytes.size)
//    }
//    if (rBytes.size < 32) {
//        rBytes = rBytes.leadingPaddingToSize32Bytes()
//    }
//
//    var sBytes = s.toByteArray()
//    if (sBytes.size > 32) {
//        sBytes = sBytes.copyOfRange(sBytes.size - 32, sBytes.size)
//    }
//    if (sBytes.size < 32) {
//        sBytes = sBytes.leadingPaddingToSize32Bytes()
//    }
//
//    return rBytes + sBytes + v
//}
//
//fun ECKey.ECDSASignature?.toHex(delimiter: String = "", includePrefix0x: Boolean = false): String {
//    val bytes = this.toByteArrayWithV()
//    if (bytes.size > 0) {
//        return bytes.toHex(delimiter, includePrefix0x)
//    } else {
//        return ""
//    }
//}
//
