package com.crayonic.device_sdk.util

import android.util.Log
import com.crayonic.device_sdk.core.Single.defaultPinCode
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.device.l1.models.AlarmZonesCollection
import com.crayonic.device_sdk.device.l1.models.CrayonicAlarmZone
import com.crayonic.device_sdk.device.l1.models.L1DeviceAssetId
import com.crayonic.device_sdk.device.CrayonicDevice
import com.crayonic.device_sdk.device.CrayonicL1
import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.suspendCancellableCoroutine
import space.lupu.kapdu.bytes.ApduBytes
import kotlin.coroutines.resume


/**
 * Access device read member values more easily
 *
 * Has to be called from coroutine
 */
suspend fun readDeviceMemberValue(connectedDevice: CrayonicDevice, readKey: CrayonicRequestCode): String {
    return suspendCancellableCoroutine<String> { cont ->
        gattRead(connectedDevice,readKey, cont)
    }
}

private fun gattRead(
    connectedDevice: CrayonicDevice,
    readKey: CrayonicRequestCode,
    cont: CancellableContinuation<String>
) {
    val onResult: (device: CrayonicDevice, readableValue: String) -> Unit =
        { device, readableValue -> cont.resume( readableValue ) }
    Log.e("AccessHelp", "Access: ${readKey.name}")
    when(readKey) {
        CrayonicRequestCode.readDeviceAddress -> connectedDevice.readDeviceAddress(onResult)
        CrayonicRequestCode.readDeviceName -> connectedDevice.readDeviceName(onResult)
        CrayonicRequestCode.readBatteryLevel -> connectedDevice.readBatteryLevel(onResult)
        CrayonicRequestCode.readModelNumber -> connectedDevice.readModelNumber(onResult)
        CrayonicRequestCode.readFirmwareVersion -> connectedDevice.readFirmwareVersion(onResult)
    }
    if(connectedDevice is CrayonicL1)
        when(readKey) {
            CrayonicRequestCode.readEthAddress -> connectedDevice.readEthAddress(onResult)
            CrayonicRequestCode.readAssetId -> connectedDevice.readAssetId(onResult)
            CrayonicRequestCode.readTemperature -> connectedDevice.readTemperature(onResult)
            CrayonicRequestCode.readHumidity -> connectedDevice.readHumidity(onResult)
            CrayonicRequestCode.readLight -> connectedDevice.readLight(onResult)
            CrayonicRequestCode.readShock -> connectedDevice.readShock(onResult)
        }
}



suspend fun readCommandValue(connectedDevice: CrayonicDevice, requestCode: CrayonicRequestCode):Any{
    return suspendCancellableCoroutine<Any> { cont ->
        apduCommandRead(connectedDevice,requestCode, onAnyResponseValue = {resValue->
            cont.resume(resValue)
        })
    }
}

fun apduCommandRead(connectedDevice: CrayonicDevice, requestCode: CrayonicRequestCode, onAnyResponseValue:(Any)-> Unit) {
    when (requestCode) {
        CrayonicRequestCode.runCommandReadPublicKey -> connectedDevice.runCommandReadPublicKey(
            onResult = { device: CrayonicDevice, publicKeyBytes: ByteArray ->
                onAnyResponseValue(publicKeyBytes)
            })
    }
    if (connectedDevice is CrayonicL1)
        when (requestCode) {
            CrayonicRequestCode.runCommandReadPackageId -> connectedDevice.runCommandReadPackageId(
                onResult = { device: CrayonicDevice, packageId: String ->
                    onAnyResponseValue(packageId)
                })
            CrayonicRequestCode.runCommandReadAlarmZones -> connectedDevice.runCommandReadAlarmZones(
                onResult = { device: CrayonicDevice, alarmZones: AlarmZonesCollection ->
                    onAnyResponseValue(alarmZones)
                })
            CrayonicRequestCode.runCommandReadAssetId -> connectedDevice.runCommandReadAssetId(
                onResult = { device: CrayonicDevice, deviceAssetId: L1DeviceAssetId ->
                    onAnyResponseValue(deviceAssetId)
                })
        }
}

suspend fun writeCommandValue(connectedDevice: CrayonicDevice, requestCode: CrayonicRequestCode, input:Any):CrayonicDevice?{
    return suspendCancellableCoroutine<CrayonicDevice?> { cont ->
        apduCommandWrite(connectedDevice,requestCode, input, onDeviceResponse = {resValue->
            cont.resume(resValue)
        })
    }
}

fun apduCommandWrite(connectedDevice: CrayonicDevice, requestCode: CrayonicRequestCode, inputValue:Any, onDeviceResponse:(CrayonicDevice?)->Unit) {
    when (requestCode) {
        CrayonicRequestCode.runCommandWriteClock ->
            if (inputValue is Long) {
                connectedDevice.runCommandWriteClock(defaultPinCode, inputValue){ onDeviceResponse(it) }
            }else {
                onDeviceResponse(null)
                return
            }
        CrayonicRequestCode.runCommandSetManagementPinCode->
            if (inputValue is String) {
                connectedDevice.runCommandSetManagementPinCode(defaultPinCode, inputValue){ onDeviceResponse(it) }
            }else {
                onDeviceResponse(null)
                return
            }

    }
    if (connectedDevice is CrayonicL1)
    when(requestCode){
        CrayonicRequestCode.runCommandWriteAssetId->
            if (inputValue is L1DeviceAssetId) {
                connectedDevice.runCommandWriteAssetId(inputValue){ onDeviceResponse(it) }
            }else {
                onDeviceResponse(null)
            }
        CrayonicRequestCode.runCommandWritePackageId ->
            if(inputValue is String){
                connectedDevice.runCommandWritePackageId(inputValue) { onDeviceResponse(it) }
            }else {
                onDeviceResponse(null)
            }
        CrayonicRequestCode.runCommandWriteAlarmZone ->{
            if (inputValue is CrayonicAlarmZone) {
                connectedDevice.runCommandWriteAlarmZone(inputValue) { onDeviceResponse(it) }
            }else {
                onDeviceResponse(null)
            }
        }
    }
}


suspend fun rawApdu(connectedDevice: CrayonicDevice, input:ByteArray):ByteArray{
    return suspendCancellableCoroutine<ByteArray> { cont ->
        connectedDevice.runCommandRawBytes(input){device: CrayonicDevice, bytes: ByteArray ->
            cont.resume(bytes)
        }
    }
}


