package com.crayonic.device_sdk.util

import kotlinx.coroutines.*

suspend fun asyncWithTimeout(timeoutInMs:Long = seconds_10, runBlock:()->Unit): Job {
    val job = Job()
    withTimeout(timeoutInMs) {
        async {
            runBlock()
            job.cancel()
        }.await()
    }
    return job
}

fun launchJobOnIO_asyncWithTimeout(parentJob: Job, timeoutInMs:Long = seconds_10, runBlock:()->Unit): Job {
    val job = Job(parentJob)
    return CoroutineScope(Dispatchers.IO + job).launch {
        withTimeout(timeoutInMs) {
            async {
                runBlock()
                job.cancel()
                // cancel the job right after out block has passed to prevent resources usage in timeout loop
            }.await()
        }
    }
}
fun launchJobOnIO_asyncWithTimeout(parentJob: Job, timeoutInMs:Long = seconds_10, finallyOnDelay:()-> Unit = {}, runBlock:()->Unit): Job {
    val job = Job(parentJob)
    return CoroutineScope(Dispatchers.IO + job).launch {
        withTimeout(timeoutInMs) {
            async {
                runBlock()
                job.cancel()
            }.await()
        }
        delay(timeoutInMs)
        finallyOnDelay()
    }
}
fun launchJobOnIO_asyncWithDelay(parentJob: Job, delayOfMs:Long = seconds_15, runBlock:()->Unit): Job {
    val job = Job(parentJob)
    return CoroutineScope(Dispatchers.IO + job).launch {
        async {
            delay(delayOfMs)
            runBlock()
            job.cancel()
        }
    }
}

fun launchJobOnIO_withDelay(parentJob: Job, delayOfMs:Long = seconds_10, runBlock:()->Unit): Job {
    val job = Job(parentJob)
    return CoroutineScope(Dispatchers.IO + job).launch {
        delay(delayOfMs)
        runBlock()
        job.cancel()
    }
}

fun launchJobOnIO_Timeout(parentJob: Job, timeouInMS:Long = seconds_10, runBlockAfterDelay:()->Unit): Job {
    return launchJobOnIO_withDelay(parentJob, timeouInMS, runBlockAfterDelay)
}

fun launchJobOnIO(parentJob: Job, runBlock:()->Unit): Job {
    val job = Job(parentJob)
    return CoroutineScope(Dispatchers.IO + job).launch {
        runBlock()
        job.cancel()
    }
}
fun launchJobOnIO_withTimeout(parentJob: Job, timeoutInMs:Long = seconds_10, runBlock:()->Unit): Job {
    val job = Job(parentJob)
    return CoroutineScope(Dispatchers.IO + job).launch {
        withTimeout(timeoutInMs) {
            runBlock()
            job.cancel()
        }
    }
}

fun launchOnMain(runBlock: () -> Unit): Job {
    return CoroutineScope(Dispatchers.Main).launch {
        runBlock()
    }
}

fun periodicCheck(
    parentJob: Job,
    delayCondition: () -> Boolean,
    delayPeriodMs: Long,
    runBlock: () -> Unit
): Job {
    val job = Job(parentJob)
    return CoroutineScope(Dispatchers.IO + job).launch {
        delay(delayPeriodMs)
        while (with(receiver = delayCondition) { invoke() }) {
            delay(delayPeriodMs)
        }
        runBlock.invoke()
    }
}