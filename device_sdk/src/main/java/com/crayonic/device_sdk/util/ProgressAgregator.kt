package com.crayonic.device_sdk.util

import com.crayonic.device_sdk.core.Single
import kotlinx.coroutines.*

class ProgressAggregator(val taskSubProgresses:List<SubProgress>){
    constructor(vararg taskSubProgresses:SubProgress):this(taskSubProgresses.toList())
    val taskIdMap = hashMapOf<SubProgress, Int>()
    var tasksCount = 0
        private set(value) {
            field = value
        }

    init {
        taskSubProgresses.forEach {
            tasksCount +=1
            taskIdMap.put(it, tasksCount)

        }
    }

    fun totalProgress():Progress = toTransmissionProgress()

    fun toTransmissionProgress():Progress{
        val totalProgress = Progress(0,0,0)
        taskSubProgresses.forEach {
            totalProgress.sumTotalSoFar += it.transmissionProgress.sumTotalSoFar
            totalProgress.currentChunkSize += it.transmissionProgress.currentChunkSize
            totalProgress.expectedFinalLength += it.transmissionProgress.expectedFinalLength
        }
        return totalProgress
    }
}

data class SubProgress(var transmissionProgress: Progress = zeroProgress()){
    val id = Single.cacheId("sub_task_progress")
    fun update(transmissionProgress: Progress){
        this.transmissionProgress = transmissionProgress
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SubProgress

        if (id != other.id) return false
        if (transmissionProgress.expectedFinalLength != other.transmissionProgress.expectedFinalLength) return false
        if (transmissionProgress.sumTotalSoFar != other.transmissionProgress.sumTotalSoFar) return false
        if (transmissionProgress.currentChunkSize != other.transmissionProgress.currentChunkSize) return false

        return true
    }

    fun clear() {
        transmissionProgress = zeroProgress()
    }

    fun setTo100() {
        transmissionProgress.sumTotalSoFar = transmissionProgress.expectedFinalLength
    }

    companion object{
        fun zeroProgress() = Progress(0,0,100)
    }
}

fun startPeriodicAsyncProgressUpdates(
    subProgress:SubProgress,
    targetTimeoutMs:Long = seconds_10,
    updateIntervalMs:Long = 200L,
    onProgressUpdate:(transmissionProgress: Progress)->Unit
): Job {
    // limits
    val updateInterval = if(updateIntervalMs <= 0) 200 else if( updateIntervalMs > Int.MAX_VALUE) Int.MAX_VALUE else updateIntervalMs.toInt()
    val targetTimeout = if(updateIntervalMs <= 0) seconds_10.toInt() else if( updateIntervalMs > Int.MAX_VALUE) Int.MAX_VALUE else updateIntervalMs.toInt()
    val times = targetTimeout.div(updateInterval)
    val updateIntervalLong = updateInterval.toLong()
    subProgress.transmissionProgress.expectedFinalLength = targetTimeout

    val job = CoroutineScope(Dispatchers.IO).launch {
        for (i in 0..times){
            subProgress.transmissionProgress.sumTotalSoFar += updateInterval
            onProgressUpdate(subProgress.transmissionProgress)
            delay(updateIntervalLong)
        }
    }
    return job
}