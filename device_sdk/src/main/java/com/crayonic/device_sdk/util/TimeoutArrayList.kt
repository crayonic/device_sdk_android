package com.crayonic.device_sdk.util

import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class TimeoutArrayList<T> : ArrayList<T> {

    val jailChangeLock = Mutex()
    val jailTimeouts: HashMap<T, Long> // ms times of the future soonest releases
    val parentJob = Job()

    constructor() : super(){
        jailTimeouts  = hashMapOf()
    }

    constructor(previous:TimeoutArrayList<T>) : super(previous){
        jailTimeouts = previous.jailTimeouts
    }

    private fun time(): Long = System.currentTimeMillis()

    suspend fun onTimeoutCheck(){
        jailChangeLock.withLock {
            val timeOfCheck = time()
            val toBeReleased = arrayListOf<T>()
            forEach { jailer ->
                val releaseTime = jailTimeouts.get(jailer)
                if( releaseTime == null ) {
                    // there has been an evedence error and jailer has no release time in DB
                    // we need to release jailer right away .. OOPS
                    toBeReleased.add(jailer)
                }else if(releaseTime <= timeOfCheck ){
                    // its about time to release the jailer
                    toBeReleased.add(jailer)
                }
            }
            // release from jail
            removeAll(toBeReleased)
        }
    }

    fun startTimeoutCheck(timeoutInMs: Long){
        CoroutineScope(Dispatchers.IO + Job(parentJob)).launch {
            delay(timeoutInMs)
            onTimeoutCheck()
        }
    }

    override fun clear() {
        jailTimeouts.clear()
        super.clear()
    }

    override fun remove(element: T): Boolean {
        jailTimeouts.remove(element)
        return super.remove(element)
    }

    override fun removeAll(elements: Collection<T>): Boolean {
        // remove from database
        elements.forEach { jailTimeouts.remove(it) }
        return super.removeAll(elements)
    }

    fun addWithTimeout(element: T, timeoutInMs:Long): Boolean {
        jailTimeouts.put(element, time()+timeoutInMs)
        startTimeoutCheck(timeoutInMs)
        return add(element)
    }

    fun addAllWithTimeout(elements: Collection<T>, timeoutInMs:Long): Boolean {
        elements.forEach { jailTimeouts.put(it, time()+timeoutInMs) }
        startTimeoutCheck(timeoutInMs)
        return addAll(elements)
    }

    fun rewriteWithTimeout(element: T, timeoutInMs:Long): Boolean {
        if(contains(element))
            remove(element)
        jailTimeouts.put(element, time()+timeoutInMs)
        startTimeoutCheck(timeoutInMs)
        return add(element)
    }

    fun rewriteAllWithTimeout(elements: Collection<T>, timeoutInMs:Long): Boolean {
        elements.forEach {
            if(contains(it))
                remove(it)
            jailTimeouts.put(it, time()+timeoutInMs)
        }
        startTimeoutCheck(timeoutInMs)
        return addAll(elements)
    }

}