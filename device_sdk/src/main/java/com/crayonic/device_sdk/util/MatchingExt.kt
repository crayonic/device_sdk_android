package com.crayonic.device_sdk.util

import android.util.Patterns
import com.crayonic.device_sdk.util.MatcherUtilRegexes.regex
import com.crayonic.device_sdk.util.MatcherUtilRegexes.whitechars
import space.lupu.koex.hex.HEX_CHARS_S
import kotlin.math.max
import kotlin.math.min


fun String?.isWebUrl(): Boolean {
    if (this == null)
        return false
    return android.util.Patterns.WEB_URL.matcher(this).matches()
}

fun String?.isNumber(): Boolean {
    if (this == null)
        return false
    return this.matches("\\d+".toRegex())
}

fun Double?.equalsDelta(other: Double?, delta: Double): Boolean {
    if (this == null && other == null) {
        return true
    } else if (other == null) {
        return false
    } else if (this == null) {
        return false
    } else {
        return max(this, other).minus(min(this, other)).compareTo(delta) <= 0
    }
    //return false
}

fun String?.isValidEmail(): Boolean {
    val pattern = Patterns.EMAIL_ADDRESS
    return pattern.matcher(this).matches()
}

fun String?.isValidBleAddress(): Boolean {
    if (this == null) return false

    // something like : D2:BB:3F:51:D2:42
    val split = this.split(":")
    if (split.size == 6) {
        var correctCount = 0
        split.forEach { str ->
            str.forEach { char ->
                if (HEX_CHARS_S.contains(char.toLowerCase())) {
                    correctCount++
                }
            }
        }
        if (correctCount == 12) {
            // IFF 6 HEX encoded Bytes separated by ':'
            return true
        }
    }
    return false
}

fun String.isUnprovisionedDeviceName(): Boolean {
    var name= "SOMETHING"
    split(":").let { s1 ->
        if (s1.size > 1) {
            s1.get(1).split("!").let { s2 ->
                if (s2.isNotEmpty())
                    name = s2.get(0)
            }
        }
    }
    return name.isEmpty()
}

fun String.getPackageIdBeginning(): String {
    var name = ""
    split(":").let { s1 ->
        if (s1.size > 1) {
            s1.get(1).split("!").let { s2 ->
                if (s2.size > 0)
                    name = s2.get(0)
            }
        }
    }
    return name
}

object MatcherUtilRegexes{
    val regex = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]".toRegex()
    val whitechars = "\\s".toRegex()
}

fun String?.toUrlsFromText():List<String>{
    if (this == null) return listOf()

    val list = arrayListOf<String>()
    this.split(whitechars).forEach {str ->
        if(str.matches(regex)){
            list.add(str)
        }
    }
    return list
}