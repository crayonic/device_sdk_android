package com.crayonic.device_sdk.util

import space.lupu.koex.bytes.minimalByteArray
import space.lupu.koex.bytes.paddingZeros
import space.lupu.koex.numbers.toByteArray
import java.nio.ByteOrder

/**
 * Time in seconds from Epoch
 */
fun currentUnixTime():Long = System.currentTimeMillis()/1000L

/**
 * Number represents second in millisecond, e.g 1 sec = 1000L
 */
val seconds_1 = 1_000L

/**
 * Number represents second in millisecond, e.g 1 sec = 1000L
 */
val seconds_2 = 2_000L

/**
 * Number represents second in millisecond, e.g 1 sec = 1000L
 */
val seconds_3 = 3_000L

/**
 * Number represents second in millisecond, e.g 1 sec = 1000L
 */
val seconds_5 = 5_000L

/**
 * Number represents second in millisecond, e.g 1 sec = 1000L
 */
val seconds_10 = 10_000L

/**
 * Number represents second in millisecond, e.g 1 sec = 1000L
 */
val seconds_15 = 15_000L

/**
 * Number represents second in millisecond, e.g 1 sec = 1000L
 */
val seconds_20 = 20_000L

/**
 * Number represents second in millisecond, e.g 1 sec = 1000L
 */
val seconds_25 = 25_000L

/**
 * Number represents second in millisecond, e.g 1 sec = 1000L
 */
val seconds_30 = 30_000L

/**
 * Number represents second in millisecond, e.g 1 sec = 1000L
 */
val seconds_60 = 60_000L

/**
 * Number represents second in millisecond, e.g 1 sec = 1000L
 */
val seconds_120 = 120_000L

/**
 * Number represents second in millisecond, e.g 1 sec = 1000L
 */
val seconds_600 = 600_000L

/**
 * 4 byte unix timestamp
 */
fun clockUnixTimeNowAsBytes(): () -> ByteArray=
    { currentUnixTime().toByteArray(ByteOrder.LITTLE_ENDIAN).minimalByteArray(ByteOrder.LITTLE_ENDIAN).paddingZeros(4) }
