package com.crayonic.device_sdk.util

import android.util.Base64
import com.crayonic.device_sdk.http.l1_json_api.L1ApiJson_L2Event
import com.crayonic.ambrosus.gateway.comm.http.model.L1JsonApi_Asset
import com.crayonic.device_sdk.core.Controller
import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.http.l1_json_api.L1ApiJson_L1Event
import com.crayonic.device_sdk.http.l1_json_api.L1ApiJson_Postable
import com.crayonic.device_sdk.http.l1_json_api.L1JsonApi
import com.crayonic.device_sdk.http.l1_json_api.L1JsonApi_Account
import com.google.gson.JsonObject
import kotlinx.coroutines.*
import okhttp3.Response
import java.nio.charset.Charset
import kotlin.coroutines.resume


fun String.toJsonApiToken(secret: String, validForSeconds: Long = 3600L): String {
    /*
    https://ambrosus.docs.apiary.io/#introduction/group-token-[token]

    Group Token [Token]

    Some requests like account management or accessing to restricted data fields require an access
    token for authorization. The access token is a base64-encoded serialized JSON file of following format:

    {
      "signature": $SIGNATURE,
      "idData": {
        "createdBy": $ADDRESS,
        "validUntil": $TIMESTAMP
      }
    }

    where $ADDRESS is your public deviceAddress and $TIMESTAMP is a UNIX-styled integer specifying token's expiration date.
    $SIGNATURE is an elliptic-curve signature of the idData field. Should be signed with the creator's private deviceAddress.
    We recommend to create the token locally and use it across Ambrosus API without sending your private key anywhere.

    :param account_id:
    :param secret:
    :param valid_seconds:
    :return: token
    */

    val account = this

    val idData: JsonObject = JsonObject().apply {
        addProperty("createdBy", account)
        addProperty("validUntil", currentUnixTime() + validForSeconds)
    }.sortByKeys()

    val signatureHex = idData.toShortJsonString().toEthSignature(secret)
//    val signatureBytes = signatureHex.hexStringToByteArray()

    val json: JsonObject = JsonObject().apply {
        addProperty("signature", signatureHex)
        add("idData", idData)
    }.sortByKeys()

    val jsonStr = json.toShortJsonString()

    val base64urlStr = Base64.encodeToString(
        jsonStr.toByteArray(Charset.defaultCharset()),
        Base64.NO_WRAP.or(Base64.URL_SAFE).or(Base64.NO_PADDING)
    )
    return base64urlStr

}

fun String.decodeToken(): JsonObject =
    String(Base64.decode(this, Base64.NO_WRAP.or(Base64.URL_SAFE).or(Base64.NO_PADDING))).toJson()

fun String.tokenValidUntilSeconds(): Long =
    decodeToken().getAsJsonObject("idData").getAsLongOrNull("validUntil") ?: 0L

fun String.tokenAccount(): String =
    decodeToken().getAsJsonObject("idData").getAsStringOrNull("createdBy") ?: "0x00"

fun String.tokenSignature(): String = decodeToken().getAsStringOrNull("signature") ?: "0x00"

fun <T : Any> uploadPayload(
    payloadId: String,
    payload: T,
    apiUploadingBlock: () -> Response?,
    timeoutIntervalForAllMs:Long = seconds_600,
    tryTimes:Int= 5,
    onResult:(response: Response?)->Unit
) {
    val ctrl = Controller.getInstance()
    ctrl.saveToCache(payloadId, payload)
    var retryCnt = 0
    val maxRetryCount = tryTimes
    var uploadSuccess = false
    val job = ctrl.getNewJob(Single.randomId("jsonApiJob.uploadPayload"))
    CoroutineScope(Dispatchers.IO + job).launch {
        withTimeout(timeoutIntervalForAllMs) {
            var result :Response? = null
            while (retryCnt < maxRetryCount) {
                result = apiUploadingBlock()
                if (result != null) {
                    if (result.isSuccessful) {
                        uploadSuccess = true
                        ctrl.removeFromCache(payloadId)
                        retryCnt = maxRetryCount

                    }
                }
                retryCnt+=1
            }
            onResult(result)
        }
        job.cancel()
    }
    if (!uploadSuccess) {
        // save to database and send it later
    }
}

fun uploadL1ApiPostable(
    payload: L1ApiJson_Postable,
    timeout:Long = seconds_600,
    tryTimes:Int= 5,
    onResult:(response: Response?)->Unit = {}
) {
    val ctrl = Controller.getInstance()
    val id = when(payload){
        is L1ApiJson_L1Event ->  Single.randomId("upload_payload_event",payload.eventId)
        is L1ApiJson_L2Event ->  Single.randomId("upload_payload_event",payload.eventId)
        is L1JsonApi_Asset ->   Single.randomId("upload_payload_asset")
        is L1JsonApi_Account ->   Single.randomId("upload_payload_account")
        else -> ""
    }
    if (id.isNotEmpty())
        uploadPayload(
            payloadId = id,
            payload = payload,
            apiUploadingBlock = uploadToL1ApiCall(ctrl, payload),
            timeoutIntervalForAllMs = timeout,
            tryTimes = tryTimes,
            onResult = onResult
        )
}

private fun uploadToL1ApiCall(
    ctrl: Controller,
    payload: L1ApiJson_Postable
): () -> Response? = {
    var res: Response? = null
    val job = ctrl.getNewJob(Single.randomId("jsonApiJob.uploadToL1ApiCall"))
    CoroutineScope(Dispatchers.IO + job).launch {
        res = suspendCancellableCoroutine<Response?> { cont ->
            val apiModule = ctrl.takeBestModuleForTags(TagsEnum.json_api.name)
            if (apiModule != null) {
                if (apiModule is L1JsonApi)
                    when(payload){
                        is L1ApiJson_L1Event ->  apiModule.channel.sendL1Event(payload) {cont.resume(it)}
                        is L1ApiJson_L2Event ->  apiModule.channel.sendEvent(payload) {cont.resume(it)}
                        is L1JsonApi_Asset ->  apiModule.channel.createAsset(payload) {cont.resume(it)}
                        is L1JsonApi_Account ->  apiModule.channel.createAccount(payload) {cont.resume(it)}
                    }

            } else {
                cont.resume(null)
            }
        }
        job.cancel()
    }
    res
}
