package com.crayonic.device_sdk.util

import com.crayonic.device_sdk.util.SensorCalcHelper.CONVERSION_ACC_1G_IFF_FULL_UNSIGNED_EQ_16G
import com.crayonic.device_sdk.util.SensorCalcHelper.CONVERSION_OPTICAL_POWER_TO_LUX_OF_VALUE_WATT_CM2_TO_LUX
import com.crayonic.device_sdk.util.SensorCalcHelper.decode_A_law
import com.crayonic.device_sdk.util.SensorCalcHelper.opticalPower
import org.kethereum.extensions.toMinimalByteArray
import space.lupu.koex.hex.HEX_CHARS_S
import space.lupu.koex.hex.hexStringToByteArray
import space.lupu.koex.hex.toHex
import space.lupu.koex.numbers.toByteArray
import space.lupu.koex.numbers.toInt
import space.lupu.koex.numbers.toIntFromUint8
import space.lupu.koex.numbers.toLong

import java.lang.Math.pow
import java.nio.ByteOrder
import kotlin.math.*

object SensorCalcHelper {

    // acceleration

    val ACC_1G_VALUE_IN_A_LAW: Double = 256.0
    val ACC_1G_VALUE_IN_DIRECT_ACC_10B: Double = 8.0
    val ACC_1G_VALUE_IN_DIRECT_ACC_3B: Double = 32.0

    val CONVERSION_ACC_1G_IFF_FULL_UNSIGNED_EQ_16G: Double = 4096.0

    /**
     * decode A law compressed 8 bits to 12 bits (signed 11 bits )
     * @param number of 8bits - unsigned Int
     */
    fun decode_A_law(number: Byte): Long {

        // INPUT  uint_8 number
        var temporary: Int = number.toIntFromUint8()

        // signed flag
        var isNegativeNumber = false
        if (number > 0) {
            isNegativeNumber = true
        }

        // absolute value
        temporary = temporary.and(0x7f) // leave only last 7 bits

        var decoded: Long
        val position: Int = temporary.and(0xF0).ushr(4) + 4

        if (position.equals(4)) {
            decoded = (temporary.toLong() shl 1) or 1L  // possibly missing -1 because 0 is not equal zero

        } else {
            decoded = 1L.shl(position)
                    .or((temporary and 0x0F).toLong().shl(position - 4))
                    .or(1L shl (position - 5))
        }

        // use signed flag
        return if (isNegativeNumber) {
            -decoded
        } else {
            decoded
        }

        // C pseudo code -- missing function abs(xx) - absolute number
        //int16_t ALaw_Decode(int8_t number)
        //{
        //   uint8_t sign = 0x00;
        //   uint8_t position = 0;
        //   int16_t decoded = 0;
        //   if(number&0x80)
        //   {
        //      number = abs(number);
        //      sign = -1;
        //   }
        //   position = ((number & 0xF0) >>4) + 4;
        //   if(position!=4)
        //   {
        //      decoded = ((1<<position)|((number&0x0F)<<(position-4))
        //                |(1<<(position-5)));
        //   }
        //   else
        //   {
        //      decoded = (number<<1)|1;
        //   }
        //   return (sign==0)?(decoded):(-decoded);
        //}


    }

    fun decodeAcc_10bit(_3B_msb: ByteArray, _1B_lsb: Byte): List<Double> {
        val x = (_1B_lsb.toIntFromUint8().and(48).ushr(4)
                .or(
                        _3B_msb[0].toInt().shl(2)
                )).toDouble() / ACC_1G_VALUE_IN_DIRECT_ACC_3B

        val y = (_1B_lsb.toIntFromUint8().and(12).ushr(2)
                .or(
                        _3B_msb[1].toInt().shl(2)
                )).toDouble() / ACC_1G_VALUE_IN_DIRECT_ACC_3B

        val z = (_1B_lsb.toIntFromUint8().and(3).ushr(0)
                .or(
                        _3B_msb[2].toInt().shl(2)
                )).toDouble() / ACC_1G_VALUE_IN_DIRECT_ACC_3B

        return listOf(x, y, z)
    }
//
//    /**
//     * Encode acc values to raw MSB and LSB bytes which are by defauled used by L1 devices.
//     * @return Pair(LSB, MSB) as Pair<ByteArray, ByteArray>
//     */
//    fun encodeAcc_12B(x: Double, y: Double, z: Double): Pair<ByteArray, ByteArray> {
//
//        // 10-bit number gives top at 1023 per number
//        // first bit is signed
//        // next 4 bits are significant range 0g - 16g
//        // last 5 bits are decimal
//        // basically multiply by 32 and you get the number in -512 .. +511 range
//        // then take 8 bits as MSB and 2 bits to LSB
//
//        // example
//        // msb
//        // Integer.toBinaryString((15.1*32).roundToInt().shr(2).and(0xff))
//        // lsb
//        // Integer.toBinaryString((15.1*32).roundToInt().and(3))
//        val x_msb = (x * ACC_1G_VALUE_IN_DIRECT_ACC_3B).roundToInt().shr(2).acc_10B_msb_correction(x).and(0xff)
//        var x_lsb = (x * ACC_1G_VALUE_IN_DIRECT_ACC_3B).roundToInt().and(3)
//
//        val y_msb = (y * ACC_1G_VALUE_IN_DIRECT_ACC_3B).roundToInt().shr(2).acc_10B_msb_correction(y).and(0xff)
//        var y_lsb = (y * ACC_1G_VALUE_IN_DIRECT_ACC_3B).roundToInt().and(3)
//
//        val z_msb = (z * ACC_1G_VALUE_IN_DIRECT_ACC_3B).roundToInt().shr(2).acc_10B_msb_correction(z).and(0xff)
//        var z_lsb = (z * ACC_1G_VALUE_IN_DIRECT_ACC_3B).roundToInt().and(3)
//
//        val lsb = x_lsb.shl(4).or(y_lsb.shl(2)).or(z_lsb).toMinimalByteArray()
//        val msb = ("${x_msb.takeLastBytes(1).toHex()}" +
//                "${y_msb.takeLastBytes(1).toHex()}" +
//                "${z_msb.takeLastBytes(1).toHex()}").hexStringToByteArray()
//
//        return Pair(lsb, msb)
//    }

    private fun Int.acc_10B_msb_correction(sign:Double):Int{
        if (this >= 128)
            if (sign < 0.0)
                return -128
            else
                return 127
        if (this < -127)
            return -128
        return this
    }

    // optical power and illuminance

    //Convert lux [lx] <—> watt/centimeter² (at 555 nm) [W/cm² (at 555 nm)]
    // 6830000 lux = 1 watt/centimeter² (at 555 nm)

    val CONVERSION_OPTICAL_POWER_TO_LUX_OF_VALUE_WATT_CM2_TO_LUX: Double = 6830000.0 * pow(10.0, -9.0)

    /**
     * Take 16 bits of unsigned int 16 ordered in Little Endian
     * This register contains the result of the most recent light-to-digital conversion.
     * This 16-bit register has two fields: a 4-bit exponent and a 12-bit mantissa.
     *
     * @param value - 16b Little Endian of optical power
     */
    fun opticalPower(value: ByteArray): Double {
        if (value.size >= 2) {
            // convert little endian
            val input =
                    value[1].toInt().and(0xff).shl(8)
                            .or(value[0].toInt().and(0xff))
//            Optical_Power = R[11:0] × LSB_Size
            val exponent = input.opticalPowerExponentRegister()
            val readValues = input.opticalPowerReadRegister()

            return opticalPowerEquation(
                exponent,
                readValues
            )
        }
        return 0.0
    }

    fun opticalPowerEquation(expReg: Int, readReg: Int): Double {
        return opticalPower_LSB_Size(expReg) * readReg
    }

    fun Int.opticalPowerExponentRegister(): Int {
        return this.and(0xf000).ushr(12)
    }

    fun Int.opticalPowerReadRegister(): Int {
        return this.and(0x0fff)
    }


    /**
     * Table function to get exact LSB size from exponent
     *
     * From : DataSheet - OPT3002 -  Light-to-Digital Sensor - SBOS745A – MAY 2016 – REVISED JUNE 2016
     *
     * Table 8. Result Register: Full-Scale Range (FSR) and LSB Size as a Function of Exponent Level
     *
     * E3 | E2 | E1 | E0 | FSR AT 505-nm WAVELENGTH | ( nW/cm 2 ) LSB WEIGHT AT 505-nm WAVELENGTH (nW/cm 2 )
     * 0 | 0 | 0 | 0 | 4,914 | 1.2
     * 0 | 0 | 0 | 1 | 9,828 | 2.4
     * 0 | 0 | 1 | 0 | 19,656 | 4.8
     * 0 | 0 | 1 | 1 | 39,312 | 9.6
     * 0 | 1 | 0 | 0 | 78,624 | 19.2
     * 0 | 1 | 0 | 1 | 157,248 | 38.4
     * 0 | 1 | 1 | 0 | 314,496 | 76.8
     * 0 | 1 | 1 | 1 | 628,992 | 153.6
     * 1 | 0 | 0 | 0 | 1,257,984 | 307.2
     * 1 | 0 | 0 | 1 | 2,515,968 | 614.4
     * 1 | 0 | 1 | 0 | 5,031,936 | 1,228.8
     * 1 | 0 | 1 | 1 | 10,063,872 | 2,457.6
     *
     *
     * @param exponent - range 0 to 15
     */
    fun opticalPower_LSB_Size(exponent: Int): Double {
        return if (exponent in 0..15) {
            1.2 * 2.0.pow(exponent)
        } else {
            1.2
        }
    }

    /**
     * Encode optical power from nW/m2 to raw register bytes
     * @return op in exactly 2 raw Bytes
     */
    fun encodeOpticalPowerHex(optPower: Double): String {
//        val optPower =   10063872.0
        val tests = arrayListOf<Pair<Boolean, String>>()
        var correct = "ffff"
        for (expBit in 0..15) {
            val hex = encodeOpticalPowerHexFixExp(
                optPower,
                expBit
            )
            if(hex != null)
                return  hex
        }
        return correct
    }
    fun encodeOpticalPowerHexFixExp(optPower: Double, expByte:Int): String? {
//        val optPower =   10063872.0
        val tests = arrayListOf<Pair<Boolean, String>>()
        val lsbSize = opticalPower_LSB_Size(expByte)
        var fsr = (optPower / lsbSize).toInt().toMinimalByteArray()
        if (fsr.size == 1) {
            fsr = ByteArray(1) + fsr
        }
        val hex = HEX_CHARS_S[expByte] + fsr.toHex().takeLast(3).toString()
        val condition = fsr.size <= 2 && fsr.toHex().first().equals('0')
        tests.add(Pair(condition, hex))
        if (condition) {
            return hex
        }
        return null
    }

    fun encodeOpticalPower(optPower: Double): ByteArray {
        return encodeOpticalPowerHex(optPower).hexStringToByteArray().reversedArray()
    }

    fun encodeOpticalPowerThresholds(op:Double):ByteArray{
        var lo = op.roundToLong().absoluteValue
        if(lo > 0xffffffff){
            lo = 0xffffffff
        }
        return lo.toByteArray().takeLast(4).toByteArray()
    }

//
//
//        for (expBit in 0..11) {
//            val lsbSize = opticalPower_LSB_Size(expBit)
//            var compressedValue: Long = optPower.toRawBits()
//            val res = (optPower / lsbSize).toInt().toMinimalByteArray()
//            if (   res.size <= 2 && res.toHex().first().equals('0') )
//                val expHex = "${HEX_CHARS_S[expBit]}000".hexStringToByteArray().fromBigEndian2Int()
//                return compressedValue
//                        .takeLastBytes(2) // take last four bytes
//                        .fromBigEndian2Int()
//                        .or(expHex) // bitwise or
//                        .takeLastBytes(2)
//                        .reversedArray()
//            }
//
//        }
//        // fallback - number`s too big?
//        return 0xbfff.takeLastBytes(2).reversedArray()
//}


    /**
     * Decode raw temperature to human readable format
     * @return huma readable temperature in range :
     */
    fun decodeTemperature(byte: Byte): Double {
        return (byte.toInt() + 32).toDouble() / 2.0
    }

    /**
     * Encode human readable temperature to raw bytes by default used by L1 devices.
     * @return raw temperature byte
     */
    fun encodeTemperature(temp: Double): Byte {
        /*
        tested
        ((-48.0-16.0)*2.0)  == -128.0
        ((79.5-16.0)*2.0)   == 127.0
         */

        val numeric = ((temp - 16.0) * 2.0).toInt()
        return numeric.toByte()
    }

    val SENSOR_MULT_TEMP = 100.0

    /**
     * Encode human readable temperature to raw bytes by default used by L1 devices.
     * @return raw temperature byte
     */
    fun encodeTemperatureForThresholds(temp: Double): ByteArray {
        /*
        tested by

        val t = 150.0

        var numeric = ( t*10 ).roundToInt()
        val min = -32767
        val max = 32767
        numeric = min(max, numeric)
        numeric = max(min, numeric)

        val bytes = numeric.toMinimalByteArray().takeLast(2).toByteArray()
        bytes.toHex()

        var n = bytes.fromBigEndian2Int()
        if(n > 0x8000)
            n = n - 0x010000
        n / 10.0

         */
        var numeric = ( temp* SENSOR_MULT_TEMP).roundToInt()
        val min = -32767
        val max = 32767
        numeric = min(max, numeric)
        numeric = max(min, numeric)

        val bytes = numeric.toByteArray().takeLast(2).toByteArray()
        return bytes
    }

    /**
     * Decode human readable temperature to raw bytes by default used by L1 devices.
     * @return raw temperature byte
     */
    fun decodeTemperatureForThresholds(bytes: ByteArray): Double{
        /*
        tested
        ((-48.0-16.0)*2.0)  == -128.0
        ((79.5-16.0)*2.0)   == 127.0
         */
        if(bytes.size == 0){
            return 0.0
        }

        var n = bytes.shortBytesToInt()
        return n / SENSOR_MULT_TEMP
    }

    private fun ByteArray.shortBytesToInt(): Int{
        var n = this.toInt(ByteOrder.BIG_ENDIAN)
        if(n > 0x8000)
            n = n - 0x010000
        return n
    }

    /**
     * Encode humidity to raw byte representing range 0 - 100
     * @return raw humidity byte
     */
    fun encodeHumidity(percentage: Int): ByteArray {
        if(percentage == 0)
            return "0000".hexStringToByteArray()
        return (percentage.toMinimalByteArray().take(1).toByteArray().toHex()+"00").hexStringToByteArray()
    }

    fun decodeHumidity(bytes: ByteArray): Int {
        return bytes.toInt(ByteOrder.LITTLE_ENDIAN)
    }


    /**
     * Decode human readable shock in range <0, 25.5> G to raw bytes by default used by L1 devices.
     * @return raw shock bytes
     */
//    fun decodeShock(bytes: ByteArray): Double{
//        if(bytes.size == 0){
//            return 0.0
//        }
//
//        val number = bytes.first().toIntFromUint8()
//        return number / 10.0
//    }

    val SENSOR_MULT_SHOCK = 100.0

    fun decodeShock(bytes: ByteArray): Double{
        if(bytes.size == 0){
            return 0.0
        }

        val number = bytes.toInt(ByteOrder.LITTLE_ENDIAN).toDouble() / SENSOR_MULT_SHOCK
        return number
    }

    /**
     * Encode human readable shock in range <0, 25.5> G to raw bytes by default used by L1 devices.
     * @return raw shock bytes
     */
    fun encodeShock(shock: Double): ByteArray {
        var number = (shock * SENSOR_MULT_SHOCK).absoluteValue.roundToInt()
//        if(number > 255){
//            number = 255
//        }
        return number.toByteArray().takeLast(2).toByteArray()
    }

    fun encodeShockLittleEndian(shock:Double):ByteArray{
        return encodeShock(shock).reversedArray()
    }

    /**
     * Decode human readable incline in range <-180, 180> G to raw bytes by default used by L1 devices.
     * @return raw shock bytes
     */
    fun decodeIncline(bytes: ByteArray): List<Double>{
        val x = bytes.copyOfRange(0,2).shortBytesToInt().toDouble()
        val y = bytes.copyOfRange(2,4).shortBytesToInt().toDouble()
        val z = bytes.copyOfRange(4,6).shortBytesToInt().toDouble()
        return listOf(x,y,z)
    }
    fun decodeInclineLittleEndian(bytes: ByteArray): List<Double>{
        val x = bytes.copyOfRange(0,2).reversedArray().shortBytesToInt().toDouble()
        val y = bytes.copyOfRange(2,4).reversedArray().shortBytesToInt().toDouble()
        val z = bytes.copyOfRange(4,6).reversedArray().shortBytesToInt().toDouble()
        return listOf(x,y,z)
    }

    /**
     * Encode human readable incline in range <-180, 180> G to raw bytes by default used by L1 devices.
     * @return raw shock bytes
     */
    fun encodeIncline(incline_x: Double, incline_y: Double, incline_z: Double): ByteArray {
        var x = incline_x.limitIncline().toByteArray().takeLast(2).toByteArray()
        var y = incline_y.limitIncline().toByteArray().takeLast(2).toByteArray()
        var z = incline_z.limitIncline().toByteArray().takeLast(2).toByteArray()
        return x + y + z
    }
    fun encodeInclineLittle(incline_x: Double, incline_y: Double, incline_z: Double): ByteArray {
        var x = incline_x.limitIncline().toByteArray().takeLast(2).toByteArray().reversedArray()
        var y = incline_y.limitIncline().toByteArray().takeLast(2).toByteArray().reversedArray()
        var z = incline_z.limitIncline().toByteArray().takeLast(2).toByteArray().reversedArray()
        return x + y + z
    }

    fun Double.limitIncline():Int{
        var number = this.roundToInt()
        if(number > 180){
            number = 180
        }
        if(number < -180){
            number = -180
        }
        return number
    }

}


fun Byte.toAcceleration(): Double {
    val dalaw = decode_A_law(this)
    //Log.d("Conversion", "ByteArray.toAcceleration()_in:"+this.toHex()+":dalaw:"+dalaw+":result:"+ dalaw.toDouble()/ CONVERSION_ACC_1G_IFF_FULL_UNSIGNED_EQ_16G)
    return dalaw.toDouble() / CONVERSION_ACC_1G_IFF_FULL_UNSIGNED_EQ_16G

}

fun ByteArray.toLux(): Double {
    val op = opticalPower(this)
    //Log.d("Conversion", "ByteArray.toLux()_in:"+this.toHex()+":op:"+op+":lux:"+ op* CONVERSION_OPTICAL_POWER_TO_LUX_OF_VALUE_WATT_CM2_TO_LUX)
    return op * CONVERSION_OPTICAL_POWER_TO_LUX_OF_VALUE_WATT_CM2_TO_LUX
}

fun ByteArray.toOp(): Double {
    val op = opticalPower(this)
    //Log.d("Conversion", "ByteArray.toLux()_in:"+this.toHex()+":op:"+op+":lux:"+ op* CONVERSION_OPTICAL_POWER_TO_LUX_OF_VALUE_WATT_CM2_TO_LUX)
    return op
}
fun ByteArray.toOpThresholds(): Double {
    val op = this.toLong(ByteOrder.BIG_ENDIAN).toDouble()
    //Log.d("Conversion", "ByteArray.toLux()_in:"+this.toHex()+":op:"+op+":lux:"+ op* CONVERSION_OPTICAL_POWER_TO_LUX_OF_VALUE_WATT_CM2_TO_LUX)
    return op
}