package com.crayonic.device_sdk.ble

import android.util.Log
import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.device.channels.CrayonicBleMemberChannel
import com.crayonic.device_sdk.device.channels.CrayonicDeviceChannel
import com.crayonic.device_sdk.device.channels.CrayonicMemberChannel
import com.crayonic.device_sdk.device.errors.CrayonicDeviceError
import com.crayonic.device_sdk.device.errors.DeviceOutOfReachError
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.util.TagsEnum
import com.crayonic.device_sdk.util.runOnBleStack
import space.lupu.koex.hex.toHexPrefixed0xSeparated
import java.nio.charset.Charset

/**
 * Implements ble stack interaction
 */
open class BleCrayonicDeviceChannel(
    val deviceAddress: String, // set by device provider after the node is found and created
    val rxTxWriteCharacteristic: String,
    val rxTxNotifyCharacteristic: String,
    supportedCommands: List<CrayonicRequestCode>,
    memberChannel: CrayonicBleMemberChannel
) : CrayonicDeviceChannel(
    Single.cacheId("bleCrayoDevCha"),
    listOf(TagsEnum.l1, TagsEnum.apdu),
    supportedCommands,
    memberChannel
) {
    private val TAG = "BleCrayoDevCha"
    // implements
    override fun send(
        payload: ByteArray,
        onReceived: (ByteArray) -> Unit
    ) {
        runOnBleStack(onBleStackFail(), { bleStack ->
            Log.e(TAG, "send bytes to ble stack : ${payload.toHexPrefixed0xSeparated()}")
            bleStack.writeCharacteristic(
                deviceAddress,
                rxTxWriteCharacteristic,
                rxTxNotifyCharacteristic,
                onCommunicationFail(rxTxNotifyCharacteristic, rxTxWriteCharacteristic),
                payload
            ) { bytes: ByteArray ->
                Log.e(TAG, "received bytes from ble stack : ${bytes.toHexPrefixed0xSeparated()}")
                onReceived(bytes)
            }
        })
    }

    private fun onBleStackFail(): (String) -> Unit = { reason ->
        registeredOnError?.invoke(
            CrayonicDeviceError(
                "Application level Ble Stack failure : $reason"
            )
        )
    }
    private fun onCommunicationFail(nofChar:String, wrChar: String): (String) -> Unit = { reason ->
        registeredOnError?.invoke(
            DeviceOutOfReachError(
                "Failed writing characteristic $wrChar( with notification on $nofChar ) channel: $this. Reason : $reason"
            )
        )
    }



}