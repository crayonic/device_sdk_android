package com.crayonic.device_sdk.ble

import com.crayonic.device_sdk.ble.BleDeviceRequiredCharSetup.SubServiceHolder
import com.crayonic.device_sdk.util.TagsEnum

object BleDeviceCharacteristics {
    //////////////////
    // Generic
    val Generic_Access_Service = "00001800-0000-1000-8000-00805f9b34fb"
    val Generic_Access_Read_DeviceName = "00002a00-0000-1000-8000-00805f9b34fb"
    val Generic_Access_Read_Appearance = "00002a01-0000-1000-8000-00805f9b34fb"

    val Generic_Battery_Service = "0000180f-0000-1000-8000-00805f9b34fb"
    val Generic_Battery_Read_Level = "00002a19-0000-1000-8000-00805f9b34fb"
    val Generic_Battery_Descriptor_CCCD = "00002902-0000-1000-8000-00805f9b34fb"

    val Generic_DeviceInfo_Service = "0000180a-0000-1000-8000-00805f9b34fb"
    val Generic_DeviceInfo_Read_FwVersion = "00002a26-0000-1000-8000-00805f9b34fb"
    val Generic_DeviceInfo_Read_ModelInfo = "00002a24-0000-1000-8000-00805f9b34fb"
    val Generic_DeviceInfo_Read_HwRevision = "00002a27-0000-1000-8000-00805f9b34fb"

    val Generic_Access_Service_Reads =
//        Generic_Access_Service to
                listOf(
            Generic_Access_Read_DeviceName,
            Generic_Access_Read_Appearance
        )
    val Generic_Battery_Service_Reads =
//        Generic_Battery_Service to
                listOf(Generic_Battery_Read_Level)
    val Generic_DevInfo_Service_Reads =
//        Generic_DeviceInfo_Service to
                listOf(
                    Generic_DeviceInfo_Read_FwVersion,
                    Generic_DeviceInfo_Read_ModelInfo
//                    Generic_DeviceInfo_Read_HwRevision
                )

    /////////////////
    // L1
    val L1_RxTx_Service = "9eb70001-8c04-4c98-ae44-2ca32bfa549a"
    val L1_RxTx_Write = "9eb70002-8c04-4c98-ae44-2ca32bfa549a"
    val L1_RxTx_Notify = "9eb70003-8c04-4c98-ae44-2ca32bfa549a"
    val L1_RxTx_Descriptor_CCCD = "2902"

    val L1_State_Service = "9eb7fd00-8c04-4c98-ae44-2ca32bfa549a"
    val L1_State_Read_EthAddress = "9eb7d001-8c04-4c98-ae44-2ca32bfa549a"
    val L1_State_Read_AssetId = "9eb7d002-8c04-4c98-ae44-2ca32bfa549a"
    val L1_State_Read_Temperature = "9eb7d011-8c04-4c98-ae44-2ca32bfa549a"
    val L1_State_Read_Humidity = "9eb7d012-8c04-4c98-ae44-2ca32bfa549a"
    val L1_State_Read_Light = "9eb7d014-8c04-4c98-ae44-2ca32bfa549a"
    val L1_State_Read_Shock = "9eb7d015-8c04-4c98-ae44-2ca32bfa549a"
    val L1_State_Read_Incline = "9eb7d016-8c04-4c98-ae44-2ca32bfa549a" // not implemented as of fw version 2.6
    val L1_State_Value_Descriptor = "2901"

    val L1_BleReqCharsSetup = BleDeviceRequiredCharSetup(listOf(
        SubServiceHolder(Generic_Access_Service, Generic_Access_Service_Reads, listOf(), listOf()),
        SubServiceHolder(Generic_Battery_Service, Generic_Battery_Service_Reads, listOf(), listOf()),
        SubServiceHolder(Generic_DeviceInfo_Service, Generic_DevInfo_Service_Reads, listOf(), listOf()),
        SubServiceHolder(L1_RxTx_Service, listOf(), listOf(L1_RxTx_Write), listOf(L1_RxTx_Notify)),
        SubServiceHolder(L1_State_Service, listOf(
            L1_State_Read_EthAddress,
            L1_State_Read_AssetId,
            L1_State_Read_Temperature,
            L1_State_Read_Humidity,
            L1_State_Read_Light,
            L1_State_Read_Shock
//            ,         L1_State_Read_Incline
        ), listOf(), listOf())
        )
    )

    val L1_mainService = L1_RxTx_Service
    //////////////////
    // Pen
    val Pen_RxTx_Service = "0bs51666-e7cb-469b-8e4d-2742f1ba77cc"
    val Pen_RxTx_Write = "9eb70002-8c04-4c98-ae44-2ca32bfa549a"
    val Pen_RxTx_Notify = "9eb70003-8c04-4c98-ae44-2ca32bfa549a"

    val Pen_BleReqCharsSetup = BleDeviceRequiredCharSetup(listOf(
        SubServiceHolder(Generic_Access_Service, Generic_Access_Service_Reads, listOf(), listOf()),
        SubServiceHolder(Generic_Battery_Service, Generic_Battery_Service_Reads, listOf(), listOf()),
//        SubServiceHolder(Generic_DeviceInfo_Service, Generic_DevInfo_Service_Reads, listOf(), listOf()),
        SubServiceHolder(Pen_RxTx_Service, listOf(), listOf(Pen_RxTx_Write), listOf(Pen_RxTx_Notify))
    ))
    val Pen_mainService = Pen_RxTx_Service
    //////////////////
    // DFU OTA
    val Dfu_Ota_Service = "0000fe59-0000-1000-8000-00805f9b34fb"

    val Dfu_mainService = Dfu_Ota_Service

    //////////////////
    // Mapping of all the services
//    val MapServicesTree = mapOf<String, List<String>>(
//        L1_RxTx_Service to listOf(L1_RxTx_Write, L1_RxTx_Notify),
//        L1_State_Service to listOf(
//            L1_State_Read_EthAddress,
//            L1_State_Read_AssetId,
//            L1_State_Read_Temperature,
//            L1_State_Read_Humidity,
//            L1_State_Read_Light,
//            L1_State_Read_Shock,
////            L1_State_Read_Incline,
//            L1_State_Value_Descriptor
//        ),
//        Pen_RxTx_Service to listOf(Pen_RxTx_Write, Pen_RxTx_Notify),
//        Dfu_Ota_Service to listOf(),
//        Generic_Access_Service to listOf(
//            Generic_Access_Read_DeviceName,
//            Generic_Access_Read_Appearance
//        ),
//        Generic_Battery_Service to listOf(
//            Generic_Battery_Read_Level,
//            Generic_Battery_Descriptor_CCCD
//        ),
//        Generic_DeviceInfo_Service to listOf(
//            Generic_DeviceInfo_Read_FwVersion,
//            Generic_DeviceInfo_Read_HwRevision,
//            Generic_DeviceInfo_Read_ModelInfo
//        )
//    )

    val L1_map_key = TagsEnum.l1.name
    val Pen_map_key = TagsEnum.pen.name
    val DfuTarg_map_key = TagsEnum.dfu.name

    val MapMainServicesByDevice = mapOf<String, String>(
        L1_map_key to L1_mainService,
        Pen_map_key to Pen_mainService,
        DfuTarg_map_key to Dfu_mainService
    )
    //////////////////
    // convenience definitions
    val BleChars = this
}
