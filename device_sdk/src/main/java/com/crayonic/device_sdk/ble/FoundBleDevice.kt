package com.crayonic.device_sdk.ble

import com.crayonic.device_sdk.ble.BleDeviceCharacteristics.BleChars
import com.crayonic.device_sdk.ble.BleDeviceNode.Companion.defaultChannel

data class FoundBleDevice(
    var address:String,
    var name:String,
    var bluetoothClass:String,
    var bondState:String,
    var services:List<String>
){
    fun isPen(): Boolean = services.contains(BleChars.Pen_mainService)
    fun toPen():BleDeviceNodePen = BleDeviceNodePen(this)

    fun isL1(): Boolean = services.contains(BleChars.L1_mainService)
    fun toL1():BleDeviceNodeL1 = BleDeviceNodeL1(this)

    @Suppress("UNCHECKED_CAST")
    fun toDeviceNode():BleDeviceNode<BleCrayonicDeviceChannel> = when{
        isL1() -> toL1() as BleDeviceNode<BleCrayonicDeviceChannel>
        isPen() -> toPen() as BleDeviceNode<BleCrayonicDeviceChannel>
//        isDfu() -> toDfu() as BleDeviceNode<BleCrayonicDeviceChannel>
        else -> BleDeviceNode(this, defaultChannel(address))
    }

    constructor(
        address:String,
        name:String,
        bluetoothClass:String,
        bondState:String,
        vararg uuids:String
    ):this(address, name, bluetoothClass, bondState, uuids.toList())

    constructor():this("","","","", listOf())
}
