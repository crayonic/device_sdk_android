package com.crayonic.device_sdk.ble

import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.device.CrayonicDeviceNode
import com.crayonic.device_sdk.device.DeviceFilter
import com.crayonic.device_sdk.device.DevicesModule
import com.crayonic.device_sdk.device.DevicesProvider
import com.crayonic.device_sdk.util.TagsEnum
import space.lupu.konnector.tags.Tags

class BleDevicesModule:DevicesModule(
    Single.cacheId("bleDeviceModule"), 10
){
    init {
        safeAttachOfMultipleProviders(BleDeviceProvider())
    }
}