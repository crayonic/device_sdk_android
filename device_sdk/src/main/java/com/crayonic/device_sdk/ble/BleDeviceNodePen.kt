package com.crayonic.device_sdk.ble

import com.crayonic.device_sdk.ble.BleDeviceCharacteristics.BleChars
import com.crayonic.device_sdk.device.channels.CrayonicBleMemberChannel
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode.*
import com.crayonic.device_sdk.device.pen.CrayonicPen
import com.crayonic.device_sdk.device.CrayonicDevice


class BleDeviceNodePen(foundBleDevice: FoundBleDevice) :
    BleDeviceNode<BleDeviceNodePen.BleDeviceChannelPen>(
        foundBleDevice,
        BleDeviceChannelPen(
            foundBleDevice.address,
            CrayonicBleMemberChannel(foundBleDevice.address)
        )
    ) {

    constructor(
        deviceAddress: String,
        name: String,
        bluetoothClass: String,
        bondState: String,
        services: List<String>
    ) : this(
        FoundBleDevice(deviceAddress, name, bluetoothClass, bondState, services)
    )

    override fun toCrayonicDevice(): CrayonicDevice {
        return CrayonicPen(this)
    }

    override fun provideNodeName(): String = name

    override fun provideNodeAddress(): String = deviceAddress

    override fun provideNodeServices(): List<String> = services

    fun toFoundBleDevice(): FoundBleDevice =
        FoundBleDevice(deviceAddress, name, bluetoothClass, bondState, services)

    class BleDeviceChannelPen(deviceAddress: String, memberChannelL1: CrayonicBleMemberChannel) :
        BleCrayonicDeviceChannel(
            deviceAddress,
            BleChars.Pen_RxTx_Write, BleChars.Pen_RxTx_Notify,
            supportedCommands,
            memberChannelL1
        )

    companion object {
        val supportedCommands = listOf<CrayonicRequestCode>(
            // all devices
            connectDevice,
            disconnectDevice,
            readDeviceAddress,
            readDeviceName,
            readBatteryLevel,
            readModelNumber,
            readFirmwareVersion,

            runCommandRawBytes,

            runCommandReadClock,
            runCommandWriteClock,
            runCommandReadPublicKey,
            runCommandReadEthAddress,
            runCommandResetToPowerOff,
            runCommandResetToUpdateMode,

            runTaskUpdateDevice,

            // Pen requests
//            runCommandSetPinCode,
            runCommandSignEthTx,
            runTaskInitializePen,
            runTaskReadData
        )
    }
}