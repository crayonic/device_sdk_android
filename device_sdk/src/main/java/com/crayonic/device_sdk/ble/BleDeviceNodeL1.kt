package com.crayonic.device_sdk.ble

import com.crayonic.device_sdk.ble.BleDeviceCharacteristics.BleChars
import com.crayonic.device_sdk.device.channels.CrayonicBleMemberChannelL1
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode.*
import com.crayonic.device_sdk.device.CrayonicDevice
import com.crayonic.device_sdk.device.CrayonicL1

class BleDeviceNodeL1(foundBleDevice: FoundBleDevice) :
    BleDeviceNode<BleDeviceNodeL1.BleDeviceChannelL1>(
        foundBleDevice,
        BleDeviceChannelL1(
            foundBleDevice.address,
            CrayonicBleMemberChannelL1(foundBleDevice.address)
        )
    ) {

    constructor(
        deviceAddress: String,
        name: String,
        bluetoothClass: String,
        bondState: String,
        services: List<String>
    ) : this(
        FoundBleDevice(deviceAddress, name, bluetoothClass, bondState, services)
    )


    override fun toCrayonicDevice(): CrayonicDevice {
        return CrayonicL1(this)
    }

    override fun provideNodeName(): String = name

    override fun provideNodeAddress(): String = deviceAddress

    override fun provideNodeServices(): List<String> = services

    fun toFoundBleDevice(): FoundBleDevice =
        FoundBleDevice(deviceAddress, name, bluetoothClass, bondState, services)

    class BleDeviceChannelL1(deviceAddress: String, memberChannelL1: CrayonicBleMemberChannelL1) :
        BleCrayonicDeviceChannel(
            deviceAddress,
            BleChars.L1_RxTx_Write, BleChars.L1_RxTx_Notify,
            supportedCommands,
            memberChannelL1
        )

    companion object {
        val supportedCommands = listOf<CrayonicRequestCode>(
            // all devices
            connectDevice,
            disconnectDevice,
            readDeviceAddress,
            readDeviceName,
            readBatteryLevel,
            readModelNumber,
            readFirmwareVersion,

            runCommandRawBytes,

            runCommandReadClock,

            runCommandWriteClock,
            runCommandReadPublicKey,
            runCommandReadEthAddress,
            runCommandResetToPowerOff,
            runCommandResetToUpdateMode,
            runTaskUpdateDevice,
            // L1 requests
            readEthAddress,
            readAssetId,
            readTemperature,
            readHumidity,
            readLight,
            readShock,
//            readIncline,
            runCommandReadAlarmZones,
            runCommandWriteAlarmZone,
            runCommandReadAssetId,
            runCommandWriteAssetId,
            runCommandReadPackageId,
            runCommandWritePackageId,
            runCommandSetManagementPinCode,
            runTaskReadEvents,
            runTaskInitialization,
            runTaskFactorySetup,
            runTaskL1DeviceTest
        )
    }
}