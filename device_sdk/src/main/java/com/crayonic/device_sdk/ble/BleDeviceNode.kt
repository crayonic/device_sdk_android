package com.crayonic.device_sdk.ble

import android.util.Log
import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.device.CrayonicDeviceNode
import com.crayonic.device_sdk.device.channels.CrayonicBleMemberChannel
import com.crayonic.device_sdk.device.errors.DeviceOutOfReachError
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.util.runOnBleStack
import space.lupu.konnector.nodes.Channel
import space.lupu.konnector.nodes.Node
import space.lupu.konnector.tags.Tags
import space.lupu.konnector.tags.toTags


open class BleDeviceNode<CH : BleCrayonicDeviceChannel>(
    var deviceAddress: String,
    var name: String,
    var bluetoothClass: String,
    var bondState: String,
    var services: List<String>,
    val channel: CH,
    tag: List<String>
) : CrayonicDeviceNode(
    Single.cacheId("bleDeviceNode"),
    Tags(tag.toTags())
) {
    constructor(
        deviceAddress: String,
        name: String,
        bluetoothClass: String,
        bondState: String,
        services: List<String>,
        channel: CH,
        vararg tags: String
    ) : this(
        deviceAddress,
        name,
        bluetoothClass,
        bondState,
        services,
        channel,
        tags.toList()
    )

    constructor(foundBleDevice: FoundBleDevice, channel: CH) : this(
        foundBleDevice.address,
        foundBleDevice.name,
        foundBleDevice.bluetoothClass,
        foundBleDevice.bondState,
        foundBleDevice.services,
        channel
    )

    override fun hashCode(): Int {
        return tags.toString().hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if(other == null ) return false
        if (other is BleDeviceNode<*>){
            return tags.toString().equals(other.tags.toString())
        }
        return false
    }

    override fun provideNodeName(): String = name

    override fun provideNodeAddress(): String = deviceAddress

    override fun provideNodeServices(): List<String> = services


    init {
        (channel.memberChannel as CrayonicBleMemberChannel).parentChannel = channel
        channel.memberChannel.parentNode = this
    }


    init {
        tags.addAll(
            arrayListOf(deviceAddress, name).apply { addAll(services) }.filter { it.isNotEmpty() }
        )
    }

    override fun connect(
        onChannelOpened: (Node, Channel) -> Unit,
        onNodeDisconnected: (Node) -> Unit
    ) {
        runOnBleStack(
            onFail = { reason ->
                connectionErrorDisconnect(reason, onNodeDisconnected)
            }) { bleStack ->
                bleStack.connect(
                    bleAddress = deviceAddress,
                    onConnected = {
                        onChannelOpened(this, channel)
                    },
                    onError = { reason ->
                        connectionErrorDisconnect("Connection error detected : $reason", onNodeDisconnected)
//                        connectionError("Connection error detected : $reason")
                    },
                    onDisconnected = {
                        onNodeDisconnected(this)
                    }
            )
        }
    }

    override fun disconnect(onNodeDisconnected: (Node) -> Unit) {
        Log.i(TAG, "BLE Dev Node disconnect initiated")
        runOnBleStack(
            onFail = { reason ->
                connectionErrorDisconnect(reason, onNodeDisconnected)
            },
            action = { bleStack ->
                bleStack.disconnect(deviceAddress) {
                onNodeDisconnected(this)
            }
        })
    }


    private fun connectionError(reason: String) {
        val msg = "Cannot connect to node $this : $reason"
        Log.e(TAG, msg)
        registeredOnError?.invoke(DeviceOutOfReachError(msg))

    }
    private fun connectionErrorDisconnect(reason: String, onNodeDisconnected: (Node) -> Unit) {
        connectionError(reason)
        onNodeDisconnected(this)
    }

    override val isDfuUpdateSupported = false

    companion object {
        val TAG = "BleDeviceNode"
        fun defaultChannel(deviceAddress: String) = BleCrayonicDeviceChannel(
            deviceAddress,
            "",
            "",
            listOf(
                CrayonicRequestCode.connectDevice,
                CrayonicRequestCode.disconnectDevice,
                CrayonicRequestCode.readDeviceAddress,
                CrayonicRequestCode.readDeviceName,
                CrayonicRequestCode.readBatteryLevel,
                CrayonicRequestCode.readModelNumber,
                CrayonicRequestCode.readFirmwareVersion,

                CrayonicRequestCode.runCommandReadClock,
                CrayonicRequestCode.runCommandReadPublicKey,
                CrayonicRequestCode.runCommandReadEthAddress,

                CrayonicRequestCode.runTaskUpdateDevice,
                CrayonicRequestCode.runCommandResetToPowerOff,
                CrayonicRequestCode.runCommandResetToUpdateMode,

                CrayonicRequestCode.runCommandRawBytes

            ),
            CrayonicBleMemberChannel(deviceAddress)
        )
    }
}

