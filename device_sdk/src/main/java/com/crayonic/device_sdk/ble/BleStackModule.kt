package com.crayonic.device_sdk.ble

import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.util.TagsEnum
import com.crayonic.device_sdk.util.appendToList
import com.crayonic.device_sdk.util.initializeTags
import space.lupu.konnector.impl.one_node_module.FunctionalityChannel
import space.lupu.konnector.impl.one_node_module.OneNodeModule

open class BleStackModule<CH: BleStackModule.BleStackChannel>(
    channel:CH,
    id: String,
    modulePriority: Int,
    specificTags: List<String>
) : OneNodeModule<CH>(
    channel,
    id,
    modulePriority,
    specificTags.appendToList(TagsEnum.module, TagsEnum.ble_stack_module, TagsEnum.ble_stack),
    specificTags.appendToList(TagsEnum.provider, TagsEnum.ble_stack_provider, TagsEnum.ble_stack),
    specificTags.appendToList(TagsEnum.node, TagsEnum.ble_stack_node, TagsEnum.ble_stack)
){

    constructor(channel: CH, modulePriority: Int, tags: List<String>):this(
        channel = channel,
        modulePriority = modulePriority,
        id = Single.cacheId("bleStackModule"),
        specificTags = tags
    )
    constructor(channel: CH, modulePriority: Int, vararg tags :String):this(
        channel = channel,
        modulePriority = modulePriority,
        tags = tags.toList()
    )

    // implement BleStackChannel with gatt functionality and connection identifications
    abstract class BleStackChannel(id: String, specificTags: List<String>
    ):FunctionalityChannel(
        id,
        specificTags.initializeTags(
            TagsEnum.channel,
            TagsEnum.ble_stack_channel,
            TagsEnum.ble_stack
        )
        ){

        constructor(vararg tags :String):this(tags.toList())
        constructor(tags :List<String>):this(Single.cacheId("bleStackChannel"), tags)

        // used by node
        abstract fun connect(bleAddress: String, onConnected:()->Unit, onError:(reason:String)->Unit, onDisconnected: () -> Unit)
        abstract fun disconnect(bleAddress: String, onDisconnected: () -> Unit)
        // used by channel
        abstract fun readCharacteristic(bleAddress: String, serviceChar:String, onFail: (reason: String) -> Unit, onResult:(ByteArray)->Unit)
//        abstract fun enableNotificationCharacteristic(bleAddress: String, serviceChar:String, onFail: (reason: String) -> Unit, onResult:()->Unit)
        abstract fun writeCharacteristic(bleAddress: String, serviceCharSend:String, serviceCharReceive:String, onFail: (reason: String) -> Unit, bytesToSend:ByteArray, onResult:(ByteArray)->Unit)

        // search for devices
        abstract fun findBleDevices(onFound:(foundBleDevs: List<FoundBleDevice>) -> Unit)
        abstract fun stopAllSearches()
    }

    // gatt functions that can be called from node

    fun connect(bleAddress: String, onConnected:()->Unit, onError:(reason:String)->Unit, onDisconnected: () -> Unit)=
        channel.connect(bleAddress, onConnected, onError, onDisconnected)

    fun disconnect(bleAddress: String, onDisconnected: () -> Unit)=
        channel.disconnect(bleAddress, onDisconnected)

    // used by member channel to read gatt member values
    fun readCharacteristic(bleAddress: String, serviceChar:String, onFail: (reason: String) -> Unit, onResult:(ByteArray)->Unit) =
        channel.readCharacteristic(bleAddress, serviceChar, onFail, onResult)

    // used by apdu channel to send bytes
//    fun writeWithEnabledNotifications(bleAddress: String, characteristicNotify:String, characteristicWrite:String, onFail: (reason: String) -> Unit, onResult:(ByteArray)->Unit){
//        enableNotificationCharacteristic(bleAddress, characteristicNotify, onFail){
//            writeCharacteristic(bleAddress, characteristicWrite, onFail, onResult)
//        }
//    }

//    fun enableNotificationCharacteristic(bleAddress: String, characteristic: String, onFail: (reason: String) -> Unit, onResult:()->Unit) =
//        channel.enableNotificationCharacteristic(bleAddress, characteristic, onFail, onResult)

    fun writeCharacteristic(bleAddress: String, characteristicWrite: String, characteristicNotify: String, onFail: (reason: String) -> Unit, sendBytes:ByteArray, onResult:(ByteArray)->Unit)=
        channel.writeCharacteristic(bleAddress, characteristicWrite, characteristicNotify, onFail, sendBytes, onResult)

    // searching for all ble devices around
    // filtering is done in provider
    fun findBleDevices(onFound:(foundBleDevs: List<FoundBleDevice>) -> Unit) =
        channel.findBleDevices(onFound)

    fun stopSearchingForDevices() = channel.stopAllSearches()
}