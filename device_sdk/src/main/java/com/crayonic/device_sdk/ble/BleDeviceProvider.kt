package com.crayonic.device_sdk.ble

import android.util.Log
import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.device.CrayonicDeviceNode
import com.crayonic.device_sdk.device.DeviceFilter
import com.crayonic.device_sdk.device.DevicesProvider
import com.crayonic.device_sdk.util.TagsEnum
import com.crayonic.device_sdk.util.runOnBleStack
import space.lupu.konnector.tags.Tags

class BleDeviceProvider: DevicesProvider(
    Single.cacheId("ble_device_provider"),
    TagsEnum.ble.name,
    TagsEnum.device.name
    ){

    private fun onBleStackFail(): (String) -> Unit = { reason ->
//        val msg= "Application level Ble Stack failure : $reason"
//        somecallback.invoke(
//            CrayonicDeviceError( msg )
//        )
    }

    @Suppress("UNCHECKED_CAST")
    fun Collection<CrayonicDeviceNode>.filter(filter: DeviceFilter) =
        filter.filterNodes(this) as List<CrayonicDeviceNode>

    fun Collection<FoundBleDevice>.transformToNodes():Collection<BleDeviceNode<BleCrayonicDeviceChannel>> =
        this.map{ foundBleDevice: FoundBleDevice ->
            val node = foundBleDevice.toDeviceNode()
            node.name = foundBleDevice.name
            node.deviceAddress = foundBleDevice.address
            when {
                foundBleDevice.isL1() -> node.tags.addAll("L1", TagsEnum.l1.name)
                foundBleDevice.isPen() -> node.tags.addAll("Pen", TagsEnum.pen.name)
            }
            node.tags.addAll(node.name, node.deviceAddress)
            node
        }


    override fun listDeviceNodes(
        filter: DeviceFilter,
        onNodesListed: (nodes: List<CrayonicDeviceNode>, originFilter: DeviceFilter) -> Unit,
        deviceTags: Tags
    ): Boolean {
        return runOnBleStack(onBleStackFail(), { bleStack ->
            bleStack.findBleDevices {foundBleDevs ->
//                Log.e("BleDeviceProvider", "listDeviceNodes: foundBleDevs[${foundBleDevs.size}] ${foundBleDevs.joinToString()}" )
                val bleNodes = foundBleDevs.transformToNodes()
                onNodesListed.invoke( bleNodes.filter(filter) , filter)
            }
        })
    }

}
