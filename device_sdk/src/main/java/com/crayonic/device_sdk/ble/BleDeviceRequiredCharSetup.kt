package com.crayonic.device_sdk.ble

data class BleDeviceRequiredCharSetup(
    var requireServices: List<SubServiceHolder>
){
    data class SubServiceHolder(
        val service:String,
        val readChars : List<String>,
        val writeChars : List<String>,
        val notifyChars : List<String>
        )
}

