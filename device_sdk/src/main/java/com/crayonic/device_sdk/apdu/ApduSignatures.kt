package com.crayonic.device_sdk.apdu

import com.crayonic.device_sdk.core.Single.eventCrc32
import com.crayonic.device_sdk.core.Single.pin32
import com.crayonic.device_sdk.core.Single.zeros32
import com.crayonic.device_sdk.core.Single.zeros64
import com.crayonic.device_sdk.device.l1.models.CrayonicAlarmZone
import com.crayonic.device_sdk.device.l1.models.AlarmZonesCollection
import com.crayonic.device_sdk.util.clockUnixTimeNowAsBytes
import com.crayonic.device_sdk.util.seconds_30
import com.crayonic.device_sdk.util.seconds_60
import space.lupu.koex.bytes.BytesRange
import space.lupu.koex.strings.times
import space.lupu.koex.types.Types

object ApduSignatures {
    // map of supported apdu commands
    val supportedApdus = mapOf(
//        ApduSignature(0xffb20e00, "Read Low Watermark" ,4, -1).toMapPair(), // fills length as 4 bytes and expecting 4 bytes in output
//        ApduSignature(0xffd20e00, "Write Low Watermark" ,4,900).toMapPair(),
//        ApduSignature(0xffb20f00, "Read High Watermark" ,4,-1).toMapPair(),
//        ApduSignature(0xffd20f00, "Write High Watermark" ,4,900).toMapPair(),
//        ApduSignature(0xffb20c00, "Read Sampling" ,4,1).toMapPair(),
//        ApduSignature(0xffd20c00, "Set Sampling" ,4,1).toMapPair(),
//        ApduSignature(0xffb20c01, "Read Variable Sampling" ,4,4).toMapPair(),
//        ApduSignature(0xffd20c01, "Set Variable Sampling" ,4,4).toMapPair(),
        ApduSignature(0xff000000, "Read all data" ,0,BytesRange(0,64000)).toMapPair(),
        ApduSignature(0xffb20d00, "Read Clock" ,4, 0L).toMapPair(),
        ApduSignature(0xffd20d00, "Write Clock" ,4, clockUnixTimeNowAsBytes()).toMapPair(),
        ApduSignature(0xffb22300, "Read ETH Address" ,40, "00".times(20)).toMapPair(),
        ApduSignature(0xffd20a00, "Soft Reset Device" ,32, pin32).toMapPair(),
        ApduSignature(0xffd20b00, "FW Update Mode Restart" ,32, pin32).toMapPair(),
        ApduSignature(0xffb22200, "Read Changing Public Key" ,64, zeros64).toMapPair(),
        ApduSignature(0xffd22200, "Generate Changing Keys" ,32, pin32).toMapPair(),
        ApduSignature(0xffb22700, "Read Identity Public Key" ,64, zeros64).toMapPair(),
        ApduSignature(0xffd22700, "Generate Identity" ,32, pin32).toMapPair(),
        ApduSignature(0xffd22701, "Write Identity" ,96, ByteArray(96)).toMapPair(),
        ApduSignature(0xffd22f00, "Write Management PIN" ,64, zeros64).toMapPair(),
        ApduSignature(0xffb22c00, "Read Root of Trust" ,64, zeros64).toMapPair(),
        ApduSignature(0xffd22c00, "Write Root of Trust" ,64, zeros64).toMapPair(),
//        ApduSignature(0xffb21100, "Read Modem Setup" ,-1,"").toMapPair(),
//        ApduSignature(0xffd21100, "Set Modem Setup" ,-1,"").toMapPair(),
        ApduSignature(0xffb2df01, "Pen Buffer Length" ,4,0).toMapPair(),
        ApduSignature(0xffb2df02, "Pen Buffer Data" ,-1, Types.byteArray).toMapPair(),
        ApduSignature(0xffd22139, "Set ETH Secret Seed Bip39" ,64, zeros64).toMapPair(),
        ApduSignature(0xffd21000, "Enter PUK Code" ,32, pin32).toMapPair(),
        ApduSignature(0xffb2b44e, "Read ETH Tx" ,-1, Types.byteArray).toMapPair(),
        ApduSignature(0xffd2b44e, "Sign ETH Tx" ,-1, Types.byteArray).toMapPair(),
        ApduSignature(0xffd20100, "Event Start" ,32, pin32, defaultTimeout = seconds_60).toMapPair(),
        ApduSignature(0xffb20101, "Event Read Length" ,4,0, defaultTimeout = seconds_60).toMapPair(),
        ApduSignature(0xffb20102, "Event Read Data" ,-1, Types.byteArray, defaultTimeout = seconds_60).toMapPair(),
        ApduSignature(0xffd20102, "Event Write Data" ,-1, Types.byteArray).toMapPair(),
        ApduSignature(0xffd20103, "Event Finish" ,32, eventCrc32, defaultTimeout = seconds_30).toMapPair(),
        ApduSignature(0xffb21e00, "Read Alarm Zones" ,
            AlarmZonesCollection.MAX_BYTE_SIZE,
            AlarmZonesCollection()
        ).toMapPair(),
        ApduSignature(0xffd21e00, "Write Alarm Zone" ,
            CrayonicAlarmZone.BYTE_SIZE,
            CrayonicAlarmZone()
        ).toMapPair(),
        ApduSignature(0xffb22000, "Read Asset ID" ,32, Types.byteArray).toMapPair(),
        ApduSignature(0xffd22000, "Write Asset ID" ,32, zeros32).toMapPair(),
        ApduSignature(0xffd23000, "Set Package ID" ,-1,"").toMapPair(),
        ApduSignature(0xffd2ca00, "Toggle secure channel" ,1,false).toMapPair(),
        ApduSignature(0xffd2c500, "Send Signal Encrypted Message" ,-1,Types.byteArray).toMapPair(),
        ApduSignature(0xffd2c511, "Send Signal Init" ,-1,Types.byteArray).toMapPair(),
        ApduSignature(0xffd2fa00, "Chat Text Message" ,-1,"").toMapPair(),
        ApduSignature(0xffd2fe00, "Encrypted Chat Text Message" ,-1,Types.byteArray).toMapPair(),
        ApduSignature(0xffd2fb00, "Chat Byte Message" ,-1,Types.byteArray).toMapPair()


        )




}