package com.crayonic.device_sdk.apdu

import com.crayonic.device_sdk.apdu.CrayonicStatusWords.encryption_error
import com.crayonic.device_sdk.apdu.CrayonicStatusWords.l2_app_error
import com.crayonic.device_sdk.apdu.CrayonicStatusWords.success_repeat
import com.crayonic.device_sdk.apdu.CrayonicStatusWords.undefined_error
import com.crayonic.device_sdk.apdu.CrayonicStatusWords.unexpected_input
import com.crayonic.device_sdk.apdu.CrayonicStatusWords.wrong_key_length
import com.crayonic.device_sdk.apdu.CrayonicStatusWords.wrong_length
import space.lupu.kapdu.statusWords.ApduStatusWord
import space.lupu.kapdu.statusWords.ApduStatusWordSignatures
import space.lupu.kapdu.statusWords.KnownApduStatusWords
object CrayonicStatusWords {
    val success_repeat = 0x7000
    val l2_app_error = 0xFFFF
    val wrong_length = 0x6c00
    val wrong_key_length = 0x6700
    val unexpected_input = 0x7100
    val undefined_error = 0x7fff
    val encryption_error = 0x7f00
}
val stopStatusWords: ApduStatusWordSignatures =
    ApduStatusWordSignatures(
        listOf(
            ApduStatusWord(KnownApduStatusWords.ok), // standard OK - 9000
            ApduStatusWord(success_repeat), // Success / Success with possible repeat of the same command

            ApduStatusWord(l2_app_error), // Error / Application Error - L2 App generic error
            ApduStatusWord(wrong_length), // Error / Wrong length / Aborted
            ApduStatusWord(wrong_key_length), // Error / Wrong key length - thrown by generate keys
            ApduStatusWord(unexpected_input), // Error / Unexpected Input - Mismatch of APDU header bytes (eg. Unknown command) or data bytes (eg. CRC fail)
            ApduStatusWord(undefined_error), // Error / Undefined Error
            ApduStatusWord(encryption_error), // Error / Encryption Error

            ApduStatusWord(0x6400), // Error / Timeout
            ApduStatusWord(0x6983), // Error / Authentication Method Blocked
            ApduStatusWord(0x6982), // Error / Security Status Not Satisfied
            ApduStatusWord(0x6401), // Error / User Aborted
            ApduStatusWord(0x6b00), // Error / Wrong Parameters
            ApduStatusWord(0x6300)  // Error / Warning

        )
    )

