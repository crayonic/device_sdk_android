package com.crayonic.device_sdk.apdu

object ApduType{

    // possibly deprecated - or on older HW

    /**
     * "Read Low Watermark"
     */
    val ro_data_watermark_low = 0xffb20e00 // "Read Low Watermark"
    /**
     * "Write Low Watermark"
     */
    val wr_data_watermark_low = 0xffd20e00 // "Write Low Watermark"

    /**
     * "Read High Watermark"
     */
    val ro_data_watermark_high = 0xffb20f00 // "Read High Watermark"
    /**
     * "Write High Watermark"
     */
    val wr_data_watermark_high = 0xffd20f00 // "Write High Watermark"

    /**
     * "Read Sampling"
     */
    val ro_sampling = 0xffb20c00 // "Read Sampling"
    /**
     * "Write Sampling"
     */
    val wr_sampling = 0xffd20c00 // "Set Sampling"

    /**
     * "Read Variable Sampling"
     */
    val ro_sampling_provision = 0xffb20c01 // "Read Variable Sampling"
    /**
     * "Set Variable Sampling"
     */
    val wr_sampling_provision = 0xffd20c01 // "Set Variable Sampling"

    /**
     * "Read all data"
     */
    val ro_all_data = 0xff000000 // "Read all data"

    /////////////////////////////////////////
    // all devices
    /**
     * "Read Clock"
     */
    val ro_clock = 0xffb20d00 // "Read Clock"
    /**
     * "Write Clock"
     */
    val wr_clock = 0xffd20d00 // "Write Clock"

    /**
     * "Read ETH Address"
     */
    val ro_eth_address = 0xffb22300 // "Read ETH Address"

    /**
     * "Soft Reset Device"
     */
    val wr_reset = 0xffd20a00 // "Soft Reset Device"
    /**
     * "FW Update Mode Restart"
     */
    val wr_fw_update_reset = 0xffd20b00 // "FW Update Mode Restart"

    /**
     * "Read Public Changing Key"
     */
    val ro_public_changing_key = 0xffb22200 // "Read Changing Public Key"
    /**
     * "Generate Changing Keys"
     */
    val wr_generate_changing_keys = 0xffd22200 // "Generate Changing Keys"

    /**
     * "Read Identity Public Key"
     */
    val ro_identity_pubkey = 0xffb22700 // "Read Identity Public Key"
    /**
     * "Generate Identity"
     */
    val wr_identity_generate = 0xffd22700 // "Generate Identity"
    /**
     * "Inject Identity"
     */
    val wr_identity_inject = 0xffd22701 // "Write Identity"

    /**
     * "Read Management PIN"
     */
    val ro_management_pin = 0xffb22f00 // "Read Management PIN"
    /**
     * "Write Management PIN"
     */
    val wr_management_pin = 0xffd22f00 // "Write Management PIN"

    /**
     * "Read Root of Trust"
     */
    val ro_root_of_trust_pubkey = 0xffb22c00 // "Read Root of Trust"
    /**
     * "Write Root of Trust"
     */
    val wr_root_of_trust_pubkey = 0xffd22c00 // "Write Root of Trust"

    /////////////////////////////////////////
    // modem only
    /**
     * "Read Modem Setup"
     */
    val ro_modem_setup = 0xffb21100 // "Read Modem Setup"
    /**
     * "Set Modem Setup"
     */
    val wr_modem_setup = 0xffd21100 // "Set Modem Setup"


    /////////////////////////////////////////
    // pen only
    /**
     * "Pen Buffer Length"
     */
    val ro_pen_length = 0xffb2df01 // "Pen Buffer Length"
    /**
     * "Pen Buffer Data"
     */
    val rw_pen_data = 0xffb2df02 // "Pen Buffer Data"

    /**
     *"Set ETH Secret Seed Bip39"
     */
    val wr_eth_secret_seed_bip39 = 0xffd22139 // "Set ETH Secret Seed Bip39"
    /**
     * "Enter PUK Code"
     */
    val wr_enter_puk_code = 0xffd21000 // "Enter PUK Code"

    /**
     * "Read ETH Tx"
     */
    val ro_bip44_tx_sign = 0xffb2b44e // "Read ETH Tx"
    /**
     * "Sign ETH Tx"
     */
    val wr_bip44_tx_sign = 0xffd2b44e // "Sign ETH Tx"

    /////////////////////////////////////////
    // L1 only

    /**
     * "Event Start"
     */
    val wr_event_start = 0xffd20100 // "Event Start"

    /**
     * "Event Read Length"
     */
    val ro_event_length = 0xffb20101 // "Event Read Length"

    /**
     * "Event Read Data"
     */
    val ro_event_data = 0xffb20102 // "Event Read Data"

    /**
     * "Event Write Data"
     */
    val wr_event_data = 0xffd20102 // "Event Write Data"

    /**
     * "Event Finish"
     */
    val wr_event_finish = 0xffd20103 // "Event Finish"

    /**
     * "Read Alarm Zones"
     */
    val ro_alarm_zones = 0xffb21e00 // "Read Alarm Zones"
    /**
     * "Write Alarm Zone"
     */
    val wr_alarm_zone = 0xffd21e00 // "Write Alarm Zone"

    /**
     * "Read Asset ID"
     */
    val ro_asset_id = 0xffb22000 // "Read Asset ID"
    /**
     * "Write Asset ID"
     */
    val wr_asset_id = 0xffd22000 // "Write Asset ID"

    /**
     * "Read Package ID"
     */
    val ro_package_id = 0xffd23000 // "Read Package ID"
    /**
     * "Set Package ID"
     */
    val wr_package_id = 0xffd23000 // "Set Package ID"


    /////////////////////////////////////////
    // encryption

    /**
     * "Toggle secure communicationAES"
     */
    val wr_cipher_aes = 0xffd2ca00 // "Toggle secure communication AES"

    /**
     * "Send Signal Encrypted Message"
     */
    val wr_signal_enc = 0xffd2c500 // "Send Signal Encrypted Message"
    /**
     * "Send Signal Init"
     */
    val wr_signal_init = 0xffd2c511 // "Send Signal Init"

    /////////////////////////////////////////
    // experimental or testing
    /**
     * "Chat Text Message"
     */
    val wr_chat_text = 0xffd2fa00 // "Chat Text Message"
    /**
     * "Encrypted Chat Text Message"
     */
    val wr_chat_text_enc = 0xffd2fe00 // "Encrypted Chat Text Message"
    /**
     * "Chat Byte Message"
     */
    val wr_chat_bytes = 0xffd2fb00 // "Chat Byte Message"


    /* create apdu signatures map values in bash

cat ApduTypekt | grep -v grep | grep " val " | awk '{print "ApduSignature(" $4 ", "}{for(i=6;i<=NF;++i)print $i}{print  ",listOf(),listOf()).toMapPair(),="}' | tr -s '\n' ' ' | tr '=' '\n'

     */
}