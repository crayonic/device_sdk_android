package com.crayonic.device_sdk.apdu

import com.crayonic.device_sdk.device.ExecutionStatus
import com.crayonic.device_sdk.device.toApduExecutionStatus
import com.crayonic.device_sdk.util.toKeccak
import space.lupu.kapdu.bytes.ApduBytes
import space.lupu.kapdu.statusWords.ApduStatusWord
import space.lupu.koex.bytes.*
import space.lupu.koex.numbers.toByteArray
import java.io.ByteArrayOutputStream
import java.nio.ByteOrder


/**
 * Apdu length in range 0 .. 65536 (excluded)
 *
 * @return on success 3 LE bytes | on fail null
 */
fun intToLengthBytes(expectedBytesCount: Int): ByteArray {
    if (expectedBytesCount in 0..0xffff) {
        return expectedBytesCount.toByteArray().minimalByteArray().prependZeros(3)
    }
    return byteArrayOf()
}

/**
 * Check if status word is from the allowed status words set in ApduSignature
 *
 * @return true on success
 */
private fun isCorrectStatusWord(
    apduSignature: ApduSignature,
    apduBytes: ApduBytes
) =
    apduSignature.expectedStatusWords.contains(ApduStatusWord(apduBytes.statusWord()))

/**
 * Preset of output lambda used in APDU read record or 2 way apdu read
 *
 * @return lambda preset
 */
fun prepOutputPreset_StandardReadValue():(ApduSignature, ApduBytes)->Any = {apduSignature: ApduSignature, apduBytes: ApduBytes ->
    val returnValue: Any
    val target = apduSignature.dataDefault.invoke()
    // sw sent to remote as response to 2 way apdu
    val statusSent = apduBytes.secondaryStatusWord

    // checks
    when {
        // 2 way apdu response
        statusSent.isNotEmpty() -> returnValue = decodeFromBytes(apduBytes.data(), target)?.let {
            it
        }?: ExecutionStatus.ok

        // checks in standard read record
        !apduBytes.isFinalizedCorrectly() -> returnValue = ExecutionStatus.interrupted
        !isCorrectStatusWord(apduSignature, apduBytes) -> returnValue = ApduStatusWord(apduBytes.statusWord()).toApduExecutionStatus()

        // we don`t have any function implemented to convert bytes to target type
        !target.hasFromByteArrayImplemented() -> returnValue = ExecutionStatus.conversion_error

        // standard read recod response
        else -> {
            // valid apdu continue to byte transformation
            returnValue = decodeFromBytes(apduBytes.data(), target)?.let {
                it
            }?: ExecutionStatus.ok
        }
    }
    returnValue
}

/**
 * Preset of output lambda used in APDU read record
 *
 * @return lambda preset
 */
fun defaultOutputPrep() = prepOutputPreset_StandardReadValue()

/**
 * Preset of input lambda used in APDU write record
 *
 * @return lambda preset
 */
fun prepInputPreset_StandardWriteValue():(ApduSignature,Any)->ByteArray = { _: ApduSignature, inputs: Any ->
    val byteStream : ByteArrayOutputStream = ByteArrayOutputStream(128)
    if(inputs is List<*>){
        inputs.forEach {
            it?.let{
                if (it.hasToByteArrayImplemented())
                    byteStream.write(encodeToBytes(it)) }
        }
    }else{
        if (inputs.hasToByteArrayImplemented())
            byteStream.write(encodeToBytes(inputs))
    }
    byteStream.toByteArray()
}

/**
 * Preset of input lambda used in APDU write record
 *
 * @return lambda preset
 */
fun defaultInputPrep() = prepInputPreset_StandardWriteValue()


/**
 * Create byte array from object if possible.
 *
 * To be sure it is possible first call input.hasToByteArrayImplemented().
 *
 * @param input any object
 * @return bytes or null on failure
 */
fun encodeToBytes(input:Any):ByteArray? = toByteArrayOrNull(input, ByteOrder.LITTLE_ENDIAN)

/**
 * Decode bytes to object of type specified by target value instance.
 *
 * @param bytes origin
 * @param target used as instance type
 * @return instance or null on failure
 */
fun decodeFromBytes(bytes:ByteArray, target:Any)=
    fromByteArrayOrNull(bytes, target, ByteOrder.LITTLE_ENDIAN)

fun adminPinToByteArray(adminPin:String):ByteArray = adminPin.toKeccak()