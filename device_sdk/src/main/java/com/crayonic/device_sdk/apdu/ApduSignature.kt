package com.crayonic.device_sdk.apdu

import com.crayonic.device_sdk.util.seconds_10
import space.lupu.kapdu.bytes.ApduBytes
import space.lupu.kapdu.statusWords.ApduStatusWordSignatures
import space.lupu.koex.bytes.removeLeadingZeros
import space.lupu.koex.hex.toHex
import space.lupu.koex.numbers.toByteArray
import space.lupu.koex.numbers.toInt
import space.lupu.koex.types.Types

/**
 * Signature of a APDU command, including accepted I/O values
 *
 * @param apduType header CLA,INS,P1,P2 represented as integer
 * @param inDefaults default values used for instance and type reference in APDU command input data
 * @param outDefaults default values used for instance and type reference in APDU command response data
 */
data class ApduSignature(
    val apduType: Long,
    val readableName: String,
    val expectedLength: Int,
    val dataDefault: () -> Any,
    // consumes input/s and performs validation and steps necessary to transform inputs to len + data bytes
    val onPrepInputToBytes: (ApduSignature, Any) -> ByteArray = defaultInputPrep(),
    // consumes received bytes and transforms them to expected output
    val onPrepBytesToOutput: (ApduSignature, ApduBytes) -> Any = defaultOutputPrep(),
    val expectedStatusWords: ApduStatusWordSignatures = stopStatusWords,
    val defaultTimeout: Long = seconds_10
) {

    constructor(
        apduType: Long,
        readableName: String,
        expectedLength: Int,
        dataDefaultObject: Any,
        onPrepInputToBytes: (ApduSignature, Any) -> ByteArray = defaultInputPrep(),
        onPrepBytesToOutput: (ApduSignature, ApduBytes) -> Any = defaultOutputPrep(),
        expectedStatusWords: ApduStatusWordSignatures = stopStatusWords,
        defaultTimeout: Long = seconds_10
    ) : this(
        apduType,
        readableName,
        expectedLength,
        { dataDefaultObject },
        onPrepInputToBytes,
        onPrepBytesToOutput,
        expectedStatusWords,
        defaultTimeout
    )


    fun toApduBytes(): ApduBytes = ApduBytes(apduType)

    fun toMapPair(): Pair<Long, ApduSignature> = apduType to this

    fun isReadRecord(): Boolean = toApduBytes().isReadRecord()
    fun isWriteRecord(): Boolean = toApduBytes().isWriteRecord()

    override fun toString(): String {
        val sb = StringBuilder("ApduSignature ( ")
        for (prop in ApduSignature::class.members) {
            when {
                prop.name == "apduType" -> sb.append("${prop.name} = ${ApduBytes(apduType).toStringVerbose()}, ")
                prop.name == "readableName" -> sb.append("${prop.name} = ${readableName}, ")
                else -> {}//sb.append("${prop.name} = ${prop.toString()}, ")
            }
        }
        return sb.append(" )").toString()
    }

    fun readableApduType(): String {
        return apduType.toByteArray().removeLeadingZeros().toHex()
    }

    companion object {
        /**
         * onPrepInputToBytes is ignored
         */
        fun rawApduSignature(
            inputBytes: ByteArray,
            expectedStatusWords: ApduStatusWordSignatures = stopStatusWords,
            defaultTimeout: Long = seconds_10
        ): ApduSignature? {
            return if(inputBytes.size >= 7) {
                val apduBytes = ApduBytes(inputBytes)
                ApduSignature(
                    apduBytes.apduType(),
                    "Raw APDU",
                    apduBytes.length().toInt(),
                    Types.byteArray,
                    { _: ApduSignature, any -> if(any is ByteArray) any else byteArrayOf() }, // probably ignored
                    { _: ApduSignature, apduBytesResult -> apduBytesResult.bytes },
                    expectedStatusWords,
                    defaultTimeout
                )
            } else null
        }

    }
}