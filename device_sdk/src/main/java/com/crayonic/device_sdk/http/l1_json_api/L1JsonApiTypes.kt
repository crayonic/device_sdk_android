package com.crayonic.device_sdk.http.l1_json_api

object L1JsonApiTypes{
    const val l1RawData = "crayonic.amb.l1.data.v2_0_0"
    const val packageId = "crayonic.amb.l1.pid.v1_0_0"
    const val email = "crayonic.amb.l1.email.v1_0_0"
    const val provisioningZones = "crayonic.amb.l1.zones.v1_0_0"
    const val proofOfDelivery = "crayonic.amb.l1.pod.v1_0_0"
    const val triggeredAlarms = "crayonic.amb.l1.alarms.v1_0_0"
}