package com.crayonic.device_sdk.http.json_api

import com.crayonic.device_sdk.util.TagsEnum
import com.crayonic.device_sdk.util.appendToList
import com.crayonic.device_sdk.util.initializeTags
import space.lupu.konnector.impl.one_node_module.OneNodeModule

open class JsonApiModule<CH:JsonApiChannel>(
    channel:CH,
    id: String,
    modulePriority: Int,
    specificTags: List<String>
) : OneNodeModule<CH>(
    channel,
    id,
    modulePriority,
    specificTags.appendToList(TagsEnum.module, TagsEnum.json_api_module, TagsEnum.json_api),
    specificTags.appendToList(TagsEnum.provider, TagsEnum.json_api_provider, TagsEnum.json_api),
    specificTags.appendToList(TagsEnum.node, TagsEnum.json_api_node, TagsEnum.json_api)
)