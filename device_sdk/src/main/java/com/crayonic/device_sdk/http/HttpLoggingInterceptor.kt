package com.crayonic.device_sdk.http

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

internal class HttpLoggingInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val t1 = System.nanoTime()
        println("HTTP : ${t1} : Sending request : $request")

        val response = chain.proceed(request)

        val t2 = System.nanoTime()
        println("HTTP : ${t2} : Received response: $response")

        return response
    }
}
