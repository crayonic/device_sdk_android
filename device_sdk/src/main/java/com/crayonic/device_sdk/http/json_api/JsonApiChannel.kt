package com.crayonic.device_sdk.http.json_api

import com.crayonic.device_sdk.http.HttpController
import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.http.HttpTextInterface
import com.crayonic.device_sdk.util.TagsEnum
import com.crayonic.device_sdk.util.initializeTags
import okhttp3.Response
import space.lupu.konnector.impl.one_node_module.FunctionalityChannel

abstract class JsonApiChannel(
    id: String = Single.cacheId(TagsEnum.json_api_channel.name),
    vararg tags: String
) : FunctionalityChannel(
    id,
    tags.toList().initializeTags(
        listOf(
            TagsEnum.json_api_channel,
            TagsEnum.json_api,
            TagsEnum.channel
        )
    )
), HttpTextInterface {
    override fun send(payload: Any, onReceived: (Any) -> Unit) {
    }

    override fun get(
        url: String,
        onResponse: (okHttpResponse: Response) -> Unit,
        headers: Map<String, String>
    ) {
        HttpController().get(url, onResponse = onResponse)
    }

    override fun post(
        url: String,
        payloadText: String,
        onResponse: (okHttpResponse: Response) -> Unit,
        headers: Map<String, String>
    ) {
        HttpController().postJson(url, payloadText, headers, onResponse)
    }

}