package com.crayonic.device_sdk.http.json_api

import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.util.TagsEnum
import space.lupu.konnector.NodeProvider
import space.lupu.konnector.tags.Tags

abstract class JsonApiProvider:NodeProvider(
    Single.cacheId(TagsEnum.json_api_provider.name),
    Tags.emptyTags()
)