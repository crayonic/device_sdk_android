package com.crayonic.device_sdk.http

import okhttp3.Response

interface HttpTextInterface{
    fun get( url:String, onResponse:(okHttpResponse:Response)-> Unit, headers: Map<String,String>)
    fun post( url:String, payloadText:String, onResponse:(okHttpResponse:Response)-> Unit, headers: Map<String,String>)
}