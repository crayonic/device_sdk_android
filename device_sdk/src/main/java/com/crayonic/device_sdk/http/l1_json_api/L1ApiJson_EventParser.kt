package com.crayonic.device_sdk.http.l1_json_api

import com.crayonic.device_sdk.util.getAsIntOrNull
import com.crayonic.device_sdk.util.getAsLongOrNull
import com.crayonic.device_sdk.util.getAsStringOrNull
import com.crayonic.device_sdk.util.toJson
import com.google.gson.JsonArray
import com.google.gson.JsonObject


class L1ApiJson_EventParser(jsonString: String){
    val assetId:String
    val createdBy:String
    val timestamp:Long
    val accessLevel:Int
    val data: JsonArray
    val signedEvent:String
    val eventId:String
    val content: JsonObject
    val idData: JsonObject
    val json: JsonObject
    init {
        signedEvent = jsonString
        json = jsonString.toJson()
        content = json.getAsJsonObject("content")
        idData = content.getAsJsonObject("idData")
        eventId = content.getAsStringOrNull("eventId")?:""
        data = content.getAsJsonArray("data")
        assetId = idData.getAsStringOrNull("assetId")?:"0x00"
        createdBy = idData.getAsStringOrNull("createdBy")?:"0x00"
        timestamp= idData.getAsLongOrNull("timestamp")?:0L
        accessLevel= idData.getAsIntOrNull("accessLevel")?:0
    }
}
