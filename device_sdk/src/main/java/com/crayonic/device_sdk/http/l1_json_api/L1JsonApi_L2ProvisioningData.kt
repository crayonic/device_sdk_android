package com.crayonic.device_sdk.http.l1_json_api

import com.crayonic.device_sdk.device.l1.models.CrayonicAlarmZone
import com.google.gson.JsonArray
import com.google.gson.JsonObject


data class L1JsonApi_L2ProvisioningData(
        val packageId: String,
        val zones: List<CrayonicAlarmZone>,
        val unixTime:Long,
        val email:String,
        val deviceId:String,
        val fwVersion:String = "",
        val appVersion:String = ""
        ){

    fun toEventData(): JsonArray {

        val pidObj = JsonObject().apply{
            addProperty("alertEmail", email)
            addProperty("deviceId", deviceId)
            addProperty("l1fw_ver", fwVersion)
            addProperty("l2app_ver", appVersion)
            addProperty("pid", packageId)
            addProperty("type", L1JsonApiTypes.packageId)
        }

        val alarmZonesArray = JsonArray()
        zones.forEach { alarmZonesArray.add(it.toJsonObject()) }

        val zonesObj = JsonObject().apply {
            addProperty("type", L1JsonApiTypes.provisioningZones)
            add("zones", alarmZonesArray)
        }

        val array = JsonArray()
        array.add(pidObj)
        array.add(zonesObj)

        return array
    }

}
