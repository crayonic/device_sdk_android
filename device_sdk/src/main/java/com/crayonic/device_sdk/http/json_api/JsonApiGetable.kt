package com.crayonic.device_sdk.http.json_api

interface JsonApiGetable {
    fun provideGetUrl(baseUrl:String):String
    fun provideGetHeaders():Map<String,String>
}