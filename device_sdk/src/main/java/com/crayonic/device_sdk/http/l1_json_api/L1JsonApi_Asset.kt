package com.crayonic.ambrosus.gateway.comm.http.model


import com.crayonic.device_sdk.http.json_api.JsonApiPostable
import com.crayonic.device_sdk.http.l1_json_api.L1ApiJson_Postable
import com.crayonic.device_sdk.util.currentUnixTime
import com.crayonic.device_sdk.util.toEthKeccakHex
import com.crayonic.device_sdk.util.toEthSignature
import space.lupu.koex.hex.Hex

data class L1JsonApi_Asset(
        val assetId: String,
        val content: Content
): L1ApiJson_Postable {
    override fun providePostUrl(baseUrl: String): String = baseUrl.plus("/assets")

    override fun providePostHeaders(): Map<String, String> = mapOf(
        "Content-Type" to "application/json",
        "Accept" to "application/json"
        )

    override fun toJsonString(): String = toString()

    override fun toString(): String {
        return "{" +
                "\"assetId\":\"${assetId}\"," +
                "\"content\":${content}"+
                "}"
    }

    data class Content(
            val idData: IdData,
            val signature: String
    ) {
        override fun toString(): String {
            return "{" +
                    "\"idData\":${idData.toString()}," +
                    "\"signature\":\"${signature}\""+
                    "}"
        }

        fun toAssetID(): String {
            // keccak of stringified content
            return toString().toEthKeccakHex()
        }

        data class IdData(
                val createdBy: String,
                val sequenceNumber: Int,
                val timestamp: Long
        ) {
            fun generateSignature(secret:String): String {
                // from python
//                elliptic_signature(stringify(data_to_send['content']['idData']), secret)
                return toString().toEthSignature(secret)
            }

            override fun toString(): String {
                return "{" +
                        "\"createdBy\":\"${createdBy}\"," +
                        "\"sequenceNumber\":${sequenceNumber}," +
                        "\"timestamp\":${timestamp}"+
                "}"
            }
        }


    }

    fun toByteArray():ByteArray = Hex(assetId).toByteArray()

    companion object {
        val DEFAULT_SEQ_NUMBER = 0

        val TAG = "L1JsonApi_Asset"

        fun createInstance(createdBy: String, secret:String, sequenceNumber: Int = DEFAULT_SEQ_NUMBER): L1JsonApi_Asset {
            val time = currentUnixTime()
            val idData = Content.IdData(createdBy, sequenceNumber, time)
            val content = Content(idData, idData.generateSignature(secret))
            val assetId = content.toAssetID()
            return L1JsonApi_Asset(assetId,content)
        }
    }
}