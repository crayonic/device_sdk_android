package com.crayonic.device_sdk.http

import android.util.Log
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import java.io.IOException
import java.util.concurrent.TimeUnit

/**
 * To be used by api module
 */
class HttpController(val debugModeOn: Boolean = false) {

    val TAG = "HttpController"

    private var client = OkHttpClient.Builder().apply {
        retryOnConnectionFailure(true)
        connectTimeout(30, TimeUnit.SECONDS);
        readTimeout(30, TimeUnit.SECONDS);
        writeTimeout(30, TimeUnit.SECONDS);
        if (debugModeOn)
            addInterceptor(HttpLoggingInterceptor())
    }.build()

    companion object {
        val MEDIA_TYPE_JSON = "application/json; charset=utf-8".toMediaTypeOrNull()
    }

    @Throws(IOException::class)
    fun get(url: String, onResponse: (Response) -> Unit) {
        val request = Request.Builder()
            .url(url)
            .build()
        newCallWithErroHandled("get", request, onResponse)
    }

    @Throws(IOException::class)
    fun postText(
        url: String,
        payload: String,
        additionalHeaders: Map<String, String>,
        onResponse: (Response) -> Unit
    ) {
        val body = payload.toRequestBody()
        val requestBuilder = Request.Builder()
        additionalHeaders.forEach {
            requestBuilder.addHeader(it.key, it.value)
        }
        val request = requestBuilder
            .url(url)
            .post(body)
            .build()
        newCallWithErroHandled("postText", request, onResponse)
    }

    @Throws(IOException::class)
    fun postJson(
        url: String,
        payload: String,
        additionalHeaders: Map<String, String>,
        onResponse: (Response) -> Unit
    ) {
        val body = payload.toRequestBody(MEDIA_TYPE_JSON)
        val requestBuilder = Request.Builder()
        additionalHeaders.forEach {
            requestBuilder.addHeader(it.key, it.value)
        }
        val request = requestBuilder
            .url(url)
            .post(body)
            .build()
        newCallWithErroHandled("postJson", request, onResponse)
    }

    private fun newCallWithErroHandled(
        methodTag:String,
        request: Request,
        onResponse: (Response) -> Unit
    ) {
        try {
            client.newCall(request).execute().use { response -> onResponse.invoke(response) }
        } catch (e: Exception) {
            Log.e(TAG, "$methodTag: Failed with Error", e)
            onResponse.invoke(Response.Builder().code(599).message(e.localizedMessage).build())
        }
    }

}