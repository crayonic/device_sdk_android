package com.crayonic.device_sdk.http.l1_json_api

import com.crayonic.device_sdk.http.json_api.JsonApiGetable

import com.crayonic.device_sdk.util.*
import com.google.gson.JsonArray
import com.google.gson.JsonObject

data class L1ApiJson_L2Event(
        val assetId:String,
        val createdBy:String,
        val timestamp:Long,
        val accessLevel:Int,
        val data:JsonArray
): L1ApiJson_Postable, JsonApiGetable{

    override fun provideGetUrl(baseUrl: String): String  = "$baseUrl/events/$eventId"

    override fun provideGetHeaders(): Map<String, String> = mapOf(
        "Content-Type" to "application/json",
        "Accept" to "application/json"
    )

    override fun providePostUrl(baseUrl: String): String = "$baseUrl/assets/$assetId/events"

    override fun providePostHeaders(): Map<String, String> = mapOf(
        "Content-Type" to "application/json",
        "Accept" to "application/json"
    )

    override fun toJsonString(): String = signedEvent

    var eventId = ""

    private var signedEvent = ""

    fun loadSignedEventFromParser(parser:L1ApiJson_EventParser){
        eventId = parser.eventId
        signedEvent = parser.signedEvent

    }

    fun toSignedEvent(secret:String):String?{
        val strData = data.toString()
        println(".toSignedEvent: data - ${data}")
        val dataHashHex = strData.toEthKeccakHex(includePrefix0x = true) // as hex

        println(".toSignedEvent: dataHashHex - ${dataHashHex}")
        val idData = JsonObject()
        idData.addProperty("accessLevel", accessLevel)
        idData.addProperty("assetId", assetId)
        idData.addProperty("createdBy", createdBy)
        idData.addProperty("dataHash", dataHashHex)
        idData.addProperty("timestamp", timestamp)

        println(".toSignedEvent: idData - ${idData}")
        val signature = idData.toShortJsonString().toEthSignature(secret) // as hex

        println(".toSignedEvent: signature - ${signature}")

        val content = JsonObject()

        content.add("data",data)
        content.add("idData",idData)
        content.addProperty("signature",signature)


        println(".toSignedEvent: content - ${content}")

        val contentHashHex = content.toShortJsonString().toEthKeccakHex(includePrefix0x = true) // hex


        println(".toSignedEvent: contentHashHex - ${contentHashHex}")

        val event = JsonObject()
        event.add("content",content)
        event.addProperty("eventId",contentHashHex)

        this.eventId = contentHashHex

        println(".toSignedEvent: event - ${event}")

        signedEvent = event.toShortJsonString()

        return signedEvent
    }


    companion object {
        val TAG = "L1ApiJson_L2Event"

        fun createInstance(
            secret:String,
            assetId:String,
            createdBy:String,
            timestamp:Long,
            accessLevel:Int,
            data:JsonArray
        ): L1ApiJson_L2Event {
            return L1ApiJson_L2Event(assetId, createdBy, timestamp, accessLevel, data).apply {
                toSignedEvent(secret)
            }
        }

        fun createInstance(jsonString:String): L1ApiJson_L2Event?{
            val parser = L1ApiJson_EventParser(jsonString)
            val event = L1ApiJson_L2Event(
                parser.assetId,
                parser.createdBy,
                parser.timestamp,
                parser.accessLevel,
                parser.data
                )
            event.loadSignedEventFromParser(parser)
            return event
        }

    }

}
