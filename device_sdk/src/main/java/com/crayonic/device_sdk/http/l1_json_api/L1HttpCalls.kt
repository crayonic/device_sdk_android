package com.crayonic.device_sdk.http.l1_json_api

import com.crayonic.ambrosus.gateway.comm.http.model.L1JsonApi_Asset
import okhttp3.Response

interface L1HttpCalls {
    fun sendL1Event(l1Event:L1ApiJson_L1Event, onResponse:(okHttpResponse: Response)->Unit):Boolean
    fun sendEvent(jsonStr:String, onResponse:(okHttpResponse: Response)->Unit):Boolean
    fun sendEvent(l2Event: L1ApiJson_L2Event, onResponse:(okHttpResponse: Response)->Unit):Boolean
    fun createAccount(account:L1JsonApi_Account, onResponse:(okHttpResponse: Response)->Unit):Boolean
    fun createAsset(asset: L1JsonApi_Asset, onResponse:(okHttpResponse: Response)->Unit):Boolean
}