package com.crayonic.device_sdk.http.l1_json_api

import com.crayonic.ambrosus.gateway.comm.http.model.L1JsonApi_Asset
import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.http.json_api.JsonApiChannel
import com.crayonic.device_sdk.http.json_api.JsonApiModule
import com.crayonic.device_sdk.util.TagsEnum
import okhttp3.Response

class L1JsonApi(
    baseUrl: String,
    serverPriority:Int = 0,
    serverTags:List<String> = listOf(baseUrl)
):JsonApiModule<L1JsonApi.L1JsonApiChannel>(
    L1JsonApiChannel(baseUrl),
    Single.cacheId(TagsEnum.json_api_module.name.plus("_").plus(baseUrl)),
    serverPriority,
    arrayListOf("amb", "l1", "api").apply { addAll(serverTags) }
){
    class L1JsonApiChannel(val baseUrl: String): JsonApiChannel(), L1HttpCalls{

        override fun sendL1Event(event: L1ApiJson_L1Event, onResponse:(okHttpResponse: Response)->Unit): Boolean {
            post(event.providePostUrl(baseUrl), event.toJsonString(), headers = event.providePostHeaders(), onResponse = onResponse)
            return true
        }

        override fun sendEvent(l2Event: L1ApiJson_L2Event, onResponse:(okHttpResponse: Response)->Unit): Boolean {
            post(l2Event.providePostUrl(baseUrl), l2Event.toJsonString(), headers = l2Event.providePostHeaders(), onResponse = onResponse)
            return true
        }

        override fun sendEvent(jsonStr: String, onResponse:(okHttpResponse: Response)->Unit): Boolean {
            val event = L1ApiJson_L2Event.createInstance(toString())
            if (event == null){
                onResponse.invoke(
                    Response.Builder().apply {
                        // Wrong Input
                        // 422 (Unprocessable Entity)
                        code(422)
                    }.build()
                )
                return false
            }else{
                post(event.providePostUrl(baseUrl), jsonStr, headers = event.providePostHeaders(), onResponse = onResponse)
                return true
            }
        }

        override fun createAccount(account: L1JsonApi_Account, onResponse:(okHttpResponse: Response)->Unit): Boolean {
            post(account.providePostUrl(baseUrl), account.toJsonString(), headers = account.providePostHeaders(), onResponse = onResponse)
            return true
        }

        override fun createAsset(asset: L1JsonApi_Asset, onResponse:(okHttpResponse: Response)->Unit): Boolean {
            post(asset.providePostUrl(baseUrl), asset.toJsonString(), headers = asset.providePostHeaders(), onResponse = onResponse)
            return true
        }
    }

    companion object{
        val baseUrls = listOf<String>(
            "https://gateway-test.ambrosus.com"

//
//            https://track-and-trace.ambrosus-dev.com/%1$s
//        <string name="url_amb_api_alarms_dup_server">https://demo-dashboard-api.ambrosus-test.com/sensor/alert</string>
//        <string name="url_amb_api_alarms_dup_server_test">https://webhook.site/ba1c8ba5-8577-4230-b412-4c71d5589ed4</string>
//        <string name="url_amb_api_create_account">https://gateway-test.ambrosus.com/accounts</string>
//        <string name="url_amb_api_create_asset">https://gateway-test.ambrosus.com/assets</string>
//        <string name="url_amb_api_create_event_for_L2">https://gateway-test.ambrosus.com/assets/%1$s/events</string>
//        <string name="url_amb_api_create_event_for_L2_suffix">/assets/%1$s/events</string>
//        <string name="url_amb_api_create_event_for_L1M">http://pkstation2.synology.me:8080/assets/%1$s/events</string>
//        <string name="url_crayonic_logging_l1">http://pkstation2.synology.me:8080/logs/l1</string>
//        <string name="url_crayonic_logging_l1m">http://pkstation2.synology.me:8080/logs/l1m</string>
//        <string name="url_crayonic_logging_l2app">http://pkstation2.synology.me:8080/logs/l2app</string>
//        <string name="url_crayonic_logging_l2app_log_asset">http://pkstation2.synology.me:8080/logs/l2events/assets/%1$s/events</string>
//        <string name="url_crayonic_logging_l2app_log_asset_event">http://pkstation2.synology.me:8080/logs/l2events/assets/%1$s/event/%2$s</string>
//        <string name="url_crayonic_logging_amazon">https://cc9005t96e.execute-api.eu-central-1.amazonaws.com/dev/test</string>
//        <string name="url_crayonic_logging_webhook">http://crayonic.com/l1m/dump.php</string>
//        <string name="url_amb_api_fetch_event">https://gateway-test.ambrosus.com/events/%1$s</string>
//        <string name="url_amb_api_fetch_event_suffix">/events/%1$s</string>
        )
    }
}