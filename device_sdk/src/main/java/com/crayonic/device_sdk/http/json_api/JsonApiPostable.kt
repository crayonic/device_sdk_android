package com.crayonic.device_sdk.http.json_api

interface JsonApiPostable {
    fun providePostUrl(baseUrl:String):String
    fun providePostHeaders():Map<String,String>
    fun toJsonString():String
}