package com.crayonic.device_sdk.http.l1_json_api

import com.crayonic.device_sdk.http.json_api.JsonApiPostable
import com.crayonic.device_sdk.util.currentUnixTime
import com.crayonic.device_sdk.util.toJsonApiToken
import com.crayonic.device_sdk.util.toShortJsonString
import com.crayonic.device_sdk.util.tokenValidUntilSeconds
import com.google.gson.JsonArray
import com.google.gson.JsonObject

class L1JsonApi_Account(val ethAddress: String, var token:String = "", var permissionsList: List<String> = defaultAccountPermissionsForL2(), accessLevel: Int = 0): L1ApiJson_Postable {
    override fun providePostUrl(baseUrl: String): String = "$baseUrl/accounts"

    override fun providePostHeaders(): Map<String, String> = mapOf(
        "Content-Type" to "application/json",
        "Accept" to "application/json",
        "Authorization" to TOKEN_PREFIX+token
    )

    override fun toJsonString(): String = toJsonString()

    override fun toString(): String = json.toShortJsonString()

    val json : JsonObject

    init {
        // construct the json
        // like : {"accessLevel":0,"deviceAddress":"0xEd6b2DcA1D749339fC82f9DeA06CE61fe319a623","permissions":["register_account","create_entity"]}
        json = JsonObject().apply {
            addProperty(JSON_KEY_ACCESS_LEVEL, accessLevel)
            addProperty(JSON_KEY_ACCOUNT_ADDRESS, ethAddress)
            add(JSON_KEY_PERMISSIONS, JsonArray().apply {
                permissionsList.forEach {permission ->
                    add(permission)
                }
            })
        }
    }

    fun recreateToken(secret:String, validForSeconds: Long = 3600L) : String {
        token = when {
            token.isEmpty() -> ethAddress.toJsonApiToken(secret, validForSeconds)
            token.tokenValidUntilSeconds() < (currentUnixTime() + 300) -> token.toJsonApiToken(secret, validForSeconds)
            else -> token
        }
        return token
    }

    companion object {
        val TAG = "L1JsonApi_Account"
        val TOKEN_PREFIX = "AMB_TOKEN "

        val JSON_KEY_ACCESS_LEVEL = "accessLevel" // “<level of access required to see the data field>“
        val JSON_KEY_ACCOUNT_ADDRESS = "deviceAddress" // “<level of access required to see the data field>“
        val JSON_KEY_PERMISSIONS = "permissions" // “<level of access required to see the data field>“

        fun defaultAccountPermissionsForL2():List<String>{
            return listOf("register_accounts","create_asset","create_event")
        }

        fun createInstance(ethAddress: String, secret:String):L1JsonApi_Account =
            L1JsonApi_Account(ethAddress).apply {
                recreateToken(secret)
            }
    }


}