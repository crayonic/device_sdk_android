package com.crayonic.device_sdk.http.l1_json_api

import com.crayonic.device_sdk.device.l1.models.L1EventParser
import com.crayonic.device_sdk.device.l1.models.L1EventValidator
import com.crayonic.device_sdk.http.json_api.JsonApiGetable
import com.crayonic.device_sdk.http.json_api.JsonApiPostable

class L1ApiJson_L1Event(val parser: L1EventParser) : L1ApiJson_Postable, JsonApiGetable{

    constructor(validator: L1EventValidator):this(validator.parser)

    val parsedEventStr :String

    val eventId : String
    val assetId : String

    init {
        parsedEventStr = parser.getEventStr()
        assetId = parser.getAssetId()
        eventId = parser.getEventId()
    }

    constructor(initialByteArray: ByteArray):this(L1EventParser(initialByteArray))

    override fun provideGetUrl(baseUrl: String): String  = "$baseUrl/events/$eventId"

    override fun provideGetHeaders(): Map<String, String> = mapOf(
        "Content-Type" to "application/json",
        "Accept" to "application/json"
    )

    override fun providePostUrl(baseUrl: String): String = "$baseUrl/assets/$assetId/events"

    override fun providePostHeaders(): Map<String, String> = mapOf(
        "Content-Type" to "application/json",
        "Accept" to "application/json"
    )

    override fun toJsonString(): String = parsedEventStr

}