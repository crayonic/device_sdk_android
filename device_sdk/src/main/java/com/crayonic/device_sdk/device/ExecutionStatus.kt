package com.crayonic.device_sdk.device

import com.crayonic.device_sdk.apdu.CrayonicStatusWords.success_repeat
import space.lupu.kapdu.statusWords.ApduStatusWord
import space.lupu.kapdu.statusWords.KnownApduStatusWords

enum class ExecutionStatus {
    ok, read_next_event, error, interrupted, conversion_error
}

fun ApduStatusWord.toApduExecutionStatus(): ExecutionStatus = when (this.value ) {
    KnownApduStatusWords.ok -> ExecutionStatus.ok
    success_repeat -> ExecutionStatus.read_next_event
    else -> ExecutionStatus.error
}