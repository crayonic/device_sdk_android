package com.crayonic.device_sdk.device

import com.crayonic.device_sdk.core.Single
import space.lupu.konnector.filters.AnyFilter
import space.lupu.konnector.filters.MatchingCondition
import space.lupu.konnector.filters.MergeFilter

class DeviceFilter(
    val matchAnyTags:List<String> = listOf(),
    val matchAllTags:List<String> = listOf()
):MergeFilter(
    Single.cacheId("deviceFilter"),
    if(matchAllTags.isNotEmpty() || matchAnyTags.isNotEmpty())
        listOf(
        MergeFilter.tagsAtLeastOne(Single.randomId("devSubFilterOr"),matchAnyTags),
        MergeFilter.tagsMatchAll(Single.randomId("devSubFilterAnd"),matchAllTags))
    else
        listOf(AnyFilter(Single.randomId("devSubFilterAny")))
    ,
    MatchingCondition.Or
){

    constructor(vararg tags:String):this(listOf(),tags.toList())

    init {
        Single.cacheObject(id, this)
    }

    override fun toString(): String {
        if (matchAllTags.isEmpty() && matchAnyTags.isEmpty()){
            return "Device Filter : match everything"
        }
        return "DeviceFilter:{ matchAnyTags:[${matchAnyTags.joinToString(separator = ",")}], matchAllTags:[${matchAllTags.joinToString(separator = ",")}] }"
    }

    companion object {
        /**
         * Helper function
         * Creates DeviceFilter matching all devices containing ANY of the tags provided.
         * Equal to logical OR.
         */
        fun matchAnyTagsFilter(vararg tags: String): DeviceFilter =
            DeviceFilter(tags.toList(), listOf())

        /**
         * Helper function
         * Creates DeviceFilter matching only devices containing ALL of the tags provided.
         * Equal to logical AND.
         */
        fun matchAllTagsFilter(vararg tags: String): DeviceFilter =
            DeviceFilter(tags.toList(), listOf())
    }
}

