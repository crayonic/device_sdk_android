package com.crayonic.device_sdk.device.channels

import space.lupu.konnector.nodes.Channel
import space.lupu.konnector.tags.Tags

abstract class ByteArrayChannel(id:String, tags: Tags):Channel(id, tags) {

    abstract fun send(payload: ByteArray, onReceived: (ByteArray) -> Unit)

    override fun send(payload: Any, onReceived: (Any) -> Unit) {
        if (payload is ByteArray){
            send(payload){bytes: ByteArray -> onReceived.invoke(bytes) }
        }
    }
}