package com.crayonic.device_sdk.device.kotojava;

import com.crayonic.device_sdk.device.CrayonicDevice;
import com.crayonic.device_sdk.device.errors.CrayonicDeviceError;
import com.crayonic.device_sdk.util.Progress;

import java.util.Map;

import space.lupu.kapdu.transmission.TransmissionProgress;

public interface CrayonicDeviceListener {
    // prevents callback hell with RequestCode and Map of "variableKey" to Objects
    // each variable key has a predefined cast type

    /**
     * Response for a specific communication with device.
     *
     *
     * @param device CrayonicDevice : device instance request was send to
     * @param requestKey CrayonicRequestCode : request enum identifier
     * @param values Map
     */
    public void onResult(CrayonicDevice device, CrayonicRequestCode requestKey, Map<String, Object> values);

    /**
     * When device was connected and the communication channel to it was opened.
     *
     * You should use this as entry point for device interaction.
     *
     * @param device : device instance that was connected
     */
    public void onConnect(CrayonicDevice device);

    /**
     * When device was disconnected and communication with it is no longer possible.
     *
     * @param device : device instance that was disconnected
     */
    public void onDisconnect(CrayonicDevice device);

    /**
     * Error callback with references to identify failed request call.
     *
     * @param device CrayonicDevice : device instance request was send to
     * @param requestKey CrayonicRequestCode : request enum identifier
     * @param error CrayonicDeviceError : Same as device.errorFlag in the unixTime of callback
     */
    public void onError(CrayonicDevice device, CrayonicRequestCode requestKey, CrayonicDeviceError error);


    /**
     * Progress
     *
     * @param progress
     */
    public void onProgress(CrayonicDevice device, CrayonicRequestCode requestKey, Progress progress);
}
