package com.crayonic.device_sdk.device

import com.crayonic.device_sdk.device.errors.CrayonicDeviceError
import com.crayonic.device_sdk.util.TagsEnum
import space.lupu.konnector.NodeProvider
import space.lupu.konnector.filters.TaggedObjectFilter
import space.lupu.konnector.nodes.Node
import space.lupu.konnector.tags.Tags

abstract class DevicesProvider(id:String, vararg deviceTags: String): NodeProvider(id){
    val deviceTags = Tags.emptyTags()
    init{
        deviceTags.forEach { this.deviceTags.addTag(it) }
        this.deviceTags.addTag(TagsEnum.device.toString())
        this.deviceTags.addTag(TagsEnum.device_node.toString())
    }

    /**
     * Search and tag devices
     */
    abstract fun listDeviceNodes(
        filter: DeviceFilter,
        onNodesListed: (nodes: List<CrayonicDeviceNode>, originFilter: DeviceFilter) -> Unit,
        deviceTags: Tags
    ):Boolean

    override fun listNodes(filter: TaggedObjectFilter, onNodesListed: (nodes: List<Node>, originFilter: TaggedObjectFilter) -> Unit): Boolean {
        // works only for device specific filter
        if (filter is DeviceFilter)
            return listDeviceNodes(filter, onNodesListed, deviceTags)
        return false
    }

}