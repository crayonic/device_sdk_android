package com.crayonic.device_sdk.device

import com.crayonic.device_sdk.device.CrayonicDevice
import com.crayonic.device_sdk.util.TagsEnum
import com.crayonic.device_sdk.util.initializeTags
import space.lupu.konnector.Module
import space.lupu.konnector.NodeProvider
import space.lupu.konnector.nodes.Node

open class DevicesModule(id: String, modulePriority:Int, tags: List<String>, nodeTags: List<String>) : Module(
    id,
    modulePriority,
    tags.initializeTags(TagsEnum.device_module, TagsEnum.device),
    tags.initializeTags(TagsEnum.device_provider, TagsEnum.device),
    nodeTags.initializeTags(TagsEnum.device_node, TagsEnum.device)

) {
    constructor(id: String, modulePriority:Int, vararg tags: String):this(id, modulePriority, tags.toList(), listOf())

    fun safeAttachOfMultipleProviders(vararg providers: NodeProvider) {
        providers.forEach {
            if (it is DevicesProvider)
                attachProvider(it)
        }
    }

    companion object {
        fun transformNodesToCrayonicDevices(nodes: List<Node>): List<CrayonicDevice> {
            val resList = arrayListOf<CrayonicDevice>()
            nodes.filter { it is CrayonicDeviceNode }.toSet().forEach { node ->
                if (node is CrayonicDeviceNode)
                    resList.add(node.toCrayonicDevice()) // this is the place where device is changed to L1 or Pen Instance
            }
            return resList
        }
    }


}