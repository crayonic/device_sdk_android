@file:Suppress("unused")

package com.crayonic.device_sdk.device.l1.models

import com.crayonic.device_sdk.util.*
import com.crayonic.device_sdk.util.SensorCalcHelper.SENSOR_MULT_SHOCK
import com.crayonic.device_sdk.util.SensorCalcHelper.SENSOR_MULT_TEMP
import com.crayonic.device_sdk.util.SensorCalcHelper.decodeHumidity
import com.crayonic.device_sdk.util.SensorCalcHelper.decodeInclineLittleEndian
import com.crayonic.device_sdk.util.SensorCalcHelper.decodeShock
import com.crayonic.device_sdk.util.SensorCalcHelper.decodeTemperatureForThresholds
import com.crayonic.device_sdk.util.SensorCalcHelper.encodeHumidity
import com.crayonic.device_sdk.util.SensorCalcHelper.encodeInclineLittle
import com.crayonic.device_sdk.util.SensorCalcHelper.encodeOpticalPowerThresholds
import com.crayonic.device_sdk.util.SensorCalcHelper.encodeShockLittleEndian
import com.crayonic.device_sdk.util.SensorCalcHelper.encodeTemperatureForThresholds
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import space.lupu.koex.hex.toHexSeparated
import space.lupu.koex.numbers.toByteArray
import space.lupu.koex.numbers.toLong
import java.io.ByteArrayOutputStream
import java.lang.Exception
import java.nio.ByteOrder

class L1_AlarmZoneTriggersModel() {
    class InternalStructureModel(val minimal: Boolean) {
        fun equals(other: InternalStructureModel): Boolean {
            var ok = temperature.equalsDelta(other.temperature, 0.1)
            ok = ok && time.equals(other.time)
            ok = ok && humidity.equals(other.humidity)
            ok = ok && optical_power.equalsDelta(other.optical_power, 1.0)
            ok = ok && shock.equalsDelta(other.shock, 0.125)
            ok = ok && incline_x.equalsDelta(other.incline_x, 0.125)
            ok = ok && incline_y.equalsDelta(other.incline_y, 0.125)
            ok = ok && incline_z.equalsDelta(other.incline_z, 0.125)
            return ok
        }

        fun toByteArray(): ByteArray {

            // target structure
            /*
                typedef struct
                    {
                        uint32_t  unixTime;      // unix timestamp   uint 0 uint max
                        int16_t   temp;      // < −3276.7, +3276.7 > °C  short min max
                        uint16_t   humid;     // < 0, 100 >      --   0 - 255
                        uint32_t  lux;       //  < 0, 1  ... see sensors calc
                        uint16_t   shock;     // < 0, 25.5 > G ~ number of 0,255
                        int16_t   incline[3]; // -180 to 180 degree
                    } sensors_data_t;
             */
            var debugHex = StringBuilder()
            val outputBytes = ByteArrayOutputStream()
//            outputBytes.write(temperature.toLittleEndian())
            // convert number back to raw register format
            // then write it into bytes in order of target structure
            val time = raw_time()
            outputBytes.write(time)
            debugHex.append("[unixTime](${this.time}){${time.toHexSeparated()}} ")

            val temp = raw_temperature()
            outputBytes.write(raw_temperature())
            debugHex.append("[temp](${this.temperature}){${temp.toHexSeparated()}} ")

            val humidity = raw_humidity()
            outputBytes.write(humidity)
            debugHex.append("[hum](${this.humidity}){${humidity.toHexSeparated()}} ")

            val light = raw_optical_power()
            outputBytes.write(light)
            debugHex.append("[light](${this.optical_power}){${light.toHexSeparated()}} ")

            val shock = raw_shock()
            outputBytes.write(shock)
            debugHex.append("[shock](${this.shock}){${shock.toHexSeparated()}} ")

            val incline = raw_incline()
            outputBytes.write(incline)
            debugHex.append("[incline](${this.incline_x},${this.incline_y},${this.incline_z}){${incline.toHexSeparated()}} ")
            lastDebugHex = debugHex.toString()
            return outputBytes.toByteArray()
        }

        private var lastDebugHex = ""

        fun debugHex(): String {
            toByteArray()
            return lastDebugHex
        }

        fun toJsonObject(): JsonObject {
            val json = JsonObject()
            json.addProperty("humidity", humidity.toInt())
            json.addProperty("incline_x", incline_x.toInt())
            json.addProperty("incline_y", incline_y.toInt())
            json.addProperty("incline_z", incline_z.toInt())
            json.addProperty("optical_power", optical_power.toLong())
            json.addProperty("shock", (shock * SENSOR_MULT_SHOCK).toInt())
            json.addProperty("temperature", (temperature * SENSOR_MULT_TEMP).toInt())
            json.addProperty("unixTime", time)
            return json
        }

        fun fromJsonObject(json: JsonObject) {
            if (json.has("unixTime")) {
                time = json.get("unixTime").asLong
                isTime = true
            }
            if (json.has("temperature")) {
                temperature = json.get("temperature").asDouble.div(SENSOR_MULT_TEMP)
                isTemperature = true
            }
            if (json.has("humidity")) {
                humidity = json.get("humidity").asInt
                isHumidity = true
            }
            if (json.has("optical_power")) {
                optical_power = json.get("optical_power").asDouble
                isOpticalPower = true
            }
            if (json.has("shock")) {
                shock = json.get("shock").asDouble.div(SENSOR_MULT_SHOCK)
                isShock = true
            }
            if (json.has("incline_x")) {
                incline_x = json.get("incline_x").asDouble
                isInclineX = true
            }
            if (json.has("incline_y")) {
                incline_y = json.get("incline_y").asDouble
                isInclineY = true
            }
            if (json.has("incline_z")) {
                incline_z = json.get("incline_z").asDouble
                isInclineZ = true
            }
        }

        fun raw_optical_power(): ByteArray {
            return encodeOpticalPowerThresholds(optical_power).reversedArray()
        }

        var time: Long = 0L
        var isTime = false
        var temperature: Double = 0.0
        var isTemperature = false
        var humidity: Int = 0
        var isHumidity = false
        var optical_power: Double = 0.0
        var isOpticalPower = false
        var shock: Double = 0.0
        var isShock = false
        var incline_x: Double = 0.0
        var isInclineX = false
        var incline_y: Double = 0.0
        var isInclineY = false
        var incline_z: Double = 0.0
        var isInclineZ = false

        init {
            // default values for alarm zone
            if (minimal) {
                time = 0xffffffffL
                temperature = -3276.7
                humidity = 0
                optical_power = 0.0
                shock = 0.0
                incline_x = -180.0
                incline_y = -180.0
                incline_z = -180.0
            } else {
                time = 0xffffffffL
                temperature = 3276.7
                humidity = 100
                optical_power = Int.MAX_VALUE.toDouble()
                shock = 100.0    // 100 G
                incline_x = 180.0
                incline_y = 180.0
                incline_z = 180.0
            }
        }

        fun raw_time(): ByteArray {
            val arr = time.toByteArray().takeLast(4).toByteArray().reversedArray()
            return arr
        }

        fun raw_temperature(): ByteArray {
            val arr = encodeTemperatureForThresholds(temperature).reversedArray()
            return arr
        }

        fun raw_humidity(): ByteArray {
//            val arr = ByteArray(1)
//            arr.set(0,encodeHumidity(this.humidity))
            return encodeHumidity(this.humidity)
        }

        fun raw_shock(): ByteArray {
            return encodeShockLittleEndian(shock)
        }

        fun raw_incline(): ByteArray {
            return encodeInclineLittle(incline_x, incline_y, incline_z)
        }

        fun fromByteArray(bytes: ByteArray) {
            if (bytes.size == SIZE_INTERNAL_STRUCT_BYTES) {
                val offsetTime = 0
                val offsetTemperature = 4
                val offsetHumidity = 6
                val offsetOP = 8
                val offsetShock = 12
                val offsetIncline = 14

                // 4B
                this.time = bytes.copyOfRange(offsetTime, offsetTime + TYPE_TIME_SIZE)
                    .toLong(ByteOrder.LITTLE_ENDIAN)

                // 2B
                this.temperature = decodeTemperatureForThresholds(
                    bytes.copyOfRange(
                        offsetTemperature,
                        offsetTemperature + TYPE_TEMP_SIZE
                    ).reversedArray()
                )

                // 2B
                this.humidity = decodeHumidity(
                    bytes.copyOfRange(
                        offsetHumidity,
                        offsetHumidity + TYPE_HUM_SIZE
                    )
                )

                // 4B
                this.optical_power =
                    bytes.copyOfRange(offsetOP, offsetOP + TYPE_OPTOPOW_SIZE).reversedArray()
                        .toOpThresholds()

                // 2B
                this.shock =
                    decodeShock(bytes.copyOfRange(offsetShock, offsetShock + TYPE_SHOCK_SIZE))

                // 6B = 2B + 2B + 2B
                val incline_Array = decodeInclineLittleEndian(
                    bytes.copyOfRange(
                        offsetIncline,
                        offsetIncline + TYPE_INCLINE_SIZE
                    )
                )

                this.incline_x = incline_Array[0]
                this.incline_y = incline_Array[1]
                this.incline_z = incline_Array[2]

                // mark all as set
                isTime = true
                isTemperature = true
                isHumidity = true
                isOpticalPower = true
                isShock = true
                isInclineX = true
                isInclineY = true
                isInclineZ = true
            }
        }
    }

    constructor(input: ByteArray) : this() {
        if (input.size.rem(SIZE_TOTAL_STRUCT_BYTES) == 0) {
            val minimalBytes = input.copyOfRange(0, SIZE_INTERNAL_STRUCT_BYTES)
            val maximalBytes =
                input.copyOfRange(SIZE_INTERNAL_STRUCT_BYTES, SIZE_TOTAL_STRUCT_BYTES)

//            val min = InternalStructureModel(true)
            minimal_tresh.fromByteArray(minimalBytes)
//            val max = InternalStructureModel(false)
            maximal_tresh.fromByteArray(maximalBytes)

        }
    }

    companion object {
        val TAG = "L1_VarAdvTriggersModel"
        val TYPE_TIME = 0
        val TYPE_TIME_HEXSTR = "00"
        val TYPE_TIME_SIZE = 4
        val TYPE_TEMP = 1
        val TYPE_TEMP_HEXSTR = "01"
        val TYPE_TEMP_SIZE = 2
        val TYPE_HUM = 2
        val TYPE_HUM_HEXSTR = "02"
        val TYPE_HUM_SIZE = 2
        val TYPE_OPTOPOW = 3
        val TYPE_OPTOPOW_HEXSTR = "03"
        val TYPE_OPTOPOW_SIZE = 4
        val TYPE_SHOCK = 4
        val TYPE_SHOCK_HEXSTR = "04"
        val TYPE_SHOCK_SIZE = 2
        val TYPE_INCLINE = 5
        val TYPE_INCLINE_HEXSTR = "05"
        val TYPE_INCLINE_SIZE = 6

        val SIZE_INTERNAL_STRUCT_BYTES =
            TYPE_TIME_SIZE +
                    TYPE_TEMP_SIZE +
                    TYPE_HUM_SIZE +
                    TYPE_OPTOPOW_SIZE +
                    TYPE_SHOCK_SIZE +
                    TYPE_INCLINE_SIZE

        val SIZE_TOTAL_STRUCT_BYTES = SIZE_INTERNAL_STRUCT_BYTES * 2
        val TOTAL_DEVICE_BUFFERRED_STRUCTS = 16

        val known_types = listOf(
            TYPE_TIME, TYPE_TEMP, TYPE_HUM, TYPE_OPTOPOW, TYPE_SHOCK, TYPE_INCLINE
        )

        val DEFAULT_VALUES_ZONE = L1_AlarmZoneTriggersModel()

        fun DEFAULT_ZONE(): L1_AlarmZoneTriggersModel =
            L1_AlarmZoneTriggersModel()
                .setTime(0L, 0L)
                .setTemperature(-48.0, +79.5)
                .setHumidity(25, 95)
                .setOpticalPower(0.0, 10000.0)
                .setShock(0.0, 25.5)
                .setIncline(-180.0, 180.0, -180.0, 180.0, -180.0, 180.0)



        fun IGNORE_ZONE(): L1_AlarmZoneTriggersModel = L1_AlarmZoneTriggersModel()
//                .setUnixTime(Int.MAX_VALUE.toLong(),Int.MAX_VALUE.toLong())
            .setTime(30, 20) // expiration , snooze - in seconds  // TESTING L1 FW
//                .setTemperature(-48.0, 79.0)
//                .setHumidity(0,100)
//                .setOpticalPower(0.0,10000000.0)
//                .setShock(25.0,25.0)
//                .setIncline(-180.0,180.0,-180.0,180.0,-180.0,180.0)

        val DEFAULT_UI_PRESET_ZONE = L1_AlarmZoneTriggersModel()
            .setTime(5, 20)
            .setTemperature(-5.0, 25.0)
            .setHumidity(30, 90)
            .setOpticalPower(0.0, 10000.0)
            .setShock(0.0, 16.0)
            .setIncline(-180.0, 180.0, -180.0, 180.0, -180.0, 180.0)

        fun defaultAlarmZones(): List<L1_AlarmZoneTriggersModel> {
            val zonesList = arrayListOf<L1_AlarmZoneTriggersModel>()
            // everywhere - alarm min unixTime  = 1 second      Snooze unixTime =10 min

            // zone zero -  zone 0 exp. date     = 10 minutes from now (rout creation)
            var zone = L1_AlarmZoneTriggersModel().setTime(600, 600)
            zonesList.add(zone)
            // 1 -  zone 1 temperature     = -45 to 0°C
            zone = L1_AlarmZoneTriggersModel()
                .setTime(1, 600)
                .setTemperature(-45.0, 0.0)
            zonesList.add(zone)
            // 2 -  zone 2 temperature     =  12 to 74.5°C
            zone = L1_AlarmZoneTriggersModel()
                .setTime(1, 600)
                .setTemperature(12.0, 74.5)
            zonesList.add(zone)
            // 3 -  zone 3 humidity        =  20% to 100%
            zone = L1_AlarmZoneTriggersModel()
                .setTime(1, 600)
                .setHumidity(20, 99)
            zonesList.add(zone)
            // 4 -  zone 4 light         = 1000 nW/cm^2
            zone = L1_AlarmZoneTriggersModel()
                .setTime(1, 600)
                .setOpticalPower(0.0, 1000.0)
            zonesList.add(zone)
            // 5 -  zone 5 shock         =  0.8 G (edited)
            zone = L1_AlarmZoneTriggersModel()
                .setTime(1, 600)
                .setShock(0.0, 0.8)
            zonesList.add(zone)

            return zonesList
        }

    }

    val minimal_tresh = InternalStructureModel(true)
    val maximal_tresh = InternalStructureModel(false)

    fun setTime(min: Long, max: Long): L1_AlarmZoneTriggersModel {
        // in new implementation min and max unixTime are unrelated
        // min is relative alarm unixTime (or expiration unixTime in case of default zone a.k.a. zone zero)
        // max is relative snooze unixTime
        minimal_tresh.time = min
        maximal_tresh.time = max
        minimal_tresh.isTime = true
        maximal_tresh.isTime = true
        return this
    }

    fun setTemperature(min: Double, max: Double): L1_AlarmZoneTriggersModel {
        if (min <= max) {
            minimal_tresh.temperature = min
            maximal_tresh.temperature = max
            minimal_tresh.isTemperature = true
            maximal_tresh.isTemperature = true
        }
        return this
    }

    fun setHumidity(min: Int, max: Int): L1_AlarmZoneTriggersModel {
        if (min <= max) {
            minimal_tresh.humidity = min
            maximal_tresh.humidity = max
            minimal_tresh.isHumidity = true
            maximal_tresh.isHumidity = true
        }
        return this
    }

    fun setOpticalPower(min: Double, max: Double): L1_AlarmZoneTriggersModel {
        if (min <= max) {
            minimal_tresh.optical_power = min
            maximal_tresh.optical_power = max
            minimal_tresh.isOpticalPower = true
            maximal_tresh.isOpticalPower = true
        }
        return this
    }

    fun setShock(min_x: Double, max_x: Double): L1_AlarmZoneTriggersModel {
        if (min_x <= max_x) {
            minimal_tresh.shock = min_x
            maximal_tresh.shock = max_x
            minimal_tresh.isShock = true
            maximal_tresh.isShock = true
        }
        return this
    }

    fun setIncline(
        min_x: Double, max_x: Double,
        min_y: Double, max_y: Double,
        min_z: Double, max_z: Double
    ): L1_AlarmZoneTriggersModel {
        if (min_x <= max_x) {
            minimal_tresh.incline_x = min_x
            maximal_tresh.incline_x = max_x
            minimal_tresh.isInclineX = true
            maximal_tresh.isInclineX = true
        }
        if (min_y <= max_y) {
            minimal_tresh.incline_y = min_y
            maximal_tresh.incline_y = max_y
            minimal_tresh.isInclineY = true
            maximal_tresh.isInclineY = true
        }
        if (min_z <= max_z) {
            minimal_tresh.incline_z = min_z
            maximal_tresh.incline_z = max_z
            minimal_tresh.isInclineZ = true
            maximal_tresh.isInclineZ = true
        }
        return this
    }

    fun setInclineX(min: Double, max: Double): L1_AlarmZoneTriggersModel {
        if (min <= max) {
            minimal_tresh.incline_x = min
            maximal_tresh.incline_x = max
            minimal_tresh.isInclineX = true
            maximal_tresh.isInclineX = true
        }
        return this
    }

    fun setInclineY(min: Double, max: Double): L1_AlarmZoneTriggersModel {
        if (min <= max) {
            minimal_tresh.incline_y = min
            maximal_tresh.incline_y = max
            minimal_tresh.isInclineY = true
            maximal_tresh.isInclineY = true
        }
        return this
    }

    fun setInclineZ(min: Double, max: Double): L1_AlarmZoneTriggersModel {
        if (min <= max) {
            minimal_tresh.incline_z = min
            maximal_tresh.incline_z = max
            minimal_tresh.isInclineZ = true
            maximal_tresh.isInclineZ = true
        }
        return this
    }

    fun hasNonDefaultTimeMin(): Boolean {
        return !DEFAULT_VALUES_ZONE.minimal_tresh.time.equals(minimal_tresh.time)
    }

    fun hasNonDefaultTimeMax(): Boolean {
        return !DEFAULT_VALUES_ZONE.maximal_tresh.time.equals(maximal_tresh.time)
    }

    fun hasNonDefaultTemperature(): Boolean {
        return !DEFAULT_VALUES_ZONE.minimal_tresh.temperature.equals(minimal_tresh.temperature)
                || !DEFAULT_VALUES_ZONE.maximal_tresh.temperature.equals(maximal_tresh.temperature)

    }

    fun hasNonDefaultHumidity(): Boolean {
        return !DEFAULT_VALUES_ZONE.minimal_tresh.humidity.equals(minimal_tresh.humidity)
                || !DEFAULT_VALUES_ZONE.maximal_tresh.humidity.equals(maximal_tresh.humidity)

    }

    fun hasNonDefaultLight(): Boolean {
        return !DEFAULT_VALUES_ZONE.minimal_tresh.optical_power.equals(minimal_tresh.optical_power)
                || !DEFAULT_VALUES_ZONE.maximal_tresh.optical_power.equals(maximal_tresh.optical_power)

    }

    fun hasNonDefaultShock(): Boolean {
        return !DEFAULT_VALUES_ZONE.minimal_tresh.shock.equals(minimal_tresh.shock)
                || !DEFAULT_VALUES_ZONE.maximal_tresh.shock.equals(maximal_tresh.shock)

    }

    fun hasNonDefaultInclineX(): Boolean {
        return !DEFAULT_VALUES_ZONE.minimal_tresh.incline_x.equals(minimal_tresh.incline_x)
                || !DEFAULT_VALUES_ZONE.maximal_tresh.incline_x.equals(maximal_tresh.incline_x)

    }

    fun hasNonDefaultInclineY(): Boolean {
        return !DEFAULT_VALUES_ZONE.minimal_tresh.incline_y.equals(minimal_tresh.incline_y)
                || !DEFAULT_VALUES_ZONE.maximal_tresh.incline_y.equals(maximal_tresh.incline_y)

    }

    fun hasNonDefaultInclineZ(): Boolean {
        return !DEFAULT_VALUES_ZONE.minimal_tresh.incline_z.equals(minimal_tresh.incline_z)
                || !DEFAULT_VALUES_ZONE.maximal_tresh.incline_z.equals(maximal_tresh.incline_z)

    }

    fun fromJson(jsonStr: String): Boolean {
        try {
            val json = jsonStr.toJson()
            return fromJson(json)
        } catch (e: Exception) {
        }
        return false
    }

    fun fromJson(json: JsonObject): Boolean {
        if (json.has("min")) {
            minimal_tresh.fromJsonObject(json.getAsJsonObject("min"))
        }
        if (json.has("max")) {
            maximal_tresh.fromJsonObject(json.getAsJsonObject("max"))
        }
        return true
    }

    fun toByteArray(): ByteArray {
        val outputBytes = ByteArrayOutputStream()
        outputBytes.write(minimal_tresh.toByteArray())
        outputBytes.write(maximal_tresh.toByteArray())
        return outputBytes.toByteArray()
    }

    fun toJsonObject(): JsonObject {
        val json = JsonObject()
        val min = minimal_tresh.toJsonObject()
        val max = maximal_tresh.toJsonObject()
        json.add("max", max)
        json.add("min", min)
        return json
    }

    override fun toString(): String {
//        return "{ min: { " +
//                "unixTime : ${minimal_tresh.unixTime}, " +
//                "temperature : ${minimal_tresh.temperature}, " +
//                "optical_power : ${minimal_tresh.optical_power}, " +
//                "humidity : ${minimal_tresh.humidity}, " +
//                "acceleration : ${minimal_tresh.shock} G, " +
//                "incline : [${minimal_tresh.incline_x},${minimal_tresh.incline_y},${minimal_tresh.incline_z}] " +
//                "}, max : {" +
//                "unixTime : ${this.maximal_tresh.unixTime}, " +
//                "temperature : ${this.maximal_tresh.temperature}, " +
//                "optical_power : ${maximal_tresh.optical_power}, " +
//                "humidity : ${maximal_tresh.humidity}, " +
//                "acceleration : ${maximal_tresh.shock} G, " +
//                "incline : [${maximal_tresh.incline_x},${maximal_tresh.incline_y},${maximal_tresh.incline_z}] " +
//                "}"
        return toJsonObject().toShortJsonString()
    }


    fun equals(other: L1_AlarmZoneTriggersModel): Boolean {
        return minimal_tresh.equals(other.minimal_tresh)
                && maximal_tresh.equals(other.maximal_tresh)
    }

    fun debugHexString(): String {
        return "All Hex: ${toByteArray().toHexSeparated()}\n" +
                "Min : ${minimal_tresh.debugHex()}\n" +
                "Max : ${maximal_tresh.debugHex()}\n"
    }
}

fun ByteArray.toVariableAdvertisingTriggers(): L1_AlarmZoneTriggersModel {
    return L1_AlarmZoneTriggersModel(this)
}

fun JsonObject.toVariableAdvertisingTriggers(): L1_AlarmZoneTriggersModel {
    val tmp = L1_AlarmZoneTriggersModel()
    tmp.fromJson(this)
    return tmp
}

fun String.toVariableAdvertisingTriggers(): L1_AlarmZoneTriggersModel {
    val tmp = L1_AlarmZoneTriggersModel()
    tmp.fromJson(this)
    return tmp
}

fun List<L1_AlarmZoneTriggersModel>.toJsonArrayString(): String {
    val jsonArray = JsonArray()
    this.forEach { zone ->
        jsonArray.add(zone.toJsonObject())
    }
    return jsonArray.toString()
}

fun String.toAlarmZoneList(): List<L1_AlarmZoneTriggersModel> {
    val arrayList = arrayListOf<L1_AlarmZoneTriggersModel>()
    try {
        val jsonArray = this.toJsonArray()
        jsonArray.forEach { elem ->
            arrayList.add(elem.asJsonObject.toVariableAdvertisingTriggers())
        }
    } catch (e: Exception) {
        // consume
    }
    return arrayList
}

fun ByteArray.readDeviceBufferedZones(): List<L1_AlarmZoneTriggersModel>? {
    // incoming min structs then max structs
    // expecting STRUCT SIZE * MAX COUNT // 40*16
    val convertedArray = arrayListOf<L1_AlarmZoneTriggersModel>()
    val expSize =
        L1_AlarmZoneTriggersModel.TOTAL_DEVICE_BUFFERRED_STRUCTS * L1_AlarmZoneTriggersModel.SIZE_TOTAL_STRUCT_BYTES
    if (size != expSize)
        return null
    val oneStructSize = L1_AlarmZoneTriggersModel.SIZE_INTERNAL_STRUCT_BYTES
    val twoStructSize = L1_AlarmZoneTriggersModel.SIZE_TOTAL_STRUCT_BYTES

    var foundZonesCount = 0
    for (i in 0 until L1_AlarmZoneTriggersModel.TOTAL_DEVICE_BUFFERRED_STRUCTS) { // 0 - 15
        try {
            val testingBytes = copyOfRange(i * twoStructSize, i * twoStructSize + twoStructSize)
            // zero check
            var onlyZeros = true
            testingBytes.forEach {
                if (onlyZeros && it != 0.toByte())
                    onlyZeros = false
            }
            if (onlyZeros)
            // less alarm zones than MAX COUNT
                break

            foundZonesCount += 1
        } catch (e: Exception) {
        }
    }
    val offsetMax = foundZonesCount * oneStructSize

    if (foundZonesCount > 0 && offsetMax > 0)
        for (i in 0 until foundZonesCount) { // 0 - 15
            try {
                val minBytes = copyOfRange(i * oneStructSize, i * oneStructSize + oneStructSize)
                val maxBytes = copyOfRange(
                    offsetMax + i * oneStructSize,
                    offsetMax + i * oneStructSize + oneStructSize
                )
                val combinedBytes = (minBytes + maxBytes)
                val alarmZone = combinedBytes.toVariableAdvertisingTriggers()
                convertedArray.add(alarmZone)

            } catch (e: Exception) {
            }

        }

    if (convertedArray.isEmpty()) {
        return null
    }
    // return all zones
    return convertedArray
}