package com.crayonic.device_sdk.device.errors

class DeviceOutOfReachError(detailedMessage: String = ""):
    CrayonicDeviceError(
        detailedMessage = detailedMessage,
        errorType = CrayonicDeviceErrorType.DeviceOutOfReachError
    )