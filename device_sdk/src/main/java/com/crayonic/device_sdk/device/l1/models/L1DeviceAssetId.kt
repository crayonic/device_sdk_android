package com.crayonic.device_sdk.device.l1.models

import com.crayonic.device_sdk.core.Single.zeros32
import space.lupu.koex.bytes.BytesInterface
import space.lupu.koex.hex.hexStringToByteArray
import space.lupu.koex.hex.toHex

class L1DeviceAssetId(initBytes: ByteArray):BytesInterface<L1DeviceAssetId>{
    val bytes:ByteArray
    init{
        if (initBytes.size == 32)
            bytes = initBytes
        else
            bytes = zeros32
    }

    override fun fromByteArray(bytes: ByteArray): L1DeviceAssetId {
        return L1DeviceAssetId(bytes)
    }

    override fun toByteArray(): ByteArray {
        return bytes
    }

    override fun toString(): String {
        return bytes.toHex(includePrefix0x = true)
    }

    companion object{
        fun fromHex(hexString: String): L1DeviceAssetId =
            L1DeviceAssetId(hexString.hexStringToByteArray())

    }
}