package com.crayonic.device_sdk.device.l1.models

import com.google.gson.JsonObject
import space.lupu.koex.bytes.BytesInterface

class CrayonicAlarmZone :BytesInterface<CrayonicAlarmZone>{

    constructor(){
        l1_AlarmZone = L1_AlarmZoneTriggersModel.DEFAULT_ZONE()
    }

    constructor(bytes: ByteArray) {
        l1_AlarmZone = bytes.toVariableAdvertisingTriggers()
    }

    constructor(string: String){
        l1_AlarmZone = string.toVariableAdvertisingTriggers()
    }

    constructor(jsonObject: JsonObject){
        l1_AlarmZone = jsonObject.toVariableAdvertisingTriggers()
    }

    constructor(alarmZone: L1_AlarmZoneTriggersModel){
        l1_AlarmZone = alarmZone
    }

    var l1_AlarmZone: L1_AlarmZoneTriggersModel
        private set(value) {field = value}

    override fun fromByteArray(bytes: ByteArray): CrayonicAlarmZone {
        return CrayonicAlarmZone(bytes)
    }

    override fun toByteArray(): ByteArray {
        return l1_AlarmZone.toByteArray()
    }

    override fun toString(): String = l1_AlarmZone.toString()

    fun toJsonObject():JsonObject = l1_AlarmZone.toJsonObject()

    fun toJsonString():String = l1_AlarmZone.toString()

    companion object{
        val BYTE_SIZE = 40
    }
}

