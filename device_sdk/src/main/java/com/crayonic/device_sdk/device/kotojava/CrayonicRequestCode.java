package com.crayonic.device_sdk.device.kotojava;

public enum CrayonicRequestCode{
    unspecified,
    // all devices
    connectDevice,
    disconnectDevice,
    readDeviceAddress,
    readDeviceName,
    readBatteryLevel,
    readModelNumber,
    readFirmwareVersion,
    runCommandRawBytes,
    runCommandReadClock,
    runCommandWriteClock,
    runCommandReadPublicKey,
    runCommandReadEthAddress,
    runCommandResetToPowerOff,
    runCommandResetToUpdateMode,
    runTaskUpdateDevice,

    // Pen requests
//    runCommandSetPinCode,
    runCommandSignEthTx,
    runTaskInitializePen,
    runTaskReadData,

    // L1 requests
    readEthAddress,
    readAssetId,
    readTemperature,
    readHumidity,
    readLight,
    readShock,
    readIncline,
    runCommandReadAlarmZones,
    runCommandWriteAlarmZone,
    runCommandReadAssetId,
    runCommandWriteAssetId,
    runCommandReadPackageId,
    runCommandWritePackageId,
    runCommandSetManagementPinCode,
    runTaskReadEvents,
    runTaskInitialization,
    runTaskFactorySetup,
    runTaskL1DeviceTest
}
