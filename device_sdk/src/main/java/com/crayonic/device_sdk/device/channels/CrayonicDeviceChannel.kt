package com.crayonic.device_sdk.device.channels

import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.util.TagsEnum
import com.crayonic.device_sdk.util.toTags

abstract class CrayonicDeviceChannel(
    id: String = Single.cacheId(TagsEnum.device_channel.name),
    tags: List<TagsEnum> = listOf(TagsEnum.device_channel, TagsEnum.device),
    val supportedRequests: List<CrayonicRequestCode>,
    val memberChannel: CrayonicMemberChannel
) : CrayonicMemberChannel by memberChannel,
    CrayonicApduChannel(id, tags.toTags()) {

    override fun supportsRequestCode(requestKey: CrayonicRequestCode): Boolean {
        return supportedRequests.contains(requestKey) || memberChannel.supportsRequestCode(requestKey)
    }
}