package com.crayonic.device_sdk.device.channels

import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode

/**
 * Each Crayonic device has access to following member values
 */
interface CrayonicMemberChannel{
    fun readMemberForRequest(requestKey: CrayonicRequestCode, onMidResult: (String) -> Unit)

    fun readDeviceAddress(onMidResult: (String)->Unit):Boolean
    fun readDeviceName(onMidResult: (String)->Unit):Boolean
    fun readBatteryLevel(onMidResult: (String)->Unit):Boolean
    fun readFirmwareVersion(onMidResult: (String)->Unit):Boolean
    fun readModelNumber(onMidResult: (String)->Unit):Boolean

    fun supportsRequestCode(requestKey: CrayonicRequestCode): Boolean
}
