package com.crayonic.device_sdk.device.l1.models

import space.lupu.koex.bytes.BytesInterface

class AlarmZonesCollection(initList: List<CrayonicAlarmZone> = listOf()) :
    BytesInterface<AlarmZonesCollection> {
    private var internalList = arrayListOf<CrayonicAlarmZone>()

    init {
        internalList.addAll(initList)
    }

    override fun toByteArray(): ByteArray {
        var result = byteArrayOf()
        internalList.map { it.l1_AlarmZone.toByteArray() }.forEach {
            result = result.plus(it)
        }
        return result
    }

    override fun fromByteArray(bytes: ByteArray): AlarmZonesCollection {
        val g = AlarmZonesCollection()
        g.appendL1AlarmZoneGroup(bytes)
        return g
    }

    /**
     * Append alarm zones extracted from special format L1 creates for group of alarm zones
     *
     * Zones are not ordered normally
     *
     * @return true on success
     */
    fun appendL1AlarmZoneGroup(bytes: ByteArray): Boolean {
        val newZones = l1AlarmZoneGroupToList(bytes)
        if (newZones != null) {
            internalList.addAll(newZones)
            return true
        }
        return false
    }

    /**
     * Append alarm zones extracted from special format L1 creates for group of alarm zones
     *
     * @return null on fail , list otherwise
     */
    fun l1AlarmZoneGroupToList(bytes: ByteArray):List<CrayonicAlarmZone>?{
        return bytes.readDeviceBufferedZones()?.map { CrayonicAlarmZone(it) }
    }


    fun toList() = internalList.toList()

    companion object {
        val MAX_BYTE_SIZE = 40 * 16

        fun defaultAlarmZones():AlarmZonesCollection{
            return AlarmZonesCollection(
                L1_AlarmZoneTriggersModel.defaultAlarmZones().map {
                    CrayonicAlarmZone(it)
                }
            )
        }
    }
}