package com.crayonic.device_sdk.device

import android.util.Log
import com.crayonic.device_sdk.http.l1_json_api.L1ApiJson_L2Event
import com.crayonic.ambrosus.gateway.comm.http.model.L1JsonApi_Asset
import com.crayonic.device_sdk.apdu.ApduType
import com.crayonic.device_sdk.apdu.adminPinToByteArray
import com.crayonic.device_sdk.core.Controller
import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.core.Single.eventCrc32
import com.crayonic.device_sdk.device.errors.CrayonicDeviceError
import com.crayonic.device_sdk.device.kotojava.CrayonicDeviceListener
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode.*
import com.crayonic.device_sdk.device.kotojava.OfflineProvisioningListener
import com.crayonic.device_sdk.device.kotojava.OfflineReadEventListener
import com.crayonic.device_sdk.device.l1.models.*
import com.crayonic.device_sdk.http.l1_json_api.L1ApiJson_L1Event
import com.crayonic.device_sdk.http.l1_json_api.L1JsonApi_Account
import com.crayonic.device_sdk.util.*
import kotlinx.coroutines.*
import okhttp3.Response
import space.lupu.kapdu.statusWords.ApduStatusWord
import space.lupu.koex.hex.Hex
import space.lupu.koex.hex.toHex
import space.lupu.koex.hex.toHexSeparated
import space.lupu.koex.lambda.emptyLambda1inUout
import space.lupu.koex.lambda.emptyLambda2inUout
import space.lupu.koex.numbers.toInt
import kotlin.coroutines.resume

@Suppress("UNUSED_PARAMETER", "unused", "UNUSED_ANONYMOUS_PARAMETER")
class CrayonicL1(initNode: CrayonicDeviceNode) : CrayonicDevice(initNode) {

    override val modelTag = "CrayonicL1"

    var ethAddress: String = ""
    var assetId: String = ""
    var temperature: String = ""
    var humidity: String = ""
    var light: String = ""
    var shock: String = ""
    var incline: String = ""

    override val supportedReadValueMembers: List<CrayonicRequestCode> =
        arrayListOf<CrayonicRequestCode>().apply{
            addAll(super.supportedReadValueMembers)
            addAll(listOf(
                readEthAddress,
                readAssetId,
                readTemperature,
                readHumidity,
                readLight,
                readShock
            ))
        }

    override fun interceptReadMemberValue(requestKey: CrayonicRequestCode, strValue: String) {
        // set the value to local members / properties
        super.interceptReadMemberValue(requestKey,strValue)
        when(requestKey){
            readEthAddress -> ethAddress = strValue
            readAssetId -> assetId = strValue
            readTemperature -> temperature = strValue
            readHumidity -> humidity = strValue
            readLight -> light = strValue
            readShock -> shock = strValue
            readIncline -> incline = strValue
        }
    }

    fun readEthAddress(deviceListener: CrayonicDeviceListener) =
        internal_readMemberValue(
            deviceListener = deviceListener,
            requestKey = readEthAddress
        )

    fun readEthAddress(onResult: (device: CrayonicDevice, readableValue: String) -> Unit = emptyLambda2inUout()) =
        internal_readMemberValue(
            onResult = onResult,
            requestKey = readEthAddress
        )

    fun readAssetId(deviceListener: CrayonicDeviceListener) =
        internal_readMemberValue(
            deviceListener = deviceListener,
            requestKey = readAssetId
        )

    fun readAssetId(onResult: (device: CrayonicDevice, readableValue: String) -> Unit = emptyLambda2inUout()) =
        internal_readMemberValue(
            onResult = onResult,
            requestKey = readAssetId
        )

    fun readTemperature(deviceListener: CrayonicDeviceListener) =
        internal_readMemberValue(
            deviceListener = deviceListener,
            requestKey = readTemperature
        )

    fun readTemperature(onResult: (device: CrayonicDevice, readableValue: String) -> Unit = emptyLambda2inUout()) =
        internal_readMemberValue(
            onResult = onResult,
            requestKey = readTemperature
        )

    fun readLight(deviceListener: CrayonicDeviceListener) =
        internal_readMemberValue(
            deviceListener = deviceListener,
            requestKey = readLight
        )

    fun readLight(onResult: (device: CrayonicDevice, readableValue: String) -> Unit = emptyLambda2inUout()) =
        internal_readMemberValue(
            onResult = onResult,
            requestKey = readLight
        )

    fun readHumidity(deviceListener: CrayonicDeviceListener) =
        internal_readMemberValue(
            deviceListener = deviceListener,
            requestKey = readHumidity
        )

    fun readHumidity(onResult: (device: CrayonicDevice, readableValue: String) -> Unit = emptyLambda2inUout()) =
        internal_readMemberValue(
            onResult = onResult,
            requestKey = readHumidity
        )

    fun readShock(deviceListener: CrayonicDeviceListener) =
        internal_readMemberValue(
            deviceListener = deviceListener,
            requestKey = readShock
        )

    fun readShock(onResult: (device: CrayonicDevice, readableValue: String) -> Unit = emptyLambda2inUout()) =
        internal_readMemberValue(
            onResult = onResult,
            requestKey = readShock
        )

    fun readIncline(deviceListener: CrayonicDeviceListener) =
        internal_readMemberValue(
            deviceListener = deviceListener,
            requestKey = readIncline
        )

    fun readIncline(onResult: (device: CrayonicDevice, readableValue: String) -> Unit = emptyLambda2inUout()) =
        internal_readMemberValue(
            onResult = onResult,
            requestKey = readIncline
        )

    fun runCommandReadAlarmZones(
        deviceListener: CrayonicDeviceListener,
        alarmZones: AlarmZonesCollection
    ) =
        internal_runCommandApdu(
            ApduType.ro_alarm_zones,
            runCommandReadAlarmZones,
            deviceListener = deviceListener
        )

    fun runCommandReadAlarmZones(
        onResult: (device: CrayonicDevice, alarmZones: AlarmZonesCollection) -> Unit = emptyLambda2inUout()
    ) =
        internal_runCommandApdu(
            ApduType.ro_alarm_zones,
            runCommandReadAlarmZones
        ) { onResult.invoke(this, it as AlarmZonesCollection) }

    fun runCommandWriteAlarmZone(
        alarmZone: CrayonicAlarmZone,
        deviceListener: CrayonicDeviceListener
    ) =
        internal_runCommandApdu(
            ApduType.wr_alarm_zone,
            runCommandSetManagementPinCode,
            alarmZone,
            deviceListener = deviceListener
        )

    fun runCommandWriteAlarmZone(
        alarmZone: CrayonicAlarmZone,
        onResult: (device: CrayonicDevice) -> Unit = emptyLambda1inUout()
    ) =
        internal_runCommandApdu(
            ApduType.wr_alarm_zone,
            runCommandWriteAlarmZone,
            alarmZone
        ) { onResult.invoke(this) }

    fun runCommandReadAssetId(
        deviceListener: CrayonicDeviceListener,
        deviceAssetId: L1DeviceAssetId
    ) =
        internal_runCommandApdu(
            ApduType.ro_asset_id,
            runCommandReadAssetId,
            deviceListener = deviceListener
        )

    fun runCommandReadAssetId(
        onResult: (device: CrayonicDevice, deviceAssetId: L1DeviceAssetId) -> Unit = emptyLambda2inUout()
    ) =
        internal_runCommandApdu(
            ApduType.ro_asset_id,
            runCommandReadAssetId
        ) { onResult.invoke(this, it as L1DeviceAssetId) }

    fun runCommandWriteAssetId(
        deviceAssetId: L1DeviceAssetId,
        deviceListener: CrayonicDeviceListener
    ) =
        internal_runCommandApdu(
            ApduType.wr_asset_id,
            runCommandWriteAssetId,
            deviceAssetId,
            deviceListener = deviceListener
        )

    fun runCommandWriteAssetId(
        deviceAssetId: L1DeviceAssetId,
        onResult: (device: CrayonicDevice) -> Unit = emptyLambda1inUout()
    ) =
        internal_runCommandApdu(
            ApduType.wr_asset_id,
            runCommandWriteAssetId,
            deviceAssetId
        ) { onResult.invoke(this) }

    fun runCommandReadPackageId(deviceListener: CrayonicDeviceListener, packageId: String) =
        internal_runCommandApdu(
            ApduType.ro_package_id,
            runCommandReadPackageId,
            deviceListener = deviceListener
        )

    fun runCommandReadPackageId(
        onResult: (device: CrayonicDevice, packageId: String) -> Unit = emptyLambda2inUout()
    ) =
        internal_runCommandApdu(
            ApduType.ro_package_id,
            runCommandReadPackageId
        ) { onResult.invoke(this, it as String) }

    fun runCommandWritePackageId(packageId: String, deviceListener: CrayonicDeviceListener) =
        internal_runCommandApdu(
            ApduType.wr_package_id,
            runCommandWritePackageId,
            packageId,
            deviceListener = deviceListener
        )

    fun runCommandWritePackageId(
        packageId: String, onResult: (device: CrayonicDevice) -> Unit = emptyLambda1inUout()
    ) =
        internal_runCommandApdu(
            ApduType.wr_package_id,
            runCommandWritePackageId,
            packageId
        ) { onResult.invoke(this) }


    ////////////////////////
    fun runTaskReadEventsOffline(deviceListener: CrayonicDeviceListener, offlineResultListener: OfflineReadEventListener){
        internal_runTask(
            runTaskReadEvents,
            inputValue = false,
            deviceListener = deviceListener,
            onMidResult = { outputValue ->
                if(outputValue is List<*>){
                    val resultList = arrayListOf<String>()
                    outputValue.forEach { any: Any? ->
                        any?.let { if (it is L1ApiJson_L1Event) resultList.add(it.toJsonString())}
                    }
                    offlineResultListener.onResult(resultList)
                }
            },
            timeout = seconds_600, // timeout for all the read events together .. it can be less tho
            runBlock = readEvent()
        )
    }

    fun runTaskReadEventsOffline(onResult:(device: CrayonicL1, eventJsons: List<String>)->Unit){
        internal_runTask(
            runTaskReadEvents,
            inputValue = false,
            deviceListener = null,
            onMidResult = { outputValue ->
                if(outputValue is List<*>){
                    val resultList = arrayListOf<String>()
                    outputValue.forEach { any: Any? ->
                        any?.let { if (it is L1ApiJson_L1Event) resultList.add(it.toJsonString())}
                    }
                    onResult(this, resultList)
                }
            },
            timeout = seconds_600, // timeout for all the read events together .. it can be less tho
            runBlock = readEvent()
        )
    }

    fun runTaskReadEvents(uploadEventsOnSuccess: Boolean = true, deviceListener: CrayonicDeviceListener) =
        internal_runTask(
            runTaskReadEvents,
            inputValue = uploadEventsOnSuccess,
            deviceListener = deviceListener,
            onMidResult = {},
            timeout = seconds_600, // timeout for all the read events together .. it can be less tho
            runBlock = readEvent()
        )

    fun runTaskReadEvents(uploadEventsOnSuccess: Boolean = true, onResult: (device: CrayonicL1, events: List<L1ApiJson_L1Event>) -> Unit = emptyLambda2inUout()) =
        internal_runTask(
            runTaskReadEvents,
            inputValue = uploadEventsOnSuccess,
            deviceListener = null,
            onMidResult = { onResult.invoke(this, it as List<L1ApiJson_L1Event> ) },
            timeout = seconds_600, // timeout for all the read events together .. it can be less tho
            runBlock = readEvent()
        )

    protected fun readEvent(): (
        input: Any?,
        onProgress: (progress: Progress) -> Unit,
        onTaskResult: (Any) -> Unit
    ) -> ExecutionStatus = { input, onProgress, onTaskResult ->
        var executionStatus = ExecutionStatus.error
        when(input){
            is Boolean -> {
                executionStatus = readEvent_executionBlock(input, onProgress, onTaskResult)
            }
        }
        if (executionStatus != ExecutionStatus.ok){
            onTaskResult.invoke(listOf<L1ApiJson_L1Event>())
        }
        executionStatus
    }

    private fun readEvent_executionBlock(
        shouldUpload: Boolean,
        onProgress: (progress: Progress) -> Unit,
        onTaskResult: (Any) -> Unit
    ): ExecutionStatus {
        var executionStatus = ExecutionStatus.ok
        val taskReqKey = runTaskReadEvents

        val progress_wr_event_start = SubProgress()
        val progress_wr_event_finish = SubProgress()
        val overallProgress = ProgressAggregator(
            progress_wr_event_start,
            progress_wr_event_finish
        )
        var hasAnotherEvent = true


        val taskJob = Job(deviceJob)
        CoroutineScope(Dispatchers.IO + taskJob).launch {

            val gatheredEvents = arrayListOf<L1ApiJson_L1Event>()
            while (hasAnotherEvent) {
                progress_wr_event_start.clear()
                progress_wr_event_finish.clear()
                // maybe add input pin  in the future
                var dataBytes = byteArrayOf()
                val is2wayApdu = suspendCancellableCoroutine<Boolean?> { cont ->

                    internal_runCommandApdu(
                        apduType = ApduType.wr_event_start,
                        requestKey = taskReqKey,
                        onProgress = { requestKey, progress ->
//                            invokeError(CrayonicDeviceError("event start progress $progress"))
                            progress_wr_event_start.update(progress)
                            onProgress(overallProgress.totalProgress())
//                            invokeError(CrayonicDeviceError("event read total progress ${overallProgress.totalProgress()}"))
                        },
                        onMidResult = { outputValue: Any ->
                            cont.resume(
                                when (outputValue) {
                                    is ByteArray -> {
                                        dataBytes = outputValue
                                        true
                                    }
                                    is ApduStatusWord -> if (outputValue.toApduExecutionStatus() == ExecutionStatus.ok) false else null
                                    is ExecutionStatus -> if (outputValue == ExecutionStatus.ok) false else null
                                    else -> null
                                }
                            )
                        }
                    )
                }
                if (is2wayApdu == null) {
                    onTaskResult(gatheredEvents)
//                    invokeError(CrayonicDeviceError("TEST_EVENT - is2wayApdu == null / returning events : " +
//                            gatheredEvents.joinToString(separator = "\n")
//                            { event-> "eventId: ${event.eventId} assetId: ${event.assetId}"}
//                    ))
                    invokeApduErrorAndDiconnect(taskReqKey, ApduType.wr_event_start)
                    return@launch
                }
                if (is2wayApdu) {
                    // 2 way apdu read
                    // nothing to be done we already have the data .. all devices with fw above 2 covered
                } else {
                    invokeError(CrayonicDeviceError("4 step read event process is not implemented"))

                    // 4 step read event
                    TODO("implement 4 step read event")
                }
                val validator = L1EventValidator(dataBytes)
                val ctrl = Controller.getInstance()
                val event = L1ApiJson_L1Event(validator)
                val eventId = event.eventId

//                invokeError(CrayonicDeviceError("TEST_EVENT event $eventId :  isValid = ${validator.isValid()} crc = ${validator.getCRC().toHex()}"))

                if (validator.isValid()) {
                    invokeError(CrayonicDeviceError("adding event to gathered"))
                    gatheredEvents.add(event)
                }

                // finish event with crc from validator
                executionStatus = suspendCancellableCoroutine { cont ->
                    readEventFinish(
                        crcBytes = validator.getCRC(),
                        taskReqKey = taskReqKey,
                        onProgress = { progress ->
//                            invokeError(CrayonicDeviceError("event finish progress $progress"))
                            progress_wr_event_finish.update(progress)
                            onProgress(overallProgress.totalProgress())
//                            invokeError(CrayonicDeviceError("event read total progress ${overallProgress.totalProgress()}"))
                        },
                        onSubTaskResult = {
                            cont.resume(it)
                        }
                    )
                }
                invokeError(CrayonicDeviceError("TEST_EVENT event $eventId : finish execStatus = $executionStatus hasAnotherEvent ?= ${(executionStatus == ExecutionStatus.read_next_event)}"))

                if (shouldUpload) {
                    invokeError(CrayonicDeviceError("uploading event"))
                    ctrl.uploadIfPossible(event)
                }

                hasAnotherEvent = (executionStatus == ExecutionStatus.read_next_event)
            }
//            invokeError(CrayonicDeviceError("TEST_EVENT returning events : " +
//                    gatheredEvents.joinToString(separator = "\n")
//                    { event-> "eventId: ${event.eventId} assetId: ${event.assetId}"}
//            ))

            onTaskResult(gatheredEvents)
            taskJob.cancel()
        }
        return executionStatus
    }


    protected fun readEventFinish(
        crcBytes : ByteArray = eventCrc32,
        taskReqKey: CrayonicRequestCode,
        onProgress: (progress: Progress) -> Unit,
        onSubTaskResult: (ExecutionStatus) -> Unit
    ){
        val validCrc = if (crcBytes.size != eventCrc32.size) eventCrc32 else crcBytes
        invokeError(CrayonicDeviceError("Sending event finish : ${Hex(validCrc)}"))
        internal_runCommandApdu(
            apduType = ApduType.wr_event_finish,
            requestKey = taskReqKey,
            inputValue = validCrc,
            onProgress = { requestKey, progress ->
                onProgress(progress)
            },
            onMidResult = { outputValue: Any ->
                invokeError(CrayonicDeviceError("TEST_EVENT finish $outputValue "))

                when (outputValue) {
                    is ByteArray -> onSubTaskResult(ApduStatusWord(outputValue).toApduExecutionStatus())
                    is ApduStatusWord -> onSubTaskResult(outputValue.toApduExecutionStatus())
                    is ExecutionStatus -> onSubTaskResult(outputValue)
                    else -> onSubTaskResult(ExecutionStatus.error)
                }
            }
        )
    }

    /////////////////////////
    fun runTaskInitializationOffline(
        initializationValues: L1InitOfflineValues,
        deviceListener: CrayonicDeviceListener,
        offlineResultListener: OfflineProvisioningListener
    ) =
        internal_runTask(
            runTaskInitialization,
            inputValue = initializationValues,
            deviceListener = deviceListener,
            onMidResult = {outputValue: Any ->
                if(outputValue is L1ProvisionedDeviceData){
                    offlineResultListener.onResult(outputValue)
                }else{
                    offlineResultListener.onResult(L1ProvisionedDeviceData("","",""))
                }
            },
            timeout = seconds_120, // this hasto change 10 minutes is too long of a tieout
            runBlock = initializeL1()
        )

    fun runTaskInitializationOffline(
        initializationValues: L1InitOfflineValues,
        onResult: (device: CrayonicL1, deviceData: L1ProvisionedDeviceData) -> Unit = emptyLambda2inUout()
    ) =
        internal_runTask(
            runTaskInitialization,
            inputValue = initializationValues,
            deviceListener = null,
            onMidResult = { outputValue: Any ->
                if(outputValue is L1ProvisionedDeviceData){
                    onResult.invoke(this, outputValue)
                }else{
                    onResult.invoke(this, L1ProvisionedDeviceData("","",""))
                }
            },
            timeout = seconds_120, // this hasto change 10 minutes is too long of a tieout
            runBlock = initializeL1()
        )

    fun runTaskInitialization(
        initializationValues: L1InitValues, deviceListener: CrayonicDeviceListener
    ) =
        internal_runTask(
            runTaskInitialization,
            inputValue = initializationValues,
            deviceListener = deviceListener,
            onMidResult = {},
            timeout = seconds_120, // this hasto change 10 minutes is too long of a tieout
            runBlock = initializeL1()
        )

    fun runTaskInitialization(
        initializationValues: L1InitValues,
        onResult: (device: CrayonicL1) -> Unit = {}
    ) =
        internal_runTask(
            runTaskInitialization,
            inputValue = initializationValues,
            deviceListener = null,
            onMidResult = { onResult.invoke(this) },
            timeout = seconds_120, // this hasto change 10 minutes is too long of a tieout
            runBlock = initializeL1()
        )


    /**
     * Internal routing function
     */
    protected fun initializeL1(): (input: Any?, onProgress: (progress: Progress) -> Unit, onTaskResult: (Any) -> Unit) -> ExecutionStatus {
        return { input, onProgress, onTaskResult ->
            var executionStatus = ExecutionStatus.error
            when(input){
                null -> executionStatus
                is L1InitValues -> {
                    executionStatus = initializeL1_provisioningBlock(input,onProgress, onTaskResult)
                }
                is L1InitOfflineValues -> {
                    executionStatus = initializeL1_offlineProvisioningBlock(input, onProgress, onTaskResult)
                }
                else -> executionStatus
            }
            executionStatus
        }
    }


    private fun initializeL1_offlineProvisioningBlock(
        initValues:L1InitOfflineValues,
        onProgress: (progress: Progress) -> Unit,
        onTaskResult: (Any) -> Unit
    ): ExecutionStatus {
        var executionStatus = ExecutionStatus.error
        val taskReqKey = runTaskInitialization
        val progress_keys = SubProgress()
        val progress_eth_add = SubProgress()
        val progress_pub_add = SubProgress()
        val progress_admin_pin = SubProgress()
        val progress_asset = SubProgress()
        val progress_alarm_zones = SubProgress().apply { transmissionProgress.expectedFinalLength = initValues.alarmZones.size*100 }
        val progress_package_id = SubProgress()
        val progress_clock = SubProgress()

        val overallProgress = ProgressAggregator(
            progress_keys,
            progress_admin_pin,
            progress_eth_add,
            progress_pub_add,
            progress_alarm_zones,
            progress_package_id,
            progress_asset,
            progress_clock
        )

        val adminOldPin = Single.pin32

        val taskJob =
            CoroutineScope(Dispatchers.IO + Job(deviceJob)).launch {
                // Pre-requisites
                val ctrl = Controller.getInstance()
                ctrl.takeBestModuleForTags(TagsEnum.json_api.name) ?: return@launch

//                invokeError(CrayonicDeviceError("KEYS "))


                //---------------------------------------------
                // Generate Keys : blocking
                executionStatus = suspendCancellableCoroutine<ExecutionStatus> { cont ->
                    internal_runCommandApdu(
                        apduType = ApduType.wr_generate_changing_keys,
                        requestKey = taskReqKey,
                        inputValue = adminOldPin,
                        onProgress = { requestKey, progress ->
                            progress_keys.update(progress)
                            onProgress(overallProgress.totalProgress())
                        },
                        onMidResult = { outputValue: Any ->
//                            invokeError(CrayonicDeviceError("KEYS : outputValue : $outputValue"))
                            cont.resume(
                                when (outputValue) {
                                    is ByteArray -> ApduStatusWord(outputValue.toInt()).toApduExecutionStatus()
                                    is ApduStatusWord -> outputValue.toApduExecutionStatus()
                                    is ExecutionStatus -> outputValue
                                    else -> ExecutionStatus.error
                                }
                            )
                        }
                    )
                }

                if (executionStatus != ExecutionStatus.ok) {
                    onTaskResult(executionStatus)
                    return@launch
                }
                // uncomment with pin implementation on L1 FW
                // Write management / admin pin code : blocking
//            executionStatus = suspendCancellableCoroutine<ExecutionStatus>{ cont ->
//                internal_runCommandApdu(
//                    apduType = ApduType.wr_management_pin,
//                    requestKey = taskReqKey,
//                    inputValue = adminPinToByteArray(initValues.adminPin)+adminPinToByteArray(initValues.newPin),
//                    onProgress = {requestKey, progress ->
//                        progress_admin_pin.update(progress)
//                        onProgress(overallProgress.totalProgress())
//                    },
//                    onMidResult = {outputValue: Any ->
//                        cont.resume(
//                            when (outputValue) {
//                                is ApduStatusWord -> outputValue.toApduExecutionStatus()
//                                is ExecutionStatus -> outputValue
//                                else -> ExecutionStatus.error
//                            }
//                        )
//                    }
//                )
//            }
//            if(executionStatus != ExecutionStatus.ok){  return@launch }

//                invokeError(CrayonicDeviceError("ETH ADD "))

                var ethAddressOfDevice = ""

                //---------------------------------------------
                // Read Eth Address : blocking
                executionStatus = suspendCancellableCoroutine<ExecutionStatus> { cont ->
                    internal_runCommandApdu(
                        apduType = ApduType.ro_eth_address,
                        requestKey = taskReqKey,
                        onProgress = { requestKey, progress ->
                            progress_eth_add.update(progress)
                            onProgress(overallProgress.totalProgress())
                        },
                        onMidResult = { outputValue: Any ->
//                            invokeError(CrayonicDeviceError("ETH ADD: outputValue : $outputValue"))

                            cont.resume(
                                when (outputValue) {
                                    is String -> {
//                                        invokeError(CrayonicDeviceError("ETH ADD: outputValue : $outputValue"))
                                        ethAddressOfDevice = Hex(outputValue, true).toString()
                                        ExecutionStatus.ok
                                    }
                                    is ByteArray -> {
//                                        invokeError(CrayonicDeviceError("ETH ADD: outputValue : ${outputValue.toHexSeparated()}"))
                                        ethAddressOfDevice = Hex(outputValue, true).toString()
                                        ExecutionStatus.ok
                                    }
                                    else -> ExecutionStatus.error
                                }
                            )
                        }
                    )
                }

                if (ethAddressOfDevice.isEmpty()) executionStatus = ExecutionStatus.error

                if (executionStatus != ExecutionStatus.ok) {
                    onTaskResult(executionStatus)
                    return@launch
                }

//                invokeError(CrayonicDeviceError("PUB KEY"))
                var publicKeyOfDevice = ""

                //---------------------------------------------
                // Read Public Key of shipment: blocking
                executionStatus = suspendCancellableCoroutine<ExecutionStatus> { cont ->
                    internal_runCommandApdu(
                        apduType = ApduType.ro_public_changing_key,
                        requestKey = taskReqKey,
                        onProgress = { requestKey, progress ->
                            progress_pub_add.update(progress)
                            onProgress(overallProgress.totalProgress())
                        },
                        onMidResult = { outputValue: Any ->
                            invokeError(CrayonicDeviceError("PUB KEY: outputValue : $outputValue"))
                            cont.resume(
                                when (outputValue) {
                                    is String -> {
                                        invokeError(CrayonicDeviceError("PUB KEY: outputValue : $outputValue"))
                                        publicKeyOfDevice = Hex(outputValue, true).toString()
                                        ExecutionStatus.ok
                                    }
                                    is ByteArray -> {
                                        invokeError(CrayonicDeviceError("PUB KEY: outputValue : ${outputValue.toHexSeparated()}"))
                                        publicKeyOfDevice = Hex(outputValue, true).toString()
                                        ExecutionStatus.ok
                                    }
                                    else -> ExecutionStatus.error
                                }
                            )
                        }
                    )
                }

                if (publicKeyOfDevice.isEmpty()) executionStatus = ExecutionStatus.error

                if (executionStatus != ExecutionStatus.ok) {
                    onTaskResult(executionStatus)
                    return@launch
                }

                invokeError(CrayonicDeviceError("ALARM ZONES "))
                //---------------------------------------------
                // Alarm Zones : blocking
                initValues.alarmZones.forEach { alarmZone ->

                    Log.e("CrayonicL1","alarmZone = $alarmZone")
                    Log.e("CrayonicL1","alarmZone.toByteArray().toHex() = ${alarmZone.toByteArray().toHex()}")

                    executionStatus = suspendCancellableCoroutine<ExecutionStatus> { cont ->
                        internal_runCommandApdu(
                            apduType = ApduType.wr_alarm_zone,
                            requestKey = taskReqKey,
                            inputValue = alarmZone,  // has to by of type CrayonicAlarmZone, same as in ApduSignatures.supportedApdus
                            onProgress = { requestKey, progress ->
                                progress_alarm_zones.update(progress)
                                onProgress(overallProgress.totalProgress())
                            },
                            onMidResult = { outputValue: Any ->
                                cont.resume(
                                    when (outputValue) {
                                        is ByteArray -> ApduStatusWord(outputValue.toInt()).toApduExecutionStatus()
                                        is ApduStatusWord -> outputValue.toApduExecutionStatus()
                                        is ExecutionStatus -> outputValue
                                        else -> ExecutionStatus.error
                                    }
                                )
                            }
                        )
                    }
                    if (executionStatus != ExecutionStatus.ok) {
                        onTaskResult(executionStatus)
                        return@launch
                    }
                }


                invokeError(CrayonicDeviceError("PACKAGE ID"))

                //---------------------------------------------
                // write package ID : blocking
                executionStatus = suspendCancellableCoroutine<ExecutionStatus> { cont ->
                    internal_runCommandApdu(
                        apduType = ApduType.wr_package_id,
                        requestKey = taskReqKey,
                        inputValue = initValues.packageId,
                        onProgress = { requestKey, progress ->
                            progress_package_id.update(progress)
                            onProgress(overallProgress.totalProgress())
                        },
                        onMidResult = { outputValue: Any ->
                            cont.resume(
                                when (outputValue) {
                                    is ByteArray -> ApduStatusWord(outputValue.toInt()).toApduExecutionStatus()
                                    is ApduStatusWord -> outputValue.toApduExecutionStatus()
                                    is ExecutionStatus -> outputValue
                                    else -> ExecutionStatus.error
                                }
                            )
                        }
                    )
                }
                if (executionStatus != ExecutionStatus.ok) {
                    onTaskResult(executionStatus)
                    return@launch
                }

                val assetIdBytes = Hex(initValues.assetId).bytes

                if (assetIdBytes.size != 32) executionStatus = ExecutionStatus.error

                if (executionStatus != ExecutionStatus.ok) {
                    onTaskResult(executionStatus)
                    return@launch
                }

                invokeError(CrayonicDeviceError("ASSET ID"))
                //---------------------------------------------
                // Write Asset ID as bytes : blocking
                executionStatus = suspendCancellableCoroutine<ExecutionStatus> { cont ->
                    internal_runCommandApdu(
                        apduType = ApduType.wr_asset_id,
                        requestKey = taskReqKey,
                        inputValue = assetIdBytes,
                        onProgress = { requestKey, progress ->
                            progress_asset.update(progress)
                            onProgress(overallProgress.totalProgress())
                        },
                        onMidResult = { outputValue: Any ->
                            cont.resume(
                                when (outputValue) {
                                    is ByteArray -> ApduStatusWord(outputValue.toInt()).toApduExecutionStatus()
                                    is ApduStatusWord -> outputValue.toApduExecutionStatus()
                                    is ExecutionStatus -> outputValue
                                    else -> ExecutionStatus.error
                                }
                            )
                        }
                    )
                }
                if (executionStatus != ExecutionStatus.ok) {
                    onTaskResult(executionStatus)
                    return@launch
                }

                invokeError(CrayonicDeviceError("CLOCK"))
                //---------------------------------------------
                // Write current unix unixTime to clock : blocking - should be the last command in series
                executionStatus = suspendCancellableCoroutine<ExecutionStatus> { cont ->
                    internal_runCommandApdu(
                        apduType = ApduType.wr_clock,
                        requestKey = taskReqKey,
                        onProgress = { requestKey, progress ->
                            progress_clock.update(progress)
                            onProgress(overallProgress.totalProgress())
                        },
                        onMidResult = { outputValue: Any ->
                            cont.resume(
                                when (outputValue) {
                                    is ByteArray -> ApduStatusWord(outputValue.toInt()).toApduExecutionStatus()
                                    is ApduStatusWord -> outputValue.toApduExecutionStatus()
                                    is ExecutionStatus -> outputValue
                                    else -> ExecutionStatus.error
                                }
                            )
                        }
                    )
                }
                if (executionStatus != ExecutionStatus.ok) {
                    onTaskResult(executionStatus)
                    return@launch
                }

                val result = L1ProvisionedDeviceData(
                    deviceMacAddress = this@CrayonicL1.address,
                    ethAddress = ethAddressOfDevice,
                    publicKey = publicKeyOfDevice
                )

                onTaskResult(result)
            }
        return executionStatus
    }

    private fun initializeL1_provisioningBlock(
        initValues:L1InitValues,
        onProgress: (progress: Progress) -> Unit,
        onTaskResult: (Any) -> Unit
    ): ExecutionStatus {
        var executionStatus = ExecutionStatus.error
        val taskReqKey = runTaskInitialization
        val progress_keys = SubProgress()
        val progress_eth_add = SubProgress()
        val progress_admin_pin = SubProgress()
        val progress_asset = SubProgress()
        val progress_alarm_zones = SubProgress().apply { transmissionProgress.expectedFinalLength = initValues.alarmZones.size*100 }
        val progress_package_id = SubProgress()
        val progress_clock = SubProgress()
        val progress_http_create_asset = SubProgress()
        val progress_http_register_account = SubProgress()
        val progress_http_create_event = SubProgress()

        val overallProgress = ProgressAggregator(
            progress_keys,
            progress_admin_pin,
            progress_eth_add,
            progress_http_register_account,
            progress_alarm_zones,
            progress_package_id,
            progress_http_create_asset,
            progress_asset,
            progress_http_create_event,
            progress_clock

        )
        // TODO : use offline provisioning block instead of the repetitiveblocks below
        val taskJob =
        CoroutineScope(Dispatchers.IO + Job(deviceJob)).launch {
            // Pre-requisites
            val ctrl = Controller.getInstance()
            ctrl.takeBestModuleForTags(TagsEnum.json_api.name) ?: return@launch

            //---------------------------------------------
            // Generate Keys : blocking
            executionStatus = suspendCancellableCoroutine<ExecutionStatus>{ cont ->
                internal_runCommandApdu(
                    apduType = ApduType.wr_generate_changing_keys,
                    requestKey = taskReqKey,
                    inputValue = adminPinToByteArray(initValues.adminPin),
                    onProgress = {requestKey, progress ->
                        progress_keys.update(progress)
                        onProgress(overallProgress.totalProgress())
                    },
                    onMidResult = {outputValue: Any ->
                        cont.resume(
                            when (outputValue) {
                                is ApduStatusWord -> outputValue.toApduExecutionStatus()
                                is ExecutionStatus -> outputValue
                                else -> ExecutionStatus.error
                            }
                        )
                    }
                )
            }
            if(executionStatus != ExecutionStatus.ok) return@launch

            // uncomment with pin implementation on L1 FW
            // Write management / admin pin code : blocking
//            executionStatus = suspendCancellableCoroutine<ExecutionStatus>{ cont ->
//                internal_runCommandApdu(
//                    apduType = ApduType.wr_management_pin,
//                    requestKey = taskReqKey,
//                    inputValue = adminPinToByteArray(initValues.adminPin)+adminPinToByteArray(initValues.newPin),
//                    onProgress = {requestKey, progress ->
//                        progress_admin_pin.update(progress)
//                        onProgress(overallProgress.totalProgress())
//                    },
//                    onMidResult = {outputValue: Any ->
//                        cont.resume(
//                            when (outputValue) {
//                                is ApduStatusWord -> outputValue.toApduExecutionStatus()
//                                is ExecutionStatus -> outputValue
//                                else -> ExecutionStatus.error
//                            }
//                        )
//                    }
//                )
//            }
//            if(executionStatus != ExecutionStatus.ok) return@launch

            var ethAddressOfDevice = ""

            //---------------------------------------------
            // Read Eth Address : blocking
            executionStatus = suspendCancellableCoroutine<ExecutionStatus>{ cont ->
                internal_runCommandApdu(
                    apduType = ApduType.ro_eth_address,
                    requestKey = taskReqKey,
                    onProgress = {requestKey, progress ->
                        progress_eth_add.update(progress)
                        onProgress(overallProgress.totalProgress())
                    },
                    onMidResult = {outputValue: Any ->
                        cont.resume(
                            when (outputValue) {
                                is String -> {
                                    ethAddressOfDevice = Hex(outputValue, true).toString()
                                    ExecutionStatus.ok
                                }
                                is ByteArray -> {
                                    ethAddressOfDevice = Hex(outputValue, true).toString()
                                    ExecutionStatus.ok
                                }
                                else -> ExecutionStatus.error
                            }
                        )
                    }
                )
            }
            if(executionStatus != ExecutionStatus.ok) return@launch
            if(ethAddressOfDevice.isEmpty()) return@launch

            val signedDeviceAccount = L1JsonApi_Account.createInstance(ethAddressOfDevice, initValues.apiSecret)

            //---------------------------------------------
            // HTTP : send ethAddressOfDevice to register account : blocking
            val progressJobAccount = startPeriodicAsyncProgressUpdates(progress_http_register_account){ progress ->
                progress_eth_add.update(progress)
                onProgress(overallProgress.totalProgress())
            }
            executionStatus = suspendCancellableCoroutine<ExecutionStatus> { cont ->
                ctrl.uploadIfPossible(
                    payload = signedDeviceAccount,
                    timeout = seconds_25,
                    retryCount = 2,
                    onResult = { response : Response? ->
                        when {
                            response == null -> cont.resume(ExecutionStatus.error)
                            response.isSuccessful -> cont.resume(ExecutionStatus.ok)
                            else-> cont.resume(ExecutionStatus.error)
                        }
                    }
                )
            }
            progressJobAccount.cancel()
            progress_http_register_account.setTo100()
            if(executionStatus != ExecutionStatus.ok) return@launch

            //---------------------------------------------
            // Alarm Zones : blocking
            initValues.alarmZones.forEach { alarmZone ->
                executionStatus = suspendCancellableCoroutine<ExecutionStatus>{ cont ->
                    internal_runCommandApdu(
                        apduType = ApduType.wr_alarm_zone,
                        requestKey = taskReqKey,
                        inputValue = alarmZone,  // has to by of type CrayonicAlarmZone, same as in ApduSignatures.supportedApdus
                        onProgress = {requestKey, progress ->
                            progress_alarm_zones.update(progress)
                            onProgress(overallProgress.totalProgress())
                        },
                        onMidResult = {outputValue: Any ->
                            cont.resume(
                                when (outputValue) {
                                    is ApduStatusWord -> outputValue.toApduExecutionStatus()
                                    is ExecutionStatus -> outputValue
                                    else -> ExecutionStatus.error
                                }
                            )
                        }
                    )
                }
                if(executionStatus != ExecutionStatus.ok) return@launch
            }
            if(executionStatus != ExecutionStatus.ok) return@launch

            //---------------------------------------------
            // write package ID : blocking
            executionStatus = suspendCancellableCoroutine<ExecutionStatus>{ cont ->
                internal_runCommandApdu(
                    apduType = ApduType.wr_package_id,
                    requestKey = taskReqKey,
                    inputValue = initValues.packageId,
                    onProgress = {requestKey, progress ->
                        progress_package_id.update(progress)
                        onProgress(overallProgress.totalProgress())
                    },
                    onMidResult = {outputValue: Any ->
                        cont.resume(
                            when (outputValue) {
                                is ApduStatusWord -> outputValue.toApduExecutionStatus()
                                is ExecutionStatus -> outputValue
                                else -> ExecutionStatus.error
                            }
                        )
                    }
                )
            }
            if(executionStatus != ExecutionStatus.ok) return@launch

            val generatedAsset:L1JsonApi_Asset = L1JsonApi_Asset.createInstance(initValues.apiAccount, initValues.apiSecret)
            //---------------------------------------------
            /// HTTP register Asset id : blocking has to run before upload event
            val progressJobAsset = startPeriodicAsyncProgressUpdates(progress_http_create_asset){ progress ->
                progress_http_create_asset.update(progress)
                onProgress(overallProgress.totalProgress())
            }
            executionStatus = suspendCancellableCoroutine<ExecutionStatus> { cont ->
                ctrl.uploadIfPossible(
                    payload = generatedAsset,
                    timeout = seconds_25,
                    retryCount = 2,
                    onResult = { response : Response? ->
                        when {
                            response == null -> cont.resume(ExecutionStatus.error)
                            response.isSuccessful -> cont.resume(ExecutionStatus.ok)
                            else-> cont.resume(ExecutionStatus.error)
                        }
                    }
                )
            }
            progressJobAsset.cancel()
            progress_http_create_asset.setTo100()
            if(executionStatus != ExecutionStatus.ok) return@launch

            //---------------------------------------------
            // Write Asset ID as bytes : blocking
            executionStatus = suspendCancellableCoroutine<ExecutionStatus>{ cont ->
                internal_runCommandApdu(
                    apduType = ApduType.wr_asset_id,
                    requestKey = taskReqKey,
                    inputValue = generatedAsset.toByteArray(),
                    onProgress = {requestKey, progress ->
                        progress_asset.update(progress)
                        onProgress(overallProgress.totalProgress())
                    },
                    onMidResult = {outputValue: Any ->
                        cont.resume(
                            when (outputValue) {
                                is ApduStatusWord -> outputValue.toApduExecutionStatus()
                                is ExecutionStatus -> outputValue
                                else -> ExecutionStatus.error
                            }
                        )
                    }
                )
            }
            if(executionStatus != ExecutionStatus.ok) return@launch

            //---------------------------------------------
            // Write current unix unixTime to clock : blocking - should be the last command in series
            executionStatus = suspendCancellableCoroutine<ExecutionStatus>{ cont ->
                internal_runCommandApdu(
                    apduType = ApduType.wr_clock,
                    requestKey = taskReqKey,
                    onProgress = {requestKey, progress ->
                        progress_clock.update(progress)
                        onProgress(overallProgress.totalProgress())
                    },
                    onMidResult = {outputValue: Any ->
                        cont.resume(
                            when (outputValue) {
                                is ApduStatusWord -> outputValue.toApduExecutionStatus()
                                is ExecutionStatus -> outputValue
                                else -> ExecutionStatus.error
                            }
                        )
                    }
                )
            }
            if(executionStatus != ExecutionStatus.ok) return@launch

            val provisioningEvent = L1ApiJson_L2Event

            //---------------------------------------------
            // HTTP : send ethAddressOfDevice to register account : blocking
            val progressJobEvent = startPeriodicAsyncProgressUpdates(progress_http_create_event){ progress ->
                progress_http_create_event.update(progress)
                onProgress(overallProgress.totalProgress())
            }
            executionStatus = suspendCancellableCoroutine<ExecutionStatus> { cont ->
                ctrl.uploadIfPossible(
                    payload = provisioningEvent,
                    timeout = seconds_25,
                    retryCount = 2,
                    onResult = { response : Response? ->
                        when {
                            response == null -> cont.resume(ExecutionStatus.error)
                            response.isSuccessful -> cont.resume(ExecutionStatus.ok)
                            else-> cont.resume(ExecutionStatus.error)
                        }
                    }
                )
            }
            progressJobEvent.cancel()
            progress_http_create_event.setTo100()
            if(executionStatus != ExecutionStatus.ok) return@launch

            onTaskResult(executionStatus)
        }
        return executionStatus
    }

    //////////////////////////
    // following commands are used in mass production only

    fun runTaskFactorySetup(
        adminPin: String,
        deviceListener: CrayonicDeviceListener
    ) =
        internal_runTask(
            runTaskFactorySetup,
            inputValue = adminPin,
            deviceListener = deviceListener,
            onMidResult = {},
            timeout = seconds_120, // this hasto change 10 minutes is too long of a tieout
            runBlock = factorySetupL1()
        )

    fun runTaskFactorySetup(
        adminPin: String,
        onResult: (device: CrayonicL1) -> Unit = {}
    ) =
        internal_runTask(
            runTaskFactorySetup,
            inputValue = adminPin,
            deviceListener = null,
            onMidResult = { onResult.invoke(this) },
            timeout = seconds_120, // this hasto change 10 minutes is too long of a tieout
            runBlock = factorySetupL1()
        )

    protected fun factorySetupL1(): (input: Any?, onProgress: (progress: Progress) -> Unit, onTaskResult: (Any) -> Unit) -> ExecutionStatus =
        { input, onProgress, onTaskResult ->
            // input = = = adminPin
            var executionStatus = ExecutionStatus.error
                CoroutineScope(Dispatchers.IO + deviceJob).launch{
                    when {
                        input == null -> onTaskResult(Any())
                        !(input is String) -> onTaskResult(Any())
                        else -> {
                            // TODO : factory setup
                            onTaskResult(false)
                        }
                    }
                }
            executionStatus


        }

    //////////////////////////

    fun runTaskL1DeviceTest(
        adminPin: String,
        deviceListener: CrayonicDeviceListener
    ) =
        internal_runTask(
            runTaskL1DeviceTest,
            inputValue = adminPin,
            deviceListener = deviceListener,
            onMidResult = {},
            timeout = seconds_120, // this hasto change 10 minutes is too long of a tieout
            runBlock = factoryTestL1()
        )

    fun runTaskL1DeviceTest(
        adminPin: String,
//        onResult: (device: CrayonicL1, report:Report) -> Unit = {} // add the reporting feature
        onResult: (device: CrayonicL1) -> Unit = {}
    ) =
        internal_runTask(
            runTaskL1DeviceTest,
            inputValue = adminPin,
            deviceListener = null,
            onMidResult = { onResult.invoke(this) },
            timeout = seconds_120, // this hasto change 10 minutes is too long of a tieout
            runBlock = factoryTestL1()
        )



    protected fun factoryTestL1(): (input: Any?, onProgress: (progress: Progress) -> Unit, onTaskResult: (Any) -> Unit) -> ExecutionStatus =
        { input, onProgress, onTaskResult ->
            // input = = = adminPin
            var executionStatus = ExecutionStatus.error
            CoroutineScope(Dispatchers.IO + deviceJob).launch{
                when {
                    input == null -> onTaskResult(Any())
                    !(input is String) -> onTaskResult(Any())
                    else -> {
                        // TODO : magic testing
                        onTaskResult(false)
                    }
                }
            }
            executionStatus

        }


    ////////////////////////////////

    override fun toVerboseString(): String {
        return toL1DeviceVerboseHolder().toString()
    }

    fun toL1DeviceVerboseHolder():L1DeviceVerboseHolder{
        return L1DeviceVerboseHolder(
            model = modelTag,
            name = name,
            address = address,
            services = services.joinToString(", "),
            tags = this.toString(),
            battery = battery,
            ethAddress = ethAddress,
            assetId = assetId,
            temperature = temperature,
            humidity = humidity,
            light = light,
            shock = shock,
            unixTime = ""
//            ,
//            incline = incline
        )
    }

    data class L1DeviceVerboseHolder(
        var model: String = "",
        var name: String = "",
        var address: String = "",
        var services: String = "",
        var tags: String = "",
        var battery: String = "",
        var ethAddress: String = "",
        var assetId: String = "",
        var temperature: String = "",
        var humidity: String = "",
        var light: String = "",
        var shock: String = "",
        var unixTime:String = ""
//        ,
//        var incline: String = ""

    ){
        override fun hashCode(): Int {
            return address.hashCode()
        }

        override fun equals(other: Any?): Boolean {
            if (other == null) return false
            if (other is L1DeviceVerboseHolder)
                return address.equals(other.address)
            return false
        }
    }
}

