package com.crayonic.device_sdk.device

import com.crayonic.device_sdk.device.channels.CrayonicDeviceChannel
import com.crayonic.device_sdk.device.CrayonicDevice
import com.crayonic.device_sdk.device.errors.CrayonicDeviceError
import com.crayonic.device_sdk.device.errors.UnsupportedDevice
import space.lupu.konnector.nodes.Channel
import space.lupu.konnector.nodes.Node
import space.lupu.konnector.tags.Tags

@Suppress("UNUSED_PARAMETER")
abstract class CrayonicDeviceNode(id: String, tags: Tags = Tags.emptyTags()) : Node(id, tags) {


    abstract fun provideNodeName():String
    abstract fun provideNodeAddress():String
    abstract fun provideNodeServices():List<String>

    var openChannel: CrayonicDeviceChannel? = null

    protected var registeredOnError: ((CrayonicDeviceError) -> Unit)? = null
    protected val registeredOnDisconnect: ArrayList<((CrayonicDeviceNode) -> Unit)> = arrayListOf()

    fun isConnected() = openChannel?.isOpened()?:false
    fun isDisonnected() = !isConnected()

    fun connect(
        onChannelOpened: (CrayonicDeviceNode, CrayonicDeviceChannel) -> Unit,
        onDisconnected: (CrayonicDeviceNode) -> Unit,
        onError: (CrayonicDeviceError) -> Unit
    ) {
        registeredOnError = onError
        registeredOnDisconnect.add (onDisconnected)
        if (isConnected()) {
            onChannelOpened.invoke(this,openChannel!!)
        } else
            connect(
                onChannelOpened = { node: Node, channel: Channel ->
                    if (node == this) {
                        if(channel is CrayonicDeviceChannel){
                            channel.attachOnError(onError)
                            openChannel = channel
//                            onError(CrayonicDeviceError("CrayonicDeviceNode $id $tags onChannelOpened attached channel ${channel.id} ${channel.tags} which isOpened = ${channel.isOpened()}"))
                            channel.open()
//                            onError(CrayonicDeviceError("CrayonicDeviceNode $id $tags onChannelOpened opened channel ${channel.id} ${channel.tags} which isOpened = ${channel.isOpened()}"))
                            onChannelOpened.invoke(this, channel)
                        } else {
                            onError(UnsupportedDevice("could not connect to remote $node, channel is not supported"))
                            disconnect({triggerDisconnects(onDisconnected).invoke(node)}, onError)
                        }
                    }
                },
                onNodeDisconnected = triggerDisconnects(onDisconnected)
            )
    }

    private fun triggerDisconnects(onDisconnected: (CrayonicDeviceNode) -> Unit = {}): (Node?) -> Unit =
        { _: Node? ->
            registeredOnDisconnect.add(onDisconnected)
//            registeredOnError?.invoke(CrayonicDeviceError("correctly DISCOnnECTED in CrayonicDeviceNode -- handled ${registeredOnDisconnect.size} ${registeredOnDisconnect.joinToString(", ")} callbacks"))
            registeredOnDisconnect.forEach { it.invoke(this) }
            registeredOnDisconnect.clear()
        }

    fun disconnect(
        onDisconnected: (CrayonicDeviceNode) -> Unit,
        onError: (CrayonicDeviceError) -> Unit
    ) {
        registeredOnDisconnect.add(onDisconnected)
        if (isConnected()) {
            openChannel?.close()
            openChannel = null
            onError(CrayonicDeviceError("correctly DISCOnnECTED in CrayonicDeviceNode"))
            disconnect(triggerDisconnects())
        } else {
            // device is already disconnected
            onError(CrayonicDeviceError("DISCOnnECTED in CrayonicDeviceNode while previously not connected"))
            triggerDisconnects().invoke(null)
        }
    }

    // calling during device transformation : override to get another device class
    open fun toCrayonicDevice(): CrayonicDevice {
        return CrayonicDevice(this)
    }

    abstract val isDfuUpdateSupported: Boolean

    override fun toString(): String {
        return "$id $tags"
    }

    override fun equals(other: Any?): Boolean {
        if(other == null ) return false
        if(other is CrayonicDevice)
            return tags.toString().equals(other.toString())
        return false
    }

}