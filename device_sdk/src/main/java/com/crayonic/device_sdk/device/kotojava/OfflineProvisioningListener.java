package com.crayonic.device_sdk.device.kotojava;

import com.crayonic.device_sdk.device.l1.models.L1ProvisionedDeviceData;

public interface OfflineProvisioningListener {
    void onResult(L1ProvisionedDeviceData provisioningData);
}
