package com.crayonic.device_sdk.device.errors

import com.crayonic.device_sdk.device.errors.CrayonicDeviceError
import com.crayonic.device_sdk.device.errors.CrayonicDeviceErrorType

class UnsupportedDevice(detailedMessage: String = ""):
    CrayonicDeviceError(
        detailedMessage = detailedMessage,
        errorType = CrayonicDeviceErrorType.UnsupportedDevice
    )