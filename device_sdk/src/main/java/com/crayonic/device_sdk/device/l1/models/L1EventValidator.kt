package com.crayonic.device_sdk.device.l1.models

import android.util.Log
import com.crayonic.device_sdk.BuildConfig
import com.crayonic.device_sdk.core.Single.eventCrc32
import com.crayonic.device_sdk.core.Single.zeros32
import com.crayonic.device_sdk.util.*
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import org.kethereum.crypto.signedMessageToKey
import org.kethereum.crypto.toAddress
import space.lupu.koex.hex.hexStringToByteArray
import space.lupu.koex.hex.toHex
import java.nio.charset.Charset


/**
 * Class used for parsing Event Data json generated and signed on L1
 */
class L1EventValidator(val jsonStrBytes: ByteArray) {
    var jsonStr = ""
    var valid: Boolean = false
    var json: JsonObject? = null

    var assetId = ""
    var eventId = ""
    var idData = ""
    var signature = ""
    var signatureMsgHashHex = ""
    var accountEthAddress = ""
    var providedDataHash = ""
    var calculatedDataHash = ""

    var recoveredSignatureOK = false
    var dataHashMatchParsedData = false
    var isJsonParsed = false

    var rawDataHex = ""
    var rawData = ByteArray(0)

    var crc_bytes = CRC_LEAVE_EVENT_INTACT

//    var dumpLogger = DumpLogger("Dump of Event Json Parser \n")
    val parser:L1EventParser

    init {
        parser = L1EventParser(jsonStrBytes)
        crc_bytes = parser.getCheckHashCRC().hexStringToByteArray()

        try {
            parser.let { parsedEventData ->
                jsonStr = parsedEventData.getEventStr()
                Log.e(TAG, "Event analyze : ${jsonStr}")
//                //dumpLogger.append("parsed prefix : ").append(parsedEventData.prefix).append("\n")
//                //dumpLogger.append("parsed data : ").append(parsedEventData.dataRawHexString).append("\n")
//                //dumpLogger.append("parsed data hash : ").append(parsedEventData.dataHash).append("\n")
//                //dumpLogger.append("parsed suffix : ").append(parsedEventData.suffix).append("\n")
//                //dumpLogger.append("parsed idDataHash (same as messageHash) : ").append(parsedEventData.idDataHash).append("\n")

                if (jsonStr.isNotBlank() && parsedEventData.getCheckHashCRC().isNotBlank()) {
                    // save crc for later -- original json eventId

//                    //dumpLogger.append("getCheckHash/CRC in hex : ").append(parsedEventData.getCheckHash()).append("\n")

                    rawDataHex = parsedEventData.dataRawHexString
                    rawData = parsedEventData.dataRawHexString.hexStringToByteArray()

                    // parse members
                    json = JsonParser.parseString(jsonStr).asJsonObject
                    isJsonParsed = true

                    eventId = json!!.getAsJsonPrimitive("eventId").asString

                    //dumpLogger.append("eventId received : ").append(eventId).append("\n")

                    val content = json!!.getAsJsonObject("content")
                    signature = content.getAsJsonPrimitive("signature").asString

                    //dumpLogger.append("signature received : ").append(signature).append("\n")

                    val dataId = content.getAsJsonObject("idData")
                    idData = dataId.sortByKeys().toShortJsonString()
                    val messageHash = dataId.sortByKeys().toShortJsonString().toEthKeccak()
                    val dataHash = dataId.get("dataHash")
                    providedDataHash = dataHash.asString
                    calculatedDataHash = parsedEventData.dataHash
                    dataHashMatchParsedData = providedDataHash.equals(calculatedDataHash)

                    assetId = dataId.getAsJsonPrimitive("assetId").asString

                    accountEthAddress = dataId.getAsJsonPrimitive("createdBy").asString

                    signatureMsgHashHex = messageHash.toHex()

                    // implementation is not verified : skip it until its tested
                    if(BuildConfig.DEBUG && false) {
                        val signatureData = signature.hexStringToByteArray().toRSVSignatureData()
                        if (signatureData == null ) recoveredSignatureOK = false
                        else {
                            val recoveredPubKey = signedMessageToKey(messageHash,signatureData).toAddress().hex
                            // validate, verify, recover signature

                            recoveredSignatureOK = accountEthAddress.equals(recoveredPubKey, ignoreCase = true)
                        }
                    }else{
                        //dumpLogger.append("skipped signature check").append("\n")
                        recoveredSignatureOK = true
                    }
                }
            }
        } catch (e: Exception) {
            //dumpLogger.append("parsing done fail ").append("\n")
            recoveredSignatureOK = false
            isJsonParsed = false
            validateJsonString(jsonStr)
        }
        valid = validation()
        //dumpLogger.append("valid : ").append(valid).append("\n")
        Log.i(TAG, "Event received from L1 is ${if(isValid())"valid and signed correctly" else "invalid - check hashes and signature"}")
    }

    private fun applyRecovered(recovered: String, content: JsonObject) {
        //dumpLogger.append("signature does not match recovered : it was replaced in json !").append("\n")

        // set the recovered as and signature calc new eventId
        signature = recovered
        content.remove("signature")
        content.addProperty("signature", signature)
        eventId = content.sortByKeys().toShortJsonString().toEthKeccakHex()
        //dumpLogger.append("content changed : new hash is new eventId: ").append(eventId).append("\n")

        json!!.remove("content")
        json!!.remove("eventId")
        json!!.addProperty("eventId", eventId)
        json!!.add("content", content)
        //dumpLogger.append("parsing done success ").append("\n")

        recoveredSignatureOK = true
    }

    private fun validation(): Boolean {
        valid = signature.isNotBlank()
                && eventId.isNotBlank()
                && assetId.isNotBlank()
                && accountEthAddress.isNotBlank()
                && dataHashMatchParsedData
                && crc_bytes.size > 0
                && recoveredSignatureOK
                && isJsonParsed
        if (!valid) crc_bytes = CRC_LEAVE_EVENT_INTACT
        return isValid()
    }


    /**
     * Get compressed String representation of toJson()
     */
    fun getData(): String {
        try {
            return toJson().toShortJsonString()

        } catch (e: Exception) {
            Log.d(TAG, "failed to parse json")
        }
        return ""
    }

    /**
     * Get compressed String representation of toJson()
     */
    fun getLoggingHexData(): String {
        try {
            return "account : ${accountEthAddress}\n" +
                    "eventId / CRC : ${eventId}\n" +
                    "assetId : ${assetId}\n" +
                    "" + getData() + "\nhex\n" + getData().toByteArray(Charset.defaultCharset()).toHex()

        } catch (e: Exception) {
            Log.d(TAG, "failed to parse json")
        }
        return "70FF"
    }

    /**
     * Get JsonObject
     */
    fun toJson(): JsonObject  = json?: JsonObject()

    /**
     * Parsing and signature control were OK
     */
    fun isValid(): Boolean {
        return valid
    }

    /**
     * CRC used to verify corerct transmission of data from L1 to L2
     * This byte array will be used for data removal from L1 ( APDU Event Finish )
     *
     */
    fun getCRC(): ByteArray {
        return crc_bytes
    }

    /**
     * Get String of diagnostic contents
     */
    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("AMB Event json").append("\n")
        sb.append("valid : ").append(valid).append("\n")
        sb.append("assetId : ").append(assetId).append("\n")
        sb.append("eventId : ").append(eventId).append("\n")
        sb.append("idData : ").append(idData).append("\n")
        sb.append("signature : ").append(signature).append("\n")
        sb.append("signatureMsgHashHex : ").append(signatureMsgHashHex).append("\n")
        sb.append("accountEthAddress : ").append(accountEthAddress).append("\n")
        sb.append("recoveredSignatureOK : ").append(recoveredSignatureOK).append("\n")
        sb.append("dataHashMatchParsedData : ").append(dataHashMatchParsedData).append("\n")
        sb.append("rawData [${rawData.size}] \n")
        sb.append("crc_bytes : ").append(crc_bytes.toHex()).append("\n")
        return sb.toString()
    }

    companion object {
        val TAG = "AMB_L1GenPostEventJMod"

        val CRC_LEAVE_EVENT_INTACT = eventCrc32
    }
}