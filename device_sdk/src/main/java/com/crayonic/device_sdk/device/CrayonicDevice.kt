package com.crayonic.device_sdk.device


import android.util.Log
import com.crayonic.device_sdk.apdu.ApduSignature
import com.crayonic.device_sdk.apdu.ApduSignatures
import com.crayonic.device_sdk.apdu.ApduType
import com.crayonic.device_sdk.apdu.adminPinToByteArray
import com.crayonic.device_sdk.core.Controller
import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.device.CrayonicDeviceNode
import com.crayonic.device_sdk.device.ExecutionStatus
import com.crayonic.device_sdk.device.channels.CrayonicApduChannel
import com.crayonic.device_sdk.device.channels.CrayonicDeviceChannel
import com.crayonic.device_sdk.device.errors.CrayonicDeviceError
import com.crayonic.device_sdk.device.errors.DeviceOutOfReachError
import com.crayonic.device_sdk.device.errors.NoError
import com.crayonic.device_sdk.device.errors.UnsupportedOperation
import com.crayonic.device_sdk.device.kotojava.CrayonicDeviceListener
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode.*
import com.crayonic.device_sdk.util.*
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import space.lupu.koex.bytes.minimalByteArray
import space.lupu.koex.hex.toHex
import space.lupu.koex.lambda.emptyLambda1inUout
import space.lupu.koex.lambda.emptyLambda2inUout
import space.lupu.koex.lambda.emptyLambda3inUout
import space.lupu.koex.numbers.toByteArray


@Suppress("unused", "UNUSED_PARAMETER", "UNUSED_ANONYMOUS_PARAMETER")
open class CrayonicDevice(initNode: CrayonicDeviceNode) {
    var errorFlag: CrayonicDeviceError = NoError()
    val deviceTags: List<String>


    open val modelTag = "CrayonicDevice"

    // jobs
    protected var deviceJob: Job = reinitDeviceJob("init-${System.currentTimeMillis()}", true)
    protected var deviceJobMutex: Mutex = Mutex()
    private fun reinitDeviceJob(specTag:String, cancelImmediately:Boolean = false): Job {
//        CoroutineScope(Dispatchers.IO ).launch {
//            deviceJobMutex.withLock {
        val jobName = Single.randomId("deviceJob-$specTag")
        val job = Controller.getInstance().getNewJob(jobName)
//        Log.e("CrayonicDevice", "reinitDeviceJob  $jobName: started")
        if (cancelImmediately) {
//            Log.e("CrayonicDevice", "reinitDeviceJob  $jobName: cancel")
            job.cancel()
        }
        deviceJob = job
//            }
//        }
        return deviceJob
    }

    // lambdas
    protected var registeredOnProgress: (device: CrayonicDevice, requestKey: CrayonicRequestCode, progress: Progress) -> Unit =
        emptyLambda3inUout()
    private var registeredOnError: (device: CrayonicDevice, error: CrayonicDeviceError) -> Unit =
        emptyLambda2inUout()
    private var registeredOnDisconnect: (device: CrayonicDevice) -> Unit = emptyLambda1inUout()

    // listener
    private var registeredListener: CrayonicDeviceListener? = null

    /// todo move  supportedReadValueMembers  to member channel
    open val supportedReadValueMembers:List<CrayonicRequestCode> = listOf(
        readDeviceAddress,
        readDeviceName,
        readBatteryLevel,
        readModelNumber,
        readFirmwareVersion
    )


    private val deviceNode: CrayonicDeviceNode

    var address = ""
    var name = ""
    var services= listOf<String>()
    var battery = ""
    var firmwareVersion = ""
    var modelNumber = ""
//    var ethAddress = "" //  not implemented yet
//    var identity = "" //  not implemented yet

    /**
     * Updates internal device state for all supported read Gatt characteristics
     * @param onDone lambda is invoked when all values have been updated, its parameter returns count of all updated fields
     */
    fun updateAllMemberValues(onDone:(updatedCount: Int)->Unit = {}){
        CoroutineScope(Dispatchers.IO + Job(deviceJob)).launch {
            var counter = 0
            supportedReadValueMembers.forEach {reqCode->
                readDeviceMemberValue(this@CrayonicDevice, reqCode)
                counter += 1
            }
            onDone.invoke(counter)
        }
    }

    init {
        deviceNode = initNode
        val arrayList = arrayListOf<String>()
        initNode.tags.toList().forEach {
            arrayList.add(it.id)
        }
        deviceTags = arrayList.toList()

        address = deviceNode.provideNodeAddress()
        name = deviceNode.provideNodeName()
        services = deviceNode.provideNodeServices()
    }

    fun isConnected(): Boolean =  deviceNode.isConnected() && deviceJob.isActive

    /**
     * Connect device if reachable
     *
     * Non blocking connect, waiting for response from remote device on separate thread.
     *
     * Java interface will receive callbacks as response to events
     *
     * @param deviceListener Java interface
     *
     * @return true if started connecting | false otherwise
     */
    fun connect(deviceListener: CrayonicDeviceListener) =
        inner_connect(deviceListener = deviceListener)

//    /**
//     * Connect device if reachable
//     *
//     * Non blocking connect, waiting for response from remote device on separate thread.
//     *
//     * @return true if started connecting | false otherwise
//     */
//    fun connect() = connect(emptyLambda1inUout()) // empty call for java users

    /**
     * Connect device if reachable
     *
     * Non blocking connect, waiting for response from remote device on separate thread.
     *
     * @param onConnected triggered when remote device is connected
     * @param onDisconnected triggered when remote device is disconnected (only after it was connected)
     * @param onProgress triggered when remote device is connected
     * @param onError triggered when there was any significant error during communication with remote device ( including connecting and disconnecting )
     * @return true if started connecting | false otherwise
     */
    fun connect(
        onConnected: (device: CrayonicDevice) -> Unit ,
        onDisconnected: (device: CrayonicDevice) -> Unit = emptyLambda1inUout(),
        onProgress: (device: CrayonicDevice, requestKey: CrayonicRequestCode, progress: Progress) -> Unit = emptyLambda3inUout(),
        onError: (device: CrayonicDevice, error: CrayonicDeviceError) -> Unit = emptyLambda2inUout()
    ) = inner_connect(
        onConnected = onConnected,
        onDisconnected = onDisconnected,
        onError = onError,
        onProgress = onProgress
    )

    private fun inner_connect(
        deviceListener: CrayonicDeviceListener? = null,
        onConnected: (device: CrayonicDevice) -> Unit = emptyLambda1inUout(),
        onDisconnected: (device: CrayonicDevice) -> Unit = emptyLambda1inUout(),
        onProgress: (device: CrayonicDevice, requestKey: CrayonicRequestCode, progress: Progress) -> Unit = emptyLambda3inUout(),
        onError: (device: CrayonicDevice, error: CrayonicDeviceError) -> Unit = emptyLambda2inUout()
    ): Boolean {
        if (onError != emptyLambda2inUout()) registeredOnError = onError
        if (onProgress != emptyLambda2inUout()) registeredOnProgress = onProgress
        if (onDisconnected != emptyLambda1inUout()) registeredOnDisconnect = onDisconnected
        if (deviceListener != null) registeredListener = deviceListener

        onError(this, CrayonicDeviceError("CrayonicDevice inner_Connect deviceNode : ${deviceNode.id}"))

        // verify previous connect
        if (isConnected()) {
            deviceNode.openChannel?.let { Controller.getInstance().connectedDevices.put(this, it) }
            onConnected.invoke(this)
            return true
        }
//        CoroutineScope(Dispatchers.IO ).launch {
//            deviceJobMutex.withLock {
//
//            }
//        }
        // re-initialize job from previous connect
        if (deviceJob.isCancelled || deviceJob.isCompleted) {
            reinitDeviceJob("connect ${System.currentTimeMillis()}")
        }

        val currentDeviceJob = deviceJob

        // start connection timeout
        var connectTimeout: Job = Job()

        // try to connect
        val connectionJob = CoroutineScope(Dispatchers.IO + Job(currentDeviceJob)).launch {
            delay(50) // get the timeout time to start
            deviceNode.connect(
                onChannelOpened = {crayonicDeviceNode: CrayonicDeviceNode, crayonicDeviceChannel: CrayonicDeviceChannel ->
                    // successful connect
                    connectTimeout.cancel()

                    Controller.getInstance()
                        .connectedDevices.put(this@CrayonicDevice, crayonicDeviceChannel)
                    launchOnMain{
                        if (deviceListener != null) {
                            deviceListener.onConnect(this@CrayonicDevice)
                        } else {
                            onConnected.invoke(this@CrayonicDevice)
                        }
                    }
                },
                onError = {errorString ->
                    val errorNumber = 4321
                    val error = CrayonicDeviceError("Error[$errorNumber]: $errorString")
                    launchOnMain{
                        if (deviceListener != null) {
                            deviceListener.onError(this@CrayonicDevice, connectDevice, error)
                        } else {
                            onError.invoke(this@CrayonicDevice, error)
                        }
                    }
                },
                onDisconnected = {
                    launchOnMain{
                        Controller.getInstance()
                            .connectedDevices.remove(this@CrayonicDevice)
                        if (deviceListener != null) {
                            deviceListener.onDisconnect(this@CrayonicDevice)
                        } else {
                            onDisconnected.invoke(this@CrayonicDevice)
                        }
                    }
                    currentDeviceJob.cancel()
                }
            )
        }

        val timeout = seconds_30
        connectTimeout = launchJobOnIO_Timeout(connectionJob, timeout) {
            // if still not connected after 20 sec announce error and disconnect
            if (deviceNode.isDisonnected()) {
                launchOnMain{
                    invokeErrorAndDisconnect(
                        DeviceOutOfReachError("Connection timeout ${timeout.div(1000.0)} seconds has passed")
                    )
                }
                // timeout on connect stops everything that has been started under connect job
                connectionJob.cancel()
                currentDeviceJob.cancel()
            }
        }

        return true
    }

    fun disconnect() = disconnect(emptyLambda1inUout())
    fun disconnect(deviceListener: CrayonicDeviceListener) = inner_disconnect(deviceListener)
    fun disconnect(
        onDisconnected: (device: CrayonicDevice) -> Unit = emptyLambda1inUout(),
        onError: (device: CrayonicDevice, error: CrayonicDeviceError) -> Unit = emptyLambda2inUout()
    ) = inner_disconnect(onDisconnected = onDisconnected, onError = onError)

    @Suppress("UNUSED_PARAMETER")
    private fun inner_disconnect(
        deviceListener: CrayonicDeviceListener? = null,
        onDisconnected: (device: CrayonicDevice) -> Unit = emptyLambda1inUout(),
        onError: (device: CrayonicDevice, error: CrayonicDeviceError) -> Unit = emptyLambda2inUout()
    ): Boolean {
        if (onDisconnected != emptyLambda1inUout()) registeredOnDisconnect = onDisconnected
        if (deviceListener != null) registeredListener = deviceListener

        if(!isConnected()){
            invokeError(CrayonicDeviceError("Device is already disconnected"))
            onDisconnected.invoke(this)
            return false
        }

        // re-initialize job from previous connect
        if (deviceJob.isCancelled || deviceJob.isCompleted) {
            reinitDeviceJob("disconnect-${System.currentTimeMillis()}")
        }
        val currentDeviceJob = deviceJob

        val disconnectTimeout = launchJobOnIO_Timeout(currentDeviceJob, seconds_5) {
            // if still not connected after 20 sec announce error and disconnect
            CoroutineScope(Dispatchers.Main).launch {
                invokeError(DeviceOutOfReachError("Disconnect timeout 10 seconds has passed"))
                if (deviceListener != null) {
                    Controller.getInstance()
                        .connectedDevices.remove(this@CrayonicDevice)
                    deviceListener.onDisconnect(this@CrayonicDevice)
                } else {
                    onDisconnected.invoke(this@CrayonicDevice)
                }
                currentDeviceJob.cancel()
            }
        }

        deviceNode.disconnect(
            { _: CrayonicDeviceNode ->
                Controller.getInstance()
                    .connectedDevices.remove(this@CrayonicDevice)
                CoroutineScope(Dispatchers.Main).launch {
                    if (deviceListener != null) {
                        deviceListener.onDisconnect(this@CrayonicDevice)
                    } else {
                        onDisconnected.invoke(this@CrayonicDevice)
                    }
                }
                disconnectTimeout.cancel()
                currentDeviceJob.cancel()
            },
            { error: CrayonicDeviceError ->
                invokeError(error)
                currentDeviceJob.cancel()
            }
        )


        return true
    }

    fun readDeviceAddress(deviceListener: CrayonicDeviceListener) =
        internal_readMemberValue(
            deviceListener = deviceListener,
            requestKey = CrayonicRequestCode.readDeviceAddress
        )

    fun readDeviceAddress(onResult: (device: CrayonicDevice, readableValue: String) -> Unit = emptyLambda2inUout()) =
        internal_readMemberValue(
            onResult = onResult,
            requestKey = CrayonicRequestCode.readDeviceAddress
        )

    fun readDeviceName(deviceListener: CrayonicDeviceListener) =
        internal_readMemberValue(
            deviceListener = deviceListener,
            requestKey = readDeviceName
        )

    fun readDeviceName(onResult: (device: CrayonicDevice, readableValue: String) -> Unit = emptyLambda2inUout()) =
        internal_readMemberValue(
            onResult = onResult,
            requestKey = readDeviceName
        )

    fun readBatteryLevel(deviceListener: CrayonicDeviceListener) =
        internal_readMemberValue(
            deviceListener = deviceListener,
            requestKey = CrayonicRequestCode.readBatteryLevel
        )

    fun readBatteryLevel(onResult: (device: CrayonicDevice, readableValue: String) -> Unit = emptyLambda2inUout()) =
        internal_readMemberValue(
            onResult = onResult,
            requestKey = CrayonicRequestCode.readBatteryLevel
        )

    fun readModelNumber(deviceListener: CrayonicDeviceListener) =
        internal_readMemberValue(
            deviceListener = deviceListener,
            requestKey = CrayonicRequestCode.readModelNumber
        )

    fun readModelNumber(onResult: (device: CrayonicDevice, readableValue: String) -> Unit = emptyLambda2inUout()) =
        internal_readMemberValue(
            onResult = onResult,
            requestKey = CrayonicRequestCode.readModelNumber
        )


    fun readFirmwareVersion(deviceListener: CrayonicDeviceListener) =
        internal_readMemberValue(
            deviceListener = deviceListener,
            requestKey = CrayonicRequestCode.readFirmwareVersion
        )

    fun readFirmwareVersion(onResult: (device: CrayonicDevice, readableValue: String) -> Unit = emptyLambda2inUout()) =
        internal_readMemberValue(
            onResult = onResult,
            requestKey = CrayonicRequestCode.readFirmwareVersion
        )

    fun runCommandRawBytes(bytes: ByteArray, deviceListener: CrayonicDeviceListener) =
        internal_runCommandRawApdu(bytes, deviceListener = deviceListener)

    fun runCommandRawBytes(
        bytes: ByteArray,
        onResult: (device: CrayonicDevice, bytes: ByteArray) -> Unit = emptyLambda2inUout()
    ) =
        internal_runCommandRawApdu(bytes) { onResult.invoke(this, it) }

    fun runCommandReadClock(deviceListener: CrayonicDeviceListener) =
        internal_runCommandApdu(
            ApduType.ro_clock,
            CrayonicRequestCode.runCommandReadClock,
            deviceListener = deviceListener
        )

    fun runCommandReadClock(onResult: (device: CrayonicDevice, unixTime: Long) -> Unit = emptyLambda2inUout()) =
        internal_runCommandApdu(
            ApduType.ro_clock,
            CrayonicRequestCode.runCommandReadClock
        ) { onResult.invoke(this, it as Long) }

    fun runCommandWriteClock(
        adminPin: String,
        unixTime: Long = currentUnixTime(),
        deviceListener: CrayonicDeviceListener
    ) =
        internal_runCommandApdu(
            ApduType.wr_clock,
            CrayonicRequestCode.runCommandWriteClock,
//            listOf(adminPin, unixTime),
            unixTime,
            deviceListener = deviceListener
        )

    fun runCommandWriteClock(
        adminPin: String,
        unixTime: Long = currentUnixTime(),
        onResult: (device: CrayonicDevice) -> Unit = emptyLambda1inUout()
    ) =
        internal_runCommandApdu(
            ApduType.wr_clock,
            CrayonicRequestCode.runCommandWriteClock,
//            listOf(adminPin, unixTime),
            unixTime
        ) { onResult.invoke(this) }

    fun runCommandReadPublicKey(deviceListener: CrayonicDeviceListener) =
        internal_runCommandApdu(
            ApduType.ro_public_changing_key,
            CrayonicRequestCode.runCommandReadPublicKey,
            deviceListener = deviceListener
        )

    fun runCommandReadPublicKey(onResult: (device: CrayonicDevice, bytes: ByteArray) -> Unit = emptyLambda2inUout()) =
        internal_runCommandApdu(
            ApduType.ro_public_changing_key,
            CrayonicRequestCode.runCommandReadPublicKey
        ) { onResult.invoke(this, (it as ByteArray)) }

    fun runCommandReadEthAddress(deviceListener: CrayonicDeviceListener) =
        internal_runCommandApdu(
            ApduType.ro_eth_address,
            CrayonicRequestCode.runCommandReadEthAddress,
            deviceListener = deviceListener
        )

    fun runCommandReadEthAddress(onResult: (device: CrayonicDevice, ethAddress: String) -> Unit = emptyLambda2inUout()) =
        internal_runCommandApdu(
            ApduType.ro_eth_address,
            CrayonicRequestCode.runCommandReadEthAddress
        ) { onResult.invoke(this, it as String) }

    fun runCommandResetToPowerOff(adminPin: String, deviceListener: CrayonicDeviceListener) =
        internal_runCommandApdu(
            ApduType.wr_reset,
            CrayonicRequestCode.runCommandResetToPowerOff,
            adminPinToByteArray(adminPin),
            deviceListener = deviceListener
        )

    fun runCommandResetToPowerOff(
        adminPin: String,
        onResult: (device: CrayonicDevice) -> Unit = emptyLambda1inUout()
    ) =
        internal_runCommandApdu(
            ApduType.wr_reset,
            CrayonicRequestCode.runCommandResetToPowerOff,
            adminPinToByteArray(adminPin)
        ) { onResult.invoke(this) }

    fun runCommandResetToUpdateMode(adminPin: String, deviceListener: CrayonicDeviceListener) =
        internal_runCommandApdu(
            ApduType.wr_fw_update_reset,
            CrayonicRequestCode.runCommandResetToUpdateMode,
            adminPinToByteArray(adminPin),
            deviceListener = deviceListener
        )

    fun runCommandResetToUpdateMode(
        adminPin: String,
        onResult: (device: CrayonicDevice) -> Unit = emptyLambda1inUout()
    ) =
        internal_runCommandApdu(
            ApduType.wr_fw_update_reset,
            CrayonicRequestCode.runCommandResetToUpdateMode,
            adminPinToByteArray(adminPin)
        ) { onResult.invoke(this) }


    fun runCommandSetManagementPinCode(
        adminPin: String,
        newPin: String,
        deviceListener: CrayonicDeviceListener
    ) =
        internal_runCommandApdu(
            ApduType.wr_management_pin,
            CrayonicRequestCode.runCommandSetManagementPinCode,
            listOf(adminPinToByteArray(adminPin), adminPinToByteArray(adminPin)),
            deviceListener = deviceListener
        )

    fun runCommandSetManagementPinCode(
        adminPin: String,
        newPin: String,
        onResult: (device: CrayonicDevice) -> Unit = emptyLambda1inUout()
    ) =
        internal_runCommandApdu(
            ApduType.wr_management_pin,
            CrayonicRequestCode.runCommandSetManagementPinCode,
            listOf(adminPinToByteArray(adminPin), adminPinToByteArray(adminPin))
        ) { onResult.invoke(this) }

    fun runTaskUpdateDevice(
        adminPin: String,
        deviceListener: CrayonicDeviceListener,
        onProgress: (device: CrayonicDevice, requestKey: CrayonicRequestCode, progress: Progress) -> Unit = registeredOnProgress
    ) =
        internal_runTask(
            requestKey = CrayonicRequestCode.runTaskUpdateDevice,
            inputValue = adminPin,
            deviceListener = deviceListener,
            runBlock = dfuTask()
        )

    protected fun dfuTask(): (
        input: Any?,
        onProgress: (progress: Progress) -> Unit,
        onTaskResult: (Any) -> Unit
    ) -> ExecutionStatus = { input, onProgress, onTaskResult ->
        var executionStatus = ExecutionStatus.error
        if(deviceNode.isDfuUpdateSupported)
        CoroutineScope(Dispatchers.IO + deviceJob).launch{
            onTaskResult(false)
        }
        executionStatus
    }

    fun runTaskUpdateDevice(
        adminPin: String,
        onResult: (device: CrayonicDevice) -> Unit = {},
        onProgress: (device: CrayonicDevice, requestKey: CrayonicRequestCode, progress: Progress) -> Unit = defaultOnProgress()
    ) =
        internal_runTask(
            requestKey = CrayonicRequestCode.runTaskUpdateDevice,
            inputValue = adminPin,
            onMidResult = { onResult.invoke(this) },
            runBlock = dfuTask()
        )

    private fun defaultOnProgress(): (device: CrayonicDevice, requestKey: CrayonicRequestCode, progress: Progress) -> Unit =
        { device: CrayonicDevice, requestKey: CrayonicRequestCode, progress: Progress ->
            invokeOnProgress(requestKey, progress)
        }

    //////////////////////////////
    // Read Member
    protected fun internal_readMemberValue(
        requestKey: CrayonicRequestCode,
        onProgress: (requestKey: CrayonicRequestCode, progress: Progress) -> Unit = this::invokeOnProgress,
        deviceListener: CrayonicDeviceListener? = null,
        onResult: (device: CrayonicDevice, readableValue: String) -> Unit = emptyLambda2inUout()
    ): Boolean {
        if (isDisconnectedCheck(errorNumber = 7346)) return false

        return readMember(
            requestKey,
            { progress: Progress -> onProgress.invoke(requestKey, progress) }
        ) { strValue ->
            interceptReadMemberValue(requestKey, strValue)
            launchOnMain{
                // callbacks splits here for java and kotlin
                if (deviceListener != null) {
                    deviceListener.onResult(
                        this@CrayonicDevice,
                        requestKey,
                        mapWithValue(requestKey, strValue)
                    )
                }
                onResult.invoke(this@CrayonicDevice, strValue)

            }
        }
    }

    protected open fun interceptReadMemberValue(requestKey: CrayonicRequestCode, strValue: String) {
        // set the value to local members / properties
        when(requestKey){
            readDeviceName -> name = strValue
            readDeviceAddress -> address = strValue
            readFirmwareVersion -> firmwareVersion = strValue
            readModelNumber -> modelNumber = strValue
            readBatteryLevel -> battery = strValue
//            readEthAddress -> ethAddress = strValue
//            readIdentity -> identity = strValue

            else -> {}
        }
    }

    protected open fun readMember(
        requestKey: CrayonicRequestCode,
        onProgress: (progress: Progress) -> Unit,
        onMidResult: (String) -> Unit
    ): Boolean {
        // functionality gets done here
        val channel = getOpenedChannel() ?: return false
        if (!channel.supportsRequestCode(requestKey)) {
            invokeMemberErrorAndDiconnect(requestKey)
            return false
        }

        val timeoutJob = startRequestTimeout(seconds_10, requestKey)
        if(timeoutJob.isActive) {
            channel.readMemberForRequest(requestKey){
                // when result came in unixTime
                onMidResult(it)
                timeoutJob.cancel()
            }
            return true
        }
        return false
    }


    //////////////////////////////
    // APDU
    protected fun internal_runCommandRawApdu(
        inputBytes: ByteArray,
        onProgress: (requestKey: CrayonicRequestCode, progress: Progress) -> Unit = this::invokeOnProgress,
        deviceListener: CrayonicDeviceListener? = null,
        onMidResult: (outputValue: ByteArray) -> Unit = emptyLambda1inUout()
    ): Boolean {
        if (isDisconnectedCheck(errorNumber = 6547)) return false
        val channel = getOpenedChannel() ?: return false
        val requestKey = CrayonicRequestCode.runCommandRawBytes
        if (!channel.supportsRequestCode(requestKey)) {
            invokeMemberErrorAndDiconnect(requestKey)
            return false
        }

        val apduSignature = ApduSignature.rawApduSignature(inputBytes) ?: return false

        val timeoutJob = startRequestTimeout(apduSignature.defaultTimeout, requestKey)
        if(timeoutJob.isActive) {
            channel.sendApdu(
                apduSignature,
                inputBytes,
                { onProgress(requestKey, Progress(it)) }) { any ->
                CoroutineScope(Dispatchers.Main).launch {
                    val bytes = if (any is ByteArray) { any } else { byteArrayOf() }

                    if (deviceListener != null) {
                        val map = mapWithValue(requestKey, bytes)
                        val apduBytes = channel.transmissionController.apduBytes
                        val statusWord = apduBytes.statusWord()
                        map.put(mapValueName(requestKey, apduBytes), apduBytes)
                        map.put(mapValueName(requestKey, statusWord), statusWord)
                        val secondApduBytes = channel.transmissionController.secondaryApduBytes
                        if (secondApduBytes != null) {
                            val secondStatusWord = secondApduBytes.statusWord()
                            map.put(mapValueName(requestKey, secondApduBytes), secondApduBytes)
                            map.put(
                                mapValueName(requestKey, secondStatusWord),
                                secondStatusWord
                            )
                        }
                        deviceListener.onResult(this@CrayonicDevice, requestKey, map)
                    }

                    onMidResult.invoke(bytes)
                }

                timeoutJob.cancel()
            }
            return true
        }
        return false
    }


    protected fun internal_runCommandApdu(
        apduType: Long,
        requestKey: CrayonicRequestCode,
        inputValue: Any? = null,
        onProgress: (requestKey: CrayonicRequestCode, progress: Progress) -> Unit = this::invokeOnProgress,
        deviceListener: CrayonicDeviceListener? = null,
        onMidResult: (outputValue: Any) -> Unit = emptyLambda1inUout()
    ): Boolean {
        if (isDisconnectedCheck(errorNumber = 6548)) return false
        val channel = getOpenedChannel() ?: return false
        if (!channel.supportsRequestCode(requestKey)) {
            invokeMemberErrorAndDiconnect(requestKey)
            return false
        }
        val apduSig = getApduSignature(apduType) ?: return false

        val timeoutJob = startRequestTimeout(apduSig.defaultTimeout, requestKey)
        if(timeoutJob.isActive) {
            sendApduCommand(channel, apduSig, inputValue, { onProgress(requestKey, it) }) {
                CoroutineScope(Dispatchers.Main).launch {
//                    onProgress.invoke(requestKey, Progress(100,100,100))
                    if (deviceListener != null) {
                        val map = mapWithValue(requestKey, it)
                        val apduBytes = channel.transmissionController.apduBytes
                        val statusWord = apduBytes.statusWord()
                        map.put(mapValueName(requestKey, apduBytes), apduBytes)
                        map.put(mapValueName(requestKey, statusWord), statusWord)
                        val secondApduBytes = channel.transmissionController.secondaryApduBytes
                        if (secondApduBytes != null) {
                            val secondStatusWord = secondApduBytes.statusWord()
                            map.put(mapValueName(requestKey, secondApduBytes), secondApduBytes)
                            map.put(
                                mapValueName(requestKey, secondStatusWord),
                                secondStatusWord
                            )
                        }
                        deviceListener.onResult(this@CrayonicDevice, requestKey, map)
                    }
                    onMidResult.invoke(it)

                }
                timeoutJob.cancel()
            }
            return true
        }
        return false
    }


    protected fun sendApduCommand(
        channel: CrayonicApduChannel,
        apduSig: ApduSignature,
        inputValue: Any?,
        onProgress: (Progress) -> Unit,
        onValueReceived: (Any) -> Unit
    ) {
        channel.sendApdu(apduSig, inputValue, onProgress, onValueReceived)
    }

    protected fun getApduSignature(apduType: Long): ApduSignature? {
        val apduSig = ApduSignatures.supportedApdus.get(apduType)
        if (apduSig == null) {
            this.errorFlag =
                UnsupportedOperation("Missing APDU Signature for ${apduType.toByteArray().minimalByteArray().toHex()}")
            return null
        }
        return apduSig
    }

    //////////////////////////////
    // Scripting and tasks

    protected fun internal_runTask(
        requestKey: CrayonicRequestCode,
        inputValue: Any? = null,
        timeout: Long = seconds_120,
        runBlock: (input: Any?, onProgress: (progress: Progress) -> Unit, onTaskResult: (Any) -> Unit) -> ExecutionStatus,
        onProgress: (requestKey: CrayonicRequestCode, progress: Progress) -> Unit = this::invokeOnProgress,
        deviceListener: CrayonicDeviceListener? = null,
        onMidResult: (outputValue: Any) -> Unit = emptyLambda1inUout()

    ): Boolean {
        if (isDisconnectedCheck(errorNumber = 9543)) return false

        val timeoutJob = startRequestTimeout(timeout, requestKey)
        if(timeoutJob.isActive) {
            runBlock.invoke(inputValue, { onProgress(requestKey, it) }) {
                //propagate results
                CoroutineScope(Dispatchers.Main).launch {
                    // callbacks splits here for java and kotlin
                    if (deviceListener != null) {
                        deviceListener.onResult(
                            this@CrayonicDevice,
                            requestKey,
                            mapWithValue(requestKey, it)
                        )
                    }
                    onMidResult.invoke(it)


                }
                timeoutJob.cancel()
            }
            return true
        }
        return false
    }

    private fun startRequestTimeout(
        timeout: Long,
        requestKey: CrayonicRequestCode
    ): Job {
        invokeError(CrayonicDeviceError("${currentUnixTime()}  Starting Timeout ${timeout.div(1000)} sec  ; Request Key : ${requestKey.name} "))
        if(deviceJob.isActive)
            return launchJobOnIO_Timeout(deviceJob, timeout) {
                // when unixTime runs out
                invokeErrorAndDisconnect(CrayonicDeviceError("Request Timeout : ${requestKey.name} "))
            }
        else
            return deviceJob
    }

    protected fun internal_runTaskOfScriptables(
        requestKey: CrayonicRequestCode,
        script: String,
        onProgress: (requestKey: CrayonicRequestCode, progress: Progress) -> Unit = this::invokeOnProgress,
        deviceListener: CrayonicDeviceListener? = null,
        onMidResult: (outputValue: Any) -> Unit = emptyLambda1inUout(),
        timeout: Long = seconds_120
    ): Boolean {
        if (isDisconnectedCheck(9574)) return false
        val timeoutJob = startRequestTimeout(timeout, requestKey)
        if(timeoutJob.isActive) {
//            runSomething{
//                // TODO create scriptables - functional parts that can be done on device or outside of the box
//
//                timeoutJob.cancel()
//            }
            return true
        }
        return false
    }

    //////////////////////////////
    // Helpers

    protected fun getOpenedChannel(): CrayonicDeviceChannel? {
        return deviceNode.openChannel
//        return Controller.getInstance().connectedDevices.get(this)?.let { it }
    }

    protected fun mapWithValue(
        requestKey: CrayonicRequestCode,
        value: Any,
        otherKey: String = ""
    ): HashMap<String, Any> =
        hashMapOf(mapValueName(requestKey, value) to value)

    protected fun mapValueName(
        requestKey: CrayonicRequestCode,
        value: Any,
        otherKey: String = ""
    ): String {
        return "${requestKey}" +
                (if (otherKey.isEmpty()) "" else "_$otherKey") +
                "_${value::class.simpleName.toString()}"
    }

    /////////////////////////////
    // Progress update
    fun invokeOnProgress(requestKey: CrayonicRequestCode, progress: Progress) {
        registeredOnProgress.invoke(this, requestKey, progress)
        registeredListener?.onProgress(this, requestKey, progress)
    }

    /////////////////////////////
    // Error handling

    protected fun isDisconnectedCheck(errorNumber:Int): Boolean =
       if (isConnected()) false
       else {
            invokeErrorAndDisconnect(DeviceOutOfReachError("$errorNumber: First connect the device. Cannot send bytes to closed channel:$this"))
            true
       }

    protected fun invokeApduError(requestKey: CrayonicRequestCode, apduSig: ApduSignature) {
        invokeError(UnsupportedOperation("APDU command failed : $requestKey $apduSig"))
    }

    protected fun invokeMemberError(requestKey: CrayonicRequestCode) {
        invokeError(UnsupportedOperation("Member read failed : $requestKey"))
    }

    protected fun invokeError(
        error: CrayonicDeviceError,
        requestKey: CrayonicRequestCode = CrayonicRequestCode.unspecified
    ) {
        launchOnMain {
            errorFlag = error
            registeredOnError.invoke(this, error)
            registeredListener?.onError(this, requestKey, error)
        }
    }

    protected fun invokeApduErrorAndDiconnect(
        requestKey: CrayonicRequestCode,
        apduSig: ApduSignature
    ) {
        invokeErrorAndDisconnect(UnsupportedOperation("APDU command failed : $requestKey $apduSig"))
    }
    protected fun invokeApduErrorAndDiconnect(
        requestKey: CrayonicRequestCode,
        apduType: Long
    ) {
        invokeErrorAndDisconnect(UnsupportedOperation("APDU command failed : $requestKey ${apduType.toByteArray().minimalByteArray().toHex()}"))
    }

    protected fun invokeMemberErrorAndDiconnect(requestKey: CrayonicRequestCode) {
        invokeErrorAndDisconnect(UnsupportedOperation("Member read failed : $requestKey"))
    }

    protected fun invokeErrorAndDisconnect(error: CrayonicDeviceError) {
        CoroutineScope(Dispatchers.Main).launch {
            invokeError(error)
            disconnect(registeredOnDisconnect)
        }
    }


    /////////////////////////////
    // Comparision

    override fun hashCode(): Int {
        return deviceTags.sorted().joinToString(prefix = "-", postfix = "-", separator = "-.-")
            .hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CrayonicDevice

        if (deviceTags != other.deviceTags) return false
        if (modelTag != other.modelTag) return false

        return true
    }

    override fun toString(): String {
        return deviceTags.sorted().joinToString(prefix = "[", postfix = "]", separator = ", ")
//        return deviceTags.sorted().joinToString(prefix = "[", postfix = "]", separator = ", ")
    }

    open fun toVerboseString():String{
        return toCrayonicDeviceVerboseHolder().toString()
    }

    fun toCrayonicDeviceVerboseHolder(): CrayonicDeviceVerboseHolder {
        return CrayonicDeviceVerboseHolder(
            model = modelTag,
            name = name,
            address = address,
            battery = battery,
            firmwareVersion = firmwareVersion,
            modelNumber = modelNumber,
            services = services.sorted().joinToString(", "),
            tags = this.toString()
        )
    }


    data class CrayonicDeviceVerboseHolder(
        var model: String = "",
        var name: String = "",
        var address: String = "",
        var battery:String = "",
        var firmwareVersion: String = "",
        var modelNumber: String = "",
        var services: String = "",
        var tags: String = ""
        ){
        override fun hashCode(): Int {
            return address.hashCode()
        }

        override fun equals(other: Any?): Boolean {
            if (other == null) return false
            if (other is CrayonicDevice)
                return address.equals(other.address)
            return false
        }
    }
}