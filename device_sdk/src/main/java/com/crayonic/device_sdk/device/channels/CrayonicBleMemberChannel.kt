package com.crayonic.device_sdk.device.channels

import com.crayonic.device_sdk.ble.BleDeviceCharacteristics
import com.crayonic.device_sdk.ble.BleDeviceNode
import com.crayonic.device_sdk.device.errors.CrayonicDeviceError
import com.crayonic.device_sdk.device.errors.DeviceOutOfReachError
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode.*
import com.crayonic.device_sdk.util.runOnBleStack
import java.nio.charset.Charset

/**
 * Each Crayonic device has access to following member values
 */
open class CrayonicBleMemberChannel( val deviceAddress: String): CrayonicMemberChannel {

    var parentChannel: CrayonicDeviceChannel? = null
    var parentNode: BleDeviceNode<*>? = null

    // implements read characteristic used in derived classes
    fun readBinaryCharacteristic(
        characteristic: String,
        onReceived: (ByteArray) -> Unit
    ): Boolean {
        return runOnBleStack(onBleStackFail(), { bleStack ->
            bleStack.readCharacteristic(
                deviceAddress,
                characteristic,
                onCommunicationFail(characteristic)
            ) { bytes: ByteArray ->
                onReceived(bytes)
            }
        })
    }

    fun readStringCharacteristic(
        characteristic: String,
        onReceived: (String) -> Unit
    ):Boolean {
        return runOnBleStack(onBleStackFail(), { bleStack ->
            bleStack.readCharacteristic(
                deviceAddress,
                characteristic,
                onCommunicationFail(characteristic)
            ) { bytes: ByteArray ->
                onReceived(bytes.toString(Charset.defaultCharset()))
            }
        })
    }

    private fun onBleStackFail(): (String) -> Unit = { reason ->
        parentChannel?.registeredOnError?.invoke(
            CrayonicDeviceError(
                "Application level Ble Stack failure : $reason"
            )
        )
    }
    private fun onCommunicationFail(characteristic: String): (String) -> Unit = { reason ->
        parentChannel?.registeredOnError?.invoke(
            DeviceOutOfReachError(
                "Failed reading characteristic $characteristic from channel: $this. Reason : $reason"
            )
        )
    }


    override fun supportsRequestCode(requestKey: CrayonicRequestCode): Boolean =
        supportedCommands.contains(requestKey)

    override fun readMemberForRequest(
        requestKey: CrayonicRequestCode,
        onMidResult: (String) -> Unit
    ) {
        when (requestKey) {
            readDeviceAddress -> readDeviceAddress(onMidResult)
            readDeviceName -> readDeviceName(onMidResult)
            readBatteryLevel -> readBatteryLevel(onMidResult)
            readFirmwareVersion -> readFirmwareVersion(onMidResult)
            readModelNumber -> readModelNumber(onMidResult)
        }
    }

    override fun readDeviceAddress(onMidResult: (String) -> Unit): Boolean {
        onMidResult(deviceAddress)
        return true
    }

    override fun readDeviceName(onMidResult: (String) -> Unit): Boolean =
        readStringCharacteristic(BleDeviceCharacteristics.BleChars.Generic_Access_Read_DeviceName) { name ->
            parentNode?.name = name
            onMidResult(name)
        }

    override fun readBatteryLevel(onMidResult: (String) -> Unit): Boolean =
        readBinaryCharacteristic(BleDeviceCharacteristics.BleChars.Generic_Battery_Read_Level){bytes: ByteArray ->
            val convert = if (bytes.isNotEmpty())  "${bytes[0].toInt()}" else "n/a"
            onMidResult.invoke(convert)
        }

    override fun readFirmwareVersion(onMidResult: (String) -> Unit): Boolean =
        readStringCharacteristic(BleDeviceCharacteristics.BleChars.Generic_DeviceInfo_Read_FwVersion, onMidResult)

    override fun readModelNumber(onMidResult: (String) -> Unit): Boolean =
        readStringCharacteristic(BleDeviceCharacteristics.BleChars.Generic_DeviceInfo_Read_ModelInfo, onMidResult)

    companion object {
        val supportedCommands = listOf<CrayonicRequestCode>(
            readDeviceAddress,
            readDeviceName,
            readBatteryLevel,
            readFirmwareVersion,
            readModelNumber
        )
    }
}
