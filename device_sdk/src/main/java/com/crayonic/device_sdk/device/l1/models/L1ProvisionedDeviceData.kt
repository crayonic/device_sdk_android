package com.crayonic.device_sdk.device.l1.models

data class L1ProvisionedDeviceData(
    var publicKey: String,
    var ethAddress: String,
    var deviceMacAddress: String
)