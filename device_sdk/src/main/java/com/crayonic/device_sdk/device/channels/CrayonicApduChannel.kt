package com.crayonic.device_sdk.device.channels

import com.crayonic.device_sdk.apdu.ApduSignature
import com.crayonic.device_sdk.apdu.ApduSignatures
import com.crayonic.device_sdk.apdu.intToLengthBytes
import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.util.TagsEnum
import com.crayonic.device_sdk.device.errors.CrayonicDeviceError
import com.crayonic.device_sdk.device.errors.DeviceOutOfReachError
import com.crayonic.device_sdk.device.errors.UnsupportedOperation
import com.crayonic.device_sdk.device.ExecutionStatus
import com.crayonic.device_sdk.util.Progress
import com.crayonic.device_sdk.util.currentUnixTime
import equalsInstanceType
import space.lupu.kapdu.bytes.ApduBytes
import space.lupu.kapdu.statusWords.KnownApduStatusWords
import space.lupu.kapdu.transmission.ApduTransmissionController
import space.lupu.kapdu.transmission.TransmissionProgress
import space.lupu.koex.bytes.minimalByteArray
import space.lupu.koex.hex.toHex
import space.lupu.koex.hex.toHexPrefixed0x
import space.lupu.koex.hex.toHexSeparated
import space.lupu.koex.numbers.toByteArray
import space.lupu.konnector.tags.Tags

@Suppress("UNUSED_ANONYMOUS_PARAMETER")
abstract class CrayonicApduChannel(
    id: String = Single.cacheId(TagsEnum.device_channel.name+"_apdu"),
    tags: Tags = Tags(TagsEnum.device_channel.name, TagsEnum.apdu.name )
) : ByteArrayChannel(id, tags) {

    /**
     * Implementation of the RX/TX channel may differ on devices. What stays is the in out values.
     *
     * You should implement this function depending on the target device (BLE,TCP) and platform (Android,iOS).
     * @param payload bytes to be send over TX
     * @param onReceived callback that is invoked after bytes are received at RX
     */
    abstract override fun send(payload: ByteArray, onReceived: (ByteArray) -> Unit)

    ////////////////////////
    // channel control

    var registeredOnError: ((CrayonicDeviceError) -> Unit)? = null
        private set(value) {
            field = value
        }

    fun attachOnError(onError: (CrayonicDeviceError) -> Unit) {
        registeredOnError = onError
    }

    private var openFlag = true // by default open channel

    fun isOpened(): Boolean = openFlag
    fun isClosed(): Boolean = !openFlag
    fun close() {
        openFlag = false
    }

    fun open() {
        openFlag = true
    }

    ////////////////////////
    // apdu control

    var transmissionController = ApduTransmissionController(
        ApduBytes(),
        stopOnStatusWordSignatures = KnownApduStatusWords.successStatusWordSignatures,
        onReadyToTransmit = { apduBytes -> },
        onTransmissionDone = { apduBytes, secondaryApduBytes -> },
        onTransmissionProgress = { progress -> }
    )

    fun sendApdu(
        apduSignature: ApduSignature,
        inputValue: Any?,
        onProgress: (Progress) -> Unit,
        onApduResult: (Any) -> Unit
    ) {
        transmissionController.resetTransmission(
            apduBytes = apduSignature.toApduBytes(),
            onReadyToTransmit = { bytesToSend: ApduBytes ->
                if (isOpened()) {
                    registeredOnError?.invoke(CrayonicDeviceError("SEND Apdu Sig : ${apduSignature.toApduBytes().toStringVerbose()} \nBytes to send : ${bytesToSend} "))
                    send(bytesToSend.bytes) { bytes: ByteArray ->
                        registeredOnError?.invoke(CrayonicDeviceError("${currentUnixTime()} RECEIVED Apdu Sig : ${apduSignature.toApduBytes().toStringVerbose()} \nBytes to send : ${bytesToSend}  \nBytes received  : ${bytes.toHex()} "))
                        registeredOnError?.invoke(CrayonicDeviceError("${currentUnixTime()} RECEIVED Bytes : ${bytes.toHex()} "))

                        transmissionController.append(
                            bytes
                        )
                    }
                }else{
                    registeredOnError?.invoke(DeviceOutOfReachError("First connect the device. Cannot send bytes to closed channel:$this"))
                }
            },
            onTransmissionProgress = {transmissionProgress: TransmissionProgress ->
                onProgress.invoke(Progress(transmissionProgress))
            },
            onTransmissionDone = { apduBytes: ApduBytes, secondaryApduBytes: ApduBytes? ->
                if(secondaryApduBytes != null){
                    val ok = KnownApduStatusWords.ok.toByteArray().minimalByteArray()
                    send(ok) {bytes:ByteArray->
                        registeredOnError?.invoke(CrayonicDeviceError("${currentUnixTime()} Received unexpected bytes. Sent OK (${ok.toHexPrefixed0x()}), Received: ${bytes.toHex()} "))
                    }
                    secondaryApduBytes.secondaryStatusWord = ok
                }
                registeredOnError?.invoke(CrayonicDeviceError("Transmission done : Received Apdu \n${apduBytes.toStringVerbose()} \nSecondary Apdu \n${secondaryApduBytes?.toStringVerbose()?:"NULL"}"))
                onApduResult.invoke(
                    prepResult(
                        apduSignature,
                        apduBytes,
                        secondaryApduBytes
                    )
                )
            },
            stopOnStatusWordSignatures = apduSignature.expectedStatusWords
        )
        if (!transmissionController.isReadyForTransmit)
            transmissionController.append(prepInput(apduSignature, inputValue)) // length bytes and data
        // if not prepped than throw an error
        if (!transmissionController.isReadyForTransmit) {
            registeredOnError?.invoke(UnsupportedOperation("Incomplete APDU. Not enough bytes provided.\n input: ${inputValue}\n APDU : $apduSignature .\n"))
        }
    }


//    /**
//     * Uses apdu signature to obtain expected length of data field
//     * @return -1 in case it is not possible to obtain correct length
//     */
//    private fun defaultExpectedLength(inputValue: Any?, sig: ApduSignature): Int {
//        val default = sig.dataDefault
//        if (default == null) {
//            return -1
//        } else
//            return when (default) {
//                is Number -> default.toInt()
//                is ByteArray -> default.size
//                is String -> default.length
//
//                is BytesRange -> if (inputValue is ByteArray) {
//                    if (inputValue.size in default.getRange()) {
//                        inputValue.size
//                    } else -1
//                } else -1
//
//                is StringRange -> if (inputValue is String) {
//                    if (inputValue.length in default.getRange()) {
//                        inputValue.length * 2 // 2 bytes per char
//                    } else -1
//                } else -1
//                else -> -1
//            }
//    }


    /**
     * @return empty byte array on check fail | correct apdu len+data bytes based on input and ApduSignature
     */
    private fun prepInput(apduSignature: ApduSignature, inputValue: Any?): ByteArray {
        if (apduSignature.isReadRecord()) {
            // take only length, ignore inputs data for now
            return intToLengthBytes(apduSignature.expectedLength)
        } else if(apduSignature.isWriteRecord()) {
            var inputs = apduSignature.dataDefault.invoke()
            if (inputValue != null && inputs.equalsInstanceType(inputValue)) inputs = inputValue
            val dataBytes = apduSignature.onPrepInputToBytes(apduSignature, inputs)
            var expectedLength = apduSignature.expectedLength
            if(expectedLength < 0){
                expectedLength = dataBytes.size
            }
            if (dataBytes.size != expectedLength)
                registeredOnError?.invoke(UnsupportedOperation("Unsupported input. Input byte size (${dataBytes.size}) does not match the Expected length ${apduSignature.expectedLength}. \n input: ${inputs}\n APDU : $apduSignature .\n"))
            return intToLengthBytes(expectedLength) + dataBytes
        }else{
            // we don`t support anything else
            registeredOnError?.invoke(UnsupportedOperation("Unsupported APDU.\n APDU : $apduSignature .\n"))
            return byteArrayOf()
        }

    }

    /**
     * Prepare received bytes to higher level before sending it to callback
     *
     * Translates bytes to expected type by ApduSignature if possible.
     *
     * @return Any() on failure | expected type up-casted to Any on success
     */
    private fun prepResult(
        apduSignature: ApduSignature,
        apduBytes: ApduBytes,
        secondaryApduBytes: ApduBytes?
    ): Any {
        var workingApduBytes = apduBytes
        var workingApduSig: ApduSignature? = apduSignature
        if (apduBytes.isWriteRecord()) {
            // write record -- ignore despite it is two way apdu
            if (secondaryApduBytes != null) {
                workingApduBytes = secondaryApduBytes
                workingApduSig = ApduSignatures.supportedApdus.get(workingApduBytes.apduType())
            } else {
                return workingApduBytes.statusWordOrNull()?: ExecutionStatus.error // nothing to be done - by default write record ignores onPrepBytesToOutput
            }
        }


        if (workingApduSig == null) {
            registeredOnError?.invoke(
                UnsupportedOperation(
                    "Unsupported 2 way apdu. \n" +
                            " initial apdu : $apduSignature\n" +
                            " secondary apdu : ${workingApduBytes.toStringVerbose()}\n"
                )
            )
            return Any()
        }

        // now it is either read record or 2nd apdu of 2way apdu communication
        // in each case the data value is of interest here

        val targetInstance = workingApduSig.dataDefault.invoke()
        val resultValue = workingApduSig.onPrepBytesToOutput(workingApduSig, workingApduBytes)
        if (!targetInstance.equalsInstanceType(resultValue)) {
            registeredOnError?.invoke(
                UnsupportedOperation(
                    "Unsupported output. \n" +
                            " output: ${resultValue}\n" +
                            " default output (target): $targetInstance\n" +
                            " received : ${workingApduBytes.toStringVerbose()}\n" +
                            " APDU : $workingApduSig .\n"
                )
            )
        }
        return resultValue
//        }else{
//            registeredOnError?.invoke(
//                UnsupportedOperation(
//                    "Unsupported APDU. \n received : ${workingApduBytes.toStringVerbose()}\n APDU : $apduSignature .\n"
//                )
//            )
//        }
//        return Any()
    }


}

