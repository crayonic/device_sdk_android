package com.crayonic.device_sdk.device.channels

/**
 * Each Crayonic device has access to following member values
 */
interface CrayonicMemberChannelL1 : CrayonicMemberChannel {
    fun readEthAddress(onMidResult: (String) -> Unit): Boolean
    fun readAssetId(onMidResult: (String) -> Unit): Boolean
    fun readTemperature(onMidResult: (String) -> Unit): Boolean
    fun readHumidity(onMidResult: (String) -> Unit): Boolean
    fun readLight(onMidResult: (String) -> Unit): Boolean
    fun readShock(onMidResult: (String) -> Unit): Boolean
    fun readIncline(onMidResult: (String) -> Unit): Boolean
}
