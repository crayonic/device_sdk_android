package com.crayonic.device_sdk.device.channels

import com.crayonic.device_sdk.ble.BleDeviceCharacteristics.BleChars
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode.*
import com.crayonic.device_sdk.util.SensorCalcHelper
import com.crayonic.device_sdk.util.toOpThresholds
import space.lupu.koex.hex.toHexPrefixed0x
import java.nio.charset.Charset

/**
 * L1 Crayonic device has access to following member values
 */
class CrayonicBleMemberChannelL1(
    deviceAddress: String
) : CrayonicBleMemberChannel(deviceAddress),
    CrayonicMemberChannelL1 {


    override fun supportsRequestCode(requestKey: CrayonicRequestCode): Boolean =
        supportedCommands.contains(requestKey)

    override fun readMemberForRequest(
        requestKey: CrayonicRequestCode,
        onMidResult: (String) -> Unit
    ) {
        super.readMemberForRequest(requestKey, onMidResult)
        when (requestKey) {
            readEthAddress -> readEthAddress(onMidResult)
            readAssetId -> readAssetId(onMidResult)
            readTemperature -> readTemperature(onMidResult)
            readHumidity -> readHumidity(onMidResult)
            readLight -> readLight(onMidResult)
            readShock -> readShock(onMidResult)
//            readIncline -> readIncline(onMidResult)
        }
    }

    override fun readEthAddress(onMidResult: (String) -> Unit): Boolean =
        readBinaryCharacteristic(BleChars.L1_State_Read_EthAddress) { bytes:ByteArray->
            val converted = bytes.toString(Charset.defaultCharset())
            onMidResult.invoke(converted)
        }

    override fun readAssetId(onMidResult: (String) -> Unit): Boolean =
        readBinaryCharacteristic(BleChars.L1_State_Read_AssetId ){ bytes:ByteArray->
            val converted = bytes.toHexPrefixed0x()
            onMidResult.invoke(converted)
        }

    override fun readTemperature(onMidResult: (String) -> Unit): Boolean =
        readBinaryCharacteristic(BleChars.L1_State_Read_Temperature ){ bytes:ByteArray->
            val converted = "${SensorCalcHelper.decodeTemperatureForThresholds(bytes)}"
            onMidResult.invoke(converted)
        }

    override fun readHumidity(onMidResult: (String) -> Unit): Boolean =
        readBinaryCharacteristic(BleChars.L1_State_Read_Humidity ){ bytes:ByteArray->
            val converted = "${SensorCalcHelper.decodeHumidity(bytes)}"
            onMidResult.invoke(converted)
        }

    override fun readLight(onMidResult: (String) -> Unit): Boolean =
        readBinaryCharacteristic(BleChars.L1_State_Read_Light ){ bytes:ByteArray->
            val converted = "${bytes.toOpThresholds()}"
            onMidResult.invoke(converted)
        }

    override fun readShock(onMidResult: (String) -> Unit): Boolean =
        readBinaryCharacteristic(BleChars.L1_State_Read_Shock ){ bytes:ByteArray->
            val converted = "${SensorCalcHelper.decodeShock(bytes)}"
            onMidResult.invoke(converted)
        }

    override fun readIncline(onMidResult: (String) -> Unit): Boolean =
//        "not supported"
            readBinaryCharacteristic(BleChars.L1_State_Read_Incline ){ bytes:ByteArray->
            val converted = ""
            onMidResult.invoke(converted)
        }


    companion object {
        val supportedCommands = listOf<CrayonicRequestCode>(
            readDeviceAddress,
            readDeviceName,
            readBatteryLevel,
            readFirmwareVersion,
            readModelNumber,

            readEthAddress,
            readAssetId,
            readTemperature,
            readHumidity,
            readLight,
            readShock
//            ,
//            readIncline
        )
    }
}
