package com.crayonic.device_sdk.device.errors;

public enum CrayonicDeviceErrorType {
    NoError, GenericError, DeviceOutOfReachError, UnsupportedDevice, UnsupportedOperation
}
