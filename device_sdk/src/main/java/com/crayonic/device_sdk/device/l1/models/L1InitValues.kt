package com.crayonic.device_sdk.device.l1.models

import com.crayonic.device_sdk.device.l1.models.CrayonicAlarmZone

data class L1InitValues(
    var packageId: String,
    var notificationEmail: String,
    var alarmZones: List<CrayonicAlarmZone>,
    var apiAccount: String,
    var apiSecret: String,
    var adminPin: String,
    var newPin: String
)