package com.crayonic.device_sdk.device

import com.crayonic.device_sdk.core.Single
import com.crayonic.device_sdk.util.TagsEnum
import space.lupu.konnector.NodeProvider
import space.lupu.konnector.filters.TaggedObjectFilter
import space.lupu.konnector.nodes.Node
import space.lupu.konnector.tags.Tags

@Suppress("UNCHECKED_CAST")
open class CrayonicDeviceNode_ListProvider(val nodes:List<CrayonicDeviceNode>, id:String = Single.cacheId(
    "listProvider"
)
): DevicesProvider(id){
    override fun listDeviceNodes(
        filter: DeviceFilter,
        onNodesListed: (nodes: List<CrayonicDeviceNode>, originFilter: DeviceFilter) -> Unit,
        deviceTags: Tags
    ): Boolean {
        onNodesListed.invoke(
            (filter.filterNodes(nodes.map { crayonicDeviceNode ->
                crayonicDeviceNode.apply {
                    tags.addAll( crayonicDeviceNode.provideNodeServices() )
                    tags.addAll(
                        crayonicDeviceNode.provideNodeAddress(),
                        crayonicDeviceNode.provideNodeName(),
                        TagsEnum.l1.name
                    )
                }

            }) as List<CrayonicDeviceNode>)

                , filter)
        return true
    }

    init {
        Single.cacheObject(id, this)
    }

}