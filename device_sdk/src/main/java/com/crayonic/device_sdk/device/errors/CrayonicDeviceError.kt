package com.crayonic.device_sdk.device.errors

import com.crayonic.device_sdk.device.errors.CrayonicDeviceErrorType

open class CrayonicDeviceError(
        detailedMessage: String = "",
        val errorType: CrayonicDeviceErrorType = CrayonicDeviceErrorType.GenericError
) : Exception(detailedMessage)

