package com.crayonic.device_sdk.device.kotojava;

import java.util.List;

public interface OfflineReadEventListener {
    void onResult(List<String> deviceEvents);
}
