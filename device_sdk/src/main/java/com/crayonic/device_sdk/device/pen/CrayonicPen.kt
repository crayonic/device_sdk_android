package com.crayonic.device_sdk.device.pen

import com.crayonic.device_sdk.apdu.ApduType
import com.crayonic.device_sdk.device.CrayonicDeviceNode
import com.crayonic.device_sdk.device.ExecutionStatus
import com.crayonic.device_sdk.device.kotojava.CrayonicDeviceListener
import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
import com.crayonic.device_sdk.device.CrayonicDevice
import com.crayonic.device_sdk.util.Progress
import com.crayonic.device_sdk.util.seconds_120
import space.lupu.koex.lambda.emptyLambda2inUout


@Suppress("UNUSED_ANONYMOUS_PARAMETER", "unused")
class CrayonicPen(initNode:CrayonicDeviceNode) : CrayonicDevice(initNode) {

    override val modelTag = "CrayonicPen"

//    fun runCommandSetPinCode( adminPin:String, newPin:String, deviceListener: CrayonicDeviceListener)=
//        internal_runCommandApdu(
//            ApduType.wr_clock,
//            CrayonicRequestCode.runCommandSetManagementPinCode,
//                listOf(adminPinToByteArray(adminPin), adminPinToByteArray(newPin)),
//            deviceListener = deviceListener
//        )
//
//    fun runCommandSetPinCode( adminPin:String, newPin:String, onResult:(device: CrayonicPen) -> Unit = {} ) {/*  */}

    //////////////////////////

    fun runCommandSignEthTx(ethTx:ByteArray, deviceListener: CrayonicDeviceListener) =
        internal_runCommandApdu(
            ApduType.wr_bip44_tx_sign,
            CrayonicRequestCode.runCommandSignEthTx,
            ethTx,
            deviceListener = deviceListener
        )

    fun runCommandSignEthTx(ethTx:ByteArray, onResult: (device: CrayonicDevice, signedEthTx:ByteArray) -> Unit = emptyLambda2inUout()
    ) =
        internal_runCommandApdu(
            ApduType.wr_bip44_tx_sign,
            CrayonicRequestCode.runCommandSignEthTx,
            ethTx
        ) { onResult.invoke(this, it as ByteArray) }

    //////////////////////

    fun runTaskInitializePen(adminPin:String, deviceListener: CrayonicDeviceListener) =
        internal_runTask(
            CrayonicRequestCode.runTaskInitializePen,
            inputValue = adminPin,
            deviceListener = deviceListener,
            onMidResult = {},
            timeout = seconds_120, // this hasto change 10 minutes is too long of a tieout
            runBlock = initializePen()
        )

    fun runTaskInitializePen(adminPin:String, onResult:(device: CrayonicPen) -> Unit = {} ) =
        internal_runTask(
            CrayonicRequestCode.runTaskInitializePen,
            inputValue = adminPin,
            deviceListener = null,
            onMidResult = {onResult.invoke(this)},
            timeout = seconds_120, // this hasto change 10 minutes is too long of a tieout
            runBlock = initializePen()
        )

    protected fun initializePen(): (input: Any?, onProgress: (progress: Progress) -> Unit, onTaskResult: (Any) -> Unit) -> ExecutionStatus =
        { input, onProgress, onTaskResult ->
            // input = = = adminPin
//            when {
//                input == null -> onTaskResult(Any())
//                !(input is String) -> onTaskResult(Any())
//                else -> {
//                    val adminPin = input as String
//                    // TODO : init pen device
//
//                }
//            }
            ExecutionStatus.error
        }

    ///////////////////

    fun readPenData(adminPin:String, deviceListener: CrayonicDeviceListener) =
        internal_runTask(
            CrayonicRequestCode.runTaskReadData,
            inputValue = adminPin,
            deviceListener = deviceListener,
            onMidResult = {},
            timeout = seconds_120, // this hasto change 10 minutes is too long of a tieout
            runBlock = readPenData()
        )

    fun readPenData(adminPin:String, onResult:(device: CrayonicPen) -> Unit = {} ) =
        internal_runTask(
            CrayonicRequestCode.runTaskReadData,
            inputValue = adminPin,
            deviceListener = null,
            onMidResult = {onResult.invoke(this)},
            timeout = seconds_120, // this hasto change 10 minutes is too long of a tieout
            runBlock = readPenData()
        )

    protected fun readPenData(): (input: Any?, onProgress: (progress: Progress) -> Unit, onTaskResult: (Any) -> Unit) -> ExecutionStatus =
        { input, onProgress, onTaskResult ->
            // input = = = adminPin
//            when {
//                input == null -> onTaskResult(Any())
//                !(input is String) -> onTaskResult(Any())
//                else -> {
//                    val adminPin = input as String
//                    // TODO : read Data from pen
//
//                }
//            }
            ExecutionStatus.error
        }

}