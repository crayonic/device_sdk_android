package com.crayonic.device_sdk.device.errors

class UnsupportedOperation(detailedMessage: String = ""):
    CrayonicDeviceError(
        detailedMessage = detailedMessage,
        errorType = CrayonicDeviceErrorType.UnsupportedOperation
    )