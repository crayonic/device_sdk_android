package com.crayonic.device_sdk.device.l1.models

import com.crayonic.device_sdk.util.toEthKeccakHex
import com.crayonic.device_sdk.util.toJson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import space.lupu.koex.hex.Hex
import space.lupu.koex.hex.toHex
import java.nio.charset.Charset

class L1EventParser(val bytes:ByteArray){

    private var jsonStr= ""
    private var prefix = ""
    private var suffix = ""

    private var idDataHash = ""
    private var prefixLength = 0
    private var suffixLength = 0

    private var dataArrayStart = ""
    private var dataArrayEnd = ""

    var dataString :String = ""
        private set(value) { field = value }

    var isValid = false
        private set(value) { field = value }

    var dataHash: String = ""
        private set(value) { field = value }

    var dataRawHexString :String = ""
        private set(value) { field = value }

    init {
        //decompress intercepted data to String of Json and header
        jsonStr = decodedJsonFromEncodedData(bytes)
    }

    private fun decodedJsonFromEncodedData(received: ByteArray): String {
        // prefix len from header
        var slidingIndex = -1
        var compareChar = ""
        while (compareChar != HEADER_SPLIT_CHAR_0){
            slidingIndex++
            compareChar = String(received.copyOfRange(slidingIndex, slidingIndex+1))
        }
        prefixLength = Integer.parseInt(String(received.copyOfRange(0,slidingIndex)))

        // suffix len from header
        var readIndex = ++slidingIndex
        while (compareChar != HEADER_SPLIT_CHAR_1){
            slidingIndex++
            compareChar = String(received.copyOfRange(slidingIndex, slidingIndex+1))
        }
        suffixLength = Integer.parseInt(String(received.copyOfRange(readIndex,slidingIndex)))
//        BigInteger(String(received.copyOfRange(readIndex,slidingIndex-1))).toLong()
        val headerLength = slidingIndex

        dataArrayStart = startOfDataArray(received, headerLength, prefixLength)
        val isEndingIdDataHex = containsIdDataHashAtTheEndOfJson(received)
        dataArrayEnd = endOfDataArray(received,suffixLength)

        val sb = StringBuilder()
        // reading prefix
        prefix = String(received.copyOfRange(headerLength, headerLength + prefixLength ))
        sb.append(prefix)
        if(isEndingIdDataHex){
            // reading data
            dataRawHexString = received.copyOfRange(headerLength + prefixLength , received.size - suffixLength ).toHex()
            sb.append(dataRawHexString)
            dataString = dataArrayStart+dataRawHexString+dataArrayEnd
            dataHash = dataString.toEthKeccakHex()
            // reading sufix from data position until the last }
            suffix = fixForMissingAccount(
                String(received.copyOfRange(received.size - suffixLength , received.size -ID_DATA_DIGEST_LEN ))
            )
            sb.append(suffix)
        }else{
            // reading data
            dataRawHexString = received.copyOfRange(headerLength + prefixLength , received.size - suffixLength ).toHex()
            sb.append(dataRawHexString)
            dataString = dataArrayStart+dataRawHexString+dataArrayEnd
            dataHash = dataString.toEthKeccakHex()
            // reading sufix from data position until the last }
            suffix = fixForMissingAccount(
                String(received.copyOfRange(received.size - suffixLength , received.size ))
            )
            sb.append(suffix)
        }
        validate()
        return sb.toString()
    }

    private fun containsIdDataHashAtTheEndOfJson(received: ByteArray): Boolean {
        // idData hash - input to signature
        idDataHash = String(received.copyOfRange(received.size - ID_DATA_DIGEST_LEN, received.size ))

        if(!idDataHash.last().equals('}')){ // warning comparing char - don`t forget '' not ""
            return true
        }
        idDataHash = ""
        return false
    }


    private fun validate() {
        isValid = isStartOfSuffixCorrect(suffix)
                && prefix.isNotBlank()
                && suffix.isNotBlank()
                && dataHash.isNotBlank()
                && dataRawHexString.isNotBlank()
    }

    private fun fixForMissingAccount(suffix:String):String {
        if(isStartOfSuffixCorrect(suffix))
            return suffix
        else
            return correctedSuffix(suffix)
    }

    private fun isStartOfSuffixCorrect(suffix:String): Boolean {
        return suffix.startsWith("\",\"type\":\"")
    }

    private fun correctedSuffix(suffix:String): String {
        return suffix.removePrefix(suffix.substringBefore("\",\"type\":\""))
    }

    fun getEventStr(): String {
        return jsonStr
    }

    fun getJson(): JsonObject {
        return jsonStr.toJson()
    }

    fun getAssetId() : String {
        try {
            val json = jsonStr.toJson()
            val content = json.getAsJsonObject("content")
            val dataId = content.getAsJsonObject("idData")
            val assetId = dataId.getAsJsonPrimitive("assetId").asString
            return assetId
        }catch (e:Exception){
        }
        return ""
    }

    fun getEventId():String{
        return jsonStr.toJson().get("eventId").asString
    }

    fun getCheckHashCRC(): String {
        try {
            return Hex(getEventId()).toString()
        }catch (e:Exception){
            return ""
        }
    }

    companion object {
        val HEADER_SPLIT_CHAR_0 = ":"
        val HEADER_SPLIT_CHAR_1 = "{"
        val ID_DATA_DIGEST_LEN = 66
        fun startOfDataArray(received: ByteArray, headerLength :Int, prefixLength :Int):String{
            val sqBracketIndex = received.copyOfRange(0 , headerLength + prefixLength ).toString(
                Charset.defaultCharset()).indexOf('[')
            return received.copyOfRange(sqBracketIndex, headerLength + prefixLength ).toString(
                Charset.defaultCharset())
        }

        fun endOfDataArray(received: ByteArray, suffixLength:Int):String{
            val totLen = received.size
            val sqBracketIndex = received.copyOfRange(totLen-suffixLength, totLen).toString(Charset.defaultCharset()).lastIndexOf(']')
            return received.copyOfRange(totLen-suffixLength, totLen-suffixLength+sqBracketIndex+1).toString(
                Charset.defaultCharset())
        }
    }

}