package com.crayonic.device_sdk.device.l1.models

import com.crayonic.device_sdk.device.l1.models.CrayonicAlarmZone

data class L1InitOfflineValues(
    var packageId: String,
    var assetId: String,
    var alarmZones: List<CrayonicAlarmZone>
)