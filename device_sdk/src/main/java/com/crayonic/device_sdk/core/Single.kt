package com.crayonic.device_sdk.core

import space.lupu.koex.hex.toHex
import space.lupu.koex.strings.nextStringInt
import kotlin.random.Random

/**
 * Single is short for Singleton Holder
 *
 * Any useful methods that should survive during runtime are here
 */
object Single{

    val random = Random(System.currentTimeMillis())

    //
    //////////////////////////
    // id section

    private var slidingUniqueIdLength = 5

    fun randomId(prefix:String="", postfix:String="", separator:String="-",slidingLength:Int = slidingUniqueIdLength): String =
        (if (prefix.isEmpty())"" else prefix.plus(separator))
            .plus(random.nextStringInt(slidingLength))
            .plus(separator).plus(postfix)

    fun cacheId(prefix:String="", postfix:String="", separator:String="-",slidingLength: Int = slidingUniqueIdLength): String {
        var id = randomId(prefix, postfix, separator, slidingLength)
        var retryCount = 0
        while(idMap.containsKey(id)){
            id = randomId(prefix, postfix, separator, slidingLength)
            if (retryCount>10 && slidingLength<9) { // when you hit 10 times the same random as was generated before ... something is wrong
                slidingUniqueIdLength+=1
                retryCount = 0
            }
            retryCount+=1
        }
        idMap.put(id, Any())
        return id
    }

    fun cacheObject(uid:String, obj:Any?)= idMap.put(uid, obj)
    fun retrieveObject(uid:String)= idMap.get(uid)
    fun removeObject(uid:String)= idMap.remove(uid)

    private val idMap = hashMapOf<String,Any?>()

    // id section
    //////////////////////////
    // constants

    val zeros32 = ByteArray(32)
    val eventCrc32 = ByteArray(32)
    val pin32 = ByteArray(32)
    val zeros64 = ByteArray(64)


    val defaultPinCode = pin32.toHex()
}