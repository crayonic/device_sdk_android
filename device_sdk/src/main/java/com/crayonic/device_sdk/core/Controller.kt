package com.crayonic.device_sdk.core

import android.util.Log
import com.crayonic.device_sdk.device.DevicesModule
import com.crayonic.device_sdk.device.channels.CrayonicDeviceChannel
import com.crayonic.device_sdk.memory.MemoryModule
import com.crayonic.device_sdk.memory.RunningCache
import com.crayonic.device_sdk.memory.SaveLoadInterface
import com.crayonic.device_sdk.device.CrayonicDevice
import com.crayonic.device_sdk.device.DeviceFilter
import com.crayonic.device_sdk.http.l1_json_api.L1ApiJson_Postable
import com.crayonic.device_sdk.util.*
import kotlinx.coroutines.*
import okhttp3.Response
import space.lupu.koex.singleton.SingletonInitializer
import space.lupu.konnector.Konnector
import space.lupu.konnector.Module
import space.lupu.konnector.filters.MergeFilter
import space.lupu.konnector.filters.TaggedObjectFilter
import space.lupu.konnector.nodes.Channel
import space.lupu.konnector.nodes.Node
import space.lupu.konnector.utils.highestPriorityModuleOrNull
import space.lupu.konnector.utils.takeModulesMatchingAllTags

class Controller private constructor (setup: Setup) {
    data class Setup(val attachModules: List<Module>)

    val connectedDevices = hashMapOf<CrayonicDevice, CrayonicDeviceChannel>()

    val job_parent = Job()

    val jobsCache = hashMapOf<String,Job>("parent" to job_parent)


    fun cleanupJobsChache(): Int {
        val toRemove = jobsCache.filterNot { entry -> entry.value.isActive }.keys
        toRemove.forEach {
            jobsCache.remove(it)
            Log.e("Controller", "removingJob($it)")
        }
        return toRemove.size
    }

    fun getNewJob(tag:String) : Job{
        Log.e("Controller", "getNewJob($tag)")
        val job = Job(job_parent)
        if(jobsCache.containsKey(tag)) Log.e("Controller", "getNewJob($tag) - Such job tag already exist")
        jobsCache.put(tag, job)
        return job
    }

    val listingJobsPool = hashMapOf<DeviceFilter, Job>()

    /**
     * When true there is no other listing job for such deviceFilter.
     * Meaning you we can start a new listing job.
     */
    fun isNoListingForDeviceFilter(deviceFilter: DeviceFilter):Boolean{
        val job = listingJobsPool.get(deviceFilter)
        return when {
            job == null -> true     // not contain
            !job.isActive -> true   // not active
            else -> false           // anything else
        }
    }

    val konnector = Konnector()

    init {
        setup.attachModules.forEach { konnector.attachModule(it) }
        konnector.attachModule(RunningCache())
    }

    companion object : SingletonInitializer<Controller, Setup>(::Controller) {
        fun getInstance(): Controller = getInstance(Setup(listOf()))
    }

    val seenDevices = TimeoutArrayList<CrayonicDevice>()

    var seenDeviceTimeout = seconds_5

    val registeredListingCallbacks = hashMapOf<DeviceFilter, MutableSet<(originFilter: DeviceFilter, List<CrayonicDevice>) -> Unit>>()

    fun listDevices(
        deviceFilter: DeviceFilter,
        onDevicesFound: (originFilter: DeviceFilter, List<CrayonicDevice>) -> Unit
    ) {
        // attach new onDevicesFound callback

//        Log.d("Controller", "listDevices deviceFilter = ${deviceFilter} onDevicesFound = ${onDevicesFound.hashCode().toString()} ")
        val setOfCallbacks =
            if(registeredListingCallbacks.containsKey(deviceFilter)) {
                registeredListingCallbacks.get(deviceFilter)
            } else {
                mutableSetOf()
            }?:mutableSetOf()
        setOfCallbacks.add(onDevicesFound)
//        Log.d("Controller", "setOfCallbacks = ${setOfCallbacks.joinToString(", ") { function -> function.hashCode().toString() }}")
        registeredListingCallbacks.put(deviceFilter, setOfCallbacks)
//        Log.d("Controller", "registeredListingCallbacks = ${registeredListingCallbacks.entries.joinToString {
//                entry -> "filterKey = '${entry.key}' setValue = ${entry.value.joinToString(", ") { function -> function.hashCode().toString() }}"}}")
//
//
//        Log.d("Controller", "listingJobsPool = ${listingJobsPool.entries.joinToString(", ") { e -> "keyFilter = '${e.key}' jobs = ${e.value}" }}")
//        Log.d("Controller", "listingJobsPool contains not deviceFilter = ${isNoListingForDeviceFilter(deviceFilter) }")
        if(isNoListingForDeviceFilter(deviceFilter)) {
            val listingJob = getNewJob(Single.randomId("listingJob for deviceFilter = ${deviceFilter.toString()}"))
//            Log.d("Controller", "listingJob = $listingJob")
            listingJobsPool.put(deviceFilter, listingJob)
            konnector.listNodesFor(deviceFilter) { listOfNodes, nodeFilter ->
                if (nodeFilter is DeviceFilter) {
//                    Log.e("Controller", "listDevices : listOfNodes[${listOfNodes.size}] ${listOfNodes.joinToString(separator = "\n")}" )
                    val listOfDevices = DevicesModule.transformNodesToCrayonicDevices(listOfNodes)
                    seenDevices.rewriteAllWithTimeout(listOfDevices, seenDeviceTimeout)
                    Log.e("Controller", "seenDevices : seenDevices[${seenDevices.size}] ${seenDevices.joinToString(separator = "\n")}" )

                    setOfCallbacks.forEach {
                        it.invoke(nodeFilter, seenDevices)
                    }
                }
            }
        }

    }

    fun clearDevicesCache() {
        seenDevices.clear()
        connectedDevices.forEach { t -> t.key.disconnect() }
        connectedDevices.clear()
    }

    fun stopListingFor(filter: DeviceFilter) {
        konnector.stopListingFor(filter)
        listingJobsPool.get(filter)?.cancel()
    }

    fun stopAllDeviceListings() {
        konnector.stopAllListings()
        listingJobsPool.values.forEach { it.cancel() }
    }

    fun modulesWithTag(vararg tags: String): List<Module> =
        takeModulesMatchingAllTags(konnector.modules, tags.toList())

    fun modulesWithTag(tags: List<String>): List<Module> =
        takeModulesMatchingAllTags(konnector.modules, tags)

    fun hasAtLeastOneModuleWithTags(vararg tags: String) =
        modulesWithTag(tags.toList()).isNotEmpty()

    fun hasAtLeastOneModuleWithTags(tags: List<String>) = modulesWithTag(tags).isNotEmpty()

    fun takeBestModuleForTags(vararg tags: String): Module? = takeBestModuleForTags(tags.toList())
    fun takeBestModuleForTags(tags: List<String>): Module? =
        modulesWithTag(tags).highestPriorityModuleOrNull()

    ///////////////////////////////
    //  Save Load

    fun save(id: String, instance: Any?, onDone: (success: Boolean) -> Unit) =
        save(id, instance, onDone, TagsEnum.memory.name)

    fun save(id: String, instance: Any?, onDone: (success: Boolean) -> Unit, vararg tags: String) =
        save(id, instance, tags.toList(), onDone)

    fun save(
        id: String,
        instance: Any?,
        memoryTags: List<String>,
        onDone: (success: Boolean) -> Unit = {}
    ) {
        CoroutineScope(Dispatchers.IO + getNewJob(Single.randomId("saveJob"))).launch {
            withTimeout(seconds_10) {
                var triggered = false
                takeBestModuleForTags(memoryTags)?.let { module ->
                    if (module is MemoryModule<*>) {
                        val channel = module.channel
                        if (channel is SaveLoadInterface) {
                            channel.save(id, instance, { success ->
                                CoroutineScope(Dispatchers.Main).launch {
                                    onDone(success)
                                }
                            })
                            triggered = true
                        }
                    }
                }
                if (!triggered) {
                    Single.cacheObject(id, instance)
                    CoroutineScope(Dispatchers.Main).launch {
                        onDone(true)
                    }
                }
            }
        }
    }

    inline fun <reified T> load(
        id: String,
        noinline onDone: (success: Boolean, instance: T?) -> Unit
    ) =
        load(id, onDone, TagsEnum.memory.name)

    inline fun <reified T> load(
        id: String,
        noinline onDone: (success: Boolean, instance: T?) -> Unit,
        vararg tags: String
    ) = load(id, tags.toList(), onDone)

    inline fun <reified T> load(
        id: String,
        memoryTags: List<String>,
        noinline onDone: (success: Boolean, instance: T?) -> Unit = { _, _ -> }
    ) {
        CoroutineScope(Dispatchers.IO + getNewJob(Single.randomId("loadJob"))).launch {
            withTimeout(seconds_10) {
                var triggered = false
                takeBestModuleForTags(memoryTags)?.let { module ->
                    if (module is MemoryModule<*>) {
                        val channel = module.channel
                        if (channel is SaveLoadInterface) {
                            channel.load(id) { success, value: T? ->
                                CoroutineScope(Dispatchers.Main).launch {
                                    onDone.invoke(success, value)
                                }
                            }
                            triggered = true
                        }
                    }
                }
                if (!triggered) {
                    val im = Single.retrieveObject(id)
                    CoroutineScope(Dispatchers.Main).launch {
                        if (im is T) {
                            onDone.invoke(true, im)
                        } else
                            onDone.invoke(false, null)
                    }
                }
            }
        }
    }

    fun saveToCache(id:String, any:Any) = Single.cacheObject(id,any)
    fun loadFromCache(id:String): Any? = Single.retrieveObject(id)
    fun removeFromCache(id:String) =  Single.removeObject(id)

    ///////////////////////////
    // Connect

    protected fun connectNodeByTags(
        onChannelOpened: (Node, Channel) -> Unit,
        onDisconnected: (Node) -> Unit,
        vararg tags: String
    ) = connectNodeByTags(tags.toList(), onChannelOpened, onDisconnected)

    protected fun connectNodeByTags(
        tags: List<String>, onChannelOpened: (Node, Channel) -> Unit,
        onDisconnected: (Node) -> Unit = {}
    ) {
        Log.e("Controller", "connectNodeByTags : $tags , $onChannelOpened, $onDisconnected" )
        CoroutineScope(Dispatchers.IO + getNewJob(Single.randomId("ctrl.connJob"))).launch {
            var isNotConnected = true
            konnector.listNodesFor(
                MergeFilter.tagsMatchAll(
                    "node-connect-filter",
                    tags
                )
            ) { list: List<Node>, _: TaggedObjectFilter ->
                Log.e("Controller", "connectNodeByTags : before run blocking" )

                runBlocking {
                    if (isNotConnected && list.isNotEmpty()) {
                        list.get(0).connect(onChannelOpened = { node: Node, channel: Channel ->
                            isNotConnected = false
                            CoroutineScope(Dispatchers.Main).launch {
                                onChannelOpened.invoke(node, channel)
                            }
                        }, onNodeDisconnected = {
                            CoroutineScope(Dispatchers.Main).launch {
                                onDisconnected.invoke(it)
                            }
                        })
                    }
                }
            }

            delay(seconds_10)
            if(isNotConnected){
                // callback for error on connect
                CoroutineScope(Dispatchers.Main).launch {

                }
            }
        }
    }

    /**
     * Stops all the sdk activities
     */
    fun stopEverything() {
        konnector.stopAllListings()
        job_parent.cancel()
    }


    ///////////////////////////
    // Http

    fun uploadIfPossible(payload: Any, timeout:Long = seconds_120, retryCount:Int = 3, onResult:(response: Response?)->Unit = {}) {
        val tryTimes = 1+retryCount
        when (payload) {
            is L1ApiJson_Postable -> uploadL1ApiPostable(payload = payload, timeout = timeout, tryTimes = tryTimes, onResult = onResult)
        }
    }


}