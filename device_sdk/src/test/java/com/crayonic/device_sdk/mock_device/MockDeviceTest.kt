//package com.crayonic.device_sdk.mock_device
//
//import com.crayonic.device_sdk.core.Controller
//import com.crayonic.device_sdk.core.Single
//import com.crayonic.device_sdk.device.CrayonicDeviceNode_ListProvider
//import com.crayonic.device_sdk.device.DevicesModule
//import com.crayonic.device_sdk.util.TagsEnum
//import com.crayonic.device_sdk.device.DeviceFilter
//import com.crayonic.device_sdk.http.l1_json_api.L1JsonApi
//import com.crayonic.device_sdk.device.CrayonicDevice
//import com.crayonic.device_sdk.util.currentUnixTime
//import junit.framework.Assert.assertEquals
//import junit.framework.Assert.assertTrue
//import org.junit.Test
//import space.lupu.konnector.Module
//
//class MockDeviceTest {
//
//    val deviceProvider = CrayonicDeviceNode_ListProvider(
//        listOf(
//            MockedL1Node(deviceAddress = "AB:CD:EF:12:34:00"),
//            MockedL1Node(deviceAddress = "00:CD:EF:12:34:11"),
//            MockedL1Node(deviceAddress = "89:CD:EF:12:34:22"),
//            MockedL1Node(deviceAddress = "95:CD:EF:12:34:33"),
//            MockedL1Node(deviceAddress = "F5:CD:EF:12:34:44"),
//            MockedL1Node()  // "AB:CD:EF:12:34:56"
//        ), Single.cacheId("mock-deviceProvider")
//    )
//
//    val deviceModulePrio5 =
//        DevicesModule(Single.cacheId("mock-deviceModule"), 5, "mock", "main", TagsEnum.l1.name)
//    val deviceModulePrio2 =
//        DevicesModule(Single.cacheId("mock-deviceModule"), 2, "mock", "secondary", TagsEnum.l1.name)
//
//    init {
//        deviceModulePrio2.safeAttachOfMultipleProviders(deviceProvider, deviceProvider)
//        deviceModulePrio5.safeAttachOfMultipleProviders(deviceProvider)
//    }
//
//    val modList = listOf(
//        deviceModulePrio2,
//        deviceModulePrio5,
//        L1JsonApi("https://gateway-test.ambrosus.com")
//    )
//
//    val controller = Controller(Controller.Setup(modList))
//
//    fun attachModules(vararg modules: Module) = attachModules(modules.toList())
//    fun attachModules(modules: List<Module>) {
//        modules.forEach { module ->
//            controller.konnector.attachModule(module)
//        }
//    }
//
//    fun detachModules(vararg modules: Module) = detachModules(modules.toList())
//    fun detachModules(modules: List<Module>) {
//        modules.forEach { module ->
//            controller.konnector.detachModule(module)
//        }
//    }
//
//    @Test
//    fun setupCheck (){
//        attachModules(modList)
//        attachModules(modList)
//        assertEquals(4, controller.konnector.modules.size)
//
//        detachModules(deviceModulePrio5)
//        assertEquals(3, controller.konnector.modules.size)
//        attachModules(modList)
//        assertEquals(4, controller.konnector.modules.size)
//    }
//
//    val foundDevicesSet = hashSetOf<CrayonicDevice>()
//    val deviceFilter_mac_F5___44 = DeviceFilter("F5:CD:EF:12:34:44")
//    val deviceFilter_l1 = DeviceFilter(TagsEnum.l1.name)
//    val deviceFilter_serviceRXTX = DeviceFilter("CrayonicRXTXService")
//
//    @Test
//    fun mockDeviceListing() {
//        // zeroed filters
//        val deviceFilter_apdu = DeviceFilter(TagsEnum.apdu.name)
//        val deviceFilter_l1_apdu = DeviceFilter(TagsEnum.l1.name, TagsEnum.apdu.name)
//
//        mapOf(
//            deviceFilter_serviceRXTX to 6,
//            deviceFilter_l1 to 6,
//            deviceFilter_apdu to 0,
//            deviceFilter_l1_apdu to 0,
//            deviceFilter_mac_F5___44 to 1
//        ).forEach {
//            listAndCheckDevices(it.key, it.value)
//        }
//
//    }
//
//    /**
//     * This wont run on common library, t could be mocked but its a waste of unixTime and effort
//     *
//     * Rather mplement the platform, eg android and run this test there
//     *
//     * Exception in thread "DefaultDispatcher-worker-6 @coroutine#4" Exception in thread "DefaultDispatcher-worker-13 @coroutine#9" java.lang.IllegalStateException: Module with the Main dispatcher had failed to initialize. For tests Dispatchers.setMain from kotlinx-coroutines-test module can be used
//     *
//     */
//    fun mockDeviceRunCommand() {
//        // zeroed filters
//        val deviceFilter_apdu = DeviceFilter(TagsEnum.apdu.name)
//        val deviceFilter_l1_apdu = DeviceFilter(TagsEnum.l1.name, TagsEnum.apdu.name)
//
//        mapOf(
//            deviceFilter_serviceRXTX to currentUnixTime(),
//            deviceFilter_l1 to currentUnixTime(),
//            deviceFilter_apdu to currentUnixTime(),
//            deviceFilter_l1_apdu to currentUnixTime(),
//            deviceFilter_mac_F5___44 to currentUnixTime()
//        ).forEach {
//            listAndRunBasicCommands(it.key, currentUnixTime())
//        }
//
//    }
//
//    private fun listAndRunBasicCommands(deviceFilter: DeviceFilter, expectTime:Long) {
//        controller.listDevices(
//            deviceFilter,
//            onDevicesFound = { originFilter: DeviceFilter, devices: List<CrayonicDevice> ->
//                foundDevicesSet.addAll(devices)
//                devices.forEach {crayonicDevice: CrayonicDevice ->
//                    crayonicDevice.connect(onConnected = {device ->
//                        device.readBatteryLevel { device, readableValue ->
//                            assertEquals(99.toString(), readableValue)
//                        }
//                        device.readFirmwareVersion { device, readableValue ->
//                            assertEquals("2.5", readableValue)
//                        }
//                        println("filter : ${device.modelTag}")
//                        assertEquals(device.modelTag, "L1")
//                        device.runCommandReadClock { device, unixTime ->
//                            println("expect : $expectTime")
//                            println("received : $unixTime")
//                            // this tests the apdu round bound
//                            assertTrue(unixTime >= expectTime)
//                        }
//
//                    })
//                }
//            })
//    }
//    private fun listAndCheckDevices(deviceFilter: DeviceFilter, expect:Int) {
//        println("-------")
//        println("filter : $deviceFilter")
//        println("expect : $expect")
//
//        controller.listDevices(
//            deviceFilter,
//            onDevicesFound = { originFilter: DeviceFilter, devices: List<CrayonicDevice> ->
//                foundDevicesSet.addAll(devices)
//            })
//        println("foundDevicesSet")
//        foundDevicesSet.forEach {
//            println(it)
//        }
//
//        assertEquals(expect, foundDevicesSet.size)
//        foundDevicesSet.clear()
//
//    }
//}