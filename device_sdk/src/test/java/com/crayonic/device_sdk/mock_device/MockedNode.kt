//package com.crayonic.device_sdk.mock_device
//
//import com.crayonic.device_sdk.core.Single
//import com.crayonic.device_sdk.device.CrayonicDeviceNode
//import com.crayonic.device_sdk.device.channels.CrayonicDeviceChannel
//import space.lupu.konnector.nodes.Channel
//import space.lupu.konnector.nodes.Node
//import space.lupu.konnector.tags.Tags
//import space.lupu.konnector.tags.toTags
//
//
//abstract class MockedNode<CH: CrayonicDeviceChannel>(val channel:CH, vararg tag:String): CrayonicDeviceNode(Single.cacheId("mockedDevNode-") , Tags(tag.toList().toTags())) {
//    override fun connect(
//        onChannelOpened: (Node, Channel) -> Unit,
//        onNodeDisconnected: (Node) -> Unit
//    ) {    onChannelOpened(this,  channel)     }
//
//    override fun disconnect(onNodeDisconnected: (Node) -> Unit) {
//        onNodeDisconnected(this)
//    }
//
//    override val isDfuUpdateSupported = false
//
//}
//
