//package com.crayonic.device_sdk.mock_device
//
//import com.crayonic.device_sdk.apdu.ApduSignature
//import com.crayonic.device_sdk.apdu.ApduSignatures
//import com.crayonic.device_sdk.apdu.ApduType
//import com.crayonic.device_sdk.core.Single
//import com.crayonic.device_sdk.device.channels.CrayonicDeviceChannel
//import com.crayonic.device_sdk.device.channels.CrayonicMemberChannelL1
//import com.crayonic.device_sdk.device.kotojava.CrayonicRequestCode
//import com.crayonic.device_sdk.device.CrayonicDevice
//import com.crayonic.device_sdk.device.CrayonicL1
//import com.crayonic.device_sdk.util.TagsEnum
//import com.crayonic.device_sdk.util.currentUnixTime
//import space.lupu.kapdu.statusWords.KnownApduStatusWords
//import space.lupu.koex.bytes.minimalByteArray
//import space.lupu.koex.bytes.paddingZeros
//import space.lupu.koex.hex.Hex
//import space.lupu.koex.numbers.toByteArray
//import space.lupu.koex.numbers.toLong
//
//class aMockedL1Node(
//    var deviceName: String = Single.cacheId("MockedL1Node-"),
//    var deviceAddress: String = "AB:CD:EF:12:34:56",
//    var deviceModel:String = "L1",
//    var deviceFw: String = "2.5",
//    var services: List<String> = listOf(
//        "CrayonicRXTXService"
//    )
//): MockedNode<MockedL1Node.MockL1Channel>(MockL1Channel(MockL1MemberReadChannel( deviceName, deviceAddress, deviceModel, deviceFw ) ), TagsEnum.l1.name) {
//    override fun provideNodeName(): String = deviceName
//
//    override fun provideNodeAddress(): String  = deviceAddress
//
//    override fun provideNodeServices(): List<String> = services
//
//    override fun toCrayonicDevice(): CrayonicDevice {
//        return CrayonicL1(this)
//    }
//
//    class MockL1Channel(memberChannel: CrayonicMemberChannelL1): CrayonicDeviceChannel("mockedL1", listOf(TagsEnum.l1, TagsEnum.apdu), supportedCommands, memberChannel) {
//        /**
//         * This send bytes will run payload through mapped functions and execute appropriate command for it
//         * Acts as a real L1 would on those few commands
//         */
//        override fun send(payload: ByteArray, onReceived: (ByteArray) -> Unit) {
//            val hex = Hex(payload)
//            val header = payload.copyOfRange(0,4).toLong()
//            when{
//                apduDb.containsKey(header) -> onReceived(apduDb[header]?.invoke(hex, ApduSignatures.supportedApdus[header])?:0xffff.toByteArray().minimalByteArray())
//                else -> fallbackApduError().invoke(hex, null)
//            }
//        }
//
//        fun mockApduPair(apduType:Long, action:(Hex, ApduSignature?)->ByteArray):Pair<Long, (Hex, ApduSignature?)->ByteArray>{
//            val sig = ApduSignatures.supportedApdus[apduType]?:return apduType to fallbackApduError()
//            return apduType to action
//        }
//
//        private fun fallbackApduError() :(Hex, ApduSignature?)->ByteArray = { _, _ -> 0xffff.toByteArray().minimalByteArray() }
//
//        val apduDb = hashMapOf< Long, (Hex, ApduSignature?)->ByteArray>(
//            mockApduPair(ApduType.ro_clock, { hex, apduSignature -> currentUnixTime().toByteArray().minimalByteArray().paddingZeros( apduSignature?.expectedLength?:4)})
////            ,  // uncomment this dot to add new lines
//
////            mockApduPair(ApduType.ro_eth_address, { hex, sig -> }) ,
////            mockApduPair(ApduType.ro_public_changing_key, { hex, sig -> }) ,
////            mockApduPair(ApduType.ro_identity_pubkey, { hex, sig -> }) ,
////            mockApduPair(ApduType.ro_management_pin, { hex, sig -> }) ,
////            mockApduPair(ApduType.ro_root_of_trust_pubkey, { hex, sig -> }) ,
////            mockApduPair(ApduType.ro_alarm_zones, { hex, sig -> }) ,
////            mockApduPair(ApduType.ro_asset_id, { hex, sig -> }) ,
////            mockApduPair(ApduType.ro_package_id, { hex, sig -> }) ,
////
////            mockApduPair(ApduType.wr_event_start, { hex, sig -> }),
////            mockApduPair(ApduType.ro_event_length, { hex, sig -> }),
////            mockApduPair(ApduType.ro_event_data, { hex, sig -> }),
////            mockApduPair(ApduType.wr_event_data, { hex, sig -> }),
////            mockApduPair(ApduType.wr_event_finish, { hex, sig -> })
//
//
//
//
////      included above
////            ApduType.ro_asset_id,
////            ApduType.ro_identity_pubkey,
////            ApduType.ro_alarm_zones,
////            ApduType.ro_pen_length,
////            ApduType.ro_root_of_trust_pubkey,
////            ApduType.ro_management_pin,
////            ApduType.ro_eth_address,
////            ApduType.ro_clock,
////            ApduType.ro_public_changing_key,
////            ApduType.ro_package_id,
//
//
////      not included
////            ApduType.ro_data_watermark_low,
////            ApduType.ro_data_watermark_high,
////            ApduType.ro_sampling,
////            ApduType.ro_sampling_provision,
////            ApduType.ro_all_data,
////            ApduType.ro_bip44_tx_sign,
////            ApduType.ro_modem_setup,
//
//
////      event specific
////                    ApduType.wr_event_start,
////            ApduType.ro_event_length,
////            ApduType.ro_event_data,
////                    ApduType.wr_event_data,
////                    ApduType.wr_event_finish,
//        )
//        init {
//            // add all write records as ok
//            apduDb.putAll(
//                listOf<Long>(
//                    ApduType.wr_data_watermark_low,
//                    ApduType.wr_data_watermark_high,
//                    ApduType.wr_sampling,
//                    ApduType.wr_sampling_provision,
//                    ApduType.wr_clock,
//                    ApduType.wr_reset,
//                    ApduType.wr_fw_update_reset,
//                    ApduType.wr_generate_changing_keys,
//                    ApduType.wr_identity_generate,
//                    ApduType.wr_identity_inject,
//                    ApduType.wr_management_pin,
//                    ApduType.wr_root_of_trust_pubkey,
////                    ApduType.wr_modem_setup,
//                    ApduType.wr_eth_secret_seed_bip39,
//                    ApduType.wr_enter_puk_code,
//                    ApduType.wr_bip44_tx_sign,
//                    ApduType.wr_alarm_zone,
//                    ApduType.wr_asset_id,
//                    ApduType.wr_package_id,
//                    ApduType.wr_cipher_aes
////                    ApduType.wr_signal_enc,
////                    ApduType.wr_signal_init,
////                    ApduType.wr_chat_text,
////                    ApduType.wr_chat_text_enc,
////                    ApduType.wr_chat_bytes
//
//                    ).map { mockApduPair(it, { hex, sig -> KnownApduStatusWords.ok.toByteArray().minimalByteArray() })  }
//            )
//        }
//
//        // change the insides from outside
//        fun changeApduTestVectorsHex(vectorMap : Map<Long,Hex>){
//            vectorMap.forEach { entry ->
//                val vec = mockApduPair(entry.key, { hex, sig -> entry.value.serialize() })
//                apduDb.put(vec.first, vec.second)
//            }
//        }
//        fun changeApduTestVectorsBytes(vectorMap : Map<Long,ByteArray>){
//            vectorMap.forEach { entry ->
//                val vec = mockApduPair(entry.key, { hex, sig -> entry.value })
//                apduDb.put(vec.first, vec.second)
//            }
//        }
//        fun changeApduTestVectorsHexadecimalString(vectorMap : Map<Long,String>){
//            vectorMap.forEach { entry ->
//                val vec = mockApduPair(entry.key, { hex, sig -> Hex(entry.value).serialize() })
//                apduDb.put(vec.first, vec.second)
//            }
//        }
//
//
//        companion object{
//            val supportedCommands = listOf<CrayonicRequestCode>(
//                // all devices
//                CrayonicRequestCode.connectDevice,
//                CrayonicRequestCode.disconnectDevice,
//
//                CrayonicRequestCode.readDeviceAddress,
//                CrayonicRequestCode.readDeviceName,
//                CrayonicRequestCode.readBatteryLevel,
//                CrayonicRequestCode.readModelNumber,
//                CrayonicRequestCode.readFirmwareVersion,
//
//                CrayonicRequestCode.runCommandReadClock,
//                CrayonicRequestCode.runCommandWriteClock,
//                CrayonicRequestCode.runCommandReadPublicKey,
//                CrayonicRequestCode.runCommandReadEthAddress,
//                CrayonicRequestCode.runCommandResetToPowerOff,
//                CrayonicRequestCode.runCommandResetToUpdateMode,
//                CrayonicRequestCode.runTaskUpdateDevice,
//                // Pen requests
////    CrayonicRequestCode.runCommandSetPinCode,
//                CrayonicRequestCode.runCommandSignEthTx,
//                CrayonicRequestCode.runTaskInitializePen,
//                CrayonicRequestCode.runTaskReadData,
//
//                // L1 requests
//                CrayonicRequestCode.readTemperature,
//                CrayonicRequestCode.readHumidity,
//                CrayonicRequestCode.readLight,
//                CrayonicRequestCode.readShock,
//                CrayonicRequestCode.readIncline,
//                CrayonicRequestCode.runCommandReadAlarmZones,
//                CrayonicRequestCode.runCommandWriteAlarmZone,
//                CrayonicRequestCode.runCommandReadAssetId,
//                CrayonicRequestCode.runCommandWriteAssetId,
//                CrayonicRequestCode.runCommandReadPackageId,
//                CrayonicRequestCode.runCommandWritePackageId,
//                CrayonicRequestCode.runCommandSetManagementPinCode,
//                CrayonicRequestCode.runTaskReadEvents,
//                CrayonicRequestCode.runTaskInitialization,
//                CrayonicRequestCode.runTaskFactorySetup,
//                CrayonicRequestCode.runTaskL1DeviceTest
//            )
//        }
//    }
//
//    class MockL1MemberReadChannel(
//        var deviceName: String = Single.cacheId("MockedL1Node-"),
//        var deviceAddress: String = "AB:CD:EF:12:34:56",
//        var deviceModel:String = "L1",
//        var deviceFw: String = "2.5"
//    ) : CrayonicMemberChannelL1 {
//        override fun readMemberForRequest(
//            requestKey: CrayonicRequestCode,
//            onMidResult: (String) -> Unit
//        ) {
//            when(requestKey){
//                CrayonicRequestCode.readDeviceAddress -> readDeviceAddress(onMidResult)
//                CrayonicRequestCode.readDeviceName -> readDeviceName(onMidResult)
//                CrayonicRequestCode.readBatteryLevel -> readBatteryLevel(onMidResult)
//                CrayonicRequestCode.readFirmwareVersion -> readFirmwareVersion(onMidResult)
//                CrayonicRequestCode.readModelNumber -> readModelNumber(onMidResult)
//                CrayonicRequestCode.readTemperature -> readTemperature(onMidResult)
//                CrayonicRequestCode.readHumidity -> readHumidity(onMidResult)
//                CrayonicRequestCode.readLight -> readLight(onMidResult)
//                CrayonicRequestCode.readShock -> readShock(onMidResult)
//                CrayonicRequestCode.readIncline -> readIncline(onMidResult)
//            }
//        }
//
//        override fun supportsRequestCode(requestKey: CrayonicRequestCode): Boolean = true
//
//        override  fun readTemperature(onMidResult: (String) -> Unit): Boolean {
//            onMidResult("26°C")
//            return true
//        }
//
//        override  fun readHumidity(onMidResult: (String) -> Unit): Boolean {
//            onMidResult("45")
//            return true
//        }
//
//        override  fun readLight(onMidResult: (String) -> Unit): Boolean {
//            onMidResult("41000")
//            return true
//        }
//
//        override  fun readShock(onMidResult: (String) -> Unit): Boolean {
//            onMidResult("0g")
//            return true
//        }
//
//        override  fun readIncline(onMidResult: (String) -> Unit): Boolean {
//            onMidResult("[0,0,0]")
//            return true
//        }
//
//        override  fun readDeviceAddress(onMidResult: (String) -> Unit): Boolean {
//            onMidResult(deviceAddress)
//            return true
//        }
//
//        override  fun readDeviceName(onMidResult: (String) -> Unit): Boolean {
//            onMidResult(deviceName)
//            return true
//        }
//
//        override  fun readBatteryLevel(onMidResult: (String) -> Unit): Boolean {
//            onMidResult(99.toString())
//            return true
//        }
//
//        override  fun readFirmwareVersion(onMidResult: (String) -> Unit): Boolean {
//            onMidResult(deviceFw)
//            return true
//        }
//
//        override  fun readModelNumber(onMidResult: (String) -> Unit): Boolean {
//            onMidResult(deviceModel)
//            return true
//        }
//
//    }
//}